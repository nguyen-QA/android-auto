
OLD PACKAGE TO DO THE TESTCASE WITHOUT CODE
# MODULE TESTING
## android-auto/app/src/androidTest/java/com/caipiao/caipiao/ui/authTest
00. CaptchaObjectTest						testing with random Token generated
00. WechatObjectTest						add test files with Object Pattern
16. SeveralParameterizedTest    			move the test files to appropriate folder
17. WechatUiDeviceTest						test AuthActivity with UiDevice

## android-auto/app/src/androidTest/java/com/caipiao/caipiao/ui/customAccessibility
01. AccessibilityChecks                     for ENABLE and DISABLE AccessibilityChecks
04. TestData                                for creating random data

## android-auto/app/src/androidTest/java/com/caipiao/caipiao/ui/customAction
08. BaseCustomAction:                       for checking if a matcher was returned. ->> add some new functions and their description
09. EspressoExtensionsCustomAction          for returning a ViewAction once a match has been found within supplied rootview.
00. Swiper Interface                        implement different swipe types.
00. TypeTextActionCustomAction              Enables typing text on views. ->> add Custom Actions from BaseCustomViewAction
00. PrecisionDescriberCustomAction          Interface to implement size of click area.
00. AdapterDataLoaderAction                 Forces an AdapterView to ensure that the data matching a provided data matcher is loaded into the current view hierarchy.

## android-auto/app/src/androidTest/java/com/caipiao/caipiao/ui/customAssertion
00. LayoutAssertions                        asserts that view hierarchy contain ellipsized or cut off text views.
00. PositionAssertions                       asserts that view displayed is completely the view matching the given matcher.
00. ViewAssertions                          assert that ensures the view matcher finds all matching view in the hierarchy.

## android-auto/app/src/androidTest/java/com/caipiao/caipiao/ui/customConditionTime
05. ConditionWatcherFirst:                  for creating custom rules.
06. ConditionWatcherSecond:                 is creating functions: waitForElement, waitForElementDisappear, waitForElementIsGone.
07. Instruction:                            is data Rules.

## android-auto/app/src/androidTest/java/com/caipiao/caipiao/ui/customDrawable
10. CommonElements                          object compare icons and images
11. DrawableMatchers                        covers the TextView and ImageView drawables
00. DrawerActions							create new custom Action in	drawer layout
00. DrawerMatchers							create matcher for drawer layout

## android-auto/app/src/androidTest/java/com/caipiao/caipiao/ui/customFailureHandler
13. AllRule:                                for creating Failure Handler Exception and other Rules
12. CustomFailureHandler:                   for creating custom Failure Handler.

## android-auto/app/src/androidTest/java/com/caipiao/caipiao/ui/customIdlingResource
14. ElapsedTimeIdlingResource:              for preparing a custom Idle Resource.
15. ViewViewVisibleIdlingResource:          check if the Element can be visible.

## android-auto/app/src/androidTest/java/com/caipiao/caipiao/ui/customObjectPattern
00. BuilderScreenObject						build custom methods test following Object Pattern

## android-auto/app/src/androidTest/java/com/caipiao/caipiao/ui/customRecycler
00. CustomRecyclerViewActions				Demonstrates custom { RecyclerView} actions implementation.
00. RecyclerViewActions						create new custom Action in RecyclerView

## android-auto/app/src/androidTest/java/com/caipiao/caipiao/ui/customRobotBuilder
14. HomeScreenRobot							create robot pattern function

# MODULE TESTING
## android-auto/app/src/androidTest/java/com/caipiao/caipiao/ui/homeTest
00. HomeScreenDrawableTest					build test with Drawable Matcher
16. HomeScreenRobotTest						test the home screen by robot pattern function


# MODULE TESTING
## android-auto/app/src/androidTest/java/com/caipiao/caipiao/ui/kyqpTest
16. WebViewConditionTest:                   for testing WebView (with Condition Matcher).
17. WebViewIdlingTest:                      for testing WebView (with Idle Resource).

# MODULE TESTING
## android-auto/app/src/androidTest/java/com/caipiao/caipiao/ui/main
02. EnableAccessibilityTest                 for testing AccessibilityChecks
18. MainActivityTest:                       Default test from developer repository
00. PackageNameUiDeviceTest                 for testing and verifying package name
00. RootSettingAccesibilityTest             for accessing root permission
19. TestBall3Bet2Success:                   Default test from developer repository

# MODULE TESTING
## android-auto/app/src/androidTest/java/com/caipiao/caipiao/ui/memberTest
20. AllScreenViewTest:                      for accessing and checking all visible UI components at a glance.
22. QueryDriver:                            for make a Jdbc connection and query from database server.
23. QueryDriverFirstClone:                  is copy of QueryDriver class.
21. RegisterLoginIdleResourceTest:          for testing registration and login process(with Idle Resource).
                                            add Custom Action functions.

# MODULE TESTING
## android-auto/app/src/androidTest/java/com/caipiao/caipiao/ui/mygamesTest
25. RecyclerView_ConditionDelayTest:        for testing the list of RecyclerView Items (with Condition Matcher).
26. RecyclerView_IdlingDelayTest:           for testing the list of RecyclerView Items (with Idle Resource).
24. RecyclerViewMatcher:                    for creating custom Recycler View Matcher.

# MODULE TESTING
## android-auto/app/src/androidTest/java/com/caipiao/caipiao/ui/phoneLowTest
27. AllScreenGuestTest:                     for accessing and quick checking all screens( with Idle Resource for firmware under 7).
29. LowPhoneRegister_IdlingDelayTest:       for login testing on low devices.
28. QueryDriverSecondClone:                 is copy of QueryDriver class.

# MODULE TESTING
## android-auto/app/src/androidTest/java/com/caipiao/caipiao/ui/qrTest
00. HintTextUiDeviceTest                    for testing Ui Component with UiDevice
30. PickImageIntentMatcherTest:             for accessing image gallery.

# MODULE TESTING
## android-auto/app/src/androidTest/java/com/caipiao/caipiao/ui/suitTest
32. EspressoTestSuite:                      for testing parallel multiple Tasks at the same times.

# MODULE TESTING
## android-auto/app/src/androidTest/java/com/caipiao/caipiao/ui/verifyTest
00. VerifyObjectTest                         check the Auth Verify Screen by Object pattern

## ExampleInstrumentedTest: default test



NEW PACKAGE TO DO THE TESTCASE WITH CODE
# MODULE TESTING
## android-auto/app/src/androidTest/java/com/caipiao/caipiao/codeTest/authTest
00. MultiParameterizedTest                     check multiple wechat id by java random()
00. SocialObjectTest                           check wechat by object pattern
00. SocialParameterizedTest                    check multiple wechat and token by java random()
00. SocialUiDeviceTest                         check wechat with UiDevice
00. TokenObjectTest                            check token by object pattern

# MODULE TESTING
## android-auto/app/src/androidTest/java/com/caipiao/caipiao/codeTest/loginTest
00. LowPhone_Login_IdlingDelayTest              register on low phone test cases
00. Normal_Login_IdlingResourceTest             register on high phone test cases

