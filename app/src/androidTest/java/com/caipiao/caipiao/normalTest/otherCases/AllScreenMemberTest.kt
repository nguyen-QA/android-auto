package com.caipiao.caipiao.normalTest.otherCases

import android.content.Intent
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*

import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.SmallTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.Until
import com.caipiao.caipiao.R
import com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherSecond.waitForElement
import com.caipiao.caipiao.normalTest.customFailureHandler.AllRule
import com.caipiao.caipiao.ui.home.HomeActivity
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.*
import org.hamcrest.TypeSafeMatcher
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.lang.Thread.sleep
import java.util.regex.Pattern

@SmallTest
@RunWith(AndroidJUnit4ClassRunner::class)
    class AllScreenMemberTest {

    private val instrumentation = InstrumentationRegistry.getInstrumentation()
    private val uiDevice: UiDevice = UiDevice.getInstance(instrumentation)
    private val fourSeconds = 4000L


    @Rule
    @JvmField
    val activityTest = ActivityTestRule(HomeActivity::class.java)

    @Rule
    @JvmField
//    val mRuntimePermissionRule = GrantPermissionRule.grant(android.Manifest.permission.READ_EXTERNAL_STORAGE)
    var grantPermissionRule: GrantPermissionRule = GrantPermissionRule.grant(
        "android.permission.CAMERA", "android.permission.READ_EXTERNAL_STORAGE")

    @Before
    fun myTest() {
        val firstActivity: IntentsTestRule<HomeActivity> = IntentsTestRule(HomeActivity::class.java)
        firstActivity.launchActivity(Intent())
//        Intents.init()
    }

    @After
    fun cleanUp() {
        Intents.release()
    }

        @Test //game ng_sgp_10013 button
        fun checkFirstSGPGame() {
            sleep(3456)
        onView(allOf(withId(R.id.myGamesButton), childAtPosition(childAtPosition(
            withId(android.R.id.content), 0), 6))).perform(click())

        Thread.sleep(2110)
        onView(withId(R.id.recyclerView)).perform(
            RecyclerViewActions
            .actionOnItemAtPosition<RecyclerView.ViewHolder>(0, doubleClick()))
            sleep(2000)

        Thread.sleep(25110)
        Espresso.pressBack()

        Thread.sleep(2000)
        Espresso.pressBack()
        Thread.sleep(2120)
    }

    @Test //game ng_sgp_10014 button
    fun checkSecondSGPGame() {
        sleep(3456)
        onView(allOf(withId(R.id.myGamesButton), childAtPosition(childAtPosition(
            withId(android.R.id.content), 0), 6))).perform(click())

        Thread.sleep(2000)
        onView(withId(R.id.recyclerView)).perform(
            RecyclerViewActions
            .actionOnItemAtPosition<RecyclerView.ViewHolder>(1, doubleClick()))

        Thread.sleep(25000)
        Espresso.pressBack()

        Thread.sleep(2000)
        Espresso.pressBack()
        Thread.sleep(2000)
    }

    @Test //game ng_sgp_10001 button
    fun checkThirdSGPGame() {
        sleep(3456)
        onView(allOf(withId(R.id.myGamesButton), childAtPosition(childAtPosition(
            withId(android.R.id.content), 0), 6))).perform(click())

        Thread.sleep(2000)
        onView(withId(R.id.recyclerView)).perform(
            RecyclerViewActions
            .actionOnItemAtPosition<RecyclerView.ViewHolder>(2, doubleClick()))
        sleep(2000)

        Thread.sleep(25000)
        Espresso.pressBack()

        Thread.sleep(2000)
        Espresso.pressBack()
        Thread.sleep(2000)
    }

    @Test //game ng_sgp_10016 button
    fun checkFourthSGPGame() {
        sleep(3456)
        onView(allOf(withId(R.id.myGamesButton), childAtPosition(childAtPosition(
            withId(android.R.id.content), 0), 6))).perform(click())

        Thread.sleep(2000)
        onView(withId(R.id.recyclerView)).perform(
            RecyclerViewActions
            .actionOnItemAtPosition<RecyclerView.ViewHolder>(3, doubleClick()))

        Thread.sleep(25000)
        Espresso.pressBack()

        Thread.sleep(2000)
        Espresso.pressBack()
        Thread.sleep(2000)
    }

    @Test //game ng_sgp_10002 button
    fun checkFifthSGPgame() {
        sleep(3456)
        onView(allOf(withId(R.id.myGamesButton), childAtPosition(childAtPosition(
            withId(android.R.id.content), 0), 6))).perform(click())

        Thread.sleep(2000)
        onView(withId(R.id.recyclerView)).perform(
            RecyclerViewActions
            .actionOnItemAtPosition<RecyclerView.ViewHolder>(4, doubleClick()))

        Thread.sleep(25000)
        Espresso.pressBack()

        Thread.sleep(2000)
        Espresso.pressBack()
        Thread.sleep(2000)
    }

    @Test //game ng_sgp_10005 button
    fun checkSixthSGPGame() {
        sleep(3456)
        onView(allOf(withId(R.id.myGamesButton), childAtPosition(childAtPosition(
            withId(android.R.id.content), 0), 6))).perform(click())

        Thread.sleep(2000)
        onView(withId(R.id.recyclerView)).perform(
            RecyclerViewActions
            .actionOnItemAtPosition<RecyclerView.ViewHolder>(5, doubleClick()))

        Thread.sleep(25000)
        Espresso.pressBack()

        Thread.sleep(2000)
        Espresso.pressBack()
        Thread.sleep(2000)
    }

    @Test //game ng_sgp_10010 button
    fun checkSeventhSGPGame() {
        sleep(3456)
        onView(allOf(withId(R.id.myGamesButton), childAtPosition(childAtPosition(
            withId(android.R.id.content), 0), 6))).perform(click())

        Thread.sleep(2000)
        onView(withId(R.id.recyclerView)).perform(
            RecyclerViewActions
            .actionOnItemAtPosition<RecyclerView.ViewHolder>(6, doubleClick()))

        Thread.sleep(25000)
        Espresso.pressBack()

        Thread.sleep(2000)
        Espresso.pressBack()
        Thread.sleep(2000)
    }

    @Test //game ng_sgp_10018 button
    fun checkEighthSGPGame() {
        sleep(3456)
        onView(allOf(withId(R.id.myGamesButton), childAtPosition(childAtPosition(
            withId(android.R.id.content), 0), 6))).perform(click())

        Thread.sleep(2000)
        onView(withId(R.id.recyclerView)).perform(
            RecyclerViewActions
            .actionOnItemAtPosition<RecyclerView.ViewHolder>(7, doubleClick()))

        Thread.sleep(25000)
        Espresso.pressBack()

        Thread.sleep(2000)
        Espresso.pressBack()
        Thread.sleep(2000)
    }

    @Test //game ng_sgp_10017 button
    fun checkNinthSGPGame() {
        sleep(3456)
        onView(allOf(withId(R.id.myGamesButton), childAtPosition(childAtPosition(
            withId(android.R.id.content), 0), 6))).perform(click())

        Thread.sleep(2000)
        onView(withId(R.id.recyclerView)).perform(
            RecyclerViewActions
            .actionOnItemAtPosition<RecyclerView.ViewHolder>(8, doubleClick()))

        Thread.sleep(25000)
        Espresso.pressBack()

        Thread.sleep(2000)
        Espresso.pressBack()
        Thread.sleep(2000)
    }

    @Test //game ng_sgp_10024 button
    fun checkTenthSGPGame() {
        sleep(3456)
        onView(allOf(withId(R.id.myGamesButton), childAtPosition(childAtPosition(
            withId(android.R.id.content), 0), 6))).perform(click())

        Thread.sleep(2000)
        onView(withId(R.id.recyclerView)).perform(
            RecyclerViewActions
            .actionOnItemAtPosition<RecyclerView.ViewHolder>(9, doubleClick()))

        Thread.sleep(25000)
        Espresso.pressBack()
        Thread.sleep(2002)
        Espresso.pressBack()
        Thread.sleep(2001)
    }

        @Test //game ng_sgp_10006 button
        fun checkEleventhSGPGame() {
            sleep(3456)
        onView(allOf(withId(R.id.myGamesButton), childAtPosition(childAtPosition(
            withId(android.R.id.content), 0), 6))).perform(click())
            sleep(2000)
        onView(withId(R.id.recyclerView)).perform(
            RecyclerViewActions
            .actionOnItemAtPosition<RecyclerView.ViewHolder>(10, doubleClick()))
            sleep(2000)
        Espresso.pressBack()
            sleep(2000)
    }

    @Test //game ng_sgp_10007 button
    fun checkTwelfthSGPGame() {
        sleep(3456)
        onView(allOf(withId(R.id.myGamesButton), childAtPosition(childAtPosition(
            withId(android.R.id.content), 0), 6))).perform(click())
        sleep(2000)
        onView(withId(R.id.recyclerView)).perform(
            RecyclerViewActions
            .actionOnItemAtPosition<RecyclerView.ViewHolder>(11, doubleClick()))
        sleep(2000)
        Espresso.pressBack()
        sleep(2000)
        Espresso.pressBack()
        sleep(2000)
    }

    @Test //game ng_sgp_10008 button
    fun checkThirteenthSGPGame() {
        sleep(3456)
        onView(allOf(withId(R.id.myGamesButton), childAtPosition(childAtPosition(
            withId(android.R.id.content), 0), 6))).perform(click())
        sleep(2000)
        onView(withId(R.id.recyclerView)).perform(
            RecyclerViewActions
            .actionOnItemAtPosition<RecyclerView.ViewHolder>(12, doubleClick()))
        sleep(2000)
        Espresso.pressBack()
        sleep(2000)
        Espresso.pressBack()
        sleep(2000)
    }

////////CHECK KYGAME SCREEN
        //check KY game button
        @Test
        fun checkKyGameButton() {
    sleep(3456)
        onView(withId(R.id.kyqpButton)).check(matches(isClickable()))
    sleep(2000)
        onView(allOf(withId(R.id.kyqpButton), childAtPosition(childAtPosition(
            withId(android.R.id.content), 0), 5)))
            .perform(click())
    sleep(2000)
        pressBack()
    sleep(2000)
    }

    @Test //check Back Key has no function in WebView
    fun checkListGameKYQPVisible() {
        sleep(3456)
    waitForElement(AllRule().waitKyqpButton, 4000).perform(click())
    waitForElement(AllRule().waitKyqpWeb, 35000)
            .check(matches(isJavascriptEnabled()))

        uiDevice.pressBack()
//        waitForElement(waitErrortext, 4000).inRoot(withDecorView(not(activityTest.activity.window.decorView))).check(matches(isCompletelyDisplayed()))
        uiDevice.wait(Until.gone(By.text(Pattern.compile("[a-zA-Z.? ]*"))), fourSeconds)

    }
/*
    ///////MEMBER PROFILE UI COMPONENT
    //check exchange button
    @Test
    fun checkExchangeButton() {
        waitForElement(AllRule().waitExchangeButton, 8000)
            .check(matches(anyOf(
                withEffectiveVisibility(Visibility.VISIBLE),
                isClickable(),
                (isCompletelyDisplayed())
            )))
    }

    //check deposit button
    @Test
    fun checkDepositButton() {
        waitForElement(AllRule().waitExchangeButton, 8000).perform(click())
        waitForElement(AllRule().waitDepositButton, 8000)
            .check(matches(anyOf(
                withEffectiveVisibility(Visibility.VISIBLE),
                isClickable(),
                (isCompletelyDisplayed()))))
            .perform(click())
        waitForElement(AllRule().waitSubmitRequestButton, 8000).perform(click())
            .check(matches(anyOf(
                withEffectiveVisibility(Visibility.VISIBLE),
                isClickable(),
                (isCompletelyDisplayed()))))
            .perform(click())
        uiDevice.pressBack()
    }

    //check deposit button and make deposit request
    @Test
    fun checkDepositButtonAndRequest() {
        waitForElement(AllRule().waitExchangeButton, 8000).perform(click())
        waitForElement(AllRule().waitDepositButton, 8000)
            .perform(click())
        waitForElement(AllRule().waitCountEditText, 8000)
            .perform(scrollTo(), replaceText("1"),
                closeSoftKeyboard())
        waitForElement(AllRule().waitSubmitRequestButton, 8000)
            .perform(click())
        uiDevice.pressBack()
    }

    //check withdraw button
    @Test
    fun checkWithdrawButton() {
        waitForElement(AllRule().waitExchangeButton, 8000)
            .perform(click())
        waitForElement(AllRule().waitWithdrawButton, 8000)
            .perform(click())
        waitForElement(AllRule().waitSubmitRequestButton, 8000)
            .check(matches(anyOf(
                withEffectiveVisibility(Visibility.VISIBLE),
                isClickable(),
                (isCompletelyDisplayed()))))
            .perform(click())
        uiDevice.pressBack()
    }

    //check withdraw Button and make request
    @Test
    fun checkWithdrawButtonAndRequest() {
        waitForElement(AllRule().waitExchangeButton, 8000)
            .perform(click())
        waitForElement(AllRule().waitWithdrawButton, 8000)
            .perform(click())
        waitForElement(AllRule().waitCountEditText, 8000)
            .perform(scrollTo(), replaceText("1"),
                closeSoftKeyboard())
        waitForElement(AllRule().waitSubmitRequestButton, 8000)
            .perform(click())
        uiDevice.pressBack()
    }


    //////CHECK CAPTURE SCREEN

*/



      private fun childAtPosition(
          parentMatcher: Matcher<View>, position: Int
      ): Matcher<View> {

          return object : TypeSafeMatcher<View>() {
              override fun describeTo(description: Description) {
                  description.appendText("Child at position $position in parent ")
                  parentMatcher.describeTo(description)
              }

              public override fun matchesSafely(view: View): Boolean {
                  val parent = view.parent
                  return parent is ViewGroup && parentMatcher.matches(parent)
                          && view == parent.getChildAt(position)
              }
          }
      }



}
