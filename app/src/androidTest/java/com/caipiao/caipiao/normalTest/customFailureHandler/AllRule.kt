package com.caipiao.caipiao.normalTest.customFailureHandler

import android.Manifest
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.setFailureHandler
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import com.caipiao.caipiao.R
import com.caipiao.caipiao.ui.main.MainActivity
import org.hamcrest.CoreMatchers.startsWith
import org.junit.Before
import org.junit.Rule
import org.junit.rules.RuleChain
import org.junit.rules.TestName

open class AllRule {

    @get:Rule
    var testName = TestName()

    @get:Rule
    var ruleChain: RuleChain = RuleChain
        .outerRule(
            GrantPermissionRule.grant(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA))
        .around(TestName())


    @Before
    @Throws(Exception::class)
    open fun setUp() {
        setFailureHandler(
//            _root_ide_package_.com.caipiao.caipiao.normalTest.customFailureHandler.CustomFailureHandler(

                CustomFailureHandler(
                InstrumentationRegistry.getInstrumentation().targetContext
            )
        )
    }


    @Rule
    @JvmField
        var activityTestRule = ActivityTestRule(MainActivity::class.java)

    val waitForQrButton         = onView(withId(R.id.qrButton))
    val waitInternetErrorText =
        onView(withText(startsWith("Unable")))
    val waitBackKeyError           =
        onView(withText(startsWith("为避免意外退出游戏，请通过界面上的菜单或按钮退出")))
    val hintTextText =
        onView(withText(startsWith("Place a barcode inside the viewfinder rectangle to scan it.")))
    val waitWithdrawButton      = onView(withId(R.id.withdrawButton))
    val waitExchangeButton      = onView(withId(R.id.exchangeButton))
    val waitDepositButton       = onView(withId(R.id.depositButton))
    val waitCountEditText       = onView(withId(R.id.countEditText))
    val waitSubmitRequestButton = onView(withId(R.id.submitRequestButton))
    val waitKyqpWeb             = onView(withId(R.id.kyqpWeb))
    val waitKyqpButton          = onView(withId(R.id.kyqpButton))
    val waitQrButton            = onView(withId(R.id.qrButton))
    val waitGalleryButton       = onView(withId(R.id.galleryButton))
    val waitWechatText          = onView(withId(R.id.wechatEditText))
    val waitTokenText           = onView(withId(R.id.tokenEditText))
    val waitTokenDescription    = onView(withText("国金俱乐部会加您的微信，您接受微信邀请后，俱乐部会提供验证码，请稍候"))
    val waitShareButton         = onView(withId(R.id.shareButton))
    val waitReferImage          = onView(withId(R.id.tokenImageView))
    val waitTimerImage          = onView(withId(R.id.timerTextView))
    val waitGoldAmount          = onView(withId(R.id.goldTextView))
    val waitGoldIcon            = onView(withId(R.id.goldImageView))
    val waitMygamesButton        = onView(withId(R.id.myGamesButton))
    val waitSingleGameButton    = onView(withId(R.id.gameButton))
    val waitRecyclerView         = onView(withId(R.id.recyclerView))
    val waitSubmitButton         = onView(withId(R.id.submitButton))
    val waitScanningArea         = onView(withId(R.id.zxing_barcode_scanner))



    /**
     * Provided activity will be launched before each test.


     */
    /**
     * Makes the current test name available inside test methods.
     */


}