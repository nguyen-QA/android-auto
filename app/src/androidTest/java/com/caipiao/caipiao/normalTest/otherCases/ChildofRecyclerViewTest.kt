package com.caipiao.caipiao.normalTest.otherCases

import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.uiautomator.*
import com.caipiao.caipiao.ui.mygames.MyGamesActivity
import org.junit.*
import org.junit.runner.RunWith
import java.lang.Thread.sleep

/**
 * Demonstrates [UiWatcher] functionality in test.
 */
@RunWith(AndroidJUnit4::class)
class ChildofRecyclerViewTest {

    private val instrumentation = InstrumentationRegistry.getInstrumentation()
    private val uiDevice: UiDevice = UiDevice.getInstance(instrumentation)
    private val twoSeconds = 4000L
    @Rule
    @JvmField
    var activityTestRule = ActivityTestRule(MyGamesActivity::class.java)

    @Before
    // Register dialog watcher.
    fun before() = registerStatisticsDialogWatcher()

    @After
    fun after() = uiDevice.removeWatcher("StatisticsDialog")

    @Test
    fun checkGoldImageView() {
//        uiDevice.findObject(UiSelector().resourceId("app.src.main.res.layout:id/goldImageView")).click()
        uiDevice.findObject(UiSelector().text("∞")).waitUntilGone(twoSeconds)

    }

    @Test
    fun checkRecyclerView() {
        sleep(4321)
        uiDevice.findObject(UiSelector().className(RecyclerView::class.java.name)
            .childSelector(UiSelector().checkable(true)))
     }

    @Test
    fun checkChildImageRecyclerView() {
        uiDevice.findObject(UiSelector().className(ConstraintLayout::class.java.name)
            .childSelector(UiSelector().className(ImageView::class.java)).instance(0))
    }

    @Test
    fun checkChildOfConstraintLayout() {
        uiDevice.findObject(UiSelector().className(ConstraintLayout::class.java.name)
            .childSelector(UiSelector().className(RecyclerView::class.java)).instance(0))
    }

    @Test
    fun checkImageIsChildOfRecyclerView() {
        uiDevice.findObject(UiSelector().className(RecyclerView::class.java.name)
            .childSelector(UiSelector().className(ImageView::class.java)).instance(0))
    }

    @Test
    fun checkChildOfRecyclerView() {
        uiDevice.findObject(UiSelector().className(ImageView::class.java.name)
            .childSelector(UiSelector().className(RecyclerView::class.java)).instance(0))
    }

    /**
     * Register Statistics dialog watcher that will monitor dialog presence.
     * Dialog will be dismissed when appeared by clicking on OK button.
     */
    private fun registerStatisticsDialogWatcher() {
        uiDevice.registerWatcher("StatisticsDialog", statisticsDialogWatcher)

        // Run registered watcher.
        uiDevice.runWatchers()
    }

    companion object {
        private val instrumentation = InstrumentationRegistry.getInstrumentation()
        private val uiDevice: UiDevice = UiDevice.getInstance(instrumentation)

        val statisticsDialogWatcher = UiWatcher {
            val okDialogButton = uiDevice.findObject(By.res("android:id/recyclerView"))
//            val okDialogButton = uiDevice.findObject(By.res("android:id/recyclerView"))
            if (null != okDialogButton) {
                okDialogButton.click()
                return@UiWatcher true
            }
            false
        }
    }
}