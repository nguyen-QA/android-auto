package com.caipiao.caipiao.normalTest.customJDBC;

import android.content.res.Resources;
import android.text.format.DateUtils;

import androidx.test.espresso.IdlingPolicies;
import androidx.test.espresso.IdlingRegistry;

import com.caipiao.caipiao.normalTest.customIdlingResource.ElapsedTimeIdlingResource;

import org.jetbrains.annotations.NotNull;
import org.junit.Before;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.concurrent.TimeUnit;

// assume that conn is an already created JDBC connection

public class QueryDriver {
    @NotNull
    static Connection conn;
    @NotNull
    PreparedStatement stmt;
    @NotNull
    ResultSet rs;


    public void connectServer() throws ClassNotFoundException, InstantiationException {
        try {
            // The newInstance() call is a work around for some
            // broken Java implementations

            conn =
                    DriverManager.getConnection("jdbc:mysql://54.179.193.124/caipiao_member","admin","57kSEfQ9m32ghUjiysB2");
            Class.forName("com.mysql.jdbc.Driver").newInstance();

        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public String receiveToken(String userId) throws InterruptedException {
        String token = "NFOUND";
        try {


            stmt = conn.prepareStatement("SELECT token FROM wechat_auth WHERE create_at >= CURRENT_TIMESTAMP - 1000 LIMIT 1"); //good
//            stmt = conn.prepareStatement("SELECT token FROM wechat_auth WHERE create_at >= CURRENT_TIMESTAMP - 1000 LIMIT 1"); //good
//            stmt = conn.prepareStatement("SELECT token FROM wechat_auth ORDER BY create_at DESC LIMIT 1");
//           stmt = conn.prepareStatement("SELECT token FROM wechat_auth WHERE create_at> = CURRENT_TIMESTAMP - 1000 LIMIT 1"); //good

//            stmt = conn.prepareStatement("SELECT token FROM wechat_auth ORDER BY create_at -1000 DESC LIMIT 1"); //good
//          stmt = conn.prepareStatement("SELECT token FROM wechat_auth where user_id=? LIMIT 1"); //ORIGINAL

            stmt.setString(1, userId);

            ////////////QA ADD THIS to set a TIMEOUT
            stmt.setQueryTimeout(60);
//            TimeUnit.MINUTES.sleep(1);      ///////////////////


            rs = stmt.executeQuery();

            if (rs.first()) {
                                token = rs.getString("token");

            }
            // Now do something with the ResultSet ....
        } catch (SQLException ex) {

            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());


        } finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            if (rs != null) {
                try {
                    rs.close();

                } catch (SQLException sqlEx) {
                } // ignore
 //              rs.notify();
            }
            else if (stmt != null); {
                try {
                    stmt.close();

                } catch (SQLException sqlEx) {
                } // ignore
                stmt = null;
            }
            return token;
        }
    }
    public void disconnectServer() {

        try {
            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}