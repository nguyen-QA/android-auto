package com.caipiao.caipiao.normalTest.customAccessibility;

import org.jetbrains.annotations.NotNull;

import java.util.Random;

import kotlin.jvm.internal.Intrinsics;

public class TestData {
    @NotNull
    private String userName = "title" + System.currentTimeMillis() + (new Random()).nextInt(1000);
    @NotNull
    private String tokenCaptcha = "";

    @NotNull
    public final String getUserName() {
        return this.userName;
    }

    public final void setTitle(@NotNull String var1) {
        Intrinsics.checkParameterIsNotNull(var1, "<set-?>");
        this.userName = var1;
    }

    @NotNull
    public final String getDescription() {
        return this.tokenCaptcha;
    }

    public final void setDescription(@NotNull String var1) {
        Intrinsics.checkParameterIsNotNull(var1, "<set-?>");
        this.tokenCaptcha = var1;
    }
    public static String getToDoTitle() {
        return "item " + System.currentTimeMillis();
    }

    public static String getToDoDescription() {
        return "description " + System.currentTimeMillis();
    }

    public String getTokenCaptcha() {
        // It will generate 6 digit random Number.
        // from 0 to 999999
        Random rnd = new Random();
        int number = rnd.nextInt(999999);

        // this will convert any number sequence into 6 character.
        return this.tokenCaptcha = String.format("%06d", number);
    }
}
