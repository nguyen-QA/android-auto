package com.caipiao.caipiao.normalTest.customIdlingResource;

import android.view.View;

import androidx.test.espresso.IdlingResource;

public class ViewViewVisibleIdlingResource implements IdlingResource {

    private final View mView;
    private final int mExpectedVisibility;
    private ResourceCallback resourceCallback;
    private boolean mIdle;
    private ResourceCallback mResourceCallback;

    public ViewViewVisibleIdlingResource(final View view, final int expectedVisibility) {
        this.mView = view;
        this.mExpectedVisibility = expectedVisibility;
        this.mIdle = false;
        this.mResourceCallback = null;
    }

    @Override
    public String getName() {
        return ViewViewVisibleIdlingResource.class.getSimpleName();
    }

    @Override
    public boolean isIdleNow() {
        mIdle = mIdle || mView.getVisibility() == mExpectedVisibility;

        if (mIdle) {
            if (mResourceCallback != null) {
                mResourceCallback.onTransitionToIdle();
            }
        }

        return mIdle;
    }

    @Override
    public void registerIdleTransitionCallback(ResourceCallback callback) {
        mResourceCallback = resourceCallback;
    }
}
