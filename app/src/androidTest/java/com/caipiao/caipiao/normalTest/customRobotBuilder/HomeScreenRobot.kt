package com.caipiao.caipiao.normalTest.customRobotBuilder

import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.Until
import com.caipiao.caipiao.normalTest.customAction.BaseCustomAction
import com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherSecond.waitForElement
import com.caipiao.caipiao.normalTest.customFailureHandler.AllRule
import org.hamcrest.Matchers
import java.lang.Thread.sleep
import java.util.regex.Pattern


/**
 * Extension function that takes ToDoListRobot class function(s) as a parameter,
 * executes this function(s) and returns ToDosListRobot instance.
 */
 fun homeTask(func: HomeScreenRobot.() -> Unit) = HomeScreenRobot().apply { func() }
/**
 * Robot pattern applied to TO-DO list screen.
 */
class HomeScreenRobot {

    private val instrumentation = InstrumentationRegistry.getInstrumentation()
    private val uiDevice: UiDevice = UiDevice.getInstance(instrumentation)
    private val fourSeconds = 4000L

    fun checkNotConnection() {
//     waitForElement(AllRule()..waitInternetErrorText, 4000).check(matches(not(isDisplayed())))
        uiDevice.wait(Until.gone(By.text(Pattern.compile("[a-zA-Z.? ]*"))), fourSeconds)
    }

    fun checkGoldImage() {
        waitForElement(AllRule().waitGoldIcon, 4000)
            .check(matches(Matchers
                .anyOf(withEffectiveVisibility(Visibility.VISIBLE),
                    (isCompletelyDisplayed())
                )))
    }

    fun checkGoldText() {
        waitForElement(AllRule().waitGoldAmount, 4000)
            .check(matches(Matchers
                .anyOf(withEffectiveVisibility(Visibility.VISIBLE),
                    (isCompletelyDisplayed())
                )))
            .check(matches(withText("∞")))

    }

    fun checkScanButton() {
        waitForElement(AllRule().waitQrButton, 4000)
            .check(matches(
                Matchers.anyOf(
                    withEffectiveVisibility(Visibility.VISIBLE),
                    isClickable(),
                    (isCompletelyDisplayed())
                )
            ))
            .perform(click())

    }

    fun checkMyGameButton() {
        waitForElement(AllRule().waitMygamesButton, 4000)
            .check(matches(
                Matchers.anyOf(
                    withEffectiveVisibility(Visibility.VISIBLE),
                    isClickable(),
                    (isCompletelyDisplayed())
                )
            ))
            .perform(click())

    }

    fun checkKyqpButton() {
        waitForElement(AllRule().waitKyqpButton, 4000)
            .check(matches(
                Matchers.anyOf(
                    withEffectiveVisibility(Visibility.VISIBLE),
                    isClickable(),
                    (isCompletelyDisplayed())
                )
            ))
            .perform(click())

      }








}

