package com.caipiao.caipiao.normalTest.customObjectPattern

import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.LayoutAssertions.noOverlaps
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import com.caipiao.caipiao.R
import com.caipiao.caipiao.normalTest.customAccessibility.TestData
import com.caipiao.caipiao.normalTest.customAction.CustomViewInteraction.checkDisplayed
import com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherSecond.waitForElement
import com.caipiao.caipiao.normalTest.customFailureHandler.AllRule
import org.hamcrest.CoreMatchers.not
import org.hamcrest.Matchers.*
import org.hamcrest.text.IsEmptyString.isEmptyOrNullString
import java.lang.Thread.sleep
import java.util.*


class BuilderScreenObject {



    fun clickScanningButton(): BuilderScreenObject {
        sleep(6543)
        waitForElement(AllRule().waitQrButton, 5000)
            .check(
                matches(
                    anyOf(
                        withEffectiveVisibility(
                            Visibility.VISIBLE
                        ),
                        isClickable(),
                        (isCompletelyDisplayed())
                    )
                )
            )
            .perform(click())
        return this
    }

    fun verifyTheHintNotOverlap(): BuilderScreenObject {
        sleep(6543)
        waitForElement(AllRule().hintTextText, 5000)
            .checkDisplayed()
            .check(noOverlaps())
        return this

    }

    fun reviewWechatScreen(): BuilderScreenObject {
        sleep(6543)
        waitForElement(AllRule().waitWechatText, 5000)
            .perform(scrollTo())
            .check(
                matches(
                    anyOf(
                        withEffectiveVisibility(
                            Visibility.VISIBLE
                        ),
                        (isCompletelyDisplayed())
                    )
                )
            )
        return this
    }

    fun reviewTokenScreen(): BuilderScreenObject {
        sleep(6543)
        waitForElement(AllRule().waitTokenDescription, 5000)
            .perform(scrollTo())
            .check(
                matches(
                    anyOf(
                        withEffectiveVisibility(
                            Visibility.VISIBLE
                        ),
                        (isCompletelyDisplayed())
                    )
                )
            )
        return this
    }


    fun typeWechatText(userNameName:String): BuilderScreenObject {
//        val userNameName = "title" + System.currentTimeMillis() + (Random()).nextInt(1000)
        sleep(6543)
        waitForElement(AllRule().waitWechatText, 5000)
            .perform(
                scrollTo(),
                replaceText(userNameName),
                closeSoftKeyboard()
            )
        return this
    }

    fun verifyWeChatIsEmpty(): BuilderScreenObject {
        sleep(6543)
        waitForElement(AllRule().waitTokenText, 5000)
//            .check(matches(withText(isEmptyOrNullString())))
           .check(matches(withText("")))
        return this
    }

    fun checkSubmitButtonAreClickable(): BuilderScreenObject {
        sleep(6543)
        onView(withId(R.id.submitButton))
            .perform(scrollTo(), click())
        return this
    }

    fun reviewVerifyScreen(): BuilderScreenObject {
        sleep(6543)
        waitForElement(AllRule().waitTokenText, 5000)
            .perform(scrollTo())
            .check(
                matches(
                    anyOf(
                        withEffectiveVisibility(
                            Visibility.VISIBLE
                        ),
                        (isCompletelyDisplayed())
                    )
                )
            )
        return this
    }

    fun typeTokenText() : BuilderScreenObject {
        val userToken = TestData().tokenCaptcha
        sleep(6543)
        waitForElement(AllRule().waitTokenText, 5000)
            .check(
                matches(
                    anyOf(
                        withEffectiveVisibility(
                            Visibility.VISIBLE
                        ),
                        (isCompletelyDisplayed())
                    )
                )
            )
            .perform(
                scrollTo(),
                replaceText(userToken),
                closeSoftKeyboard()
            )
        return this
    }

    fun verifyTokenNotEmpty(): BuilderScreenObject {
        sleep(6543)
        waitForElement(AllRule().waitTokenText, 5000)
            .perform(scrollTo())
            .check(matches(not(withText(isEmptyOrNullString()))))
//            .check(matches(not(withText(""))))
        return this
    }


}