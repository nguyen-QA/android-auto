package com.caipiao.caipiao.normalTest.customConditionTime

import android.view.View
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.ViewAssertion
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.web.sugar.Web
import com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherFirst.setTimeoutLimit
import com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherFirst.waitForCondition
import org.hamcrest.Matcher

/**
 * Single place for all the [ConditionWatcherSecond] functions used in sample project.
 */
object ConditionWatcherSecond {

    private val TIMEOUT_LIMIT = 20000
    private val WATCH_INTERVAL = 400
    private val exceptionList: List<String>? = null

    init {
//        _root_ide_package_.com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherFirst.setTimeoutLimit(TIMEOUT_LIMIT)
//        _root_ide_package_.com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherFirst.setWatchInterval(400)
        ConditionWatcherFirst.setTimeoutLimit(TIMEOUT_LIMIT)
        ConditionWatcherFirst.setWatchInterval(400)
    }


    /**
     * Waits for a [View], located by [ViewInteraction], to be displayed on the screen.
     *
     * @param interaction - [ViewInteraction] that locatea a view.
     * @param timeout - amount of time in milliseconds to wait for condition.
     */
    @Throws(Exception::class)
    @JvmStatic
    fun waitForElement(
        interaction: ViewInteraction,     //ORIGINAL
//        interaction: Web.WebInteraction<Void>,

             timeout: Int = 5000): ViewInteraction {     //ORIGINAL
//        timeout: Int = 5000): Web.WebInteraction<Void> {
//        timeout: Int = 5000): Matcher<Any>? {

//        _root_ide_package_.com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherFirst.setTimeoutLimit(timeout)
//        _root_ide_package_.com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherFirst.waitForCondition(object : _root_ide_package_.com.caipiao.caipiao.normalTest.customConditionTime.Instruction() {
           ConditionWatcherFirst.setTimeoutLimit(timeout)
           ConditionWatcherFirst.waitForCondition(object :Instruction() {

            override fun getDescription(): String {
                return "waitForElement"
            }

            override fun checkCondition(): Boolean {
                try {
                    interaction.check(matches(isDisplayed()))
                    return true
                } catch (ex: NoMatchingViewException) {
                    return false
                }

            }
        })
//        return  anything()
        return interaction          //Original
    }

    /**
     * Waits for a [View], located by [ViewInteraction] to be fully visible on the screen.
     *
     * @param interaction - [ViewInteraction] that locates a view.
     * @param timeout - amount of time in milliseconds to wait for condition.
     */
    @Throws(Exception::class)
    fun waitForElementFullyVisible(
        interaction: ViewInteraction,
        timeout: Int): ViewInteraction {
//        _root_ide_package_.com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherFirst.setTimeoutLimit(timeout)
//        _root_ide_package_.com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherFirst.waitForCondition(object : _root_ide_package_.com.caipiao.caipiao.normalTest.customConditionTime.Instruction() {

         setTimeoutLimit(timeout)
         waitForCondition(object : Instruction() {


                override fun getDescription(): String {
                return "waitForElementFullyVisible"
            }

            override fun checkCondition(): Boolean {
                try {
                    interaction.check(matches(isDisplayed()))
                    return true
                } catch (ex: NoMatchingViewException) {
                    return false
                }

            }
        })
        return interaction
    }

    /**
     * Waits for a [View], located by [ViewInteraction], to be gone.
     *
     * @param interaction - [ViewInteraction] that locates a view.
     * @param timeout - amount of time in milliseconds to wait for condition.
     * @return [ViewInteraction] for located view.
     */
    @Throws(Exception::class)
    @JvmStatic
    fun waitForElementIsGone(
        interaction: ViewInteraction,
        timeout: Int = 5000): ViewInteraction {
//        _root_ide_package_.com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherFirst.setTimeoutLimit(timeout)
//        _root_ide_package_.com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherFirst.waitForCondition(object : _root_ide_package_.com.caipiao.caipiao.normalTest.customConditionTime.Instruction() {

        setTimeoutLimit(timeout)
        waitForCondition(object : Instruction() {


            override fun getDescription(): String {
                return "waitForElementIsGone"
            }

            override fun checkCondition(): Boolean {
                try {
                    interaction.check(matches(isDisplayed()))
                    return false
                } catch (ex: NoMatchingViewException) {
                    return true
                }

            }
        })
        return interaction
    }

    /**
     * Waits for [View], located by Matcher<View> to be gone.
     *
     * @param viewMatcher - Matcher<View> that locates a view.
     * @param timeout - amount of time in milliseconds to wait for condition.
     */
    @Throws(Exception::class)
    @JvmStatic
    fun waitForElementIsGone(
        viewMatcher: Matcher<View>,
        timeout: Int = 5000) {
//        _root_ide_package_.com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherFirst.setTimeoutLimit(timeout)
 //       _root_ide_package_.com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherFirst.waitForCondition(object : _root_ide_package_.com.caipiao.caipiao.normalTest.customConditionTime.Instruction() {

           setTimeoutLimit(timeout)
            waitForCondition(object :Instruction() {


                override fun getDescription(): String {
                return "waitForElementIsGone"
            }

            override fun checkCondition(): Boolean {
                try {
                    onView(viewMatcher).check(matches(isDisplayed()))
                    return false
                } catch (ex: NoMatchingViewException) {
                    return true
                }

            }
        })
    }

    /**
     * Waits for View, located by Matcher<View>, to be gone.
     *
     * @param interaction - Matcher<View> that locates a view.
     * @param timeout - amount of time in milliseconds to wait for condition.
     * @return usually ViewInteraction of the parent of the view that should be gone.
     */
    @Throws(Exception::class)
    @JvmStatic
    fun waitForElementIsDisappear(
        viewMatcher: Matcher<View>,
        timeout: Int = 5000) {
//        _root_ide_package_.com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherFirst.setTimeoutLimit(timeout)
//        _root_ide_package_.com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherFirst.waitForCondition(object : _root_ide_package_.com.caipiao.caipiao.normalTest.customConditionTime.Instruction() {

            setTimeoutLimit(timeout)
           waitForCondition(object :Instruction() {


            override fun getDescription(): String {
                return "waitForElementIsGone"
            }

            override fun checkCondition(): Boolean {
                try {
                    onView(viewMatcher).check(matches(isDisplayed()))
                    return false
                } catch (ex: NoMatchingViewException) {
                    return true
                }

            }
        })
    }


    @Throws(Exception::class)
    fun waitForElementIsGone(
        interaction: ViewInteraction,
        viewMatcher: Matcher<View>,
        timeout: Int): ViewInteraction {
//        _root_ide_package_.com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherFirst.setTimeoutLimit(timeout)
//        _root_ide_package_.com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherFirst.waitForCondition(object : _root_ide_package_.com.caipiao.caipiao.normalTest.customConditionTime.Instruction() {

        setTimeoutLimit(timeout)
        waitForCondition(object :Instruction() {



                override fun getDescription(): String {
                return "waitForElementIsGone"
            }

            override fun checkCondition(): Boolean {
                try {
                    onView(viewMatcher).check(matches(isDisplayed()))
                    return false
                } catch (ex: NoMatchingViewException) {
                    return true
                }

            }
        })
        return interaction
    }
}

private fun <R> Web.WebInteraction<R>.check(matches: ViewAssertion?) {

}
