package com.caipiao.caipiao.normalTest.customRecycler;

import android.view.View;
import android.view.ViewConfiguration;

import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.PerformException;
import androidx.test.espresso.UiController;
import androidx.test.espresso.ViewAction;
import androidx.test.espresso.util.HumanReadables;

import org.hamcrest.Matcher;

import static androidx.test.espresso.action.ViewActions.actionWithAssertions;
import static androidx.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static org.hamcrest.Matchers.allOf;

/**
 * Demonstrates custom {link RecyclerView} actions implementation.
 */
public interface CustomRecyclerViewActions extends ViewAction {
    /**
     * RecyclerView action that performs click on TO-DO checkbox based on its title.
     */
    class ClickTodoCheckBoxWithTitleViewAction implements CustomRecyclerViewActions {

        private String toDoTitle;

        public ClickTodoCheckBoxWithTitleViewAction(String toDoTitle) {
            this.toDoTitle = toDoTitle;
        }

        public static ViewAction clickTodoCheckBoxWithTitle(final String toDoTitle) {
            return actionWithAssertions(new ClickTodoCheckBoxWithTitleViewAction(toDoTitle));
        }

        @Override
        public Matcher<View> getConstraints() {
            return allOf(isAssignableFrom(RecyclerView.class), isDisplayed());
        }

        @Override
        public String getDescription() {
            return "Complete ToDo with title: \"" + toDoTitle + "\" by clicking its checkbox.";
        }

        @Override
        public void perform(UiController uiController, View view) {
            try {
                RecyclerView recyclerView = (RecyclerView) view;
                RecyclerView.Adapter adapter = recyclerView.getAdapter();
                uiController.loopMainThreadForAtLeast(ViewConfiguration.getTapTimeout());
            } catch (RuntimeException e) {
                throw new PerformException.Builder().withActionDescription(this.getDescription())
                        .withViewDescription(HumanReadables.describe(view)).withCause(e).build();
            }
        }
    }

    /**
     * ViewAction that scrolls to the last item in RecyclerView.
     */
    class ScrollToLastHolder implements CustomRecyclerViewActions {

        public static ViewAction scrollToLastHolder() {
            return actionWithAssertions(new ScrollToLastHolder());
        }

        @SuppressWarnings("unchecked")
        @Override
        public Matcher<View> getConstraints() {
            return allOf(isAssignableFrom(RecyclerView.class), isDisplayed());
        }

        @Override
        public void perform(UiController uiController, View view) {
            RecyclerView recyclerView = (RecyclerView) view;
            int itemCount = recyclerView.getAdapter().getItemCount();
            try {
                recyclerView.scrollToPosition(itemCount - 1);
                uiController.loopMainThreadUntilIdle();
            } catch (RuntimeException e) {
                throw new PerformException.Builder().withActionDescription(this.getDescription())
                        .withViewDescription(HumanReadables.describe(view)).withCause(e).build();
            }
        }

        @Override
        public String getDescription() {
            return "scroll to last holder in RecyclerView";
        }
    }


}

