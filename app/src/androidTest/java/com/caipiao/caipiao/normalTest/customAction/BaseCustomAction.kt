/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.caipiao.caipiao.normalTest.customAction

import android.app.Activity
import android.content.Context
import android.graphics.Point
import android.graphics.Rect
import android.os.Bundle
import android.os.ResultReceiver
import android.os.SystemClock
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.HorizontalScrollView
import android.widget.ScrollView
import androidx.test.espresso.*
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.*
import androidx.test.espresso.action.EspressoKey
import androidx.test.espresso.action.ViewActions.pressKey
import androidx.test.espresso.core.internal.deps.guava.base.Preconditions
import androidx.test.espresso.core.internal.deps.guava.collect.Iterables
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.isDisplayingAtLeast
import androidx.test.espresso.matcher.ViewMatchers.isRoot
import androidx.test.espresso.util.HumanReadables
import androidx.test.runner.lifecycle.ActivityLifecycleMonitorRegistry
import androidx.test.runner.lifecycle.Stage
import com.caipiao.caipiao.normalTest.customAction.EspressoExtensionsCustomAction.Companion.searchFor
import com.google.common.base.Preconditions.checkNotNull
import org.hamcrest.Matcher
import org.hamcrest.Matchers
import java.lang.Thread.sleep
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException
import java.util.concurrent.atomic.AtomicInteger


/**
 * A collection of common {@link ViewActions}.
 */
class BaseCustomAction {


    /***********************************************************************************************/
    /**
     * Clears view text by setting []s text property to "".
     */
    fun performClearTextAction(uiController: UiController, view: View) {
        (view as EditText).setText("")
    }

    /*********************************************************************************************/
    /**
     * Closes soft keyboard.
     */

    fun performCloseKeyboardAction(uiController: UiController, view: View) {
        // Retry in case of timeout exception to avoid flakiness in IMM.
        for (i in 0 until NUM_RETRIES) {
            try {
                tryToCloseKeyboard(view, uiController)
                return
            } catch (te: TimeoutException) {
                Log.w(TAG, "Caught timeout exception. Retrying.")
                if (i == 2) {
                    throw PerformException.Builder()
                        .withActionDescription(this.description)
                        .withViewDescription(HumanReadables.describe(view))
                        .withCause(te)
                        .build()
                }
            }
        }
    }

    @Throws(TimeoutException::class)
    private fun tryToCloseKeyboard(view: View, uiController: UiController) {
        val imm = getRootActivity(uiController)
            .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        val atomicResultCode = AtomicInteger()
        val latch = CountDownLatch(1)
        val result: ResultReceiver = object : ResultReceiver(null) {
            override fun onReceiveResult(resultCode: Int, resultData: Bundle) {
                atomicResultCode.set(resultCode)
                latch.countDown()
            }
        }
        if (!imm.hideSoftInputFromWindow(view.windowToken, 0, result)) {
            Log.w(TAG, "Attempting to close soft keyboard, while it is not shown.")
            return
        }
        try {
            if (!latch.await(2, TimeUnit.SECONDS)) {
                throw TimeoutException("Wait on operation result timed out.")
            }
        } catch (e: InterruptedException) {
            throw PerformException.Builder()
                .withActionDescription(this.description)
                .withViewDescription(HumanReadables.describe(view))
                .withCause(RuntimeException("Waiting for soft keyboard close result was interrupted."))
                .build()
        }
        if (atomicResultCode.get() != InputMethodManager.RESULT_UNCHANGED_HIDDEN
            && atomicResultCode.get() != InputMethodManager.RESULT_HIDDEN
        ) {
            val error =
                ("Attempt to close the soft keyboard did not result in soft keyboard to be hidden."
                        + "resultCode = " + atomicResultCode.get())
            Log.e(TAG, error)
            throw PerformException.Builder()
                .withActionDescription(this.description)
                .withViewDescription(HumanReadables.describe(view))
                .withCause(RuntimeException(error))
                .build()
        }
    }

    /*********************************************************************************************/
    /**
     * Enables pressing KeyEvents on views.
     */
    var key: EspressoKey? = null

    @JvmName("getConstraints1")
    fun getConstraints(): Matcher<View> {
        return ViewMatchers.isDisplayed();
        ViewMatchers.isAssignableFrom(
            EditText::class.java
        )
    }

    fun performKeyEventAction(uiController: UiController, view: View) {
        try {
            if (!sendKeyEvent(uiController, view)) {
                Log.e(TAG, "Failed to inject key event: $key")
                throw PerformException.Builder()
                    .withActionDescription(this.description)
                    .withViewDescription(HumanReadables.describe(view))
                    .withCause(RuntimeException("Failed to inject key event $key"))
                    .build()
            }
        } catch (e: InjectEventSecurityException) {
            Log.e(TAG, "Failed to inject key event: $key")
            throw PerformException.Builder()
                .withActionDescription(this.description)
                .withViewDescription(HumanReadables.describe(view))
                .withCause(e)
                .build()
        }
    }

    @Throws(InjectEventSecurityException::class)
    private fun sendKeyEvent(controller: UiController, view: View): Boolean {
        var injected = false
        var eventTime = SystemClock.uptimeMillis()
        run {
            var attempts = 0
            while (!injected && attempts < 4) {
                injected = controller.injectKeyEvent(
                    KeyEvent(
                        eventTime,
                        eventTime,
                        KeyEvent.ACTION_DOWN,
                        this.key!!.keyCode,
                        0,
                        this.key!!.metaState
                    )
                )
                attempts++
            }
        }
        if (!injected) {
            // it is not a transient failure... :(
            return false
        }
        injected = false
        eventTime = SystemClock.uptimeMillis()
        var attempts = 0
        while (!injected && attempts < 4) {
            if (key != null) {
                injected = controller.injectKeyEvent(
                    KeyEvent(eventTime, eventTime, KeyEvent.ACTION_UP, key!!.keyCode, 0)
                )
            }
            attempts++
        }
        if (key != null) {
            if (key!!.keyCode == KeyEvent.KEYCODE_BACK) {
                controller.loopMainThreadUntilIdle()
                val activeActivities = !ActivityLifecycleMonitorRegistry.getInstance()
                    .getActivitiesInStage(Stage.RESUMED)
                    .isEmpty()
                if (!activeActivities) {
                    val cause: Throwable = PerformException.Builder()
                        .withActionDescription(this.description)
                        .withViewDescription(HumanReadables.describe(view))
                        .build()
                    throw NoActivityResumedException("Pressed back and killed the app", cause)
                }
            }
        }
        return injected
    }

    @JvmName("getDescription1")
    fun getDescription(): String {
        return String.format("send %s key event" + "\"close keyboard\"", key)
    }


    init {
        this.key = Preconditions.checkNotNull(key)
    }


    /*************************************************************************************/
    /**
     * Enables scrolling to the given view. View must be a descendant of a ScrollView.
     */
    //    @Override
    val constraints: Matcher<View>
        get() = Matchers.allOf(
            ViewMatchers.withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE),
            ViewMatchers.isDescendantOfA(
                Matchers.anyOf(
                    ViewMatchers.isAssignableFrom(ScrollView::class.java),
                    ViewMatchers.isAssignableFrom(
                        HorizontalScrollView::class.java
                    )
                )
            )
        )

    //    @Override
    fun performScrollTo(uiController: UiController, view: View) {
        if (ViewMatchers.isDisplayingAtLeast(90).matches(view)) {
            Log.i(TAG, "View is already displayed. Returning.")
            return
        }
        val rect = Rect()
        view.getDrawingRect(rect)
        if (!view.requestRectangleOnScreen(rect, true /* immediate */)) {
            Log.w(TAG, "Scrolling to view was requested, but none of the parents scrolled.")
        }
        uiController.loopMainThreadUntilIdle()
        if (!ViewMatchers.isDisplayingAtLeast(90).matches(view)) {
            throw PerformException.Builder()
                .withActionDescription(description)
                .withViewDescription(HumanReadables.describe(view))
                .withCause(
                    RuntimeException(
                        "Scrolling to view was attempted, but the view is not displayed"
                    )
                )
                .build()
        }
    }

    val description: String
        get() = "scroll to"

    companion object {
        private const val NUM_RETRIES = 3
        private val TAG = BaseCustomAction::class.java.simpleName
        private fun getRootActivity(uiController: UiController): Activity {
            var resumedActivities =
                ActivityLifecycleMonitorRegistry.getInstance().getActivitiesInStage(
                    Stage.RESUMED
                )
            if (resumedActivities.isEmpty()) {
                uiController.loopMainThreadUntilIdle()
                resumedActivities =
                    ActivityLifecycleMonitorRegistry.getInstance().getActivitiesInStage(
                        Stage.RESUMED
                    )
            }
            return Iterables.getOnlyElement(resumedActivities)
        }
//        private val TAG = BaseCustomViewAction::class.java.simpleName
    }
    /**********************************************************************************************/
    /**
     * Returns an action that clears text on the view.<br>
     * <br>
     * View constraints:
     * <ul>
     * <li>must be displayed on screen
     * <ul>
     */
    fun clearTextAction(): ViewAction {
        return clearTextAction()
    }

    /**
     * Returns an action that clicks the view.<br>
     * <br>
     * View constraints:
     * <ul>
     * <li>must be displayed on screen
     * <ul>
     */
    fun clickImageAction(): ViewAction {
        return GeneralClickAction(Tap.SINGLE, GeneralLocation.CENTER, Press.FINGER)
    }

    /**
     * Returns an action that performs a single click on the view.
     *
     * If the click takes longer than the 'long press' duration (which is possible) the provided
     * rollback action is invoked on the view and a click is attempted again.
     *
     * This is only necessary if the view being clicked on has some different behaviour for long press
     * versus a normal tap.
     *
     * For example - if a long press on a particular view element opens a popup menu -
     * ViewActions.pressBack() may be an acceptable rollback action.
     *
     * <br>
     * View constraints:
     * <ul>
     * <li>must be displayed on screen</li>
     * <li>any constraints of the rollbackAction</li>
     * <ul>
     */
    fun clickLongPress(rollbackAction: ViewAction): ViewAction {
        checkNotNull(rollbackAction)
        return GeneralClickAction(Tap.SINGLE, GeneralLocation.CENTER, Press.FINGER, rollbackAction)
    }

    /**
     * Returns an action that performs a swipe right-to-left across the vertical center of the
     * view.<br>
     * <br>
     * View constraints:
     * <ul>
     * <li>must be displayed on screen
     * <ul>
     */
    fun swipeLeftAction(): ViewAction {
        return GeneralSwipeAction(
            Swipe.FAST, GeneralLocation.CENTER_RIGHT,
            GeneralLocation.CENTER_LEFT, Press.FINGER
        )
    }

    /**
     * Returns an action that performs a swipe left-to-right across the vertical center of the
     * view.<br>
     * <br>
     * View constraints:
     * <ul>
     * <li>must be displayed on screen
     * <ul>
     */
    fun swipeRightAction(): ViewAction {
        return GeneralSwipeAction(
            Swipe.FAST, GeneralLocation.CENTER_LEFT,
            GeneralLocation.CENTER_RIGHT, Press.FINGER
        )
    }


    /**
     * Returns an action that closes soft keyboard. If the keyboard is already closed, it is a no-op.
     */
    fun closeSoftKeyboardAction(): ViewAction {
        return CloseKeyboardAction()
    }


    /**
     * Returns an action that presses the current action button (next, done, search, etc) on the IME
     * (Input Method Editor). The selected view will have its onEditorAction method called.
     */
    fun pressImeActionButtonAction(): ViewAction {
        return EditorAction()
    }


    /**
     * Returns an action that clicks the back button.
     */
    fun pressBackAction(): ViewAction {
        return pressKey(KeyEvent.KEYCODE_BACK)
    }


    /**
     * Returns an action that presses the hardware menu key.
     */


    fun pressMenuKeyAction(): ViewAction {
        return pressKey(KeyEvent.KEYCODE_MENU)
    }


    /**
     * Returns an action that presses the key specified by the keyCode (eg. Keyevent.KEYCODE_BACK).
     */
    fun pressKeyAction(keyCode: Int): ViewAction {
        return KeyEventAction(EspressoKey.Builder().withKeyCode(keyCode).build())
    }


    /**
     * Returns an action that presses the specified key with the specified modifiers.
     */
    fun pressKeyViewAction(key: EspressoKey): ViewAction {
        return KeyEventAction(key)
    }


    /**
     * Returns an action that double clicks the view.<br>
     * <br>
     * View preconditions:
     * <ul>
     * <li>must be displayed on screen
     * <ul>
     */
    fun doubleClickAction(): ViewAction {
        return GeneralClickAction(Tap.DOUBLE, GeneralLocation.CENTER, Press.FINGER)
    }


    /**
     * Returns an action that long clicks the view.<br>
     *
     * <br>
     * View preconditions:
     * <ul>
     * <li>must be displayed on screen
     * <ul>
     */
    fun longClickAction(): ViewAction {
        return GeneralClickAction(Tap.LONG, GeneralLocation.CENTER, Press.FINGER)
    }
/***********************************************************************************************/




    /**
     * Returns an action that scrolls to the view.<br>
     * <br>
     * View preconditions:
     * <ul>
     * <li>must be a descendant of ScrollView
     * <li>must have visibility set to View.VISIBLE
     * <ul>
     */
    fun scrollToActionAction(uiController: UiController, view: View) {
//        return ScrollToAction()
        if (isDisplayingAtLeast(90).matches(view))
        {
            Log.i(TAG, "View is already displayed. Returning.")
            return
        }
        val rect = Rect()
        view.getDrawingRect(rect)
        if (!/* immediate */view.requestRectangleOnScreen(rect, true))
        {
            Log.w(TAG, "Scrolling to view was requested, but none of the parents scrolled.")
        }
        uiController.loopMainThreadUntilIdle()
        if (!isDisplayingAtLeast(90).matches(view))
        {
            throw PerformException.Builder()
                .withActionDescription(this.getDescription())
                .withViewDescription(HumanReadables.describe(view))
                .withCause(RuntimeException(
                    "Scrolling to view was attempted, but the view is not displayed"))
                .build()
        }

    }


        /***************************************************************************************/

        /**
         * combine onView(view) & perform(action)
         * now became doOnView(view , action, action...)
         */
        fun doOnView(matcher: Matcher<View>, vararg actions: ViewAction) {
            actions.forEach {
                waitForView(matcher).perform(it)
            }
        }


        /**
         * combine onView(view) & check(assertion)
         * now became assertOnView(view , assertion, assertion...)
         */
        fun assertOnView(matcher: Matcher<View>, vararg assertions: ViewAssertion) {
            assertions.forEach {
                waitForView(matcher).check(it)
            }
        }

        /**
         * Perform action of implicitly waiting for a certain view.
         * This differs from EspressoExtensions.searchFor in that,
         * upon failure to locate an element, it will fetch a new root view
         * in which to traverse searching for our @param match
         *
         * @param viewMatcher ViewMatcher used to find our view
         */
        fun waitForView(
            viewMatcher: Matcher<View>,
            waitMillis: Int = 15000,
            waitMillisPerTry: Long = 1000
        ): ViewInteraction {

            // Derive the max tries
            val maxTries = waitMillis / waitMillisPerTry.toInt()

            var tries = 0

            for (i in 0..maxTries)
                try {
                    // Track the amount of times we've tried
                    tries++

                    // Search the root for the view
                    onView(isRoot()).perform(searchFor(viewMatcher))

                    // If we're here, we found our view. Now return it
                    return onView(viewMatcher)

                } catch (e: Exception) {

                    if (tries == maxTries) {
                        throw e
                    }
                    sleep(waitMillisPerTry)
                }

            throw Exception("Error finding a view matching $viewMatcher")
        }


    }
