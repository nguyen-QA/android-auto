package com.caipiao.caipiao.normalTest.otherCases


import com.caipiao.caipiao.todoTest.uiTest.AuthActivityTest
import com.caipiao.caipiao.todoTest.uiTest.AuthVerifyAcitivityTest
import com.caipiao.caipiao.todoTest.uiTest.ContentAcitivityTest
import com.caipiao.caipiao.todoTest.uiTest.GameActivityTest
import org.junit.runner.RunWith
import org.junit.runners.Suite

@RunWith(Suite::class)
@Suite.SuiteClasses(

    //pick random test any time
    AuthActivityTest::class
    , AuthVerifyAcitivityTest::class
    , ContentAcitivityTest::class
    , GameActivityTest::class
)
class EspressoTestSuiteTest {
}