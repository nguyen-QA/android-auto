package com.caipiao.caipiao.normalTest.otherCases

import androidx.test.espresso.intent.Intents
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import com.caipiao.caipiao.normalTest.customRobotBuilder.homeTask
import com.caipiao.caipiao.ui.home.HomeActivity
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class HomeScreenRobotTest {

    @get: Rule
    val activityTest = ActivityTestRule(HomeActivity::class.java)


    @Rule
    @JvmField
//    val mRuntimePermissionRule = GrantPermissionRule.grant(android.Manifest.permission.READ_EXTERNAL_STORAGE)
    var grantPermissionRule: GrantPermissionRule = GrantPermissionRule.grant(
        "android.permission.CAMERA", "android.permission.READ_EXTERNAL_STORAGE")

    @Before
    fun myTest() {
//        val firstActivity: IntentsTestRule<HomeActivity> = IntentsTestRule(HomeActivity::class.java)
//        firstActivity.launchActivity(Intent())
        Intents.init()
    }


    @After
    fun cleanUp() {
        Intents.release()
    }

    //check Gold Icon
    @Test
    fun checkGoldIconRobot() {
        homeTask { checkGoldImage() }
    }

    //check Gold Amount
    @Test
    fun checkGoldAmountRobot() {
        homeTask { checkGoldText() }
    }


    //check KYQP Button
    @Test
    fun checkKyqpButtonRobot() {
        homeTask { checkKyqpButton() }
    }

    //check My game button
    @Test
    fun checkMygameButtonRobot() {
        homeTask { checkMyGameButton() }
}

    //check QR button
    @Test
    fun checkQrScanningRobot() {
        homeTask { checkScanButton() }
    }

        @Test
        fun checkInternetRobot() {
            homeTask { checkNotConnection() }
    }



}