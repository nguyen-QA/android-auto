package com.caipiao.caipiao.normalTest.otherCases

import android.text.format.DateUtils
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.IdlingPolicies
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.Until
import com.caipiao.caipiao.R
import com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherSecond.waitForElement
import com.caipiao.caipiao.normalTest.customFailureHandler.AllRule
import com.caipiao.caipiao.normalTest.customIdlingResource.ElapsedTimeIdlingResource
import com.caipiao.caipiao.ui.home.HomeActivity
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.*
import org.hamcrest.TypeSafeMatcher
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.lang.Thread.sleep
import java.util.concurrent.TimeUnit
import java.util.regex.Pattern

@RunWith(AndroidJUnit4ClassRunner::class)
    class AllScreenUITest {

    private val instrumentation = InstrumentationRegistry.getInstrumentation()
    private val uiDevice: UiDevice = UiDevice.getInstance(instrumentation)
    private val fourSeconds = 4000L



    @get: Rule
    val activityTest = ActivityTestRule(HomeActivity::class.java)


    @Rule
    @JvmField
//    val mRuntimePermissionRule = GrantPermissionRule.grant(android.Manifest.permission.READ_EXTERNAL_STORAGE)
    var grantPermissionRule: GrantPermissionRule = GrantPermissionRule.grant(
        "android.permission.CAMERA", "android.permission.READ_EXTERNAL_STORAGE")


/////ADDING TIME OUT FOR IDLE TEST
      @Before
      fun init() {
          //Make sure the app does not Timeout
          IdlingPolicies.setMasterPolicyTimeout(120, TimeUnit.SECONDS)
          IdlingPolicies.setIdlingResourceTimeout(120, TimeUnit.SECONDS)
      }


    @Test
    //check there is Internet Connection and no Toast message appears
        fun checkNotInternet() {
        sleep(4321)
        //Run tasks
        sleep(1000)
//        waitForElement(AllRule()..waitInternetErrorText, 4000).check(matches(not(isDisplayed())))
        uiDevice.wait(Until.gone(By.text(Pattern.compile("[a-zA-Z.? ]*"))), fourSeconds)
        sleep(500)
    }

//////CHECK HOME SCREEN
    @Test
    //check home screen gold ImageView
        fun checkHomeGoldIcon() {
        //Run tasks
        sleep(500)
        onView(withId(R.id.goldImageView)).check(matches(isDisplayed()))
    }


    @Test
    //check gold amount TextView
        fun checkHomeGoldAmount() {
        sleep(4321)
        //Run tasks
        waitForElement(AllRule().waitGoldAmount, 4000)
            .check(matches(withText("∞")))
        sleep(500)
    }

    @Test
    //check qr button
        fun checkQrButton() {
        sleep(4321)
        //Run tasks
        waitForElement(AllRule().waitQrButton, 4000)
            .check(matches(anyOf(
                withEffectiveVisibility(Visibility.VISIBLE),
                isClickable(),
                (isCompletelyDisplayed()))))
            .perform(click())
        uiDevice.pressBack()
    }



///////CHECK MYGAME SCROLLVIEW
    @Test
    //check MYGAME game button
        fun checkSGPGameButton() {
          //Run tasks
        waitForElement(AllRule().waitMygamesButton, 4000)
            .check(matches(anyOf(
                withEffectiveVisibility(Visibility.VISIBLE),
                isClickable(),
                (isCompletelyDisplayed()))))
            .perform(click())
         uiDevice.pressBack()
    }

    //check swipe function
    @Test
    fun checkSwipeFunction() {
        sleep(4321)
        //Run tasks
        waitForElement(AllRule().waitMygamesButton, 4000)
            .perform(click())
        waitForElement(AllRule().waitRecyclerView, 4000)
            .perform(ViewActions.swipeLeft())
            .perform(ViewActions.swipeRight())
        uiDevice.pressBack()
    }

    //check mygame gold ImageView
    @Test
    fun checkMyGameGoldIcon() {
        sleep(4321)
        //Run tasks
        waitForElement(AllRule().waitMygamesButton, 4000)
            .perform(click())
        waitForElement(AllRule().waitGoldIcon, 4000)
            .check(matches(anyOf(
                withEffectiveVisibility(Visibility.VISIBLE),
                isClickable(),
                (isCompletelyDisplayed()))))
        uiDevice.pressBack()
    }

    //check gold Amount TextView
    @Test
    fun checkMyGameGoldAmount() {
        sleep(4321)
        //Run tasks
        waitForElement(AllRule().waitMygamesButton, 4000)
            .perform(click())
        waitForElement(AllRule().waitGoldAmount, 4000)
            .check(matches(anyOf(
                withEffectiveVisibility(Visibility.VISIBLE),
                isClickable(),
                (isCompletelyDisplayed()))))
            .check(matches(withText("∞")))
        uiDevice.pressBack()
    }

    @Test
    //game ng_sgp_10013 button
        fun check_firstGameButtonVisible() {
        waitForMySgpGameList(DateUtils.SECOND_IN_MILLIS * 8, 0)
    }


    @Test //game ng_sgp_10014 button
    fun check_secondGameButtonVisible() {
        //Run tasks
        waitForMySgpGameList(DateUtils.SECOND_IN_MILLIS * 8, 1)
    }

    @Test //game ng_sgp_10001 button
    fun check_thirdGameButtonVisible() {
        //Run tasks
        waitForMySgpGameList(DateUtils.SECOND_IN_MILLIS * 8, 2)
    }

    @Test //game ng_sgp_10016 button
    fun check_fourthGameButtonVisible() {
        //Run tasks
        waitForMySgpGameList(DateUtils.SECOND_IN_MILLIS * 8, 3)
    }

    @Test //game ng_sgp_10002 button
    fun check_fifthGameButtonVisible() {
        //Run tasks
        waitForMySgpGameList(DateUtils.SECOND_IN_MILLIS * 8, 4)
    }

    @Test //game ng_sgp_10005 button
    fun check_sixthGameButtonVisible() {
        //Run tasks
        waitForMySgpGameList(DateUtils.SECOND_IN_MILLIS * 8, 5)
    }

    @Test //game ng_sgp_10010 button
    fun check_seventhGameButtonVisible() {
        //Run tasks
        waitForMySgpGameList(DateUtils.SECOND_IN_MILLIS * 8, 6)
    }

    @Test //game ng_sgp_10018 button
    fun check_eighthGameButtonVisible() {
        //Run tasks
        waitForMySgpGameList(DateUtils.SECOND_IN_MILLIS * 8, 7)
    }

    @Test //game ng_sgp_10017 button
    fun check_ninthGameButtonVisible() {
        //Run tasks
        waitForMySgpGameList(DateUtils.SECOND_IN_MILLIS * 8, 8)
    }

    @Test //game ng_sgp_10024 button
    fun check_tenthGameButtonVisible() {
         //Run tasks
        waitForMySgpGameList(DateUtils.SECOND_IN_MILLIS * 8, 9)
    }

    @Test //game ng_sgp_10006 button
    fun check_eleventhGameButtonVisible() {
        //Run tasks
        waitForMySgpGameList(DateUtils.SECOND_IN_MILLIS * 8, 10)
    }

    @Test //game ng_sgp_10007 button
    fun check_twelfthGameButtonVisible() {
        //Run tasks
        waitForMySgpGameList(DateUtils.SECOND_IN_MILLIS * 8, 11)
    }

    @Test //game ng_sgp_10008 button
    fun check_thirteenthGameButtonVisible() {
        //Run tasks
        waitForMySgpGameList(DateUtils.SECOND_IN_MILLIS * 8, 12)
    }

    ////////CHECK KYGAME SCREEN
    //check KY game button
    @Test
    fun checkKyGameButton() {
        //Run tasks
        sleep(4321)
        onView(allOf(withId(R.id.kyqpButton), childAtPosition(
            childAtPosition(withId(android.R.id.content), 0),
                5))).perform(click())
        sleep(500)
        ViewActions.pressBack()
        sleep(500)
    }

    //check WebView
    @Test
    fun checkWebView() {
        //Run tasks
        waitForMyKYGameList(DateUtils.SECOND_IN_MILLIS * 120)
    }


    ////////ADDING PRIVATE FUNCTION FOR IDLE RESOURCE in ScrollView
    private fun waitForMySgpGameList(waitingTime: Long, positionButton: Int) {
        // Start
        sleep(4321)
        onView(allOf(withId(R.id.myGamesButton), childAtPosition(
            childAtPosition(withId(android.R.id.content), 0),
            6))).perform(click())

        // Make sure Espresso does not time out
        IdlingPolicies.setMasterPolicyTimeout(waitingTime * 2, TimeUnit.MILLISECONDS)
        IdlingPolicies.setIdlingResourceTimeout(waitingTime * 2, TimeUnit.MILLISECONDS)

        // Now we wait
        val idlingResource = ElapsedTimeIdlingResource(waitingTime)
        IdlingRegistry.getInstance().register(idlingResource)

        // Stop and verify
        onView(withId(R.id.recyclerView)).check(matches(isDisplayed())).perform(
            RecyclerViewActions
                .actionOnItemAtPosition<RecyclerView.ViewHolder>(positionButton, click())
        )
        sleep(1000)     //waiting time can be change flexible
        pressBack()
        // Clean up
        IdlingRegistry.getInstance().unregister(idlingResource)
    }

    private fun waitForMyKYGameList(waitingTime: Long) {
        // Start
        sleep(4321)
        onView(allOf(withId(R.id.kyqpButton), childAtPosition(
            childAtPosition(withId(android.R.id.content), 0),
            5))).perform(click())

        // Make sure Espresso does not time out
        IdlingPolicies.setMasterPolicyTimeout(waitingTime * 2, TimeUnit.MILLISECONDS)
        IdlingPolicies.setIdlingResourceTimeout(waitingTime * 2, TimeUnit.MILLISECONDS)

        // Now we wait
        val idlingResource = ElapsedTimeIdlingResource(waitingTime)
        IdlingRegistry.getInstance().register(idlingResource)

        // Stop and verify
        onView(withId(R.id.kyqpWeb)).check(matches(isCompletelyDisplayed()))

        sleep(1000)     //waiting time can be change flexible
        pressBack()
//    uiDevice.wait(Until.gone(By.text(Pattern.compile("[a-zA-Z.? ]*"))), fourSeconds)

        // Clean up
        IdlingRegistry.getInstance().unregister(idlingResource)
    }

    private fun childAtPosition(
        parentMatcher: Matcher<View>, position: Int
    ): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }



}
