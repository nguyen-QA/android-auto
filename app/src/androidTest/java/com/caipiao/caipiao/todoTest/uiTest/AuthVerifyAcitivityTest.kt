package com.caipiao.caipiao.todoTest.uiTest

import android.content.Intent
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.SmallTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import com.caipiao.caipiao.R
import com.caipiao.caipiao.normalTest.customAccessibility.TestData
import com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherFirst
import com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherSecond
import com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherSecond.waitForElement
import com.caipiao.caipiao.normalTest.customFailureHandler.AllRule
import com.caipiao.caipiao.normalTest.customObjectPattern.BuilderScreenObject
import com.caipiao.caipiao.ui.auth.AuthActivity
import com.caipiao.caipiao.ui.home.HomeActivity
import com.caipiao.caipiao.ui.verify.AuthVerifyActivity
import org.hamcrest.CoreMatchers.startsWith
import org.hamcrest.Matchers
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.lang.Thread.sleep

@SmallTest
@RunWith(AndroidJUnit4ClassRunner::class)
class AuthVerifyAcitivityTest {

    @get: Rule
    val activityTest = ActivityTestRule(AuthVerifyActivity::class.java)


    @Rule
    @JvmField
//    val mRuntimePermissionRule = GrantPermissionRule.grant(android.Manifest.permission.READ_EXTERNAL_STORAGE)
    var grantPermissionRule: GrantPermissionRule = GrantPermissionRule.grant(
        "android.permission.CAMERA", "android.permission.READ_EXTERNAL_STORAGE"
    )

    @Before
    fun myTest() {
        Intents.init()
    }

    @After
    fun cleanUp() {
        Intents.release()
    }



    @Test
    fun checkTokenHintText() {
        sleep(4311)
        onView(withId(R.id.tokenEditText))
            .check(matches(withHint("验证码")))
    }

    @Test
    fun checkDescriptionLine() {
        sleep(4311)
        waitForElement(AllRule().waitTokenDescription, 4000)
            .check(matches(isDisplayed()))//国金俱乐部会加您的微信，您接受微信邀请后，俱乐部会提供验证码，请稍候
    }

    @Test
    fun checkVerifyButton() {
        sleep(4311)
        onView(withId(R.id.submitButton))
            .check(matches(withText("提交验证码")))
    }

    @Test
    fun checkRandomTokenText() {
        BuilderScreenObject()
            .typeTokenText()
    }

    @Test
    fun checkSubmitWechatClickable() {
        BuilderScreenObject()
            .typeTokenText()
            .checkSubmitButtonAreClickable()
    }

    @Test
    fun checkRandomToken() {
        BuilderScreenObject()
            .checkSubmitButtonAreClickable()
            .typeTokenText()
            .reviewTokenScreen()
            .checkSubmitButtonAreClickable()

    }
}