package com.caipiao.caipiao.todoTest.uiTest

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingPolicies
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.contrib.RecyclerViewActions.scrollToPosition
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.SmallTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import androidx.test.uiautomator.UiDevice
import com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherSecond.waitForElement
import com.caipiao.caipiao.normalTest.customFailureHandler.AllRule
import com.caipiao.caipiao.ui.mygames.MyGamesActivity
import org.hamcrest.Matchers.anything
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.lang.Thread.sleep
import java.util.concurrent.TimeUnit

@SmallTest
@RunWith(AndroidJUnit4ClassRunner::class)
class MygamesAcitivityTest {

    @Rule
    @JvmField
    val activityTest = ActivityTestRule(MyGamesActivity::class.java)

    @Rule
    @JvmField
//    val mRuntimePermissionRule = GrantPermissionRule.grant(android.Manifest.permission.READ_EXTERNAL_STORAGE)
    var grantPermissionRule: GrantPermissionRule = GrantPermissionRule.grant(
        "android.permission.CAMERA", "android.permission.READ_EXTERNAL_STORAGE")

    private val instrumentation = InstrumentationRegistry.getInstrumentation()
    private val uiDevice: UiDevice = UiDevice.getInstance(instrumentation)
    private val fourSeconds =7000L

    val mySGPGameAppearence = onView(isRoot())


    /////ADDING TIME OUT FOR IDLE TEST
    @Before
    fun init() {
        Intents.init()
        //Make sure the app does not Timeout
        IdlingPolicies.setMasterPolicyTimeout(600, TimeUnit.MILLISECONDS)
        IdlingPolicies.setIdlingResourceTimeout(600, TimeUnit.MILLISECONDS)
    }

    @After
    fun cleanUp() {
        Intents.release()
    }

    @Test
    //game ng_sgp_10013 button
    fun check_firstGame_With_ConditionMatcher() {
        sleep(4578)
        waitForElement(AllRule().waitRecyclerView, 5000)
            .perform(
                scrollToPosition<RecyclerView
                .ViewHolder>(0)
                ,actionOnItemAtPosition<RecyclerView.ViewHolder>(0, doubleClick()))

        //adding Condition Matcher
        waitForElement(mySGPGameAppearence, 20000)
            .check(matches(anything()))

        uiDevice.pressBack()
    }



    @Test //game ng_sgp_10014 button
    fun check_secondGame_With_ConditionMatcher() {
        sleep(4356)
        waitForElement(AllRule().waitRecyclerView, 5000)
            .perform(
                scrollToPosition<RecyclerView
                .ViewHolder>(1)
                ,actionOnItemAtPosition<RecyclerView.ViewHolder>(1, doubleClick()))

        //adding Condition Matcher
        waitForElement(mySGPGameAppearence, 20000)
            .check(matches(anything()))

        uiDevice.pressBack()
    }

    @Test //game ng_sgp_10001 button
    fun check_thirdGame_With_ConditionMatcher() {
        sleep(4321)
        waitForElement(AllRule().waitRecyclerView, 5000)
            .perform(
                scrollToPosition<RecyclerView
                .ViewHolder>(2)
                ,actionOnItemAtPosition<RecyclerView.ViewHolder>(2, doubleClick()))

        //adding Condition Matcher
        waitForElement(mySGPGameAppearence, 20000)
            .check(matches(anything()))

        uiDevice.pressBack()
    }

    @Test //game ng_sgp_10016 button
    fun check_fourthGame_With_ConditionMatcher() {
        sleep(4200)
        waitForElement(AllRule().waitRecyclerView, 5000)
            .perform(
                scrollToPosition<RecyclerView
                .ViewHolder>(3)
                ,actionOnItemAtPosition<RecyclerView.ViewHolder>(3, doubleClick()))

        //adding Condition Matcher
        waitForElement(mySGPGameAppearence, 20000)
            .check(matches(anything()))

        uiDevice.pressBack()
    }

    @Test //game ng_sgp_10002 button
    fun check_fifthGame_With_ConditionMatcher() {
        sleep(4300)
        waitForElement(AllRule().waitRecyclerView, 5000)
            .perform(
                scrollToPosition<RecyclerView
                .ViewHolder>(4)
                ,actionOnItemAtPosition<RecyclerView.ViewHolder>(4, doubleClick()))

        //adding Condition Matcher
        waitForElement(mySGPGameAppearence, 20000)
            .check(matches(anything()))

        uiDevice.pressBack()
    }

    @Test //game ng_sgp_10005 button
    fun check_sixthGame_With_ConditionMatcher() {
        sleep(4400)
        waitForElement(AllRule().waitRecyclerView, 5000)
            .perform(
                scrollToPosition<RecyclerView
                .ViewHolder>(5)
                ,actionOnItemAtPosition<RecyclerView.ViewHolder>(5, doubleClick()))

        //adding Condition Matcher
        waitForElement(mySGPGameAppearence, 20000)
            .check(matches(anything()))

        uiDevice.pressBack()
    }

    @Test //game ng_sgp_10010 button
    fun check_seventhGame_With_ConditionMatcher() {
        sleep(4600)
        waitForElement(AllRule().waitRecyclerView, 5000)
            .perform(
                scrollToPosition<RecyclerView
                .ViewHolder>(6)
                ,actionOnItemAtPosition<RecyclerView.ViewHolder>(6, doubleClick()))

        //adding Condition Matcher
        waitForElement(mySGPGameAppearence, 20000)
            .check(matches(anything()))

        uiDevice.pressBack()
    }

    @Test //game ng_sgp_10018 button
    fun check_eighthGame_With_ConditionMatcher() {
        sleep(3500)
        waitForElement(AllRule().waitRecyclerView, 5000)
            .perform(
                scrollToPosition<RecyclerView
                .ViewHolder>(7)
                ,actionOnItemAtPosition<RecyclerView.ViewHolder>(7, doubleClick()))

        //adding Condition Matcher
        waitForElement(mySGPGameAppearence, 20000)
            .check(matches(anything()))

        uiDevice.pressBack()
    }

    @Test //game ng_sgp_10017 button
    fun check_ninthGame_With_ConditionMatcher() {
        sleep(3700)
        waitForElement(AllRule().waitRecyclerView, 5000)
            .perform(
                scrollToPosition<RecyclerView
                .ViewHolder>(8)
                ,actionOnItemAtPosition<RecyclerView.ViewHolder>(8, doubleClick()))

        //adding Condition Matcher
        waitForElement(mySGPGameAppearence, 20000)
            .check(matches(anything()))

        uiDevice.pressBack()
    }

    @Test //game ng_sgp_10024 button
    fun check_tenthGame_With_ConditionMatcher() {
        sleep(3800)
        waitForElement(AllRule().waitRecyclerView, 5000)
            .perform(
                scrollToPosition<RecyclerView
                .ViewHolder>(9)
                ,actionOnItemAtPosition<RecyclerView.ViewHolder>(9, doubleClick()))

        //adding Condition Matcher
        waitForElement(mySGPGameAppearence, 20000)
            .check(matches(anything()))

        uiDevice.pressBack()
    }

    @Test //game ng_sgp_10006 button
    fun check_eleventhGame_With_ConditionMatcher() {
        sleep(3900)
        waitForElement(AllRule().waitRecyclerView, 5000)
            .perform(
                scrollToPosition<RecyclerView
                .ViewHolder>(10)
                ,actionOnItemAtPosition<RecyclerView.ViewHolder>(10, doubleClick()))

        //adding Condition Matcher
        waitForElement(mySGPGameAppearence, 20000)
            .check(matches(anything()))

        uiDevice.pressBack()
    }

    @Test //game ng_sgp_10007 button
    fun check_twelfthGame_With_ConditionMatcher() {
        sleep(4000)
        waitForElement(AllRule().waitRecyclerView, 5000)
            .perform(
                scrollToPosition<RecyclerView
            .ViewHolder>(11)
            ,actionOnItemAtPosition<RecyclerView.ViewHolder>(11, doubleClick()))

        //adding Condition Matcher
        waitForElement(mySGPGameAppearence, 20000)
            .check(matches(anything()))

       uiDevice.pressBack()
    }

    @Test //game ng_sgp_10008 button
    fun check_thirteenGame_With_ConditionMatcher() {
        sleep(4100)
        waitForElement(AllRule().waitRecyclerView, 5000)
            .perform(
                scrollToPosition<RecyclerView
                .ViewHolder>(12)
                ,actionOnItemAtPosition<RecyclerView.ViewHolder>(12, doubleClick()))

        //adding Condition Matcher
        waitForElement(mySGPGameAppearence, 20000)
            .check(matches(anything()))

        uiDevice.pressBack()
    }




}

