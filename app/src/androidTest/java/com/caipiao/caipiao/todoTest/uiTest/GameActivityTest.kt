package com.caipiao.caipiao.todoTest.uiTest

import android.content.ComponentName
import android.content.Intent
import androidx.test.InstrumentationRegistry
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.ResolvedIntent
import androidx.test.espresso.intent.matcher.IntentMatchers
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import androidx.test.uiautomator.UiDevice
import com.caipiao.caipiao.ui.game.GAME_ACTIVITY_EXTRA_URL
import com.caipiao.caipiao.ui.game.GameActivity
import com.caipiao.caipiao.ui.home.HomeActivity
import org.hamcrest.Matchers
import org.junit.*
import org.junit.rules.ExpectedException
import org.junit.runner.RunWith

/**
 * Unit tests for {link IntentMatchers}.
 */
//@SmallTest
//@RunWith(AndroidJUnit4::class)
class GameActivityTest {

    @get: Rule
    var expectedException = ExpectedException.none()

    //Clean up these tests - each matcher should be tested separated.

    private val instrumentation = androidx.test.platform.app.InstrumentationRegistry.getInstrumentation()
    private val uiDevice: UiDevice = UiDevice.getInstance(instrumentation)
    private val fourSeconds = 4000L


    @Rule
    @JvmField
    val activityTest = ActivityTestRule(GameActivity::class.java)

    @Rule
    @JvmField
//    val mRuntimePermissionRule = GrantPermissionRule.grant(android.Manifest.permission.READ_EXTERNAL_STORAGE)
    var grantPermissionRule: GrantPermissionRule = GrantPermissionRule.grant(
        "android.permission.CAMERA", "android.permission.READ_EXTERNAL_STORAGE")



    @Before
    fun myTest() {
 //       val firstActivity: IntentsTestRule<HomeActivity> = IntentsTestRule(GameActivity::class.java)
 //       firstActivity.launchActivity(Intent())
        Intents.init()
    }

    @After
    fun cleanUp() {
        Intents.release()
    }

    @Test
    fun hasFlagsWithStringFlag() {
        val intent = Intent(GAME_ACTIVITY_EXTRA_URL)
        val pkg = "com.caipiao.caipiao"
        val cls = "$pkg.GameActivity"
        val c = ComponentName(pkg, cls)
        intent.component = c
        Assert.assertTrue(IntentMatchers.hasComponent(intent.component!!.className).matches(intent))
        Assert.assertTrue(IntentMatchers.hasComponent(intent.component).matches(intent))
        Assert.assertTrue(IntentMatchers.hasComponent(Matchers.equalTo(c)).matches(intent))
    }


}