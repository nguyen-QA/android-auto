package com.caipiao.caipiao.todoTest.uiTest

import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.SmallTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import androidx.test.uiautomator.UiDevice
import com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherSecond.waitForElement
import com.caipiao.caipiao.normalTest.customFailureHandler.AllRule
import com.caipiao.caipiao.ui.refer.ReferActivity
import org.hamcrest.CoreMatchers.anyOf
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.lang.Thread.sleep

@SmallTest
@RunWith(AndroidJUnit4ClassRunner::class)
class ReferActivityTest {

    @Rule
    @JvmField
    val mActivityTestRule = ActivityTestRule(ReferActivity::class.java)

    @Rule
    @JvmField
    var grantPermissionRule: GrantPermissionRule = GrantPermissionRule.grant(
        "android.permission.CAMERA", "android.permission.READ_EXTERNAL_STORAGE")

    private val instrumentation = InstrumentationRegistry.getInstrumentation()
    private val uiDevice: UiDevice = UiDevice.getInstance(instrumentation)
    private val twoSeconds = 4000L


    @Before
    fun myTest() {
        Intents.init()
    }

    @After
    fun cleanUp() {
        Intents.release()
    }

    //check refer token Image
    @Test
    fun checkReferTokenImage() {
        sleep(6543)
        waitForElement(AllRule().waitReferImage, 8000)
            .check(matches(anyOf(
                withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE),
                isClickable(),
                (isCompletelyDisplayed()))))
    }

    //check sharing button
    @Test
    fun checkShareButton() {
        sleep(6543)
        waitForElement(AllRule().waitShareButton, 4000)
            .check(matches(anyOf(
                withEffectiveVisibility(Visibility.VISIBLE),
                isClickable(),
                (isCompletelyDisplayed()))))
    }

    //check timer textview
    @Test
    fun checkTimerText() {
        sleep(6543)
        waitForElement(AllRule().waitTimerImage, 8000)
            .check(matches(anyOf(
                withEffectiveVisibility(Visibility.VISIBLE),
                isFocusable(),
                (isCompletelyDisplayed()))))

    }



}