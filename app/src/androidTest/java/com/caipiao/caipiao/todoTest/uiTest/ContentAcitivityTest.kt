package com.caipiao.caipiao.todoTest.uiTest

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.espresso.matcher.ViewMatchers.Visibility
import androidx.test.filters.SmallTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import com.caipiao.caipiao.R
import com.caipiao.caipiao.normalTest.customAction.EspressoTestsMatchers.withId
import com.caipiao.caipiao.normalTest.customAction.EspressoTestsMatchers.hasBackground
import com.caipiao.caipiao.normalTest.customAction.EspressoTestsMatchers.matchesBackgroundColor
import com.caipiao.caipiao.normalTest.customObjectPattern.BuilderScreenObject
import com.caipiao.caipiao.ui.auth.AuthActivity
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.lang.Thread.sleep
import java.util.*

@SmallTest
@RunWith(AndroidJUnit4ClassRunner::class)
class ContentAcitivityTest {

    @get: Rule
    val activityTest = ActivityTestRule(AuthActivity::class.java)


    @Rule
    @JvmField
//    val mRuntimePermissionRule = GrantPermissionRule.grant(android.Manifest.permission.READ_EXTERNAL_STORAGE)
    var grantPermissionRule: GrantPermissionRule = GrantPermissionRule.grant(
        "android.permission.CAMERA", "android.permission.READ_EXTERNAL_STORAGE")

    @Before
    fun myTest() {
        Intents.init()
    }

    @After
    fun cleanUp() {
        Intents.release()
    }

    @Test
    fun checkWechatTextColor() {
        sleep(6543)
        onView(withId(R.id.wechatEditText))
            .perform(
                scrollTo(),
                replaceText("anything"),
                closeSoftKeyboard())

        sleep(6543)
        onView(withId(R.id.wechatEditText)).check(matches(hasTextColor(R.color.palePeach)))

    }


    @Test
    fun checkWechatTextHint() {
        sleep(6543)
        onView(withId(R.id.wechatEditText))
            .check(matches(withHint(R.string.wechat_id)))
    }

    @Test
    fun checkbackgroundView() {
        sleep(6543)
        onView(withId(R.id.backgroundView))
            .check(matches(isDisplayed()))
    }

    @Test
    fun checkbetOptionsScrollView() {
        sleep(6543)
        onView(withId(R.id.betOptionsScrollView))
            .check(matches(isDisplayed()))
    }

    @Test
    fun checkSubmitButtonTextColor() {
        sleep(6543)
        onView(withId(R.id.submitButton))
            .check(matches(hasTextColor(R.color.white)))
    }

    @Test
    fun checkSubmitButtonBackgroundColor() {
        sleep(6543)
        onView(withId(R.id.submitButton))
            .check(matches(matchesBackgroundColor(R.color.mudBrown)))
    }

    @Test
    fun checkSubmitButtonClickable() {
        BuilderScreenObject()
            .typeWechatText(userNameName = "title" + System.currentTimeMillis() + (Random()).nextInt(1000))
            .checkSubmitButtonAreClickable()
    }


    @Test
    fun checkprogressBar() {
        onView(withId(R.id.progressBar))
            .check(matches(
            CoreMatchers.anyOf(
                withEffectiveVisibility(Visibility.GONE),
                isEnabled()
                ,isFocusable())))

    }

}



















