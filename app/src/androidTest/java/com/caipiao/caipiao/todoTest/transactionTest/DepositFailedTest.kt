package com.caipiao.caipiao.todoTest.transactionTest

import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import androidx.test.uiautomator.UiDevice
import com.caipiao.caipiao.normalTest.customAction.CustomViewInteraction.checkMatches
import com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherSecond.waitForElement
import com.caipiao.caipiao.normalTest.customFailureHandler.AllRule
import com.caipiao.caipiao.ui.home.HomeActivity
import com.caipiao.caipiao.ui.main.MainActivity
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.lang.Thread.sleep

@RunWith(AndroidJUnit4ClassRunner::class)
    class DepositFailedTest {

    private val instrumentation = InstrumentationRegistry.getInstrumentation()
    private val uiDevice: UiDevice = UiDevice.getInstance(instrumentation)
    private val fourSeconds = 4000L


    @Rule
    @JvmField
    val activityTest = ActivityTestRule(HomeActivity::class.java)

    @Rule
    @JvmField
//    val mRuntimePermissionRule = GrantPermissionRule.grant(android.Manifest.permission.READ_EXTERNAL_STORAGE)
    var grantPermissionRule: GrantPermissionRule = GrantPermissionRule.grant(
        "android.permission.CAMERA", "android.permission.READ_EXTERNAL_STORAGE"
    )

    @Before
    fun myTest() {
//        val firstActivity: IntentsTestRule<HomeActivity> = IntentsTestRule(HomeActivity::class.java)
//        firstActivity.launchActivity(Intent())
        Intents.init()
    }

    @After
    fun cleanUp() {
        Intents.release()
    }
    // run this test after login test
    //check deposit button and make deposit request
    @Test
    fun checkDepositButtonAndRequest() {
        sleep(4321)
        waitForElement(AllRule().waitExchangeButton, 8000).perform(click())
        waitForElement(AllRule().waitDepositButton, 8000)
            .perform(click())
        waitForElement(AllRule().waitCountEditText, 8000)
            .perform(
                replaceText("1"),
                closeSoftKeyboard()
            )
        waitForElement(AllRule().waitSubmitRequestButton, 8000)
            .perform(click())


        //insert the gold amount before you deposit
        waitForElement(AllRule().waitGoldAmount, 4000)
                  .checkMatches(withText("0"))
    }




}
