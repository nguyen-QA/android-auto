package com.caipiao.caipiao.todoTest.uiTest;


import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.espresso.ViewInteraction;
import androidx.test.filters.SmallTest;
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;
import androidx.test.rule.ActivityTestRule;

import com.caipiao.caipiao.R;
import com.caipiao.caipiao.ui.home.HomeActivity;
import com.caipiao.caipiao.ui.main.MainActivity;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;


@SmallTest
@RunWith(AndroidJUnit4ClassRunner.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<HomeActivity> rule =
            new ActivityTestRule<HomeActivity>(HomeActivity.class, true);


    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void mainActivityTest() {
        sleep(10000);
        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.ball2Button), withText("第二球"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.betOptionsScrollView),
                                        0),
                                2)));
        appCompatButton.perform(scrollTo(), click());

        ViewInteraction appCompatButton2 = onView(
                allOf(withId(R.id.number1Button), withText("1"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.singleBallBetScrollView),
                                        0),
                                1)));
        appCompatButton2.perform(scrollTo(), click());

        sleep(1000);

        ViewInteraction appCompatImageButton = onView(
                allOf(withId(R.id.submitButton),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.coordinatorlayout.widget.CoordinatorLayout")),
                                        1),
                                26),
                        isDisplayed()));
        appCompatImageButton.perform(click());

        sleep(10000);

        ViewInteraction appCompatButton4 = onView(
                allOf(withId(R.id.unfinishButton), withText("未结"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.coordinatorlayout.widget.CoordinatorLayout")),
                                        1),
                                8),
                        isDisplayed()));
        appCompatButton4.perform(click());

        sleep(5000);

        ViewInteraction textView = onView(
                allOf(withId(R.id.betTextView), withText("第二球1"),
                        childAtPosition(
                                allOf(withId(R.id.finished_cell),
                                        childAtPosition(
                                                withId(R.id.recyclerView),
                                                0)),
                                0),
                        isDisplayed()));
        textView.check(matches(withText("第二球1")));

        ViewInteraction textView2 = onView(
                allOf(withId(R.id.countTextView), withText("10"),
                        childAtPosition(
                                allOf(withId(R.id.finished_cell),
                                        childAtPosition(
                                                withId(R.id.recyclerView),
                                                0)),
                                1),
                        isDisplayed()));
        textView2.check(matches(withText("10")));

        ViewInteraction textView3 = onView(
                allOf(withId(R.id.resultTextView), withText("9.60"),
                        childAtPosition(
                                allOf(withId(R.id.finished_cell),
                                        childAtPosition(
                                                withId(R.id.recyclerView),
                                                0)),
                                2),
                        isDisplayed()));
        textView3.check(matches(withText("9.60")));
    }

    private void sleep(int mil) {
        try {
            Thread.sleep(mil);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }


}
