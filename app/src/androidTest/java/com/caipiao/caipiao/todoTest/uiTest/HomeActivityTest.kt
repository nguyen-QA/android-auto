package com.caipiao.caipiao.todoTest.uiTest

import android.view.View
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.SmallTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.Until
import com.caipiao.caipiao.R
import com.caipiao.caipiao.normalTest.customAction.EspressoTestsMatchers.withId
import com.caipiao.caipiao.normalTest.customAction.EspressoTestsMatchers.matchesBackgroundColor
import com.caipiao.caipiao.normalTest.customAction.EspressoViewMatchers.withBackground
import com.caipiao.caipiao.normalTest.customObjectPattern.BuilderScreenObject
import com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherSecond.waitForElement
import com.caipiao.caipiao.normalTest.customFailureHandler.AllRule
import com.caipiao.caipiao.ui.home.HomeActivity
import com.caipiao.caipiao.ui.main.MainActivity
import org.hamcrest.CoreMatchers.*
import org.hamcrest.Matcher
import org.hamcrest.Matchers
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.util.regex.Pattern


/**
 * QA add many sleep() methods to make sure Automation tool has enough time to check all the view on low devices
 */


@SmallTest
@RunWith(AndroidJUnit4ClassRunner::class)
class HomeActivityTest {

    @Rule
    @JvmField
    val activityTest = ActivityTestRule(MainActivity::class.java)

    @Rule
    @JvmField
    var grantPermissionRule: GrantPermissionRule = GrantPermissionRule.grant(
        "android.permission.CAMERA", "android.permission.READ_EXTERNAL_STORAGE")

    private val instrumentation = InstrumentationRegistry.getInstrumentation()
    private val uiDevice: UiDevice = UiDevice.getInstance(instrumentation)
    private val fourSeconds = 4000L

    //check gold imageview icon
    @Test
    fun checkGoldIcon() {
        Thread.sleep(6543)
        onView(withId(R.id.goldImageView))
            .check(matches(isDisplayed()))
    }


    //check gold textview
    @Test
    fun checkGoldAmount() {
        Thread.sleep(6543)
        onView(withId(R.id.goldTextView))
            .check(matches(withText("∞")))
    }

    //check qr button
    @Test
    fun checkQrButton() {
        Thread.sleep(6543)
        onView(withId(R.id.qrButton))
            .check(matches(
                anyOf(
                    withEffectiveVisibility(Visibility.VISIBLE),
                    isClickable(),
                    (isCompletelyDisplayed()))))
        .perform(click())
        uiDevice.pressBack()
    }

    //check kyqp button
    @Test
    fun checkKyqpButton() {
        Thread.sleep(6543)
        onView(withId(R.id.kyqpButton))
            .check(matches(
                anyOf(
                    withEffectiveVisibility(Visibility.VISIBLE),
                    isClickable(),
                    (isCompletelyDisplayed()))))
            .perform(click())
        uiDevice.pressBack()
    }

    //check MYGAME game button
    @Test
    fun checkMYGAMEButton() {
        Thread.sleep(6543)
        onView(withId(R.id.myGamesButton))
            .check(matches(
                anyOf(
                    withEffectiveVisibility(Visibility.VISIBLE),
                    isClickable(),
                    (isCompletelyDisplayed()))))
            .perform(click())
        uiDevice.pressBack()
    }

    //check guideline1
    @Test
    fun checkGuideline1() {
        Thread.sleep(6543)
        onView(withId(R.id.guideline1))
            .check(matches(
                anyOf(
                    withEffectiveVisibility(Visibility.GONE),
                    isEnabled())))
    }

    //check guideline12
    @Test
    fun checkGuideline2() {
        Thread.sleep(6543)
        onView(withId(R.id.guideline2))
            .check(matches(
                anyOf(
                    withEffectiveVisibility(Visibility.GONE),
                    isEnabled())))
    }


}


