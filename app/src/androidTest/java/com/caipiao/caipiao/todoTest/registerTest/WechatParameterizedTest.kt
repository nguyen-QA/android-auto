package com.caipiao.caipiao.todoTest.registerTest

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.rule.ActivityTestRule
import com.caipiao.caipiao.R
import com.caipiao.caipiao.normalTest.customAccessibility.TestData
import com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherSecond.waitForElement
import com.caipiao.caipiao.normalTest.customFailureHandler.AllRule
import com.caipiao.caipiao.ui.auth.AuthActivity
import com.caipiao.caipiao.ui.home.HomeActivity
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import java.lang.Thread.sleep


/**
 * Parametrized test with multiple parameters.
 */
@RunWith(value = Parameterized::class)
class WechatParameterizedTest (
    private val userName: String)
{

    @Rule
    @JvmField
    val mmActivityTest = ActivityTestRule(AuthActivity::class.java)


    @Before
    fun myTest() {
        Intents.init()
//        val firstActivity: IntentsTestRule<CaptureActivity> = IntentsTestRule(CaptureActivity::class.java)
//        firstActivity.launchActivity(Intent())
    }
    @After
    fun cleanUp() {
        Intents.release()
    }

        //pick random a Social ID and verify
        @Test
        fun checkMultiWechat() {
            Thread.sleep(6543)
            onView(withId(R.id.wechatEditText)).check(matches(isDisplayed()))

            Thread.sleep(6543)
            onView(withId(R.id.wechatEditText))
                .perform(
                    scrollTo(),
                    replaceText(userName),
                    closeSoftKeyboard())

            Thread.sleep(6543)
            onView(withId(R.id.wechatEditText)).check(matches(withText(userName)))

            Thread.sleep(6543)
            onView(withId(R.id.submitButton)).check(matches(isDisplayed()))

            Thread.sleep(6543)
            onView(withId(R.id.submitButton)) .perform(scrollTo(), click())

            Thread.sleep(6543)
            onView(withId(R.id.submitButton)).check(matches(isDisplayed()))

        }

        companion object {
            @JvmStatic
            @Parameterized.Parameters
            fun dataInject() = listOf(
                TestData().userName
                ,TestData().userName
                ,TestData().userName
               ,TestData().userName


            )
        }

}