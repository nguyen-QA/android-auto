package com.caipiao.caipiao.todoTest.uiTest

import android.content.Intent
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.SmallTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import com.caipiao.caipiao.R
import com.caipiao.caipiao.normalTest.customObjectPattern.BuilderScreenObject
import com.caipiao.caipiao.ui.auth.AuthActivity
import com.caipiao.caipiao.ui.home.HomeActivity
import org.hamcrest.CoreMatchers.startsWith
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*

@SmallTest
@RunWith(AndroidJUnit4ClassRunner::class)
class AuthActivityTest {

    @get: Rule
    val activityTest = ActivityTestRule(AuthActivity::class.java)


    @Rule
    @JvmField
//    val mRuntimePermissionRule = GrantPermissionRule.grant(android.Manifest.permission.READ_EXTERNAL_STORAGE)
    var grantPermissionRule: GrantPermissionRule = GrantPermissionRule.grant(
        "android.permission.CAMERA", "android.permission.READ_EXTERNAL_STORAGE")


    @Before
    fun myTest() {
        Intents.init()
    }

    @After
    fun cleanUp() {
        Intents.release()
    }


    @Test
    fun checkWechatHintText() {
        onView(withId(R.id.wechatEditText))
            .check(matches(withHint("微信ID")))
    }


    @Test
    fun checkRandomWechatText() {
        BuilderScreenObject()
            .typeWechatText(userNameName = "title" + System.currentTimeMillis() + (Random()).nextInt(1000))
    }

    @Test
    fun checkSubmitWechatClickable() {
        BuilderScreenObject()
            .typeWechatText(userNameName = "title" + System.currentTimeMillis() + (Random()).nextInt(1000))
            .checkSubmitButtonAreClickable()
    }

    @Test
    fun checkSubmitButtonLabel() {
        onView(withId(R.id.submitButton))
            .check(matches(withText("获取验证码")))

    }

}