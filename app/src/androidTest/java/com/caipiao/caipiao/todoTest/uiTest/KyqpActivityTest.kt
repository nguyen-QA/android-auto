package com.caipiao.caipiao.todoTest.uiTest

import android.text.format.DateUtils
import androidx.test.espresso.Espresso
import androidx.test.espresso.IdlingPolicies
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.SmallTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.Until
import com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherSecond.waitForElement
import com.caipiao.caipiao.normalTest.customFailureHandler.AllRule
import com.caipiao.caipiao.normalTest.customIdlingResource.ElapsedTimeIdlingResource
import com.caipiao.caipiao.ui.home.HomeActivity
import com.caipiao.caipiao.ui.kyqp.KyqpActivity
import com.caipiao.caipiao.ui.main.MainActivity
import org.hamcrest.Matchers
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.util.concurrent.TimeUnit
import java.util.regex.Pattern


@SmallTest
@RunWith(AndroidJUnit4ClassRunner::class)
class KyqpActivityTest {

    private val instrumentation = InstrumentationRegistry.getInstrumentation()
    private val uiDevice: UiDevice = UiDevice.getInstance(instrumentation)
    private val fourSeconds = 8000L

    @Rule
    @JvmField
    val activityTest = ActivityTestRule(MainActivity::class.java)

    /////ADDING TIME OUT FOR IDLE TEST
    @Before
    fun init() {
        Intents.init()
        //Make sure the app does not Timeout
        IdlingPolicies.setMasterPolicyTimeout(600, TimeUnit.MILLISECONDS)
        IdlingPolicies.setIdlingResourceTimeout(600, TimeUnit.MILLISECONDS)
    }

    @After
    fun cleanUp() {
        Intents.release()
    }

    @Test
    fun check_Webview_Visible() {
        waitForElement(AllRule().waitKyqpButton, 3400)
            .check(matches(Matchers
                .anyOf(withEffectiveVisibility(Visibility
                    .VISIBLE),
                    isClickable(),
                    (isCompletelyDisplayed())
                )))
            .perform(click())

        waitForElement(AllRule().waitKyqpWeb, 35000)
            .check(matches(isCompletelyDisplayed()))

        waitingErrorAlert(DateUtils.SECOND_IN_MILLIS * 30)     //waiting time can be change flexible
    }

    private fun waitingErrorAlert(waitingTime: Long) {
        // Start
        Thread.sleep(500)
        uiDevice.pressBack()
        // Make sure Espresso does not time out
        IdlingPolicies.setMasterPolicyTimeout(waitingTime * 2, TimeUnit.MILLISECONDS)
        IdlingPolicies.setIdlingResourceTimeout(waitingTime * 2, TimeUnit.MILLISECONDS)

        // Now we wait
        val idlingResource = ElapsedTimeIdlingResource(waitingTime)
        IdlingRegistry.getInstance().register(idlingResource)

        // Stop and check Toast Message
//        waitForElement(AllRule().waitBackKeyError, 10000).inRoot(RootMatchers.withDecorView(Matchers.not(activityTest.activity.window.decorView))).check(matches(isCompletelyDisplayed()))
        uiDevice.wait(Until.gone(By.text(Pattern.compile("[a-zA-Z.? ]*"))), fourSeconds)

        // Clean up
        IdlingRegistry.getInstance().unregister(idlingResource)
        uiDevice.pressBack()

    }

    @Test
    fun check_Webview_Loading() {
        waitForElement(AllRule().waitKyqpButton, 4000)
            .check(matches(Matchers
                .anyOf(withEffectiveVisibility(Visibility
                    .VISIBLE),
                    isClickable(),
                    (isCompletelyDisplayed())
                )))
            .perform(click())

        waitForElement(AllRule().waitKyqpWeb, 35000)
//            .check(matches(isJavascriptEnabled()))
            .check(matches(isCompletelyDisplayed()))

        waitingErrorText(DateUtils.SECOND_IN_MILLIS * 40)     //waiting time can be change flexible

    }




    private fun waitingErrorText(waitingTime: Long) {
        // Start
        Thread.sleep(500)
        uiDevice.pressBack()
        // Make sure Espresso does not time out
        IdlingPolicies.setMasterPolicyTimeout(waitingTime * 2, TimeUnit.MILLISECONDS)
        IdlingPolicies.setIdlingResourceTimeout(waitingTime * 2, TimeUnit.MILLISECONDS)

        // Now we wait
        val idlingResource = ElapsedTimeIdlingResource(waitingTime)
        IdlingRegistry.getInstance().register(idlingResource)

        // check Toast message
        uiDevice.wait(Until.gone(By.text(Pattern.compile("[a-zA-Z.? ]*"))), fourSeconds)

        // Clean up
        IdlingRegistry.getInstance().unregister(idlingResource)
        Espresso.pressBack()

    }

}