package com.caipiao.caipiao.todoTest.registerTest

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import com.caipiao.caipiao.R
import com.caipiao.caipiao.normalTest.customAccessibility.TestData
import com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherSecond.waitForElement
import com.caipiao.caipiao.normalTest.customFailureHandler.AllRule
import com.caipiao.caipiao.ui.home.HomeActivity
import com.caipiao.caipiao.ui.verify.AuthVerifyActivity
import org.hamcrest.CoreMatchers.anyOf
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized


/**
 * Parametrized test with multiple parameters.
 * QA has used many sleep() instead of using IdleResource
 */
 @LargeTest
 @RunWith(value = Parameterized::class)
class WechatAndTokenParameterizedTest (
    private val userName: String,
    private val tokenCaptcha: String

) {

        @Rule
        @JvmField
        val activityTest = ActivityTestRule(HomeActivity::class.java)

    @Before
    fun myTest() {
        Intents.init()
    }

    @After
    fun cleanUp() {
        Intents.release()
    }




        //pick random a Social ID and verify
        @Test
        fun checkMultiWechatAndToken() {
            Thread.sleep(6543)
            //Scan QR code image and random enter a social ID
            onView(withId(R.id.qrButton))
                .check(matches(anyOf(withEffectiveVisibility
                    (Visibility.VISIBLE),
                    isClickable(),
                    (isCompletelyDisplayed()))))
                .perform(click())

            Thread.sleep(6543)
            onView(withId(R.id.wechatEditText)).check(matches(isDisplayed()))

            Thread.sleep(6543)
            onView(withId(R.id.wechatEditText))
                .perform(
                    scrollTo(),
                    replaceText(userName),
                    closeSoftKeyboard())

            Thread.sleep(6543)
            onView(withId(R.id.wechatEditText)).check(matches(withText(userName)))

            Thread.sleep(6543)
            onView(withId(R.id.submitButton)).check(matches(isDisplayed()))

            Thread.sleep(6543)
            onView(withId(R.id.submitButton))
                .perform(scrollTo(), click())

            Thread.sleep(6543)
            onView(withText("国金俱乐部会加您的微信，您接受微信邀请后，俱乐部会提供验证码，请稍候"))
                .check(matches(isDisplayed()))

            Thread.sleep(6543)
            onView(withId(R.id.tokenEditText)).check(matches(isDisplayed()))

            // random enter a token captcha
            Thread.sleep(6543)
            onView(withId(R.id.tokenEditText))
                .perform(scrollTo(),
                     replaceText(tokenCaptcha),
                     closeSoftKeyboard())

            Thread.sleep(6543)
            onView(withText(tokenCaptcha))
                .check(matches(isDisplayed()))

            Thread.sleep(6543)
            onView(withId(R.id.submitButton)).check(matches(isDisplayed()))

            Thread.sleep(6543)
            onView(withId(R.id.submitButton))
                .perform(scrollTo(), click())

            Thread.sleep(6543)
            onView(withId(R.id.submitButton)).check(matches(isDisplayed()))

        }

        companion object {
        @JvmStatic
        @Parameterized.Parameters
       fun dataInject() = arrayOf(
            arrayOf(TestData().userName, TestData().tokenCaptcha)
            ,arrayOf(TestData().userName, TestData().tokenCaptcha)
            ,arrayOf(TestData().userName, TestData().tokenCaptcha)
            ,arrayOf(TestData().userName, TestData().tokenCaptcha)

//            arrayOf("item 1", "description 1")
//            ,arrayOf("item 2", "description 2")
//            ,arrayOf("item 3", "description 3")


        )
    }


    }