package com.caipiao.caipiao.todoTest.uiTest

import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.SmallTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.Until
import com.caipiao.caipiao.R
import com.caipiao.caipiao.normalTest.customAction.EspressoTestsMatchers.isDisplayed
import com.caipiao.caipiao.normalTest.customAction.EspressoTestsMatchers.isFocusable
import com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherSecond
import com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherSecond.waitForElement
import com.caipiao.caipiao.normalTest.customFailureHandler.AllRule
import com.caipiao.caipiao.normalTest.customObjectPattern.BuilderScreenObject
import com.caipiao.caipiao.ui.qr.CaptureActivity
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.anyOf
import org.hamcrest.Matchers
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.lang.Thread.sleep

@SmallTest
@RunWith(AndroidJUnit4ClassRunner::class)
class QrAcitvityTest {

    private val instrumentation = InstrumentationRegistry.getInstrumentation()
    private val uiDevice: UiDevice = UiDevice.getInstance(instrumentation)
    private val twoSeconds = 4000L


    @Rule
    @JvmField
    val mActivityTestRule = ActivityTestRule(CaptureActivity::class.java)

    @Rule
    @JvmField
//    val mRuntimePermissionRule = GrantPermissionRule.grant(android.Manifest.permission.READ_EXTERNAL_STORAGE)
    var grantPermissionRule: GrantPermissionRule = GrantPermissionRule.grant(
        "android.permission.CAMERA", "android.permission.READ_EXTERNAL_STORAGE")

    @Before
    fun myTest() {
        Intents.init()
    }

    @After
    fun cleanUp() {
        Intents.release()
    }

    @Test
    fun checkHintTextText() {
        BuilderScreenObject()
            .verifyTheHintNotOverlap()
    }


    //////CHECK CAPTURE SCREEN
    @Test
    //check gallery hint
    fun checkGalleryAHintText() {
        sleep(4321)

        waitForElement(AllRule().hintTextText, 4000)
            .check(matches(isDisplayed())) //Place a barcode inside the viewfinder rectangle to scan it.
        sleep(500)
    }

    @Test
    //check barcode scanning area
    fun checkCodeScanningArea() {
        Thread.sleep(4321)
//        waitForElement(AllRule().waitScanningArea, 4000)
         onView(withId(R.id.zxing_barcode_scanner))
            .check(
                matches(anyOf(
                        withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE),
//                        isFocusable(),              //this view may not be focusable
                        (isCompletelyDisplayed()))))
    }

    @Test
    //check gallery button name
    fun checkGalleryButton() {
        Thread.sleep(4321)
        waitForElement(AllRule().waitGalleryButton, 4000)
            .check(
                matches(
                    Matchers.anyOf(
                        withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE),
                        isClickable(),
                        (isCompletelyDisplayed())
                    )
                )
            )
            .check(matches(withText("或扫描图片")))
    }

    @Test
    fun check_hintText_UiDevice() {
        sleep(4321)
        onView(withId(R.id.galleryButton))
            .check(matches(isDisplayed()))

        // Perform UI Automator actions.
//        uiDevice.openNotification()


        // Click notification by text and wait for application to appear.
        uiDevice.findObject(By.text("Place a barcode inside the viewfinder rectangle to scan it."))
//        uiDevice.findObject(By.text("或扫描图片"))
            .clickAndWait(Until.newWindow(), twoSeconds)

        // Verify application layout with Espresso.
        uiDevice.pressBack()
    }
}


