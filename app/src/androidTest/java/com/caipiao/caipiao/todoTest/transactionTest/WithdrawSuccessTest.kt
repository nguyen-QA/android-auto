package com.caipiao.caipiao.todoTest.transactionTest

import android.content.Intent
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.ViewAssertion
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist

import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.espresso.util.HumanReadables
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.Until
import com.caipiao.caipiao.R
import com.caipiao.caipiao.normalTest.customAction.CustomViewInteraction.checkMatches
import com.caipiao.caipiao.normalTest.customAction.RegexMatcher
import com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherSecond.waitForElement
import com.caipiao.caipiao.normalTest.customFailureHandler.AllRule
import com.caipiao.caipiao.ui.home.HomeActivity
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.*
import org.hamcrest.TypeSafeMatcher
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.lang.Thread.sleep
import java.util.regex.Pattern

@RunWith(AndroidJUnit4ClassRunner::class)
    class WithdrawSuccessTest {

    private val instrumentation = InstrumentationRegistry.getInstrumentation()
    private val uiDevice: UiDevice = UiDevice.getInstance(instrumentation)
    private val fourSeconds = 4000L


    @Rule
    @JvmField
    val activityTest = ActivityTestRule(HomeActivity::class.java)

    @Rule
    @JvmField
//    val mRuntimePermissionRule = GrantPermissionRule.grant(android.Manifest.permission.READ_EXTERNAL_STORAGE)
    var grantPermissionRule: GrantPermissionRule = GrantPermissionRule.grant(
        "android.permission.CAMERA", "android.permission.READ_EXTERNAL_STORAGE")

    @Before
    fun myTest() {
        val firstActivity: IntentsTestRule<HomeActivity> = IntentsTestRule(HomeActivity::class.java)
        firstActivity.launchActivity(Intent())
//        Intents.init()
    }

    @After
    fun cleanUp() {
        Intents.release()
    }

    //check withdraw Button and make request
    @Test
    fun checkWithdrawButtonAndRequest() {
        sleep(4321)
        waitForElement(AllRule().waitExchangeButton, 8000)
            .perform(click())
        waitForElement(AllRule().waitWithdrawButton, 8000)
            .perform(click())
        waitForElement(AllRule().waitCountEditText, 8000)
            .perform(
                replaceText("1"),
                closeSoftKeyboard())
        waitForElement(AllRule().waitSubmitRequestButton, 8000)
            .perform(click())


        //insert the gold amount after you withdraw
        waitForElement(AllRule().waitGoldAmount, 4000)
            .check(matches(withPattern("^[0-9]")))
    }

    private fun withPattern(regex: String): Matcher<in View>? = RegexMatcher(regex)


}
