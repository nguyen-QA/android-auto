package com.caipiao.caipiao.taskTest.nonFunctionalTest;


import android.content.ComponentName;
import android.content.Intent;

import androidx.test.InstrumentationRegistry;
import androidx.test.espresso.intent.ResolvedIntent;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.SmallTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import static androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static androidx.test.espresso.intent.matcher.IntentMatchers.hasFlags;
import static androidx.test.espresso.intent.matcher.IntentMatchers.hasPackage;
import static androidx.test.espresso.intent.matcher.IntentMatchers.isInternal;
import static androidx.test.espresso.intent.matcher.IntentMatchers.toPackage;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.rules.ExpectedException.none;
/**
 * Unit tests for {link IntentMatchers}.
 */
@SmallTest
@RunWith(AndroidJUnit4.class)
public class IntentComponentTest {


    @Rule
    public ExpectedException expectedException = none();

    //Clean up these tests - each matcher should be tested separated.


    @Test
    public void hasComponentTesting() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        String pkg = "com.caipiao.caipiao";
        String cls = pkg + ".HomeActivity";
        ComponentName c = new ComponentName(pkg, cls);
        intent.setComponent(c);
        assertTrue(hasComponent(intent.getComponent().getClassName()).matches(intent));
        assertTrue(hasComponent(intent.getComponent()).matches(intent));
        assertTrue(hasComponent(equalTo(c)).matches(intent));
    }
    @Test
    public void hasComponentDoesNotMatch() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        String pkg = "com.caipiao.caipiao";
        String cls = pkg + ".MygamesActivity";
        ComponentName c = new ComponentName(pkg, cls);
        intent.setComponent(c);
        c = new ComponentName(pkg, pkg + ".HomeAcitivity");
        assertFalse(hasComponent("not_component").matches(intent));
        assertFalse(hasComponent(c).matches(intent));
        assertFalse(hasComponent(equalTo(c)).matches(intent));
    }

    @Test
    public void toPackageTesting() {
        final String pkg = "caipiao";
        ResolvedIntent intent = new FakeResolvedIntent(pkg);
        assertTrue(toPackage(pkg).matches(intent));
        assertFalse(toPackage("notcaipiao").matches(intent));
        expectedException.expect(RuntimeException.class);
        toPackage("whatever").matches(new Intent(Intent.ACTION_VIEW));
    }

    @Test
    public void hasPackageMatches() {
        Intent intent = new Intent().setPackage("com.caipiao.caipiao");
        assertTrue(hasPackage("com.caipiao.caipiao").matches(intent));
        assertTrue(hasPackage(equalTo("com.caipiao.caipiao")).matches(intent));
    }
    @Test
    public void hasPackageDoesNotMatch() {
        Intent intent = new Intent().setPackage("com.caipiao.caipiao");
        assertFalse(hasPackage("com.something").matches(intent));
        assertFalse(hasPackage(equalTo("com.something")).matches(intent));
    }
    @Test
    public void hasPackageNoPackage() {
        Intent intent = new Intent();
        assertFalse(hasPackage("com.caipiao.caipiao").matches(intent));
        assertFalse(hasPackage(equalTo("com.caipiao.caipiao")).matches(intent));
    }
    @Test
    public void hasFlagsWithSingleFlag() {
        Intent intent = new Intent();
        assertTrue(hasFlags(0).matches(intent));
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        assertTrue(hasFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP).matches(intent));
    }
    @Test
    public void hasFlagsWithMultipleFlags() {
        Intent intent = new Intent();
        intent.setFlags(Intent.FLAG_DEBUG_LOG_RESOLUTION | Intent.FLAG_ACTIVITY_NO_HISTORY
                | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        assertTrue(hasFlags(Intent.FLAG_DEBUG_LOG_RESOLUTION).matches(intent));
        assertTrue(hasFlags(Intent.FLAG_ACTIVITY_NO_HISTORY).matches(intent));
        assertTrue(hasFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS).matches(intent));
        assertTrue(hasFlags(Intent.FLAG_DEBUG_LOG_RESOLUTION
                | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS).matches(intent));
        assertTrue(hasFlags(Intent.FLAG_DEBUG_LOG_RESOLUTION | Intent.FLAG_ACTIVITY_NO_HISTORY
                | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS).matches(intent));
    }
    @Test
    public void hasFlagsWithCustomFlags() {
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_DEBUG_LOG_RESOLUTION | 8 | 4 | 2);
        assertTrue((hasFlags(8 | 2)).matches(intent));
        assertTrue((hasFlags(Intent.FLAG_DEBUG_LOG_RESOLUTION | 4)).matches(intent));
        assertTrue((hasFlags(Intent.FLAG_DEBUG_LOG_RESOLUTION, 8, 4)).matches(intent));
    }
    @Test
    public void hasFlagsDoesNotMatch() {
        Intent intent = new Intent();
        assertFalse(hasFlags(Intent.FLAG_DEBUG_LOG_RESOLUTION).matches(intent));
        intent.setFlags(Intent.FLAG_DEBUG_LOG_RESOLUTION | Intent.FLAG_ACTIVITY_NO_HISTORY);
        assertFalse(hasFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS).matches(intent));
        assertFalse(hasFlags(Intent.FLAG_DEBUG_LOG_RESOLUTION
                | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS).matches(intent));
    }
    @Test
    public void hasFlagsWithCustomFlagsDoesNotMatch() {
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_DEBUG_LOG_RESOLUTION | 8);
        assertFalse((hasFlags(16)).matches(intent));
        assertFalse((hasFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | 8)).matches(intent));
    }
    @Test
    public void isInternalTesting() {
        String targetPackage =
                InstrumentationRegistry.getTargetContext().getPackageName();
        ComponentName targetComponent = new ComponentName(targetPackage, targetPackage + ".SomeClass ");
        assertTrue(isInternal().matches(new Intent().setComponent(targetComponent)));
        assertFalse(not(isInternal()).matches(new Intent().setComponent(targetComponent)));
    }
    @Test
    public void isInternalDoesNotMatch() {
        assertFalse(isInternal().matches(new Intent())); // no target package
        ComponentName externalComponent = new
                ComponentName("com.google.android", "com.google.android.SomeClass");
        assertFalse(isInternal().matches(new Intent().setComponent(externalComponent)));
    }
    private class FakeResolvedIntent extends Intent implements ResolvedIntent {
        private final String pkg;
        FakeResolvedIntent(String pkg) {
            this.pkg = pkg;
        }
        @Override
        public boolean canBeHandledBy(String appPackage) {
            return appPackage.equals(pkg);
        }
        @Override
        public Intent getIntent() {
            return this;
        }
    };
}
