package com.caipiao.caipiao.taskTest.nonFunctionalTest

import androidx.test.espresso.action.ViewActions.click
import androidx.test.filters.LargeTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.uiautomator.UiDevice
import com.caipiao.caipiao.normalTest.customAccessibility.TestData
import com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherSecond.waitForElement
import com.caipiao.caipiao.normalTest.customFailureHandler.AllRule
import com.caipiao.caipiao.ui.home.HomeActivity
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.lang.Thread.sleep

/**
 * Demonstrates how to set device in test friendly state to reduce test flakiness.
 */

@LargeTest
@RunWith(AndroidJUnit4ClassRunner::class)
class RootSettingAccesibilityTest {

    @Rule
    @JvmField
    val activityTest = ActivityTestRule(HomeActivity::class.java)

    private var toDoTitle = ""
    private var toDoDescription = ""

    @Before
    fun startTest() {
        AllRule().setUp()
        toDoTitle = TestData.getToDoTitle()
        toDoDescription = TestData.getToDoDescription()
    }

    @Test
    fun checkRootSetting() {
        // Adding new TO-DO.
        sleep(4123)
        waitForElement(AllRule().waitMygamesButton, 4000)
            .perform(click())

    }

    companion object {
        /**
         * Set of shell commands that should be run before test
         * which turn off System animations, increase Accessibility Touch & hold delay
         * and disable Virtual keyboard appearance.
         */
        @BeforeClass
        @JvmStatic
        fun setDevicePreferences() {
            val uiDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())

            // System animation properties.
            uiDevice.executeShellCommand("settings put global animator_duration_scale 0.0")
            uiDevice.executeShellCommand("settings put global transition_animation_scale 0.0")
            uiDevice.executeShellCommand("settings put global window_animation_scale 0.0")

            // Touch & hold delay property.
            uiDevice.executeShellCommand("settings put secure long_press_timeout 1500")

            // Virtual keyboard appearance property.
            uiDevice.executeShellCommand("settings put secure show_ime_with_hard_keyboard 0")
        }
    }


}