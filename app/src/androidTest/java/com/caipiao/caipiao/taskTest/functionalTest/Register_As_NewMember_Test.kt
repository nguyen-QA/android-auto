package com.caipiao.caipiao.taskTest.functionalTest

import android.view.View
import android.view.ViewGroup
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingPolicies
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isCompletelyDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.filters.LargeTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import com.caipiao.caipiao.R
import com.caipiao.caipiao.normalTest.customAccessibility.TestData
import com.caipiao.caipiao.normalTest.customAction.CustomViewInteraction.checkDisplayed
import com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherSecond.waitForElement
import com.caipiao.caipiao.normalTest.customFailureHandler.AllRule
import com.caipiao.caipiao.normalTest.customIdlingResource.ElapsedTimeIdlingResource
import com.caipiao.caipiao.normalTest.customJDBC.QueryDriver
import com.caipiao.caipiao.ui.home.HomeActivity
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher
import org.jetbrains.annotations.NotNull
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*
import java.util.concurrent.TimeUnit


@LargeTest
  @RunWith(AndroidJUnit4ClassRunner::class)
class Register_As_NewMember_Test() : QueryDriver() {

    @Rule
    @JvmField
    var activityTestRule = ActivityTestRule(HomeActivity::class.java)

    @Rule
    @JvmField
    var grantPermissionRule: GrantPermissionRule = GrantPermissionRule.grant(
        "android.permission.CAMERA", "android.permission.READ_EXTERNAL_STORAGE"
    )

    @Before
    fun init() {
           connectServer()
       }

       @After
        fun deinit() {
        disconnectServer()
       }

    @Test
    fun wechatRegister_IdleResource() {

        //Start
        Thread.sleep(4321)
       onView(withId(R.id.qrButton))
            .checkDisplayed()
            .perform(click())

        Thread.sleep(5432)
        //PUT THE IMAGE UNDER THE SCANNER HERE
        val userNameName = "title" + System.currentTimeMillis() + (Random()).nextInt(1000)
        onView(withId(R.id.wechatEditText))
            .perform(
                scrollTo(), replaceText(userNameName),
                closeSoftKeyboard()
            )
        Thread.sleep(3214)
        onView(withId(R.id.submitButton)).perform(scrollTo(), click())
        Thread.sleep(5643)





        // Make sure Espresso does not time out
        val idlingResource = ElapsedTimeIdlingResource(4000)
        val waitingTime = 1000
        IdlingPolicies.setMasterPolicyTimeout((waitingTime * 2).toLong(), TimeUnit.MILLISECONDS)
        IdlingPolicies.setIdlingResourceTimeout((waitingTime * 2).toLong(), TimeUnit.MILLISECONDS)

        // Now we wait
        IdlingRegistry.getInstance().register(idlingResource)

        // Start receive token
        val userId = activityTestRule.activity.viewModel.sessionProvider.getUserId()
//        val userId = Objects.requireNonNull((activityTestRule.activity as HomeActivity).viewModel.sessionProvider.getUserId())
        onView(withId(R.id.tokenEditText))
            .perform(
                scrollTo(), replaceText(receiveToken(userId)),
                closeSoftKeyboard()
            )

        // Clean up
        IdlingRegistry.getInstance().unregister(idlingResource)

        onView(withId(R.id.submitButton)).perform(scrollTo(), click())
        Thread.sleep(1000)

    }

}


