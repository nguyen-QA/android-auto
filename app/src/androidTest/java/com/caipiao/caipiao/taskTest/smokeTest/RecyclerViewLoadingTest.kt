package com.caipiao.caipiao.taskTest.smokeTest

import android.text.format.DateUtils
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingPolicies
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import androidx.test.uiautomator.UiDevice
import com.caipiao.caipiao.R
import com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherSecond.waitForElement
import com.caipiao.caipiao.normalTest.customFailureHandler.AllRule
import com.caipiao.caipiao.normalTest.customIdlingResource.ElapsedTimeIdlingResource
import com.caipiao.caipiao.ui.mygames.MyGamesActivity
import org.hamcrest.Matchers
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.lang.Thread.sleep
import java.util.concurrent.TimeUnit


@LargeTest
@RunWith(AndroidJUnit4ClassRunner::class)
class RecyclerViewLoadingTest {


    @Rule
    @JvmField
    val activityTest = ActivityTestRule(MyGamesActivity::class.java)

    @Rule
    @JvmField
//    val mRuntimePermissionRule = GrantPermissionRule.grant(android.Manifest.permission.READ_EXTERNAL_STORAGE)
    var grantPermissionRule: GrantPermissionRule = GrantPermissionRule.grant(
        "android.permission.CAMERA", "android.permission.READ_EXTERNAL_STORAGE")

    private val instrumentation = InstrumentationRegistry.getInstrumentation()
    private val uiDevice: UiDevice = UiDevice.getInstance(instrumentation)
    val mySGPGameListing = onView(withId(R.id.recyclerView))

    @Before
    fun myTest() {
        Intents.init()
//        val firstActivity: IntentsTestRule<CaptureActivity> = IntentsTestRule(CaptureActivity::class.java)
//        firstActivity.launchActivity(Intent())
    }


    @After
    fun cleanUp() {
        Intents.release()
    }

    /////ADDING TIME OUT FOR IDLE TEST
    @Before
    fun init() {
        //Make sure the app does not Timeout
        IdlingPolicies.setMasterPolicyTimeout(600, TimeUnit.MILLISECONDS)
        IdlingPolicies.setIdlingResourceTimeout(600, TimeUnit.MILLISECONDS)
    }

    @Test
    //game ng_sgp_10013 button
    fun check_firstGame_With_IdlingResource() {
        waitForMySgpGameList(DateUtils.SECOND_IN_MILLIS * 20, 0)
    }


    @Test //game ng_sgp_10014 button
    fun check_secondGame_With_IdlingResource() {
        //Run tasks
        waitForMySgpGameList(DateUtils.SECOND_IN_MILLIS * 20, 1)
    }

    @Test //game ng_sgp_10001 button
    fun check_thirdGame_With_IdlingResource() {
        //Run tasks
        waitForMySgpGameList(DateUtils.SECOND_IN_MILLIS * 20, 2)
    }

    @Test //game ng_sgp_10016 button
    fun check_fourthGame_With_IdlingResource() {
        //Run tasks
        waitForMySgpGameList(DateUtils.SECOND_IN_MILLIS * 20, 3)
    }

    @Test //game ng_sgp_10002 button
    fun check_fifthGame_With_IdlingResource() {
        //Run tasks
        waitForMySgpGameList(DateUtils.SECOND_IN_MILLIS * 20, 4)
    }

    @Test //game ng_sgp_10005 button
    fun check_sixthGame_With_IdlingResource() {
        //Run tasks
        waitForMySgpGameList(DateUtils.SECOND_IN_MILLIS * 20, 5)
    }

    @Test //game ng_sgp_10010 button
    fun check_seventhGame_With_IdlingResource() {
        //Run tasks
        waitForMySgpGameList(DateUtils.SECOND_IN_MILLIS * 20, 6)
    }

    @Test //game ng_sgp_10018 button
    fun check_eighthGame_With_IdlingResource() {
        //Run tasks
        waitForMySgpGameList(DateUtils.SECOND_IN_MILLIS * 20, 7)
    }

    @Test //game ng_sgp_10017 button
    fun check_ninthGame_With_IdlingResource() {
        //Run tasks
        waitForMySgpGameList(DateUtils.SECOND_IN_MILLIS * 20, 8)
    }

    @Test //game ng_sgp_10024 button
    fun check_tenthGame_With_IdlingResource() {
        //Run tasks
        waitForMySgpGameList(DateUtils.SECOND_IN_MILLIS * 20, 9)
    }

    @Test //game ng_sgp_10006 button
    fun check_eleventhGame_With_IdlingResource() {
        //Run tasks
        waitForMySgpGameList(DateUtils.SECOND_IN_MILLIS * 20, 10)
    }

    @Test //game ng_sgp_10007 button
    fun check_twelfthGame_With_IdlingResource() {
        //Run tasks
        waitForMySgpGameList(DateUtils.SECOND_IN_MILLIS * 20, 11)
    }

    @Test //game ng_sgp_10008 button
    fun check_thirteenGame_With_IdlingResource() {
        //Run tasks
        waitForMySgpGameList(DateUtils.SECOND_IN_MILLIS * 20, 12)
    }



    ////////ADDING PRIVATE FUNCTION FOR IDLE RESOURCE in ScrollView
    private fun waitForMySgpGameList(waitingTime: Long, positionButton: Int) {
        // Start
        sleep(4321)
        waitForElement(AllRule().waitRecyclerView, 4000)
           .check(matches(
            Matchers.anyOf(
                withEffectiveVisibility(
                    Visibility.VISIBLE
                ),
                (isCompletelyDisplayed())
            )
        ))

        // Make sure Espresso does not time out
        IdlingPolicies.setMasterPolicyTimeout(waitingTime * 2, TimeUnit.MILLISECONDS)
        IdlingPolicies.setIdlingResourceTimeout(waitingTime * 2, TimeUnit.MILLISECONDS)

        // Now we wait
        val idlingResource = ElapsedTimeIdlingResource(waitingTime)
        IdlingRegistry.getInstance().register(idlingResource)

        // Stop and verify
        mySGPGameListing.check(matches(isDisplayed()))
            .perform(RecyclerViewActions.actionOnItemAtPosition<
                    RecyclerView.ViewHolder>(positionButton, click()))
        Thread.sleep(1000)     //waiting time can be change flexible
        uiDevice.pressBack()
        // Clean up
        IdlingRegistry.getInstance().unregister(idlingResource)
    }

}