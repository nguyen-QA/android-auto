package com.caipiao.caipiao.taskTest.nonFunctionalTest;

import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.junit.Assert.*;


import android.content.ComponentName;
import androidx.test.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.SmallTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import static androidx.test.espresso.intent.matcher.ComponentNameMatchers.hasClassName;
import static androidx.test.espresso.intent.matcher.ComponentNameMatchers.hasMyPackageName;
import static androidx.test.espresso.intent.matcher.ComponentNameMatchers.hasPackageName;
import static androidx.test.espresso.intent.matcher.ComponentNameMatchers.hasShortClassName;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
/**
 * Unit tests for {link ComponentNameMatchers}.
 */
@SmallTest
@RunWith(AndroidJUnit4.class)
public class ComponentNameMatchersTest {


    private static final String PKG = "com.caipiao.caipiao";
    private static final String SHORT_CLASS_NAME = ".CaptureActivity";
    private static final String CLS = PKG + SHORT_CLASS_NAME;
    private static final ComponentName c = new ComponentName(PKG, CLS);
    private static final ComponentName d = new ComponentName(CLS, PKG);
    @Test
    public void checkClassNameTesting() {
        assertTrue(hasClassName(CLS).matches(c));
        assertTrue(hasClassName(equalTo(CLS)).matches(c));
    }
    @Test
    public void checkClassNameDoesNotMatch() {
        assertFalse(hasClassName("not_there").matches(c));
        assertFalse(hasClassName(containsString("not there")).matches(c));
    }
    @Test
    public void checkPackageNameTesting() {
        assertTrue(hasPackageName(PKG).matches(c));
        assertTrue(hasPackageName(equalTo(PKG)).matches(c));
    }
    @Test
    public void checkPackageNameDoesNotMatch() {
        assertFalse(hasPackageName("not_there").matches(c));
        assertFalse(hasPackageName(containsString(SHORT_CLASS_NAME)).matches(c));
    }
    @Test
    public void checkShortClassNameTesting() {
        assertTrue(hasShortClassName(SHORT_CLASS_NAME).matches(c));
        assertTrue(hasShortClassName(equalTo(SHORT_CLASS_NAME)).matches(c));
    }
    @Test
    public void checkShortClassNameDoesNotMatch() {
        assertFalse(hasShortClassName("not_there").matches(c));
        assertFalse(hasShortClassName(equalTo(CLS)).matches(c));
    }
    @Test
    public void checkMyPackageNameTesting() {
        String targetPackage =
                InstrumentationRegistry.getTargetContext().getPackageName();
        ComponentName targetComponent = new ComponentName(targetPackage, targetPackage + ".SomeClass ");
        assertTrue(hasMyPackageName().matches(targetComponent));
    }
    @Test
    public void checkMyPackageNameDoesNotMatch() {
        assertFalse(hasMyPackageName().matches(d));
    }
}