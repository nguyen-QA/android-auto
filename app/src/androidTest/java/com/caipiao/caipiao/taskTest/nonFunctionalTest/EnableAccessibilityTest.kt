package com.caipiao.caipiao.taskTest.nonFunctionalTest

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.rule.GrantPermissionRule
import com.caipiao.caipiao.normalTest.customAccessibility.AccessibilityChecks.enable
import com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherSecond.waitForElement
import com.caipiao.caipiao.normalTest.customFailureHandler.AllRule
import com.caipiao.caipiao.ui.main.MainActivity
import org.junit.BeforeClass
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4ClassRunner::class)
class EnableAccessibilityTest {


    @Rule
    @JvmField
//    val mRuntimePermissionRule = GrantPermissionRule.grant(android.Manifest.permission.READ_EXTERNAL_STORAGE)
    var grantPermissionRule: GrantPermissionRule = GrantPermissionRule.grant(
        "android.permission.CAMERA", "android.permission.READ_EXTERNAL_STORAGE")





    @Test
    fun accessibilityChecks() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
        waitForElement(AllRule().waitQrButton, 4000)
            .perform(click())

            waitForElement(AllRule().hintTextText, 4000)
                .check(matches(isDisplayed()))
        }
    }

    companion object {
        @BeforeClass
        @JvmStatic
        fun setAccessibilityPrefs() {
            enable()
                .setRunChecksFromRootView(true)
                .setThrowExceptionForErrors(false)
        }


    }



}