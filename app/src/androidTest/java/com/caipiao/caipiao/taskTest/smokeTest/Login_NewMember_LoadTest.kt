package com.caipiao.caipiao.taskTest.smokeTest

import android.view.View
import android.view.ViewGroup
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingPolicies
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import com.caipiao.caipiao.R
import com.caipiao.caipiao.normalTest.customAccessibility.TestData
import com.caipiao.caipiao.normalTest.customAction.CustomViewInteraction.checkDisplayed
import com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherSecond.waitForElement
import com.caipiao.caipiao.normalTest.customFailureHandler.AllRule
import com.caipiao.caipiao.normalTest.customIdlingResource.ElapsedTimeIdlingResource
import com.caipiao.caipiao.normalTest.customJDBC.QueryDriver
import com.caipiao.caipiao.ui.home.HomeActivity
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher
import org.jetbrains.annotations.NotNull
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.lang.Thread.sleep
import java.util.*
import java.util.concurrent.TimeUnit


@LargeTest
  @RunWith(AndroidJUnit4ClassRunner::class)
class Login_NewMember_LoadTest() : QueryDriver() {

    @Rule
    @JvmField
    var activityTestRule = ActivityTestRule(HomeActivity::class.java)

    @Rule
    @JvmField
    var grantPermissionRule: GrantPermissionRule = GrantPermissionRule.grant(
        "android.permission.CAMERA", "android.permission.READ_EXTERNAL_STORAGE"
    )

      @NotNull
      private var userId: String? = null

    @Before
    fun init() {
           connectServer()
       }

       @After
        fun deinit() {
        disconnectServer()
       }

    @Test
    fun wechatRegister_IdleResource() {
        val idlingResource = ElapsedTimeIdlingResource(13000)
        val waitingTime = 6000

        //Start
        Thread.sleep(4321)
//        waitForElement(AllRule().waitQrButton, 5000)      //can use either waitForElement() for low device or onView() as you wish, That does not affect the test result
        onView(withId(R.id.qrButton))
            .checkDisplayed()
            .perform(click())

        Thread.sleep(5432)
        //PUT THE IMAGE UNDER THE SCANNER HERE
//        val userNameName = "title" + System.currentTimeMillis() + (Random()).nextInt(1000)
//        waitForElement(AllRule().waitWechatText, 4000)      ////can use either waitForElement() for low device or onView() as you wish, That does not affect the test result
        onView(withId(R.id.wechatEditText))
            .perform(
//                scrollTo(), replaceText(userNameName),
                scrollTo(), replaceText("qatest"),
                closeSoftKeyboard()
            )
        Thread.sleep(3214)
        onView(withId(R.id.submitButton)).perform(scrollTo(), click())
        Thread.sleep(7654)

        // Make sure Espresso does not time out
        IdlingPolicies.setMasterPolicyTimeout((waitingTime * 2).toLong(), TimeUnit.MILLISECONDS)
        IdlingPolicies.setIdlingResourceTimeout((waitingTime * 2).toLong(), TimeUnit.MILLISECONDS)

        // Now we wait
        IdlingRegistry.getInstance().register(idlingResource)

        // Start receive token
        val userId            = activityTestRule.activity.viewModel.sessionProvider.getUserId()
//          userId = Objects.requireNonNull(((HomeActivity) this.activityTestRule.getActivity()).getViewModel().getSessionProvider().getUserId());
//        userId = Objects.requireNonNull((activityTestRule.activity as HomeActivity).viewModel.sessionProvider.getUserId())
        onView(withId(R.id.tokenEditText))
            .perform(
                 scrollTo(), replaceText(receiveToken(userId)),
                closeSoftKeyboard()
            )
        // Clean up
        IdlingRegistry.getInstance().unregister(idlingResource)

        //Break time
        sleep(7654)

        // Make sure Espresso does not time out
        IdlingPolicies.setMasterPolicyTimeout((waitingTime * 2).toLong(), TimeUnit.MILLISECONDS)
        IdlingPolicies.setIdlingResourceTimeout((waitingTime * 2).toLong(), TimeUnit.MILLISECONDS)

        // Now we wait
        IdlingRegistry.getInstance().register(idlingResource)

        // Stop and verify
//        waitForElement(AllRule().waitSubmitButton, 4000)       ////can use either waitForElement() for low device or onView() as you wish, That does not affect the test result
        onView(withId(R.id.submitButton)).perform(scrollTo(), click())
        Thread.sleep(2100)

        // Clean up
        IdlingRegistry.getInstance().unregister(idlingResource)

        Thread.sleep(1000)

    }

}

/*
         fun startActivity(activity:Activity, AuthActivity:Class<*>) {
             val intent = Intent(activity.getApplicationContext(), AuthActivity)
             activity.startActivity(intent)
         }
 */
