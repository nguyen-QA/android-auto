package com.caipiao.caipiao.taskTest.functionalTest

import android.content.Intent
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.ViewAssertion
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist

import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.espresso.util.HumanReadables
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.Until
import com.caipiao.caipiao.R
import com.caipiao.caipiao.normalTest.customAction.RegexMatcher
import com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherSecond.waitForElement
import com.caipiao.caipiao.normalTest.customFailureHandler.AllRule
import com.caipiao.caipiao.ui.home.HomeActivity
import org.hamcrest.CoreMatchers
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.*
import org.hamcrest.TypeSafeMatcher
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.lang.Thread.sleep
import java.util.regex.Pattern

@RunWith(AndroidJUnit4ClassRunner::class)
    class Review_As_Guest_Test {

    private val instrumentation = InstrumentationRegistry.getInstrumentation()
    private val uiDevice: UiDevice = UiDevice.getInstance(instrumentation)
    private val fourSeconds = 4000L


    @Rule
    @JvmField
    val activityTest = ActivityTestRule(HomeActivity::class.java)

    @Rule
    @JvmField
//    val mRuntimePermissionRule = GrantPermissionRule.grant(android.Manifest.permission.READ_EXTERNAL_STORAGE)
    var grantPermissionRule: GrantPermissionRule = GrantPermissionRule.grant(
        "android.permission.CAMERA", "android.permission.READ_EXTERNAL_STORAGE")

    @Before
    fun myTest() {
//        val firstActivity: IntentsTestRule<HomeActivity> = IntentsTestRule(HomeActivity::class.java)
//        firstActivity.launchActivity(Intent())
        Intents.init()
    }

    @After
    fun cleanUp() {
        Intents.release()
    }


////CHECK HOME SCREEM
    @Test
    //check there is Internet Connection
    fun checkNoInternetConnection() {
    sleep(3456)
    uiDevice.wait(Until.gone(By.text(Pattern.compile("[a-zA-Z.? ]*"))), fourSeconds)

    }

    //check gold imageview icon
    @Test
    fun checkHomeGoldIcon() {
        sleep(3456)
        onView(withId(R.id.goldImageView)).check(matches(isDisplayed()))
        sleep(2000)
    }

    //check gold textview
    @Test
    fun checHomeGameGoldAmount() {
        sleep(4321)
        waitForElement(AllRule().waitGoldAmount, 8000)
            .check(matches(
        CoreMatchers.anyOf(
            withPattern("^[0-9]*\$")
                    ,withPattern("^.{1,50}\$")
            ,isCompletelyDisplayed())
        ))
    }
    //check qr button
    @Test
    fun checkQrButton() {
        sleep(3456)
        onView(allOf(withId(R.id.qrButton), childAtPosition(childAtPosition(withId(
            android.R.id.content), 0), 4))).perform(click())
        sleep(2000)
        pressBack()
        sleep(2000)
    }




///////CHECK MYGAME SCROLLVIEW
        //check MYGAME game button
        @Test
        fun checkSGPGameButton() {
    sleep(3456)
        onView(withId(R.id.myGamesButton)).check(matches(isClickable()))
    sleep(2000)
        onView(allOf(withId(R.id.myGamesButton), childAtPosition(childAtPosition(
            withId(android.R.id.content), 0), 6)))
            .perform(click())
    sleep(2000)
        pressBack()
    sleep(2000)
    }
        //check gold Image Icon
        @Test
        fun checkMyGameGoldIcon() {
            sleep(3456)
        onView(allOf(withId(R.id.myGamesButton), childAtPosition(childAtPosition(
            withId(android.R.id.content), 0), 6))).perform(click())
            sleep(2000)
        onView(withId(R.id.goldImageView)).check(matches(isDisplayed()))
        Espresso.pressBack()
            sleep(2000)
    }

        //check gold Image TextView
        @Test
        fun checkMyGameGoldAmount() {
            sleep(3456)
        onView(allOf(withId(R.id.myGamesButton), childAtPosition(childAtPosition(
            withId(android.R.id.content), 0), 6))).perform(click())
            sleep(2000)
        onView(withId(R.id.goldTextView)).check(matches(isDisplayed()))
            sleep(2000)
    }

////////CHECK KYGAME SCREEN
        //check KY game button
        @Test
        fun checkKyGameButton() {
    sleep(3456)
        onView(withId(R.id.kyqpButton)).check(matches(isClickable()))
    sleep(2000)
        onView(allOf(withId(R.id.kyqpButton), childAtPosition(childAtPosition(
            withId(android.R.id.content), 0), 5)))
            .perform(click())
    sleep(2000)
        pressBack()
    sleep(2000)
    }

      private fun childAtPosition(
          parentMatcher: Matcher<View>, position: Int
      ): Matcher<View> {

          return object : TypeSafeMatcher<View>() {
              override fun describeTo(description: Description) {
                  description.appendText("Child at position $position in parent ")
                  parentMatcher.describeTo(description)
              }

              public override fun matchesSafely(view: View): Boolean {
                  val parent = view.parent
                  return parent is ViewGroup && parentMatcher.matches(parent)
                          && view == parent.getChildAt(position)
              }
          }
      }

    private fun withPattern(regex: String): Matcher<in View>? = RegexMatcher(regex)

}
