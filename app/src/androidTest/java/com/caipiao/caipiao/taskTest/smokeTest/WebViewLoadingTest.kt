package com.caipiao.caipiao.taskTest.smokeTest

import android.view.View
import android.view.ViewGroup
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingPolicies
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.Until
import com.caipiao.caipiao.R
import com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherSecond.waitForElement
import com.caipiao.caipiao.normalTest.customFailureHandler.AllRule
import com.caipiao.caipiao.ui.home.HomeActivity
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers
import org.hamcrest.TypeSafeMatcher
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.util.concurrent.TimeUnit
import java.util.regex.Pattern


@LargeTest
@RunWith(AndroidJUnit4ClassRunner::class)
class WebViewLoadingTest {

    private val instrumentation = InstrumentationRegistry.getInstrumentation()
    private val uiDevice: UiDevice = UiDevice.getInstance(instrumentation)
    private val fourSeconds =7000L

    @Rule
    @JvmField
    val activityTest = ActivityTestRule(HomeActivity::class.java)

    /////ADDING TIME OUT FOR IDLE TEST
    @Before
    fun startUp() {
        Intents.init()
    }

    @After
    fun cleanUp() {
        Intents.release()
    }
    //check Webview loading
    @Test
    fun check_WebView_Visible() {
        //start
        Thread.sleep(6543)
        onView(withId(R.id.kyqpButton))
            .check(matches(isClickable()))
                .perform(click())

        waitForElement(AllRule().waitKyqpWeb, 40000)
            .check(matches(Matchers
                .anyOf(withEffectiveVisibility(Visibility
                    .VISIBLE),
                    isClickable(),
                    (isCompletelyDisplayed())
                )))
            .check(matches(isJavascriptEnabled()))

    }

    //check Back key in webview
    @Test
    fun check_Backkey_In_WebView() {

        //start
        Thread.sleep(2000)
        onView(withId(R.id.kyqpButton))
            .check(matches(isClickable()))
            .perform(click())

        waitForElement(AllRule().waitKyqpWeb, 40000)
            .check(matches(isCompletelyDisplayed()))
            .check(matches(isJavascriptEnabled()))

        uiDevice.pressBack()
//        waitForElement(AllRule().waitBackKeyError, 10000).inRoot(RootMatchers.withDecorView(Matchers.not(activityTest.activity.window.decorView))).check(matches(isCompletelyDisplayed()))
      uiDevice.wait(Until.gone(By.text(Pattern.compile("[a-zA-Z.? ]*"))), fourSeconds)

    }


    private fun childAtPosition(
        parentMatcher: Matcher<View>, position: Int
    ): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }

}