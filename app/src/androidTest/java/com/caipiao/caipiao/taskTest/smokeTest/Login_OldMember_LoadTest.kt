package com.caipiao.caipiao.taskTest.smokeTest

import android.view.View
import android.view.ViewGroup
import androidx.test.core.app.ActivityScenario

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingPolicies
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.filters.LargeTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.rule.ActivityTestRule

import androidx.test.rule.GrantPermissionRule
import com.caipiao.caipiao.R
import com.caipiao.caipiao.normalTest.customAccessibility.TestData
import com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherSecond.waitForElement
import com.caipiao.caipiao.normalTest.customIdlingResource.ElapsedTimeIdlingResource
import com.caipiao.caipiao.normalTest.customAction.BaseCustomAction
import com.caipiao.caipiao.normalTest.customAction.CustomViewInteraction.checkDisplayed
import com.caipiao.caipiao.normalTest.customFailureHandler.AllRule
import com.caipiao.caipiao.normalTest.customJDBC.QueryDriverFirstClone
import com.caipiao.caipiao.normalTest.customJDBC.QueryDriverSecondClone
import com.caipiao.caipiao.ui.home.HomeActivity
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.*
import org.hamcrest.TypeSafeMatcher
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.lang.Thread.sleep
import java.util.*
import java.util.concurrent.TimeUnit


@LargeTest
@RunWith(AndroidJUnit4ClassRunner::class)
class Login_OldMember_LoadTest() : QueryDriverFirstClone() {

    @Rule
    @JvmField
    var activityTestRule = ActivityTestRule(HomeActivity::class.java)

    @Rule
    @JvmField
    var grantPermissionRule: GrantPermissionRule = GrantPermissionRule.grant(
        "android.permission.CAMERA", "android.permission.READ_EXTERNAL_STORAGE")




    @Before
        fun init() {
           connectServer()
       }

       @After
        fun deinit() {
        disconnectServer()
       }

   @Test
    fun wechatRegister_IdleResource() {
//       val userNameName = "randomWechatUsername001001001"
       val userNameName = "title" + System.currentTimeMillis() + (Random()).nextInt(1000)
        //Start
        waitForElement(AllRule().waitQrButton, 5000)
            .perform(click())

        Thread.sleep(7000)
        //PUT THE IMAGE UNDER THE SCANNER HERE
        onView(withId(R.id.wechatEditText)).perform(
            scrollTo(),

            replaceText(userNameName),

            closeSoftKeyboard()
        )
        Thread.sleep(4000)
        onView(withId(R.id.submitButton)).perform(scrollTo(), click())
        Thread.sleep(7600)

        //prepare token
        Thread.sleep(4900)

        // Start receive token
 //       waitingForToken(40000)
        Thread.sleep(3100)


       // Start receive token
       waitForElement(AllRule().waitTokenText, 5000)


        onView(withId(R.id.submitButton)).perform(scrollTo(), click())
        Thread.sleep(1000)


    }



    private fun waitingForToken(waitingTime: Long) {


//        onView(withId(R.id.tokenEditText)).perform(scrollTo(), replaceText(token), closeSoftKeyboard())
//        BaseRobot().doOnView(withId(R.id.tokenEditText), scrollTo(), replaceText(receiveToken(userId)), closeSoftKeyboard()

        // Make sure Espresso does not time out
        val idlingResource = ElapsedTimeIdlingResource(waitingTime)
        IdlingPolicies.setMasterPolicyTimeout(waitingTime * 2, TimeUnit.MILLISECONDS)
        IdlingPolicies.setIdlingResourceTimeout(waitingTime * 2, TimeUnit.MILLISECONDS)

        // Now we wait
        IdlingRegistry.getInstance().register(idlingResource)

        // Stop and verify
        val userId            = activityTestRule.activity.viewModel.sessionProvider.getUserId()
        // Start receive token
        waitForElement(AllRule().waitTokenText, 5000)
            .perform(scrollTo(), replaceText(receiveToken(userId)),
                closeSoftKeyboard())

        // Clean up
        IdlingRegistry.getInstance().unregister(idlingResource)

    }



    private fun childAtPosition(
        parentMatcher: Matcher<View>, position: Int
    ): Matcher<View> {

        return object : TypeSafeMatcher<View>(), Matcher<View> {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }




}


