package com.caipiao.caipiao.taskTest.smokeTest

import android.app.Activity
import android.app.Instrumentation
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Parcelable
import android.provider.MediaStore
import android.view.View
import android.widget.ImageView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.Intents.intending
import androidx.test.espresso.intent.matcher.IntentMatchers.hasAction
import androidx.test.espresso.matcher.BoundedMatcher
import androidx.test.filters.SmallTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import androidx.test.uiautomator.UiDevice
import com.caipiao.caipiao.R
import com.caipiao.caipiao.normalTest.customConditionTime.ConditionWatcherSecond.waitForElement
import com.caipiao.caipiao.normalTest.customFailureHandler.AllRule
import com.caipiao.caipiao.ui.home.HomeActivity
import org.hamcrest.Description
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.lang.Thread.sleep

@SmallTest
@RunWith(AndroidJUnit4ClassRunner::class)
class GalleryIntentMatcherTest {

    private val instrumentation = InstrumentationRegistry.getInstrumentation()
    private val uiDevice: UiDevice = UiDevice.getInstance(instrumentation)

    @Rule
    @JvmField
    val mActivityTestRule = ActivityTestRule(HomeActivity::class.java)


    @Rule
    @JvmField
    var grantPermissionRule: GrantPermissionRule = GrantPermissionRule.grant(
        "android.permission.CAMERA", "android.permission.READ_EXTERNAL_STORAGE"
    )




    @Before
    fun myTest() {
        Intents.init()
    }


    @After
    fun cleanUp() {
        Intents.release()
    }

/*
    @Test
    fun checkTestGetImage() {
        sleep(2134)
        waitForElement(AllRule().waitQrButton, 4000).perform(click())
        val expectedIntent: Matcher<Intent> = AllOf.allOf(
            IntentMatchers.hasAction(Intent.ACTION_PICK),
            IntentMatchers.hasData(MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        )
//        Intents.init()
        intending(expectedIntent).respondWith(
            ActivityResult(
                Activity.RESULT_OK,
                getGalleryIntentIntent()
            )
        )
        waitForElement(AllRule().waitGalleryButton, 4000).perform(click())
        sleep(2000)
        Intents.intended(expectedIntent)
//        Intents.release()
        uiDevice.pressBack()
        uiDevice.pressBack()
        uiDevice.pressBack()
    }
*/
    private fun getGalleryIntentIntent(): Intent? {

        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
//        intent.data = Uri.parse("content://media/external/images/media/337663")
        intent.data = Uri.parse("file://storage/emulated/Pictures/Screenshots/5000.png")
        return intent
    }

    private fun getGalleryIntent(): Intent? {
        val bundle = Bundle()
        val parcels: ArrayList<Parcelable> = ArrayList()
        val intent = Intent()
        val uri1 = Uri.parse("file://storage/emulated/Pictures/Screenshots/5000.png")
        val parcelable1: Parcelable = uri1
        parcels.add(parcelable1)
        bundle.putParcelableArrayList(Intent.EXTRA_STREAM, parcels)
        // Create the Intent that will include the bundle.
        intent.putExtras(bundle)
        return intent
    }


    @Test
    fun checkAccessGalleryFunction() {
        sleep(4321)
        waitForElement(AllRule().waitForQrButton, 4000).perform(click())

//        CameraAndGallery.savePickedImage(mActivityTestRule.activity)  //ORIGINAL
 //       val imgGalleryResult = CameraAndGallery.createImageGallerySetResultStub(mActivityTestRule.activity)     //ORIGINAL
//        intending(hasAction(Intent.ACTION_CHOOSER)).respondWith(imgGalleryResult)      //ORIGINAL
//        auctionPhotos_CreationInitialUI()
        waitForElement(AllRule().waitGalleryButton, 4000).perform(click())
        savePickedImage(mActivityTestRule.activity)

        val imgGalleryResult = createImageGallerySetResultStub(mActivityTestRule.activity)

        val imgCaptureResult = createImageCaptureActivityResultStub(mActivityTestRule.activity)
        //pick image from gallery
        intending(hasAction(Intent.ACTION_CHOOSER)).respondWith(imgGalleryResult)

        sleep(2100)

        uiDevice.pressBack()

        intending(hasAction(MediaStore.ACTION_IMAGE_CAPTURE)).respondWith(imgCaptureResult)

        sleep(2000)

        uiDevice.pressBack()


//        onView(withId(R.id.auctionphotos_bigimage_viewer)).check(matches(hasImageSet()))      //ORIGINAL
    }


    private fun createImageCaptureActivityResultStub(activity: Activity): Instrumentation.ActivityResult {
        val bundle = Bundle()
        bundle.putParcelable("IMG_DATA", BitmapFactory.decodeResource(activity.resources, R.mipmap.ic_launcher))
        // Create the Intent that will include the bundle.
        val resultData = Intent()
        resultData.putExtras(bundle)
        // Create the ActivityResult with the Intent.
        return Instrumentation.ActivityResult(Activity.RESULT_OK, resultData)
    }


    private fun createImageGallerySetResultStub(activity: Activity): Instrumentation.ActivityResult {
        val bundle = Bundle()
        val parcels = ArrayList<Parcelable>()
        val resultData = Intent()
        val dir = activity.externalCacheDir
        val file = File(dir?.path, "5000.png")
        val uri = Uri.fromFile(file)
        val parcelable1 = uri as Parcelable
        parcels.add(parcelable1)
        bundle.putParcelableArrayList(Intent.EXTRA_STREAM, parcels)
        resultData.putExtras(bundle)
        return Instrumentation.ActivityResult(Activity.RESULT_OK, resultData)
    }

    private fun hasImageSet(): BoundedMatcher<View, ImageView> {
        return object : BoundedMatcher<View, ImageView>(ImageView::class.java) {
            override fun describeTo(description: Description) {
                description.appendText("has image set.")
            }

            override fun matchesSafely(imageView: ImageView): Boolean {
                return imageView.background != null
            }
        }
    }



    private fun savePickedImage(activity: Activity) {
        val bm = BitmapFactory.decodeResource(activity.resources, R.mipmap.ic_launcher)
        val dir = activity.externalCacheDir
//        val file = File(dir?.path, "pickImageResult.jpeg")
        val file = File(dir?.path, "5000.png")
        val outStream: FileOutputStream?
        try {
            outStream = FileOutputStream(file)
            bm.compress(Bitmap.CompressFormat.JPEG, 100, outStream)
            with(outStream) {
                flush()
                close()
            }
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }





}