package com.caipiao.game.api;

import io.swagger.client.CollectionFormats.*;

import io.reactivex.Observable;
import io.reactivex.Completable;
import retrofit2.http.*;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.MultipartBody;

import com.caipiao.game.model.GetGameUrlResponseData;
import com.caipiao.game.model.GetMyGamesResponseData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface GameServiceApi {
  /**
   * 
   * get game url
   * @param userId  (required)
   * @param cert  (required)
   * @param platType  (required)
   * @param gameType  (required)
   * @param gameCode  (optional)
   * @param lottType  (optional)
   * @param demo  (optional)
   * @return Observable&lt;GetGameUrlResponseData&gt;
   */
  @GET("game/gameurl")
  Observable<GetGameUrlResponseData> getGameUrl(
    @retrofit2.http.Query("userId") String userId, @retrofit2.http.Query("cert") String cert, @retrofit2.http.Query("platType") String platType, @retrofit2.http.Query("gameType") String gameType, @retrofit2.http.Query("gameCode") String gameCode, @retrofit2.http.Query("lottType") Integer lottType, @retrofit2.http.Query("demo") Integer demo
  );

  /**
   * 
   * get my games
   * @param userId  (required)
   * @param cert  (required)
   * @return Observable&lt;GetMyGamesResponseData&gt;
   */
  @GET("game/mygames")
  Observable<GetMyGamesResponseData> getMyGames(
    @retrofit2.http.Query("userId") String userId, @retrofit2.http.Query("cert") String cert
  );

}
