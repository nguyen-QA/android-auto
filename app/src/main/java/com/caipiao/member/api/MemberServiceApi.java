package com.caipiao.member.api;

import io.swagger.client.CollectionFormats.*;

import io.reactivex.Observable;
import io.reactivex.Completable;
import retrofit2.http.*;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.MultipartBody;

import com.caipiao.member.model.ActionTranRequestData;
import com.caipiao.member.model.ActionTranResponseData;
import com.caipiao.member.model.EnrollGuestRequestData;
import com.caipiao.member.model.EnrollGuestResponseData;
import com.caipiao.member.model.EnrollWechatRequestData;
import com.caipiao.member.model.EnrollWechatResponseData;
import com.caipiao.member.model.GetMemberProfileResponseData;
import com.caipiao.member.model.ListTransResponseData;
import com.caipiao.member.model.MemberAuthResponseData;
import com.caipiao.member.model.RegisterWechatRequestData;
import com.caipiao.member.model.RegisterWechatResponseData;
import com.caipiao.member.model.RequestTranRequestData;
import com.caipiao.member.model.RequestTranResponseData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface MemberServiceApi {
  /**
   * 
   * action
   * @param body The request body (required)
   * @return Observable&lt;ActionTranResponseData&gt;
   */
  @POST("member/actiontran")
  Observable<ActionTranResponseData> actionTran(
    @retrofit2.http.Body ActionTranRequestData body
  );

  /**
   * 
   * Enroll a guest and return an enrollment certificate.
   * @param body The request body (required)
   * @return Observable&lt;EnrollGuestResponseData&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("member/enrollguest")
  Observable<EnrollGuestResponseData> enrollGuest(
    @retrofit2.http.Body EnrollGuestRequestData body
  );

  /**
   * 
   * Enroll a new identity and return an enrollment certificate.
   * @param body The request body (required)
   * @return Observable&lt;EnrollWechatResponseData&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("member/enrollwechat")
  Observable<EnrollWechatResponseData> enrollWechat(
    @retrofit2.http.Body EnrollWechatRequestData body
  );

  /**
   * 
   * get member profile
   * @param userId  (required)
   * @param cert  (required)
   * @return Observable&lt;GetMemberProfileResponseData&gt;
   */
  @GET("member/profile")
  Observable<GetMemberProfileResponseData> getMemberProfile(
    @retrofit2.http.Query("userId") String userId, @retrofit2.http.Query("cert") String cert
  );

  /**
   * 
   * get list of transactions
   * @param operatorId  (required)
   * @param type  (required)
   * @param cert  (required)
   * @return Observable&lt;ListTransResponseData&gt;
   */
  @GET("member/listtrans")
  Observable<ListTransResponseData> listTrans(
    @retrofit2.http.Query("operatorId") String operatorId, @retrofit2.http.Query("type") String type, @retrofit2.http.Query("cert") String cert
  );

  /**
   * 
   * auth member
   * @param userId  (required)
   * @param cert  (required)
   * @return Observable&lt;MemberAuthResponseData&gt;
   */
  @GET("member/auth")
  Observable<MemberAuthResponseData> memberAuth(
    @retrofit2.http.Query("userId") String userId, @retrofit2.http.Query("cert") String cert
  );

  /**
   * 
   * Give a phone number and get the token in the SMS
   * @param body The request body (required)
   * @return Observable&lt;RegisterWechatResponseData&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("member/registerwechat")
  Observable<RegisterWechatResponseData> registerWechat(
    @retrofit2.http.Body RegisterWechatRequestData body
  );

  /**
   * 
   * request transaction
   * @param body The request body (required)
   * @return Observable&lt;RequestTranResponseData&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("member/requesttran")
  Observable<RequestTranResponseData> requestTran(
    @retrofit2.http.Body RequestTranRequestData body
  );

}
