/*
 * Affiliate API
 * Affiliate API
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.caipiao.affiliate.model;

import java.util.Objects;
import java.util.Arrays;
import com.caipiao.affiliate.model.ResponseMessageData;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * GetReferTokenResponseData
 */

public class GetReferTokenResponseData {
  @SerializedName("success")
  private Boolean success = null;

  @SerializedName("error")
  private ResponseMessageData error = null;

  @SerializedName("message")
  private ResponseMessageData message = null;

  @SerializedName("token")
  private String token = null;

  @SerializedName("expire")
  private Integer expire = null;

  public GetReferTokenResponseData success(Boolean success) {
    this.success = success;
    return this;
  }

   /**
   * Boolean indicating if the request was successful.
   * @return success
  **/
  @ApiModelProperty(required = true, value = "Boolean indicating if the request was successful.")
  public Boolean isSuccess() {
    return success;
  }

  public void setSuccess(Boolean success) {
    this.success = success;
  }

  public GetReferTokenResponseData error(ResponseMessageData error) {
    this.error = error;
    return this;
  }

   /**
   * error message (i.e. code and string messages).
   * @return error
  **/
  @ApiModelProperty(value = "error message (i.e. code and string messages).")
  public ResponseMessageData getError() {
    return error;
  }

  public void setError(ResponseMessageData error) {
    this.error = error;
  }

  public GetReferTokenResponseData message(ResponseMessageData message) {
    this.message = message;
    return this;
  }

   /**
   * informational messages (i.e. code and string messages).
   * @return message
  **/
  @ApiModelProperty(value = "informational messages (i.e. code and string messages).")
  public ResponseMessageData getMessage() {
    return message;
  }

  public void setMessage(ResponseMessageData message) {
    this.message = message;
  }

  public GetReferTokenResponseData token(String token) {
    this.token = token;
    return this;
  }

   /**
   * The refer token.
   * @return token
  **/
  @ApiModelProperty(value = "The refer token.")
  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public GetReferTokenResponseData expire(Integer expire) {
    this.expire = expire;
    return this;
  }

   /**
   * Get expire
   * @return expire
  **/
  @ApiModelProperty(value = "")
  public Integer getExpire() {
    return expire;
  }

  public void setExpire(Integer expire) {
    this.expire = expire;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GetReferTokenResponseData getReferTokenResponse = (GetReferTokenResponseData) o;
    return Objects.equals(this.success, getReferTokenResponse.success) &&
        Objects.equals(this.error, getReferTokenResponse.error) &&
        Objects.equals(this.message, getReferTokenResponse.message) &&
        Objects.equals(this.token, getReferTokenResponse.token) &&
        Objects.equals(this.expire, getReferTokenResponse.expire);
  }

  @Override
  public int hashCode() {
    return Objects.hash(success, error, message, token, expire);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetReferTokenResponseData {\n");
    
    sb.append("    success: ").append(toIndentedString(success)).append("\n");
    sb.append("    error: ").append(toIndentedString(error)).append("\n");
    sb.append("    message: ").append(toIndentedString(message)).append("\n");
    sb.append("    token: ").append(toIndentedString(token)).append("\n");
    sb.append("    expire: ").append(toIndentedString(expire)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

