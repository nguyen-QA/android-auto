package com.caipiao.affiliate.api;

import io.swagger.client.CollectionFormats.*;

import io.reactivex.Observable;
import io.reactivex.Completable;
import retrofit2.http.*;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.MultipartBody;

import com.caipiao.affiliate.model.GetReferTokenResponseData;
import com.caipiao.affiliate.model.VerifyTokenRequestData;
import com.caipiao.affiliate.model.VerifyTokenResponseData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface AffiliateServiceApi {
  /**
   * 
   * get refer token if not expired. or generate and return new token if expired or force to
   * @param userId  (required)
   * @param cert  (required)
   * @param forceNew  (optional)
   * @return Observable&lt;GetReferTokenResponseData&gt;
   */
  @GET("affiliate/refertoken")
  Observable<GetReferTokenResponseData> getReferToken(
    @retrofit2.http.Query("userId") String userId, @retrofit2.http.Query("cert") String cert, @retrofit2.http.Query("forceNew") Boolean forceNew
  );

  /**
   * 
   * verify invitation token
   * @param body The request body (required)
   * @return Observable&lt;VerifyTokenResponseData&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("affiliate/verifytoken")
  Observable<VerifyTokenResponseData> verifyToken(
    @retrofit2.http.Body VerifyTokenRequestData body
  );

}
