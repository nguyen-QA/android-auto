package com.caipiao.caipiao

import org.spongycastle.jce.ECNamedCurveTable
import org.spongycastle.pkcs.jcajce.JcaPKCS10CertificationRequestBuilder
import org.spongycastle.pkcs.PKCS10CertificationRequest
import org.spongycastle.util.io.pem.PemObject
import org.spongycastle.util.io.pem.PemWriter
import java.io.IOException
import java.io.StringWriter
import org.spongycastle.operator.jcajce.JcaContentSignerBuilder
import org.spongycastle.pkcs.jcajce.JcaPKCS10CertificationRequest
import org.spongycastle.util.encoders.Base64
import toMD5
import java.security.*
import java.security.spec.PKCS8EncodedKeySpec
import java.util.*
import javax.security.auth.x500.X500Principal


object SecurityHelper {

    private val DEFAULT_SIGNATURE_ALGORITHM = "SHA256withECDSA"
    private val CN_PATTERN = "CN=%s, C=US, ST=\"North Carolina\", O=Hyperledger, OU=Fabric"

    fun csrToPEM(csr: PKCS10CertificationRequest): String {
        val pemObject = PemObject("NEW CERTIFICATE REQUEST", csr.encoded)
        val stringWriter = StringWriter()
        val pemWriter = PemWriter(stringWriter)
        pemWriter.writeObject(pemObject)
        pemWriter.close()
        return stringWriter.toString()
    }


    fun generateCsr(keyPair: KeyPair, cn: String): String {
        val principal = String.format(CN_PATTERN, cn)
        val publicKey = keyPair.public
        val privateKey = keyPair.private

        val p10Builder = JcaPKCS10CertificationRequestBuilder(X500Principal(principal), publicKey)
        val csBuilder = JcaContentSignerBuilder(DEFAULT_SIGNATURE_ALGORITHM)
        val signer = csBuilder.build(privateKey)
        val csr = p10Builder.build(signer)

        return csrToPEM(csr)
    }

    fun privateKeyToPem(privateKey: PrivateKey): String {
        val stringWriter = StringWriter()
        val pemWriter = PemWriter(stringWriter)
        val pemObject = PemObject("EC PRIVATE KEY", privateKey.encoded)
        pemWriter.writeObject(pemObject)
        pemWriter.flush()
        pemWriter.close()
        return stringWriter.toString()
    }

    fun publicKeyToPem(publicKey: PublicKey): String {
        val stringWriter = StringWriter()
        val pemWriter = PemWriter(stringWriter)
        val pemObject = PemObject("EC PUBLIC KEY", publicKey.encoded)
        pemWriter.writeObject(pemObject)
        pemWriter.flush()
        pemWriter.close()
        return stringWriter.toString()
    }

    fun pemToPublicKey(pem: String): PublicKey {
        val encoded = Base64.decode(pem)
        val provider = org.spongycastle.jce.provider.BouncyCastleProvider()
        Security.addProvider(provider)
        val kf = KeyFactory.getInstance("ECDSA", provider.name)
        val keySpec = PKCS8EncodedKeySpec(encoded)
        return kf.generatePublic(keySpec)
    }

    fun pemToPrivateKey(pem: String): PrivateKey {
        val encoded = Base64.decode(pem)

        val provider = org.spongycastle.jce.provider.BouncyCastleProvider()
        Security.addProvider(provider)
        val kf = KeyFactory.getInstance("ECDSA", provider.name)
        val keySpec = PKCS8EncodedKeySpec(encoded)
        return kf.generatePrivate(keySpec)
    }

    fun generateKeyPair(): KeyPair {
        val provider = org.spongycastle.jce.provider.BouncyCastleProvider()
        Security.addProvider(provider)
        val ecSpec = ECNamedCurveTable.getParameterSpec("secp256r1")
        val generator = KeyPairGenerator.getInstance("ECDSA", provider.name)
        generator.initialize(ecSpec, SecureRandom())
        return generator.generateKeyPair()
    }

    fun generateDeviceId(): String {
        return Math.random().toString().toMD5()
    }


}