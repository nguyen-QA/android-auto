package com.caipiao.caipiao.model

import io.swagger.client.model.WalletFlowData

class WalletFlow(
    val walletId: String,
    val productId: String,
    val amount: Long,
    val timestamp: Long
) {
}

object WalletFlowFactory {
    fun fromApiWalletFlow(flow: WalletFlowData): WalletFlow {
        return WalletFlow(flow.walletId, flow.productId, flow.amount, flow.timestamp)
    }

    fun zero(): WalletFlow {
        return WalletFlow("", "", 0L, 0L)
    }
}