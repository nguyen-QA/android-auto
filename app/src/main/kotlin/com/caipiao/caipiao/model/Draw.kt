package com.caipiao.caipiao.model

import io.swagger.client.model.DrawData
import java.util.stream.IntStream

class Draw(
    val drawId: String,
    val draw: String,
    val startTime: Long,
    val stopTime: Long,
    val endTime: Long
) {
}

object DrawFactory {
    fun fromApiDraw(draw: DrawData): Draw {
        return Draw(draw.drawId, draw.draw, draw.startTime, draw.stopTime, draw.endTime)
    }

    fun zero(): Draw {
        return Draw("", "", 0L, 0L, 0L)
    }

    fun getApplicableRewards(drawStr: String): Map<String, Int> {
        val result = emptyMap<String, Int>().toMutableMap()

        val draw = Integer.parseInt(drawStr)
        val d = intArrayOf(
            draw % 10,
            draw / 10 % 10,
            draw / 100 % 10,
            draw / 1000 % 10,
            draw / 10000 % 10
        )
        run {
            val win = String.format("%d----", d[4])
            result.put(win, DEFAULT_REWARDS.get(win)!!)
        }
        run {
            val win = String.format("-%d---", d[3])
            result.put(win, DEFAULT_REWARDS.get(win)!!)
        }
        run {
            val win = String.format("--%d--", d[2])
            result.put(win, DEFAULT_REWARDS.get(win)!!)
        }
        run {
            val win = String.format("---%d-", d[1])
            result.put(win, DEFAULT_REWARDS.get(win)!!)
        }
        run {
            val win = String.format("----%d", d[0])
            result.put(win, DEFAULT_REWARDS.get(win)!!)
        }

        run {
            val win = if (d[4] % 2 == 0) "e----" else "o----"
            result.put(win, DEFAULT_REWARDS.get(win)!!)
        }
        run {
            val win = if (d[3] % 2 == 0) "-e---" else "-o---"
            result.put(win, DEFAULT_REWARDS.get(win)!!)
        }
        run {
            val win = if (d[2] % 2 == 0) "--e--" else "--o--"
            result.put(win, DEFAULT_REWARDS.get(win)!!)
        }
        run {
            val win = if (d[1] % 2 == 0) "---e-" else "---o-"
            result.put(win, DEFAULT_REWARDS.get(win)!!)
        }
        run {
            val win = if (d[0] % 2 == 0) "----e" else "----o"
            result.put(win, DEFAULT_REWARDS.get(win)!!)
        }

        run {
            val win = if (d[4] >= 5) "L----" else "S----"
            result.put(win, DEFAULT_REWARDS.get(win)!!)
        }
        run {
            val win = if (d[3] >= 5) "-L---" else "-S---"
            result.put(win, DEFAULT_REWARDS.get(win)!!)
        }
        run {
            val win = if (d[2] >= 5) "--L--" else "--S--"
            result.put(win, DEFAULT_REWARDS.get(win)!!)
        }
        run {
            val win = if (d[1] >= 5) "---L-" else "---S-"
            result.put(win, DEFAULT_REWARDS.get(win)!!)
        }
        run {
            val win = if (d[0] >= 5) "----L" else "----S"
            result.put(win, DEFAULT_REWARDS.get(win)!!)
        }

        val sum = d[0] + d[1] + d[2] + d[3] + d[4]
        run {
            val win = if (sum % 2 == 0) "e" else "o"
            result.put(win, DEFAULT_REWARDS.get(win)!!)
        }
        run {
            val win = if (sum >= 23) "L" else "S"
            result.put(win, DEFAULT_REWARDS.get(win)!!)
        }

        if (d[4] > d[0]) {
            val win = ">"
            result.put(win, DEFAULT_REWARDS.get(win)!!)
        } else if (d[4] < d[0]) {
            val win = "<"
            result.put(win, DEFAULT_REWARDS.get(win)!!)
        } else {
            val win = "="
            result.put(win, DEFAULT_REWARDS.get(win)!!)
        }

        if (d[4] == d[3] && d[3] == d[2]) {
            val win = "===--"
            result.put(win, DEFAULT_REWARDS.get(win)!!)
        } else if (d[4] == d[3] || d[3] == d[2] || d[4] == d[2]) {
            val win = "=+=--"
            result.put(win, DEFAULT_REWARDS.get(win)!!)
        } else {
            val key = setOf(d[4], d[3], d[2])
            if (Sequence.contains(key)) {
                val win = "<<<--"
                result.put(win, DEFAULT_REWARDS.get(win)!!)
            }
        }

        if (d[3] == d[2] && d[2] == d[1]) {
            val win = "-===-"
            result.put(win, DEFAULT_REWARDS.get(win)!!)
        } else if (d[3] == d[2] || d[2] == d[1] || d[3] == d[1]) {
            val win = "-=+=-"
            result.put(win, DEFAULT_REWARDS.get(win)!!)
        } else {
            val key = setOf(d[3], d[2], d[1])
            if (Sequence.contains(key)) {
                val win = "-<<<-"
                result.put(win, DEFAULT_REWARDS.get(win)!!)
            }
        }

        if (d[2] == d[1] && d[1] == d[0]) {
            val win = "--==="
            result.put(win, DEFAULT_REWARDS.get(win)!!)
        } else if (d[2] == d[1] || d[1] == d[0] || d[2] == d[0]) {
            val win = "--=+="
            result.put(win, DEFAULT_REWARDS.get(win)!!)
        } else {
            val key = setOf(d[2], d[1], d[0])
            if (Sequence.contains(key)) {
                val win = "--<<<"
                result.put(win, DEFAULT_REWARDS.get(win)!!)
            }
        }

        return result
    }

}