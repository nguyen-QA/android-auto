package com.caipiao.caipiao.model

class Wallet(
    val walletId: String,
    val balance: Long
) {
    companion object {
        val zero = Wallet("", 0L)
    }
}