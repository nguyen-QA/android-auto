package com.caipiao.caipiao.providers

import com.caipiao.caipiao.model.Bet
import com.caipiao.caipiao.model.Draw
import com.caipiao.caipiao.model.Wallet
import io.reactivex.Completable
import io.reactivex.Single

interface ApiProvider {
    fun getNtp(): Single<Boolean>
    fun getCurrentDraws(): Single<List<Draw>>

    fun buyLottery(
        userId: String,
        name: String,
        bets: String,
        count: Int,
        plan: String): Completable

    fun getFinished(
        userId: String,
        bookmark: String): Single<Pair<List<Bet>, String>>

    fun getUnfinished(
        userId: String): Single<List<Bet>>

    fun getDraws(
        drawIds: List<String>): Single<List<Draw>>

    fun getWallet(
        userId: String): Single<Wallet>

    fun getDrawHistory(name: String, bookmark: String): Single<Pair<List<Draw>, String>>

    fun verifyToken(userId: String, token: String): Single<Boolean>

    fun getReferToken(userId: String, forceNew: Boolean): Single<Pair<String, Int>>

    fun requestTran(userId: String, amount: Long): Single<Boolean>

    fun actionTran(tranId: String, action: String): Single<Boolean>

    fun registerPhone(wechat: String, phone: String): Single<Boolean>

    fun enrollPhone(token: String): Single<Pair<String?, String?>>

    fun registerWechat(wechat: String): Single<Boolean>

    fun enrollWechat(token: String): Single<Pair<String?, String?>>
}