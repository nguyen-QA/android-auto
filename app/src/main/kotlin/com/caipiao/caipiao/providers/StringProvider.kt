package com.caipiao.caipiao.providers

import androidx.annotation.StringRes

interface StringProvider {
    fun get(@StringRes key: Int): String
    fun get(key: String): String
}