package com.caipiao.caipiao.providers.impl

import com.caipiao.caipiao.providers.MemberApiProvider
import com.caipiao.caipiao.providers.SessionProvider
import com.caipiao.game.api.GameServiceApi
import com.caipiao.member.api.MemberServiceApi
import com.caipiao.member.model.*
import io.reactivex.Observable
import io.swagger.client.ApiClient
import javax.inject.Inject

class AppMemberApiProvider  @Inject constructor(
    var sessionProvider: SessionProvider
): MemberApiProvider {
    var api = ApiClient().setBaseUrl(sessionProvider.getBaseUrl()).createDefaultAdapter().createService(MemberServiceApi::class.java)
//    var api = ApiClient().setBaseUrl("http://192.168.1.4:8080/v1").createDefaultAdapter().createService(MemberServiceApi::class.java)


    override fun getMemberProfile() : Observable<GetMemberProfileResponseData> {
        val userId = sessionProvider.getUserId()
        val cert = sessionProvider.getCert()
        return api.getMemberProfile(userId, cert)
            .doOnNext {
                if (!it.isSuccess) {
                    throw RuntimeException(it.error?.message)
                }
            }
    }

    override fun registerWechat(wechat: String): Observable<RegisterWechatResponseData> {
        val userId = sessionProvider.getUserId()
        val cert = sessionProvider.getCert()
        val referToken = sessionProvider.getReferToken()
        return api.registerWechat(
                RegisterWechatRequestData()
                    .userId(userId)
                    .cert(cert)
                    .wechaId(wechat)
                    .refererToken(referToken)
            )
            .doOnNext {
                if (it.isSuccess) {
                    sessionProvider.setWechat(wechat)
                } else {
                    throw RuntimeException(it.error?.message)
                }
            }
    }

    override fun enrollWechat(token: String): Observable<EnrollWechatResponseData> {
        val userId = sessionProvider.getUserId()
        val cert = sessionProvider.getCert()
        val wechat = sessionProvider.getWechat() ?: ""
        return api.enrollWechat(
                EnrollWechatRequestData()
                    .userId(userId)
                    .cert(cert)
                    .wechat(wechat)
                    .token(token))
            .doOnNext {
                if (it.isSuccess) {
                    if (it.userId != null) {
                        sessionProvider.setUserId(it.userId)
                    }
                    if (it.cert != null) {
                        sessionProvider.setMemberCert(it.cert)
                    }
                } else {
                    throw RuntimeException(it.error?.message)
                }
            }
    }

    override fun enrollGuest(deviceId: String): Observable<EnrollGuestResponseData>  {
        return api.enrollGuest(
            EnrollGuestRequestData()
                .deviceId(deviceId)
        )
            .doOnNext {
                if (it.isSuccess) {
                    sessionProvider.setDeviceId(deviceId)
                    if (it.cert != null) {
                        sessionProvider.setMemberCert(it.cert)
                    }
                } else {
                    throw RuntimeException(it.error?.message)
                }
            }
    }

    override fun requestTran(amount: Long): Observable<RequestTranResponseData> {
        val userId = sessionProvider.getUserId()
        val cert = sessionProvider.getCert()
        return api.requestTran(RequestTranRequestData()
                    .userId(userId)
                    .cert(cert)
                    .amount(amount))
            .doOnNext {
                if (!it.isSuccess) {
                    throw RuntimeException(it.error?.message)
                }
            }
    }
}