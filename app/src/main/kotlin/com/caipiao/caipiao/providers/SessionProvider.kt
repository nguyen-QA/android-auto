package com.caipiao.caipiao.providers

import android.content.Context
import com.caipiao.caipiao.model.Draw
import com.caipiao.caipiao.model.Wallet
import io.reactivex.Observable
import java.security.KeyPair
import java.security.PrivateKey
import java.security.PublicKey
import com.caipiao.caipiao.model.Bet

interface SessionProvider {
    enum class HistoryToggle {
        NUMBER,
        SMALL_BIG,
        ODD_EVEN,
        SPECIAL
    }

    fun getPhone(): String?
    fun setPhone(phone: String?)
    fun getWechat(): String?
    fun setWechat(wechat: String?)

    fun hasValidPhone(): Boolean
    fun getValidPhoneTimestamp(): Long

    fun getCert(): String?
    fun getFreeCert(): String?
    fun getMemberCert(): String?
    fun setFreeCert(cert: String?)
    fun setMemberCert(cert: String?)

    fun getDeviceId(): String?
    fun setDeviceId(deviceId: String?)

    fun getUserId(): String?
    fun setUserId(userId: String?)
    fun isMember(): Boolean
    fun getTitleDrawable(): Int

    fun getPrivateKeyPem(): String?
    fun getPublicKeyPem(): String?

    fun getFreePrivateKeyPem(): String?
    fun getFreePublicKeyPem(): String?
    fun getFreePrivateKey(): PrivateKey?
    fun getFreePublicKey(): PublicKey?
    fun getFreeKeyPair(): KeyPair?
    fun setFreeKeyPair(keyPair: KeyPair?)
    fun setFreePrivateKey(privateKey: PrivateKey?)
    fun setFreePrivateKeyPem(pem: String?)
    fun setFreePublicKey(publicKey: PublicKey?)
    fun setFreePublicKeyPem(pem: String?)

    fun getMemberPrivateKeyPem(): String?
    fun getMemberPublicKeyPem(): String?
    fun getMemberPrivateKey(): PrivateKey?
    fun getMemberPublicKey(): PublicKey?
    fun getMemberKeyPair(): KeyPair?
    fun setMemberKeyPair(keyPair: KeyPair?)
    fun setMemberPrivateKey(privateKey: PrivateKey?)
    fun setMemberPrivateKeyPem(pem: String?)
    fun setMemberPublicKey(publicKey: PublicKey?)
    fun setMemberPublicKeyPem(pem: String?)

    fun observeCurrentDraw(): Observable<Draw>
    fun getCurrentDraw(): Draw
    fun getLastDraw(): Draw
    fun setCurrentDraws(draws: List<Draw>)

    fun observeCurrentBets(): Observable<Map<String, Int>>
    fun getCurrentBets(): Map<String, Int>
    fun setCurrentBets(bets: Map<String, Int>)
    fun toggleBet(bet: String)
    fun removeAllBets()

    fun observeCurrentBetCategory(): Observable<String>
    fun getCurrentBetCategory(): String
    fun setCurrentBetCategory(category: String)

    fun observeCurrentTimes(): Observable<String>
    fun getCurrentTimes(): String
    fun setCurrentTimes(times: String)

    fun observeFinished(): Observable<List<Bet>>
    fun getFinished(): List<Bet>
    fun setFinished(flows: List<Bet>)
    fun addFinished(flows: List<Bet>)

    fun observeFinishedDraws(): Observable<Map<String, Draw>>
    fun getFinishedDraws(): Map<String, Draw>
    fun setFinishedDraws(draws: Map<String, Draw>)
    fun addFinishedDraws(draws: Map<String, Draw>)

    fun observeCurrentWallet(): Observable<Wallet>
    fun getCurrentWallet(): Wallet
    fun setCurrentWallet(wallet: Wallet)

    fun diffTimestamp(
        clientSendTimestamp: Long,
        clientReceiveTimestamp: Long,
        serverReceiveTimestamp: Long,
        serverResponseTimestamp: Long
    )

    fun getTimeZoneId(): String
    fun setTimezoneId(timezoneId: String)
    fun timestamp(): Long
    fun localTimestamp(): Long

    fun observeDrawHistory(): Observable<List<Draw>>
    fun getDrawHistory(): List<Draw>
    fun setDrawHistory(draws: List<Draw>)
    fun addDrawHistory(draws: List<Draw>)

    fun observeCurrentHistoryToggle(): Observable<HistoryToggle>
    fun getCurrentHistoryToggle(): HistoryToggle
    fun setCurrentHistoryToggle(toggle: HistoryToggle)

    fun observeUnfinished(): Observable<List<Bet>>
    fun getUnfinished(): List<Bet>
    fun setUnfinished(flows: List<Bet>)
    fun addUnfinished(flows: List<Bet>)

    fun getReferToken(): String?
    fun setReferToken(token: String?)

    fun getBaseUrl(): String
    fun setBaseUrl(baseUrl: String?)
}