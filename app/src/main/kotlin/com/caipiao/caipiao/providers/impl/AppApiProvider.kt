package com.caipiao.caipiao.providers.impl

import com.caipiao.caipiao.SecurityHelper
import com.caipiao.caipiao.model.*
import com.caipiao.caipiao.providers.ApiProvider
import com.caipiao.caipiao.providers.SessionProvider
import io.reactivex.Completable
import io.reactivex.Single
import io.swagger.client.ApiClient
import io.swagger.client.CollectionFormats
import io.swagger.client.api.CaipiaoCaServerApi
import io.swagger.client.model.*
import javax.inject.Inject

class AppApiProvider @Inject constructor(
    var sessionProvider: SessionProvider
): ApiProvider {
    var api = ApiClient().setBaseUrl(sessionProvider.getBaseUrl()).createDefaultAdapter().createService(CaipiaoCaServerApi::class.java)

    override fun getNtp(): Single<Boolean>  {
        val sendTimestamp = sessionProvider.localTimestamp()
        return Single.fromObservable(
            api.getNtp(sessionProvider.getTimeZoneId())
                .doOnNext {
                    val receiveTimestamp = sessionProvider.localTimestamp()
                    sessionProvider.diffTimestamp(sendTimestamp, receiveTimestamp, it.serverReceiveTimestamp, it.serverResponseTimestamp)

                    val serverTimezoneId = it.serverTimezoneId
                    sessionProvider.setTimezoneId(serverTimezoneId)
                }
            .map {
                true
            })
    }

    override fun getCurrentDraws(): Single<List<Draw>> {
        val sendTimestamp = sessionProvider.localTimestamp()
        return Single.fromObservable(
            api.getCurrentDraws()
                .map {
                    if (it.isSuccess) {
                        it.draws.map {
                            DrawFactory.fromApiDraw(it)
                        }
                    } else {
                        throw RuntimeException(it.error.message)
                    }
                }
                .doOnNext {
                    sessionProvider.apply {
                        this.setCurrentDraws(it)
                    }
                })
    }

    override fun buyLottery(
        userId: String,
        name: String,
        bets: String,
        count: Int,
        plan: String): Completable {
        return Completable.fromObservable(
            api.buyLottery(BuyLotteryRequestData()
                .userId(userId)
                .name(name)
                .bets(bets)
                .count(count)
                .plan(plan)
                .cert(sessionProvider.getCert() ?: "")
                .key(sessionProvider.getPrivateKeyPem() ?: ""))
                .map {
                    if (!it.isSuccess) {
                        throw RuntimeException(it.error.message)
                    }
                })

    }

    override fun getFinished(
        userId: String,
        bookmark: String): Single<Pair<List<Bet>, String>> {
        return Single.fromObservable(
            api.getFinished(
                sessionProvider.getPrivateKeyPem()?.replace("\\n", "\n")?.replace("\n", "\\n") ?: "",
                sessionProvider.getCert()?.replace("\\n", "\n")?.replace("\n", "\\n") ?: "",
                userId,
                bookmark
            )
                .map {
                    if (it.isSuccess) {
                        Pair(it.history.map {
                                BetFactory.fromApiBet(it)
                            }, it.bookmark)
                    } else {
                        throw RuntimeException(it.error.message)
                    }
                })
    }

    override fun getUnfinished(
        userId: String): Single<List<Bet>> {
        return Single.fromObservable(
            api.getUnfinished(
                sessionProvider.getPrivateKeyPem()?.replace("\\n", "\n")?.replace("\n", "\\n") ?: "",
                sessionProvider.getCert()?.replace("\\n", "\n")?.replace("\n", "\\n") ?: "",
                userId
            )
                .map {
                    if (it.isSuccess) {
                        it.history.map {
                            BetFactory.fromApiBet(it)
                        }
                    } else {
                        throw RuntimeException(it.error.message)
                    }
                })
    }

    override fun getDraws(
        drawIds: List<String>): Single<List<Draw>> {
        return Single.fromObservable(
            api.getDraws(
                CollectionFormats.CSVParams(drawIds)
            )
                .map {
                    if (it.isSuccess) {
                        it.getDraws()?.map {
                            DrawFactory.fromApiDraw(it)
                        }
                    } else {
                        throw RuntimeException(it.error.message)
                    }
                })
    }

    override fun getWallet(
        userId: String): Single<Wallet> {
        return Single.fromObservable(
            api.getWallet(
                sessionProvider.getPrivateKeyPem()?.replace("\\n", "\n")?.replace("\n", "\\n") ?: "",
                sessionProvider.getCert()?.replace("\\n", "\n")?.replace("\n", "\\n") ?: "",
                userId
            )
                .map {
                    if (it.isSuccess) {
                        Wallet(userId, it.balance)
                    } else {
                        throw RuntimeException(it.error.message)
                    }
                })
    }

    override fun getDrawHistory(name: String, bookmark: String): Single<Pair<List<Draw>, String>> {
        return Single.fromObservable(
            api.getDrawHistory(
                sessionProvider.getPrivateKeyPem()?.replace("\\n", "\n")?.replace("\n", "\\n") ?: "",
                sessionProvider.getCert()?.replace("\\n", "\n")?.replace("\n", "\\n") ?: "",
                name,
                bookmark
            )
                .map {
                    if (it.isSuccess) {
                        Pair(it.history.map {
                                DrawFactory.fromApiDraw(it)
                            }, it.bookmark)
                    } else {
                        throw RuntimeException(it.error.message)
                    }
                })
    }

    override fun verifyToken(userId: String, token: String): Single<Boolean> {
        return Single.fromObservable(
            api.verifyToken(
                sessionProvider.getPrivateKeyPem()?.replace("\\n", "\n")?.replace("\n", "\\n") ?: "",
                sessionProvider.getCert()?.replace("\\n", "\n")?.replace("\n", "\\n") ?: "",
                userId,
                token)
                .map {
                    it.isSuccess
                }
                .doOnNext {
                    sessionProvider.apply {
                        this.setReferToken(token)
                    }
                }
        )
    }

    override fun getReferToken(userId: String, forceNew: Boolean): Single<Pair<String, Int>> {
        return Single.fromObservable(
            api.getReferToken(sessionProvider.getPrivateKeyPem()?.replace("\\n", "\n")?.replace("\n", "\\n") ?: "",
                sessionProvider.getCert()?.replace("\\n", "\n")?.replace("\n", "\\n") ?: "",
                userId,
                forceNew)
                .map {
                    if (it.isSuccess) {
                        Pair(it.token,
                            it.expire)
                    } else {
                        throw RuntimeException(it.error.message)
                    }
                }
        )
    }

    override fun requestTran(userId: String, amount: Long): Single<Boolean> {
        return Single.fromObservable(
            api.requestTran(RequestTranRequestData()
                    .key(sessionProvider.getPrivateKeyPem()?.replace("\\n", "\n")?.replace("\n", "\\n") ?: "")
                .cert(sessionProvider.getCert()?.replace("\\n", "\n")?.replace("\n", "\\n") ?: "")
                .userId(userId)
                .amount(amount))
                .map {
                    if (it.isSuccess) {
                        true
                    } else {
                        throw RuntimeException(it.error.message)
                    }
                })
    }

    override fun actionTran(tranId: String, action: String): Single<Boolean> {
        return Single.fromObservable(
            api.actionTran(ActionTranRequestData()
                .key(sessionProvider.getPrivateKeyPem()?.replace("\\n", "\n")?.replace("\n", "\\n") ?: "")
                .cert(sessionProvider.getCert()?.replace("\\n", "\n")?.replace("\n", "\\n") ?: "")
                .tranId(tranId)
                .action(action))
                .map {
                    it.isSuccess
                })
    }

    override fun registerPhone(wechat: String, phone: String): Single<Boolean> {
        val referToken = sessionProvider.getReferToken()
        return Single.fromObservable(api.registerPhone(
            RegisterPhoneRequestData()
                .country("86")
                .phone(phone)
                .wechaId(wechat)
                .refererToken(referToken))
            .doOnNext {
                if (it.isSuccess) {
                    sessionProvider.setPhone(phone)
                }
            }
            .map {
                it.isSuccess
            })
    }

    override fun enrollPhone(token: String): Single<Pair<String?, String?>> {
        val phone = sessionProvider.getPhone() ?: ""
        val keyPair = SecurityHelper.generateKeyPair()
        sessionProvider.setMemberKeyPair(keyPair)
        val csr = SecurityHelper.generateCsr(keyPair, "86-" + phone)
        return Single.fromObservable(api.enrollPhone(
            EnrollPhoneRequestData()
                .country("86")
                .phone(phone)
                .token(token)
                .key(sessionProvider.getPrivateKeyPem()?.replace("\\n", "\n")?.replace("\n", "\\n") ?: "")
                .csr(csr))
            .doOnNext {
                if (it.isSuccess) {
                    if (it.cert != null) {
                        sessionProvider.setMemberCert(it.cert)
                    }
                    if (it.memberUrl != null) {
                        sessionProvider.setBaseUrl(it.memberUrl)
                        api = ApiClient().setBaseUrl(sessionProvider.getBaseUrl()).createDefaultAdapter().createService(CaipiaoCaServerApi::class.java)
                    }
                    sessionProvider.setUserId(it.userId)
                }
            }
            .map {
                if (it.isSuccess) Pair(it.cert, it.memberUrl) else Pair(null, null)
            })
    }

    override fun registerWechat(wechat: String): Single<Boolean> {
        val referToken = sessionProvider.getReferToken()
        return Single.fromObservable(api.registerWechat(
                RegisterWechatRequestData()
                    .wechaId(wechat)
                    .refererToken(referToken))
            .doOnNext {
                if (it.isSuccess) {
                    sessionProvider.setWechat(wechat)
                }
            }
            .map {
                it.isSuccess
            })
    }

    override fun enrollWechat(token: String): Single<Pair<String?, String?>> {
        val wechat = sessionProvider.getWechat() ?: ""
        val keyPair = SecurityHelper.generateKeyPair()
        sessionProvider.setMemberKeyPair(keyPair)
        val csr = SecurityHelper.generateCsr(keyPair, wechat)
        return Single.fromObservable(api.enrollWechat(
                EnrollWechatRequestData()
                    .wechat(wechat)
                    .token(token)
                    .key(sessionProvider.getPrivateKeyPem()?.replace("\\n", "\n")?.replace("\n", "\\n") ?: "")
                    .csr(csr))
            .doOnNext {
                if (it.isSuccess) {
                    if (it.cert != null) {
                        sessionProvider.setMemberCert(it.cert)
                    }
                    if (it.memberUrl != null) {
                        sessionProvider.setBaseUrl(it.memberUrl)
                        api = ApiClient().setBaseUrl(sessionProvider.getBaseUrl()).createDefaultAdapter().createService(CaipiaoCaServerApi::class.java)
                    }
                    sessionProvider.setUserId(it.userId)
                }
            }
            .map {
                if (it.isSuccess) Pair(it.cert, it.memberUrl) else Pair(null, null)
            })
    }


}
