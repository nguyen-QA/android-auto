package com.caipiao.caipiao.providers.impl

import com.caipiao.affiliate.api.AffiliateServiceApi
import com.caipiao.affiliate.model.GetReferTokenResponseData
import com.caipiao.affiliate.model.VerifyTokenRequestData
import com.caipiao.affiliate.model.VerifyTokenResponseData
import com.caipiao.caipiao.providers.AffiliateApiProvider
import com.caipiao.caipiao.providers.SessionProvider
import io.reactivex.Observable
import io.swagger.client.ApiClient
import javax.inject.Inject

class AppAffiliateApiProvider  @Inject constructor(
    var sessionProvider: SessionProvider
): AffiliateApiProvider {
    var api = ApiClient().setBaseUrl(sessionProvider.getBaseUrl()).createDefaultAdapter().createService(AffiliateServiceApi::class.java)
//    var api = ApiClient().setBaseUrl("http://192.168.1.4:8089/v1").createDefaultAdapter().createService(AffiliateServiceApi::class.java)

    override fun getReferToken(): Observable<GetReferTokenResponseData> {
        val userId = sessionProvider.getUserId()
        val cert = sessionProvider.getCert()
        return api.getReferToken(
                userId,
                cert,
                false
            )
            .doOnNext {
                if (it.isSuccess) {
                    if (it.token != null) {
                        sessionProvider.setReferToken(it.token)
                    }
                } else {
                    throw RuntimeException(it.error?.message)
                }
            }
    }

    override fun verifyToken(token: String) : Observable<VerifyTokenResponseData> {
        val userId = sessionProvider.getUserId()
        val cert = sessionProvider.getCert()
        return api.verifyToken(VerifyTokenRequestData().userId(userId).cert(cert).token(token))
            .doOnNext {
                if (it.isSuccess) {
                    sessionProvider.setReferToken(token)
                } else {
                    throw RuntimeException(it.error?.message)
                }
            }
    }

}