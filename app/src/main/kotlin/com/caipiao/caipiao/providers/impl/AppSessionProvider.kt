package com.caipiao.caipiao.providers.impl

import android.content.Context
import com.caipiao.caipiao.R
import com.caipiao.caipiao.SecurityHelper
import com.caipiao.caipiao.model.*
import com.caipiao.caipiao.providers.SessionProvider
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import org.joda.time.DateTimeZone
import org.joda.time.LocalDateTime
import javax.inject.Inject
import java.security.KeyPair
import java.security.PrivateKey
import java.security.PublicKey
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class AppSessionProvider @Inject constructor(private val context: Context): SessionProvider {
    private val APP_PREFERENCES = "APP_PREFERENCES"
    private val AUTH_PHONE = "AUTH_PHONE"
    private val AUTH_WECHAT = "AUTH_WECHAT"
    private val AUTH_TIMESTAMP = "AUTH_TIMESTAMP"
    private val AUTH_CERT = "AUTH_CERT"
    private val AUTH_MEMBER_CERT = "AUTH_MEMBER_CERT"
    private val AUTH_DEVICE_ID = "AUTH_DEVICE_ID"
    private val AUTH_USER_ID = "AUTH_USER_ID"
    private val AUTH_PRIVATE_KEY = "AUTH_PRIVATE_KEY"
    private val AUTH_PUBLIC_KEY = "AUTH_PUBLIC_KEY"
    private val AUTH_MEMBER_PRIVATE_KEY = "AUTH_MEMBER_PRIVATE_KEY"
    private val AUTH_MEMBER_PUBLIC_KEY = "AUTH_MEMBER_PUBLIC_KEY"
    private val BASE_URL = "BASE_URL"
    private val EXPIRE_TIME = 60


    override fun getPhone(): String? {
        return context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE).getString(AUTH_PHONE, null)
    }

    override fun setPhone(phone: String?) {
        val currentTimestamp = System.currentTimeMillis() / 1000
        context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE).edit()
            .putString(AUTH_PHONE, phone)
            .putLong(AUTH_TIMESTAMP, currentTimestamp)
            .commit()
    }

    override fun getWechat(): String? {
        return context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE).getString(AUTH_WECHAT, null)
    }

    override fun setWechat(wechat: String?) {
        val currentTimestamp = System.currentTimeMillis() / 1000
        context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE).edit()
            .putString(AUTH_WECHAT, wechat)
            .commit()
    }

    override fun hasValidPhone(): Boolean {
        val timestamp = getValidPhoneTimestamp()
        val currentTimestamp = timestamp()
        return currentTimestamp - timestamp > EXPIRE_TIME
    }

    override fun getValidPhoneTimestamp(): Long {
        return context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE).getLong(AUTH_TIMESTAMP, 0)
    }

    override fun getCert(): String? {
        return if (getMemberCert() != null) getMemberCert() else getFreeCert()
    }

    override fun getPrivateKeyPem(): String? {
        return if (getMemberCert() != null) getMemberPrivateKeyPem() else getFreePrivateKeyPem()
    }

    override fun getPublicKeyPem(): String? {
        return if (getMemberCert() != null) getMemberPublicKeyPem() else getFreePublicKeyPem()
    }

    override fun getFreeCert(): String? {
        return context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE).getString(AUTH_CERT, null)
    }

    override fun getFreePrivateKeyPem(): String? {
        return context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE).getString(AUTH_PRIVATE_KEY, null)
    }

    override fun getFreePublicKeyPem(): String? {
        return context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE).getString(AUTH_PUBLIC_KEY, null)
    }

    override fun getFreePrivateKey(): PrivateKey? {
        val pem = getFreePrivateKeyPem()
        return if (pem == null) null else SecurityHelper.pemToPrivateKey(pem)
    }

    override fun getFreePublicKey(): PublicKey? {
        val pem = getFreePublicKeyPem()
        return if (pem == null) null else SecurityHelper.pemToPublicKey(pem)
    }

    override fun getFreeKeyPair(): KeyPair? {
        return KeyPair(getFreePublicKey(), getFreePrivateKey())
    }

    override fun getMemberCert(): String? {
        return context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE).getString(AUTH_MEMBER_CERT, null)
    }

    override fun getMemberPrivateKeyPem(): String? {
        return context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE).getString(AUTH_MEMBER_PRIVATE_KEY, null)
    }

    override fun getMemberPublicKeyPem(): String? {
        return context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE).getString(AUTH_MEMBER_PUBLIC_KEY, null)
    }

    override fun getMemberPrivateKey(): PrivateKey? {
        val pem = getMemberPrivateKeyPem()
        return if (pem == null) null else SecurityHelper.pemToPrivateKey(pem)
    }

    override fun getMemberPublicKey(): PublicKey? {
        val pem = getMemberPublicKeyPem()
        return if (pem == null) null else SecurityHelper.pemToPublicKey(pem)
    }

    override fun getMemberKeyPair(): KeyPair? {
        return KeyPair(getMemberPublicKey(), getMemberPrivateKey())
    }

    override fun setFreeCert(cert: String?) {
        context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE).edit()
            .putString(AUTH_CERT, cert)
            .commit()
    }

    override fun setFreeKeyPair(keyPair: KeyPair?) {
        setFreePrivateKey(keyPair?.private)
        setFreePublicKey(keyPair?.public)
    }


    override fun setFreePrivateKey(privateKey: PrivateKey?) {
        if (privateKey == null) {
            setFreePrivateKeyPem(null)
        } else {
            val pem = SecurityHelper.privateKeyToPem(privateKey)
            setFreePrivateKeyPem(pem)
        }
    }

    override fun setFreePrivateKeyPem(pem: String?) {
        context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE).edit()
            .putString(AUTH_PRIVATE_KEY, pem)
            .commit()
    }

    override fun setFreePublicKey(publicKey: PublicKey?) {
        if (publicKey == null) {
            setFreePublicKey(null)
        } else {
            val pem = SecurityHelper.publicKeyToPem(publicKey)
            setFreePublicKeyPem(pem)
        }
    }

    override fun setFreePublicKeyPem(pem: String?) {
        context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE).edit()
            .putString(AUTH_PUBLIC_KEY, pem)
            .commit()
    }

    override fun setMemberCert(cert: String?) {
        context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE).edit()
            .putString(AUTH_MEMBER_CERT, cert)
            .commit()
    }

    override fun setMemberKeyPair(keyPair: KeyPair?) {
        setMemberPrivateKey(keyPair?.private)
        setMemberPublicKey(keyPair?.public)
    }

    override fun setMemberPrivateKey(privateKey: PrivateKey?) {
        if (privateKey == null) {
            setMemberPrivateKeyPem(null)
        } else {
            val pem = SecurityHelper.privateKeyToPem(privateKey)
            setMemberPrivateKeyPem(pem)
        }
    }

    override fun setMemberPrivateKeyPem(pem: String?) {
        context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE).edit()
            .putString(AUTH_MEMBER_PRIVATE_KEY, pem)
            .commit()
    }

    override fun setMemberPublicKey(publicKey: PublicKey?) {
        if (publicKey == null) {
            setMemberPublicKey(null)
        } else {
            val pem = SecurityHelper.publicKeyToPem(publicKey)
            setMemberPublicKeyPem(pem)
        }
    }

    override fun setMemberPublicKeyPem(pem: String?) {
        context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE).edit()
            .putString(AUTH_MEMBER_PUBLIC_KEY, pem)
            .commit()
    }

    override fun getDeviceId(): String? {
        return context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE).getString(AUTH_DEVICE_ID, null)
    }

    override fun setDeviceId(deviceId: String?) {
        context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE).edit()
            .putString(AUTH_DEVICE_ID, deviceId)
            .commit()
    }

    override fun getUserId(): String? {
        return context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE).getString(AUTH_USER_ID, getDeviceId()!!.substring(0, 13))
    }

    override fun setUserId(userId: String?) {
        context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE).edit()
            .putString(AUTH_USER_ID, userId)
            .commit()
    }

    override fun isMember(): Boolean {
        return context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE).getString(AUTH_USER_ID, null) != null
    }

    override fun getTitleDrawable(): Int {
        return if (isMember()) R.drawable.club else R.drawable.appname
    }

    private val currentDraws: BehaviorSubject<List<Draw>> = BehaviorSubject.createDefault(listOf(DrawFactory.zero()))

    override fun observeCurrentDraw(): Observable<Draw> {
        return currentDraws.
                switchMap { draws ->
                    val timestamp = timestamp()

                    Observable.fromIterable(draws)
                        .flatMap { draw ->
                            if (draw.startTime <= timestamp && draw.endTime > timestamp) {
                                Observable.just<Draw>(draw)
                            } else if (draw.startTime > timestamp) {
                                Observable.just<Draw>(draw)
                                    .delay(draw.startTime - timestamp, TimeUnit.SECONDS)
                            } else {
                                Observable.empty()
                            }
                        }

                }.distinctUntilChanged()
    }

    override fun getCurrentDraw(): Draw {
        for (draw in currentDraws.value ?: listOf()) {
            val timestamp = timestamp()
            if (draw.startTime <= timestamp && draw.endTime > timestamp) {
                return draw
            }
        }
        return DrawFactory.zero()
    }

    override fun getLastDraw(): Draw {
        return (currentDraws.value ?: listOf(DrawFactory.zero())).last()
    }

    override fun setCurrentDraws(draws: List<Draw>) {
        currentDraws.onNext(draws)
    }

    private val currentBets: BehaviorSubject<Map<String, Int>> = BehaviorSubject.createDefault(HashMap())

    override fun observeCurrentBets(): Observable<Map<String, Int>> {
        return currentBets
    }

    override fun getCurrentBets(): Map<String, Int> {
        return currentBets.value ?: HashMap()
    }

    override fun setCurrentBets(bets: Map<String, Int>) {
        currentBets.onNext(bets)
    }

    override fun toggleBet(bet: String) {
        val bets = (currentBets.value ?: HashMap()).toMutableMap()
        if (bets.containsKey(bet)) {
            bets.remove(bet)
        } else {
            bets[bet] = DEFAULT_REWARDS[bet]!!
        }

        currentBets.onNext(bets)
    }

    override fun removeAllBets() {
        currentBets.onNext(HashMap())
    }

    private val currentBetCategory: BehaviorSubject<String> = BehaviorSubject.createDefault("1")

    override fun observeCurrentBetCategory(): Observable<String> {
        return currentBetCategory
    }

    override fun getCurrentBetCategory(): String {
        return currentBetCategory.value ?: "1"
    }

    override fun setCurrentBetCategory(category: String) {
        currentBetCategory.onNext(category)
    }

    private val currentTimes: BehaviorSubject<String> = BehaviorSubject.createDefault("10")

    override fun observeCurrentTimes(): Observable<String> {
        return currentTimes
    }

    override fun getCurrentTimes(): String {
        return currentTimes.value ?: "10"
    }

    override fun setCurrentTimes(times: String) {
        currentTimes.onNext(times)
    }

    private val finished: BehaviorSubject<List<Bet>> = BehaviorSubject.createDefault(ArrayList())

    override fun observeFinished(): Observable<List<Bet>> {
        return finished
    }

    override fun getFinished(): List<Bet> {
        return finished.value ?: ArrayList()
    }

    override fun setFinished(bets: List<Bet>) {
        finished.onNext(bets)
    }

    override fun addFinished(bets: List<Bet>) {
        var append: MutableList<Bet> = ArrayList<Bet>(finished.value ?: ArrayList())
        val productIds = append.map {
            it.bets
        }
        for (bet in bets) {
            if (!productIds.contains(bet.bets)) {
                append.add(bet)
            }
        }
        finished.onNext(append)
    }

    private val finishedDraws: BehaviorSubject<Map<String, Draw>> = BehaviorSubject.createDefault(HashMap())

    override fun observeFinishedDraws(): Observable<Map<String, Draw>> {
        return finishedDraws
    }

    override fun getFinishedDraws(): Map<String, Draw> {
        return finishedDraws.value ?: HashMap()
    }

    override fun setFinishedDraws(draws: Map<String, Draw>) {
        finishedDraws.onNext(draws)
    }

    override fun addFinishedDraws(draws: Map<String, Draw>) {
        var map = (finishedDraws.value ?: HashMap()).toMutableMap()
        map.putAll(draws)
        finishedDraws.onNext(map)
    }

    private val currentWallet: BehaviorSubject<Wallet> = BehaviorSubject.createDefault(Wallet.zero)

    override fun observeCurrentWallet(): Observable<Wallet> {
        return currentWallet
    }

    override fun getCurrentWallet(): Wallet {
        return currentWallet.value ?: Wallet.zero
    }

    override fun setCurrentWallet(wallet: Wallet) {
        currentWallet.onNext(wallet)
    }

    private var diffTimestamp: Long = 0

    override fun diffTimestamp(clientSendTimestamp: Long, clientReceiveTimestamp: Long, serverReceiveTimestamp: Long, serverResponseTimestamp: Long) {
        diffTimestamp = (clientReceiveTimestamp + clientSendTimestamp - serverReceiveTimestamp - serverResponseTimestamp) / 2
    }

    private var timeZoneId: String = "Asia/Shanghai"
    override fun getTimeZoneId(): String {
        return timeZoneId
    }
    override fun setTimezoneId(timezoneId: String) {
        this.timeZoneId = timezoneId;
    }

    override fun timestamp(): Long {
        return localTimestamp() - diffTimestamp
    }

    override fun localTimestamp(): Long {
        val localDateTime = LocalDateTime.now(DateTimeZone.forID(timeZoneId))
        val dateTime = localDateTime.toDateTime(DateTimeZone.UTC)
        return dateTime.millis / 1000
    }

    private val drawHistory: BehaviorSubject<List<Draw>> = BehaviorSubject.createDefault(ArrayList())

    override fun observeDrawHistory(): Observable<List<Draw>> {
        return drawHistory
    }

    override fun getDrawHistory(): List<Draw> {
        return drawHistory.value ?: ArrayList()
    }

    override fun setDrawHistory(draws: List<Draw>) {
        drawHistory.onNext(draws)
    }

    override fun addDrawHistory(draws: List<Draw>) {
        var append: MutableList<Draw> = ArrayList<Draw>(drawHistory.value ?: ArrayList())
        val drawIds = append.map {
            it.drawId
        }
        for (result in draws) {
            if (!drawIds.contains(result.drawId)) {
                append.add(result)
            }
        }
        drawHistory.onNext(append)
    }

    private val currentHistoryToggle: BehaviorSubject<SessionProvider.HistoryToggle> = BehaviorSubject.createDefault(SessionProvider.HistoryToggle.NUMBER)

    override fun observeCurrentHistoryToggle(): Observable<SessionProvider.HistoryToggle> {
        return currentHistoryToggle
    }

    override fun getCurrentHistoryToggle(): SessionProvider.HistoryToggle {
        return currentHistoryToggle.value ?: SessionProvider.HistoryToggle.NUMBER
    }

    override fun setCurrentHistoryToggle(toggle: SessionProvider.HistoryToggle) {
        currentHistoryToggle.onNext(toggle)
    }

    private val unfinished: BehaviorSubject<List<Bet>> = BehaviorSubject.createDefault(ArrayList())

    override fun observeUnfinished(): Observable<List<Bet>> {
        return unfinished
    }

    override fun getUnfinished(): List<Bet> {
        return unfinished.value ?: ArrayList()
    }

    override fun setUnfinished(bets: List<Bet>) {
        unfinished.onNext(bets)
    }

    override fun addUnfinished(bets: List<Bet>) {
        var append: MutableList<Bet> = ArrayList<Bet>(unfinished.value ?: ArrayList())
        val productIds = append.map {
            it.bets
        }
        for (bet in bets) {
            if (!productIds.contains(bet.bets)) {
                append.add(bet)
            }
        }
        unfinished.onNext(append)
    }

    private var referToken: String? = null

    override fun getReferToken(): String? {
        return referToken
    }

    override fun setReferToken(token: String?) {
        referToken = token
    }

//    private var defaultBaseUrl = "http://3.234.13.232:8080"
//    private var baseUrl = "http://3.92.178.47:8080";
//    private var defaultBaseUrl = "http://3.1.116.181:8080";
//    private var defaultBaseUrl = "http://54.83.154.80:8080";
//    private var defaultBaseUrl = "https://j68zmmyowa.execute-api.ap-southeast-1.amazonaws.com/alpha";
//    private var defaultBaseUrl = "https://cyegkfsv9b.execute-api.us-west-2.amazonaws.com";
//    private var defaultBaseUrl = "http://localhost:8082";
//    private var defaultBaseUrl = "http://18.136.71.79/ngapi/v1/"
    private var defaultBaseUrl = "https://vktm1ci2z7.execute-api.ap-southeast-1.amazonaws.com/v1/"

    override fun getBaseUrl(): String {
        return context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE).getString(BASE_URL, defaultBaseUrl)!!
    }

    override fun setBaseUrl(baseUrl: String?) {
        context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE).edit()
            .putString(BASE_URL, baseUrl)
            .commit()
    }
}
