package com.caipiao.caipiao.providers

import android.net.Uri

interface UtilProvider {
    fun readQrCode(image: Uri): List<String>

    fun getRedirectUrl(url: String): String

    fun getReferUrl(referToken: String): String
}