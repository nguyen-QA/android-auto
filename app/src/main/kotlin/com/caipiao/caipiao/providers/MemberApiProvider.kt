package com.caipiao.caipiao.providers

import com.caipiao.member.model.*
import io.reactivex.Observable

interface MemberApiProvider {
    fun getMemberProfile() : Observable<GetMemberProfileResponseData>

    fun registerWechat(wechat: String): Observable<RegisterWechatResponseData>

    fun enrollWechat(token: String): Observable<EnrollWechatResponseData>

    fun enrollGuest(deviceId: String): Observable<EnrollGuestResponseData>

    fun requestTran(amount: Long): Observable<RequestTranResponseData>
}