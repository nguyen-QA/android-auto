package com.caipiao.caipiao.providers

import com.caipiao.game.model.GetGameUrlResponseData
import com.caipiao.game.model.GetMyGamesResponseData
import io.reactivex.Observable
import retrofit2.http.Query

interface GameApiProvider {

    fun getMyGames() : Observable<GetMyGamesResponseData>

    fun getGameUrl(
        platType: String,
        gameType: String,
        gameCode: String?,
        lottType: Int?
    ): Observable<GetGameUrlResponseData>

}
