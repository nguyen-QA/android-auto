package com.caipiao.caipiao.providers.impl

import android.content.Context
import android.graphics.BitmapFactory
import android.net.Uri
import boofcv.abst.fiducial.QrCodeDetector
import boofcv.android.ConvertBitmap
import boofcv.factory.fiducial.FactoryFiducial
import boofcv.struct.image.GrayU8
import com.caipiao.caipiao.providers.UtilProvider
import java.io.IOException
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import javax.inject.Inject

class AppUtilProvider @Inject constructor(private val context: Context) : UtilProvider {
    override fun readQrCode(image: Uri): List<String> {
        val inputStream = context.getContentResolver().openInputStream(image)

        val bitmap = BitmapFactory.decodeStream(inputStream)
        val gray = ConvertBitmap.bitmapToGray(bitmap, null as GrayU8?, null)

        val detector: QrCodeDetector<GrayU8> = FactoryFiducial.qrcode(
            null,
            GrayU8::class.java
        )

        detector.process(gray)

        // Get's a list of all the qr codes it could successfully detect and decode
        return detector.detections.map {
            it.message
        }
    }

    override fun getRedirectUrl(url: String): String {
        var redUrl: String = url
        try {
            var urlTmp = URL(url)
            var connection = urlTmp.openConnection() as HttpURLConnection
            connection.getResponseCode()
            redUrl = connection.getURL().toString()
            connection.disconnect()
        } catch (e1: MalformedURLException) {
            e1.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return redUrl
    }

    override fun getReferUrl(referToken: String): String {
        return "http://entertainment/vt/" + referToken
    }
}