package com.caipiao.caipiao.providers

import com.caipiao.affiliate.model.GetReferTokenResponseData
import com.caipiao.affiliate.model.VerifyTokenResponseData
import io.reactivex.Observable

interface AffiliateApiProvider {

    fun getReferToken(): Observable<GetReferTokenResponseData>

    fun verifyToken(token: String) : Observable<VerifyTokenResponseData>
}
