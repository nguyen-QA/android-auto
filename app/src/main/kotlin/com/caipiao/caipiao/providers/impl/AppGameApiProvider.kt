package com.caipiao.caipiao.providers.impl

import com.caipiao.caipiao.providers.GameApiProvider
import com.caipiao.caipiao.providers.SessionProvider
import com.caipiao.game.api.GameServiceApi
import com.caipiao.game.model.GetGameUrlResponseData
import com.caipiao.game.model.GetMyGamesResponseData
import io.reactivex.Observable
import io.swagger.client.ApiClient
import javax.inject.Inject

class AppGameApiProvider  @Inject constructor(
    var sessionProvider: SessionProvider
): GameApiProvider {
    var api = ApiClient().setBaseUrl(sessionProvider.getBaseUrl()).createDefaultAdapter().createService(GameServiceApi::class.java)
//    var api = ApiClient().setBaseUrl("http://192.168.1.4:8088/v1").createDefaultAdapter().createService(GameServiceApi::class.java)


    override fun getMyGames() : Observable<GetMyGamesResponseData> {
        val userId = sessionProvider.getUserId()
        val cert = sessionProvider.getCert()
        return api.getMyGames(userId, cert)
            .doOnNext {
                if (!it.Success()) {
                    throw RuntimeException(it.error?.message)
                }
            }
    }

    override fun getGameUrl(
        platType: String,
        gameType: String,
        gameCode: String?,
        lottType: Int?
    ): Observable<GetGameUrlResponseData> {
        val userId = sessionProvider.getUserId()
        val cert = sessionProvider.getCert()
        val demo = if (sessionProvider.isMember()) null else 1
        return api.getGameUrl(
                userId,
                cert,
                platType,
                gameType,
                gameCode,
                lottType,
                demo
            )
            .doOnNext {
                if (!it.Success()) {
                    throw RuntimeException(it.error?.message)
                }
            }
    }
}