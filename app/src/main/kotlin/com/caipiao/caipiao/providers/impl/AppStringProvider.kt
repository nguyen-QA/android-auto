package com.caipiao.caipiao.providers.impl

import android.content.Context
import android.content.res.Resources
import androidx.annotation.StringRes
import com.caipiao.caipiao.providers.StringProvider
import javax.inject.Inject

class AppStringProvider @Inject constructor(private val context: Context): StringProvider {
    override fun get(@StringRes key: Int): String {
        return context.getString(key)
    }

    override fun get(key: String): String {
        val res = context.resources
        return res.getText(res.getIdentifier(key, "string", context.packageName), key).toString()
    }
}