package com.caipiao.caipiao.ui.me

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.caipiao.caipiao.providers.ApiProvider
import com.caipiao.caipiao.providers.SessionProvider
import com.caipiao.caipiao.providers.StringProvider
import com.caipiao.caipiao.ui.AppViewModel
import com.freeletics.rxredux.StateAccessor
import com.freeletics.rxredux.reduxStore
import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers

class MeViewModel(
    val api: ApiProvider,
    val sessionProvider: SessionProvider,
    val stringProvider: StringProvider
) : AppViewModel() {
    private val TAG = MeViewModel::class.qualifiedName

    data class State(
        val balance: String,
        val isMember: Boolean?,
        val depositVisible: Boolean,
        val withdrawVisible: Boolean,
        val requestCount: String,
        val submitRequestText: String,
        val submitRequestEnabled: Boolean,

        val progressVisible: Boolean,
        val token: String,
        val submitEnabled: Boolean,
        val showLogin: Boolean,
        val showInvalidToken: Boolean,
        val showRequestSuccess: Boolean,
        val showToken: Boolean,
        val error: String
    )

    sealed class Action {
        object Start : Action()
        object ShowMember: Action()
        object ShowFree: Action()
        object ShowDeposit: Action()
        object ShowWithdraw: Action()
        object HideDepositWithdraw: Action()
        object StartProgress: Action()
        object StopProgress: Action()
        object RefreshRequired: Action()
        data class InvitationTokenChanged(val token: String): Action()
        data class RequestCountChanged(val count: String): Action()
        object SubmitToken: Action()
        object SubmitRequest: Action()
        object ShowLogin: Action()
        object HideLogin: Action()
        object ShowToken: Action()
        object HideToken: Action()
        object ShowInvalidToken: Action()
        object HideInvalidToken: Action()
        object ShowRequestSuccess: Action()
        object HideRequestSuccess: Action()

        data class ShowMe(val balance: String) : Action()
        data class ShowError(val error: Throwable) : Action()
        object HideError: Action()
    }

    private val _state = MutableLiveData<State>()
    val state: LiveData<State> = _state

    private val _actions: Relay<Action> = PublishRelay.create()
    val actions: Consumer<Action> = _actions

    private val disposables = CompositeDisposable()

    init {
        disposables.add(
            _actions.reduxStore(
                initialState = State(
                    balance = "",
                    isMember = null,
                    depositVisible = false,
                    withdrawVisible = false,
                    requestCount = "",
                    submitRequestText = "",
                    submitRequestEnabled = false,

                    progressVisible = false,
                    token = "",
                    submitEnabled = false,
                    showLogin = false,
                    showToken = false,
                    showInvalidToken = false,
                    showRequestSuccess = false,
                    error = ""
                ),
                sideEffects = listOf(
                    ::start,
                    ::refreshRequired,
                    ::submitToken,
                    ::submitRequest
                ),
                reducer = ::reducer
            )
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { newState ->
                    _state.value = newState
                })
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }

    private fun reducer(state: State, action: Action): State {
        return when (action) {
            is Action.ShowMe -> state.copy(
                balance = action.balance
            )
            is Action.ShowMember -> state.copy(
                isMember = true
            )
            is Action.ShowFree -> state.copy(
                isMember = false
            )
            is Action.ShowDeposit -> state.copy(
                depositVisible = true,
                withdrawVisible = false,
                submitRequestText = stringProvider.get("submit_deposit_request"),
                submitRequestEnabled = (try { state.requestCount.toInt() } catch(t: Throwable) { 0 }) > 0
            )
            is Action.ShowWithdraw -> state.copy(
                depositVisible = false,
                withdrawVisible = true,
                submitRequestText = stringProvider.get("submit_withdraw_request"),
                submitRequestEnabled = (try { state.requestCount.toInt() } catch(t: Throwable) { 0 }) > 0
            )
            is Action.HideDepositWithdraw -> state.copy(
                depositVisible = false,
                withdrawVisible = false
            )
            is Action.ShowError -> state.copy(
                error = stringProvider.get(action.error.message ?: ""),
                progressVisible = false
            )
            is Action.HideError -> state.copy(
                error = ""
            )
            is Action.StartProgress -> state.copy(
                progressVisible = true
            )
            is Action.StopProgress -> state.copy(
                progressVisible = false
            )
            is Action.InvitationTokenChanged -> state.copy(
                token = action.token,
                submitEnabled = action.token.length == 6
            )
            is Action.RequestCountChanged -> state.copy(
                requestCount = action.count,
                submitRequestEnabled = (try { action.count.toInt() } catch(t: Throwable) { 0 }) > 0
            )
            is Action.ShowLogin -> state.copy(
                showLogin = true
            )
            is Action.HideLogin -> state.copy(
                showLogin = false
            )
            is Action.ShowRequestSuccess -> state.copy(
                showRequestSuccess = true,
                requestCount = "",
                submitRequestEnabled = false
            )
            is Action.HideRequestSuccess -> state.copy(
                showRequestSuccess = false
            )
            is Action.ShowInvalidToken -> state.copy(
                showInvalidToken = true
            )
            is Action.HideInvalidToken -> state.copy(
                showInvalidToken = false
            )
            is Action.ShowToken -> state.copy(
                showToken = true
            )
            is Action.HideToken -> state.copy(
                showToken = false
            )
            else -> state
        }
    }


    private fun start(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.Start::class.java)
            .switchMap {
                Observable.just(
                    if (sessionProvider.isMember()) Action.ShowMember else Action.ShowFree,
                    Action.RefreshRequired)
            }

    private fun refreshRequired(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.RefreshRequired::class.java)
            .switchMap {
                fetchWallet(state())
            }

    private fun fetchWallet(state: State): Observable<Action> {
        val userId = sessionProvider.getUserId()
        return api.getWallet(
            userId ?: ""
        )
            .subscribeOn(Schedulers.newThread())
            .toObservable()
            .doOnNext {
                sessionProvider.setCurrentWallet(it)
            }
            .flatMap {
                reloadMe(state)
            }
            .concatWith(Observable.just(Action.StopProgress))
            .onErrorReturn { error -> Action.ShowError(error) }
            .startWith(Action.StartProgress)
    }

    private fun reloadMe(state: State): Observable<Action> {
        val wallet = sessionProvider.getCurrentWallet()
        return Observable.just(
            Action.ShowMe(
                String.format("%d.%02d", wallet.balance / 100, wallet.balance % 100)
            ) as Action
        )

    }

    private fun submitToken(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.SubmitToken::class.java)
            .switchMap {
                val token = state().token
                api.verifyToken(
                    userId = sessionProvider.getUserId() ?: "",
                    token = token
                )
                    .toObservable()
                    .subscribeOn(Schedulers.newThread())
                    .map { verified -> if (verified) Action.ShowLogin as Action else Action.ShowInvalidToken as Action }
                    .concatWith(Observable.just(Action.StopProgress as Action))
                    .onErrorReturn { error -> Action.ShowError(error) }
                    .startWith(Observable.just(Action.StartProgress))
            }

    private fun submitRequest(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.SubmitRequest::class.java)
            .switchMap {
                var applyCount = state().requestCount
                if (state().withdrawVisible) {
                    applyCount = "-" + applyCount
                }
                api.requestTran(
                    userId = sessionProvider.getUserId() ?: "",
                    amount = applyCount.toLong()
                )
                    .toObservable()
                    .subscribeOn(Schedulers.newThread())
                    .flatMap {
                        Observable.just(Action.ShowRequestSuccess as Action,
                            Action.HideDepositWithdraw)
                    }
                    .concatWith(Observable.just(Action.StopProgress as Action))
                    .onErrorReturn { error -> Action.ShowError(error) }
                    .startWith(Observable.just(Action.StartProgress))
            }

    class Factory(val api: ApiProvider, val sessionProvider: SessionProvider, val stringProvider: StringProvider) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return MeViewModel(api, sessionProvider, stringProvider) as T
        }
    }
}