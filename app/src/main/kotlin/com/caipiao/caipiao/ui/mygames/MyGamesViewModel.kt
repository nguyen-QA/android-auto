package com.caipiao.caipiao.ui.mygames

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.caipiao.caipiao.SecurityHelper
import com.caipiao.caipiao.providers.*
import com.caipiao.caipiao.ui.AppViewModel
import com.freeletics.rxredux.StateAccessor
import com.freeletics.rxredux.reduxStore
import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import toMD5
import java.lang.Exception

class MyGamesViewModel(
    val gameApi: GameApiProvider,
    val memberApi: MemberApiProvider,
    val sessionProvider: SessionProvider,
    val stringProvider: StringProvider,
    val schedulerProvider: SchedulerProvider
) : AppViewModel() {
    private val TAG = MyGamesViewModel::class.qualifiedName

    data class State(
        val progressVisible: Boolean,
        val items: List<Item>,
        val gold: String,
        val showUrl: String,
        val showScanQr: Boolean,
        val showRefer: Boolean,
        val showAuth: Boolean,
        val error: String
    )

    sealed class Action {
        object Start : Action()

        object StartProgress : Action()
        object StopProgress : Action()
        object FetchMyGames : Action()
        data class ShowMyGames(val myGames: List<Item.Game>): Action()
        data class GamePicked(val index: Int) : Action()
        data class ShowGame(val url: String) : Action()
        object GameShown : Action()
        object FetchMemberProfile: Action()
        data class ShowGold(val gold: String): Action()

        data class ShowError(val error: Throwable) : Action()
        object HideError : Action()
    }

    sealed class Item {
        data class Game(val platType: String, val gameType: String, val gameCode: String?, val lottType: Int?, val thumbnail: String): Item()
    }

    private val _state = MutableLiveData<State>()
    val state: LiveData<State> = _state

    private val _actions: Relay<Action> = PublishRelay.create()
    val actions: Consumer<Action> = _actions

    private val disposables = CompositeDisposable()

    init {
        disposables.add(
            _actions.reduxStore(
                    initialState = State(
                        progressVisible = false,
                        items = listOf(),
                        gold = "",
                        showUrl = "",
                        showScanQr = false,
                        showRefer = false,
                        showAuth = false,
                        error = ""
                    ),
                    sideEffects = listOf(
                        ::start,
                        ::gamePicked,
                        ::fetchMyGames,
                        ::fetchMemberProfile
                    ),
                    reducer = ::reducer
                )
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { newState ->
                    _state.value = newState
                })
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }

    private fun reducer(state: State, action: Action): State {
        return when (action) {
            is Action.ShowGold -> state.copy(
                gold = action.gold
            )
            is Action.ShowGame -> state.copy(
                showUrl = action.url
            )
            is Action.GameShown -> state.copy(
                showUrl = ""
            )
            is Action.ShowError -> state.copy(
                error = stringProvider.get(action.error.message ?: ""),
                progressVisible = false
            )
            is Action.HideError -> state.copy(
                error = ""
            )
            is Action.StartProgress -> state.copy(
                progressVisible = true
            )
            is Action.StopProgress -> state.copy(
                progressVisible = false
            )
            is Action.ShowMyGames -> state.copy(
                items = action.myGames
            )
            else -> state
        }
    }


    private fun start(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.Start::class.java)
            .switchMap {
                var deviceId = sessionProvider.getDeviceId()
                if (deviceId == null) {
                    deviceId = SecurityHelper.generateDeviceId()
                    memberApi.enrollGuest(deviceId)
                        .subscribeOn(schedulerProvider.io())
                        .flatMap {
                            Observable.just(
                                Action.ShowGold(
                                    "∞"
                                ),
                                Action.FetchMyGames
                            )
                        }
                } else {
                    if (sessionProvider.isMember()) {
                        Observable.just(
                            Action.FetchMemberProfile,
                            Action.FetchMyGames
                        )
                    } else {
                        Observable.just(
                            Action.ShowGold(
                                "∞"
                            ),
                            Action.FetchMyGames
                        )
                    }
                }
            }

    private fun fetchMyGames(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.FetchMyGames::class.java)
            .switchMap { action ->
                gameApi.getMyGames()
                    .subscribeOn(Schedulers.newThread())
                    .map {
                        if (it.Success()) Action.ShowMyGames(
                            it.games.map {
                                Item.Game(
                                    it.platType,
                                    it.gameType,
                                    it.gameCode,
                                    it.lottType,
                                    it.thumbnail
                                )
                            }) as Action else throw Exception("连接错误")
                    }
                    .concatWith(Observable.just(Action.StopProgress as Action))
                    .onErrorReturn { error ->
                        Action.ShowError(
                            error
                        )
                    }
                    .startWith(Action.StartProgress)
            }

    private fun gamePicked(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.GamePicked::class.java)
            .switchMap { action ->
                val game = state().items.get(action.index) as Item.Game
                val userId = Math.random().toString().toMD5().substring(0, 13)
                gameApi.getGameUrl(
                        game.platType,
                        game.gameType,
                        game.gameCode,
                        game.lottType
                    )
                    .subscribeOn(Schedulers.newThread())
                    .map {
                        Action.ShowGame(
                            it.url
                        ) as Action
                    }
                    .concatWith(Observable.just(Action.StopProgress as Action))
                    .onErrorReturn { error ->
                        Action.ShowError(
                            error
                        )
                    }
                    .startWith(Action.StartProgress)
            }

    private fun fetchMemberProfile(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.FetchMemberProfile::class.java)
            .switchMap { action ->
                memberApi.getMemberProfile()
                    .subscribeOn(Schedulers.newThread())
                    .map {
                        Action.ShowGold(
                            it.user.balance.toString()
                        ) as Action
                    }
                    .concatWith(Observable.just(Action.StopProgress as Action))
                    .onErrorReturn { error ->
                        Action.ShowError(
                            error
                        )
                    }
                    .startWith(Action.StartProgress)
            }

    class Factory(val gameApi: GameApiProvider, val memberApi: MemberApiProvider, val sessionProvider: SessionProvider, val stringProvider: StringProvider, val schedulerProvider: SchedulerProvider) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return MyGamesViewModel(
                gameApi,
                memberApi,
                sessionProvider,
                stringProvider,
                schedulerProvider
            ) as T
        }
    }
}