package com.caipiao.caipiao.ui.auth

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.caipiao.caipiao.R
import com.caipiao.caipiao.providers.SessionProvider
import com.caipiao.caipiao.ui.AppActivity
import com.caipiao.caipiao.ui.verify.AuthVerifyIntent
import com.shopify.livedataktx.distinct
import com.shopify.livedataktx.map
import dagger.android.HasAndroidInjector
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.content_auth.*
import kotlinx.android.synthetic.main.custom_actionbar.*
import javax.inject.Inject

fun Context.AuthIntent(): Intent {
    return Intent(this, AuthActivity::class.java).apply {

    }
}

class AuthActivity : AppActivity(), HasAndroidInjector {
    @Inject
    lateinit var viewModelFactory: AuthViewModel.Factory

    val viewModel: AuthViewModel
        get() = _viewModel as AuthViewModel

    private lateinit var view: View
    private val TAG = AuthActivity::class.qualifiedName
    private val disposables = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _viewModel = ViewModelProviders.of(this, viewModelFactory).get(AuthViewModel::class.java)

        setContentView(R.layout.activity_auth)

        view = findViewById<View>(android.R.id.content)

        wechatEditText.addTextChangedListener { editable ->
            viewModel.actions.accept(
                AuthViewModel.Action.WechatChanged(
                    editable?.toString() ?: ""
                )
            )
        }

        submitButton.setOnClickListener {
            wechatEditText.clearFocus()
            val imm =
                getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(
                wechatEditText.windowToken,
                InputMethodManager.HIDE_NOT_ALWAYS
            )
            viewModel.actions.accept(AuthViewModel.Action.Submit)
        }

        viewModel.state.map { state ->
                state!!.progressVisible
            }
            .distinct()
            .observe(this, Observer { progressVisible ->
                progressBar.visibility = if (progressVisible) View.VISIBLE else View.GONE
            })

        viewModel.state.map { state ->
            state!!.showVerify
        }
            .distinct()
            .observe(this, Observer { showVerify ->
                if (showVerify) {
                    startActivity(AuthVerifyIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION))
                    finish()
                    overridePendingTransition(0, 0)
                }
            })

        viewModel.state.map { state ->
            state!!.showInvalidPhone
        }
            .distinct()
            .observe(this, Observer { showInvalidPhone ->
                if (showInvalidPhone) {
                    Toast.makeText(applicationContext, stringProvider.get(R.string.invalid_phone), Toast.LENGTH_LONG).show()
                    viewModel.actions.accept(AuthViewModel.Action.HideInvalidPhone)
                }
            })

        viewModel.state.map { state ->
            state!!.submitEnabled
        }
            .distinct()
            .observe(this, Observer { submitEnabled ->
                submitButton.setBackgroundColor(if (submitEnabled) resources.getColor(R.color.palePeach) else resources.getColor(R.color.mudBrown))
                submitButton.setTextColor(if (submitEnabled) resources.getColor(R.color.black) else resources.getColor(R.color.white))
                submitButton.isEnabled = submitEnabled
            })

        viewModel.state.map { state ->
                state!!.error
            }
            .distinct()
            .observe(this, Observer { error ->
                if (!error.isEmpty()) {
                    val msgs = error.split("|")
                    if (msgs.size == 2) {
                        Toast.makeText(applicationContext, stringProvider.get(msgs[0]) + "(" + msgs[1] + ")", Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(applicationContext, error, Toast.LENGTH_LONG).show()
                    }
                    viewModel.actions.accept(AuthViewModel.Action.HideError)
                }
            })

        viewModel.actions.accept(AuthViewModel.Action.Start)

    }

}
