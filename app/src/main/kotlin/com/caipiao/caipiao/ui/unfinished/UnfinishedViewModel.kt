package com.caipiao.caipiao.ui.unfinished

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.caipiao.caipiao.model.*
import com.caipiao.caipiao.providers.ApiProvider
import com.caipiao.caipiao.providers.SessionProvider
import com.caipiao.caipiao.providers.StringProvider
import com.caipiao.caipiao.ui.AppViewModel
import com.freeletics.rxredux.StateAccessor
import com.freeletics.rxredux.reduxStore
import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import java.text.DecimalFormat
import java.util.concurrent.TimeUnit

class UnfinishedViewModel(
    val api: ApiProvider,
    val sessionProvider: SessionProvider,
    val stringProvider: StringProvider
) : AppViewModel() {
    private val TAG = UnfinishedViewModel::class.qualifiedName
    data class CurrentDrawData(val drawId: String, val formattedStopTime: String, val formattedEndTime: String)

    data class State(
        val currentDraw: CurrentDrawData,

        val items: List<Item>,
        val bookmark: String,

        val progressVisible: Boolean,
        val error: String
    )

    sealed class Action {
        object Start : Action()
        object StartProgress: Action()
        object StopProgress: Action()
        object RefreshRequired: Action()
        object LoadNextPage: Action()
        object DrawsRefreshRequired: Action()

        object ClearUnfinished: Action()
        data class ShowUnfinished(val unfinished: List<Bet>, val draw: Draw) : Action()
        data class SetBookmark(val bookmark: String): Action()
        data class ShowError(val error: Throwable) : Action()
        data class ShowDraw(val drawId: String, val formattedStopTime: String, val formattedEndTime: String): Action()
        object HideError: Action()
    }

    private val _state = MutableLiveData<State>()
    val state: LiveData<State> = _state

    private val _actions: Relay<Action> = PublishRelay.create()
    val actions: Consumer<Action> = _actions

    private val disposables = CompositeDisposable()

    init {
        disposables.add(
            _actions.reduxStore(
                initialState = State(
                    currentDraw = CurrentDrawData("", "", ""),

                    items = emptyList(),
                    bookmark = "",

                    progressVisible = false,
                    error = ""
                ),
                sideEffects = listOf(
                    ::start,
                    ::refreshRequired,
                    ::loadNextPage
                ),
                reducer = ::reducer
            )
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { newState ->
                    _state.value = newState
                })
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }

    sealed class Item {
        data class Draw(val drawId: String, val number1: String, val number2: String, val number3: String, val number4: String, val number5: String): Item()
        data class Bet(val bet: String, val count: String, val reward: String): Item()
    }


    private fun reducer(state: State, action: Action): State {
        return when (action) {
            is Action.ShowDraw -> state.copy(
                currentDraw = CurrentDrawData(action.drawId, action.formattedStopTime, action.formattedEndTime)
            )
            is Action.ClearUnfinished -> state.copy(
                items = listOf()
            )
            is Action.ShowUnfinished -> state.copy(
                items = {
                    var items = mutableListOf<Item>()
                    for (bet in action.unfinished) {
                        val bets = bet.bets
                        val count = bet.count

                        val myBets = bets.split(",")
                        for (myBet in myBets) {
                            val rate = if (DEFAULT_REWARDS[myBet] != null) DEFAULT_REWARDS[myBet]!! - 100 else 0
                            items.add(
                                Item.Bet(
                                    BET_DISPLAY.get(myBet) ?: "",
                                    count.toString(),
                                    String.format("%d.%2d", rate / 100, rate % 100)
                                )
                            )
                        }
                    }
                    items
                }())
            is Action.SetBookmark -> state.copy(
                bookmark = action.bookmark
            )
            is Action.ShowError -> state.copy(
                error = stringProvider.get(action.error.message ?: ""),
                progressVisible = false
            )
            is Action.HideError -> state.copy(
                error = ""
            )
            is Action.StartProgress -> state.copy(
                progressVisible = true
            )
            is Action.StopProgress -> state.copy(
                progressVisible = false
            )
            else -> state
        }
    }


    private fun start(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.Start::class.java)
            .switchMap {
                reloadUnfinshed(state())
                    .mergeWith(Observable.just(Action.DrawsRefreshRequired))
                    .concatWith(startCurrentDrawTimer(state()))
            }

    private fun startCurrentDrawTimer(state: State): Observable<Action> {
        return sessionProvider.observeCurrentDraw()
            .switchMap { draw ->
                val timestamp = sessionProvider.timestamp()
                val duration = draw.endTime - timestamp

                Observable.interval(0, 1, TimeUnit.SECONDS)
                    .take(duration)
                    .map { seconds ->
                        val remaingSeconds = duration - seconds - 1
                        Action.ShowDraw(
                            draw.drawId,
                            formatStopTimer(draw, remaingSeconds),
                            formatEndTimer(draw, remaingSeconds)
                        ) as Action
                    }
                    .onErrorReturn { error ->
                        Log.w(TAG, error)
                        Action.ShowError(error)
                    }
                    .startWith(Observable.just(Action.RefreshRequired).delay(1, TimeUnit.SECONDS))
            }
    }

    private fun formatEndTimer(draw: Draw, remaingSeconds: Long): String {
        val endDuration = draw.endTime - draw.stopTime
        if (endDuration > remaingSeconds ) {
            return formatTimer(remaingSeconds)
        } else {
            return formatTimer(endDuration)
        }
    }

    private fun formatStopTimer(draw: Draw, remaingSeconds: Long): String {
        val endDuration = draw.endTime - draw.stopTime
        val stopDuration = draw.stopTime - draw.startTime
        if (endDuration > remaingSeconds ) {
            return formatTimer(0)
        } else if (stopDuration > remaingSeconds - endDuration){
            return formatTimer(remaingSeconds - endDuration)
        } else {
            return formatTimer(stopDuration)
        }
    }

    private fun formatTimer(seconds: Long): String {
        return String.format("%02d:%02d", (seconds % 3600) / 60, (seconds % 60))
    }

    private fun refreshRequired(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.RefreshRequired::class.java)
            .switchMap {
                fetchUnfinished(state(), "")
                    .startWith(Action.ClearUnfinished)
            }

    private fun loadNextPage(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.LoadNextPage::class.java)
            .switchMap {
                fetchUnfinished(state(), state().bookmark)
            }

    private fun fetchUnfinished(state: State, bookmark: String): Observable<Action> {
        val userId = sessionProvider.getUserId()
        val draw = sessionProvider.getCurrentDraw()
        return api.getUnfinished(
            userId ?: ""
        )
            .subscribeOn(Schedulers.newThread())
            .toObservable()
            .doOnNext {
                sessionProvider.setUnfinished(it)
            }
            .map {
                Action.ShowUnfinished(it, draw) as Action
            }
            .concatWith(Observable.just(Action.StopProgress))
            .onErrorReturn { error ->
                Action.ShowError(
                    error
                )
            }
            .startWith(Action.StartProgress)
    }

    private fun reloadUnfinshed(state: State): Observable<Action> {
        val unfinished = sessionProvider.getUnfinished()
        val draw = sessionProvider.getCurrentDraw()
        return Observable.just(
            Action.ShowUnfinished(
                unfinished,
                draw
            ) as Action
        )

    }

    class Factory(val api: ApiProvider, val sessionProvider: SessionProvider, val stringProvider: StringProvider) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return UnfinishedViewModel(
                api,
                sessionProvider,
                stringProvider
            ) as T
        }
    }
}