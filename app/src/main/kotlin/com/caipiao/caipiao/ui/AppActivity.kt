package com.caipiao.caipiao.ui

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.caipiao.caipiao.providers.SessionProvider
import com.caipiao.caipiao.providers.StringProvider
import com.caipiao.caipiao.providers.UtilProvider
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

open class AppActivity: AppCompatActivity(), HasAndroidInjector {
    @Inject
    lateinit var androidInjector : DispatchingAndroidInjector<Any>

    override fun androidInjector(): AndroidInjector<Any> = androidInjector

    lateinit var _viewModel: AppViewModel

    @Inject
    lateinit var stringProvider: StringProvider
    @Inject
    lateinit var sessionProvider: SessionProvider
    @Inject
    lateinit var utilProvider: UtilProvider

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    fun showError(title: String, message: String) {
        Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
    }
}