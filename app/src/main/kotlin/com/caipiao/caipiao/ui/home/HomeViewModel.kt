package com.caipiao.caipiao.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.caipiao.caipiao.SecurityHelper
import com.caipiao.caipiao.providers.*
import com.caipiao.caipiao.ui.AppViewModel
import com.freeletics.rxredux.StateAccessor
import com.freeletics.rxredux.reduxStore
import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import java.lang.Exception

class HomeViewModel(
    val gameApi: GameApiProvider,
    val memberApi: MemberApiProvider,
    val affiliateApi: AffiliateApiProvider,
    val sessionProvider: SessionProvider,
    val stringProvider: StringProvider,
    val schedulerProvider: SchedulerProvider
) : AppViewModel() {
    private val TAG = HomeViewModel::class.qualifiedName

    data class State(
        val showExchange: Boolean,
        val showExchangeRequest: Boolean,
        val depositVisible: Boolean,
        val withdrawVisible: Boolean,
        val requestCount: String,
        val submitRequestText: String,
        val submitRequestEnabled: Boolean,
        val showRequestSuccess: Boolean,

        val progressVisible: Boolean,
        val gold: String,
        val showKyqp: String,
        val showMyGames: Boolean,
        val showScanQr: Boolean,
        val showRefer: Boolean,
        val showAuth: Boolean,
        val error: String
    )

    sealed class Action {
        object Start : Action()
        data class Update(val data: String?): Action()
        data class ShowUpgrade(val url: String): Action()

        object ShowExchange: Action()
        object HideExchange: Action()
        object ShowExchangeRequest: Action()
        object HideExchangeRequest: Action()
        object ShowDeposit: Action()
        object ShowWithdraw: Action()
        object HideDepositWithdraw: Action()
        data class RequestCountChanged(val count: String): Action()
        object SubmitRequest: Action()
        object ShowRequestSuccess: Action()
        object HideRequestSuccess: Action()
        object ToggleExchange: Action()

        object StartProgress : Action()
        object StopProgress : Action()
        object KyqpPicked: Action()
        object MyGamesPicked : Action()
        data class ShowKyqp(val url: String): Action()
        object KyqpShown: Action()
        object ShowMyGames : Action()
        object MyGamesShown : Action()
        object FetchMemberProfile: Action()
        data class ShowGold(val gold: String): Action()
        object ShowScanQr:  Action()
        object ScanQrShown: Action()
        object ShowRefer: Action()
        object ReferShown: Action()
        data class VerifyToken(val token: String): Action()
        object ShowAuth: Action()
        object AuthShown: Action()
        object QrPicked: Action()

        data class ShowError(val error: Throwable) : Action()
        object HideError : Action()
    }

    sealed class Item {
        data class Game(val platType: String, val gameType: String, val gameCode: String?, val lottType: Int?, val thumbnail: String): Item()
    }

    private val _state = MutableLiveData<State>()
    val state: LiveData<State> = _state

    private val _actions: Relay<Action> = PublishRelay.create()
    val actions: Consumer<Action> = _actions

    private val disposables = CompositeDisposable()

    init {
        disposables.add(
            _actions.reduxStore(
                    initialState = State(
                        showExchange = false,
                        showExchangeRequest = false,
                        depositVisible = false,
                        withdrawVisible = false,
                        requestCount = "",
                        submitRequestText = "",
                        submitRequestEnabled = false,
                        showRequestSuccess = false,

                        progressVisible = false,
                        gold = "",
                        showKyqp = "",
                        showMyGames = false,
                        showScanQr = false,
                        showRefer = false,
                        showAuth = false,
                        error = ""
                    ),
                    sideEffects = listOf(
                        ::start,
                        ::kyqpPicked,
                        ::myGamesPicked,
                        ::update,
                        ::qrPicked,
                        ::fetchMemberProfile,
                        ::verifyToken,
                        ::submitRequest,
                        ::toggleExchange
                    ),
                    reducer = ::reducer
                )
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { newState ->
                    _state.value = newState
                })
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }

    private fun reducer(state: State, action: Action): State {
        return when (action) {
            is Action.ShowError -> state.copy(
                error = stringProvider.get(action.error.message ?: ""),
                progressVisible = false
            )
            is Action.ShowExchange -> state.copy(
                showExchange = true
            )
            is Action.HideExchange -> state.copy(
                showExchange = false
            )
            is Action.ShowExchangeRequest -> state.copy(
                showExchangeRequest = true
            )
            is Action.HideExchangeRequest -> state.copy(
                showExchangeRequest = false
            )
            is Action.ShowDeposit -> state.copy(
                depositVisible = true,
                withdrawVisible = false,
                submitRequestText = stringProvider.get("submit_deposit_request"),
                submitRequestEnabled = (try { state.requestCount.toInt() } catch(t: Throwable) { 0 }) > 0
            )
            is Action.ShowWithdraw -> state.copy(
                depositVisible = false,
                withdrawVisible = true,
                submitRequestText = stringProvider.get("submit_withdraw_request"),
                submitRequestEnabled = (try { state.requestCount.toInt() } catch(t: Throwable) { 0 }) > 0
            )
            is Action.HideDepositWithdraw -> state.copy(
                depositVisible = false,
                withdrawVisible = false
            )
            is Action.RequestCountChanged -> state.copy(
                requestCount = action.count,
                submitRequestEnabled = (try { action.count.toInt() } catch(t: Throwable) { 0 }) > 0
            )
            is Action.ShowRequestSuccess -> state.copy(
                showRequestSuccess = true,
                requestCount = "",
                submitRequestEnabled = false
            )
            is Action.HideRequestSuccess -> state.copy(
                showRequestSuccess = false
            )
            is Action.ShowGold -> state.copy(
                gold = action.gold
            )
            is Action.ShowMyGames -> state.copy(
                showMyGames = true
            )
            is Action.MyGamesShown -> state.copy(
                showMyGames = false
            )
            is Action.ShowKyqp -> state.copy(
                showKyqp = action.url
            )
            is Action.KyqpShown -> state.copy(
                showKyqp = ""
            )
            is Action.ShowScanQr -> state.copy(
                showScanQr = true
            )
            is Action.ScanQrShown -> state.copy(
                showScanQr = false
            )
            is Action.HideError -> state.copy(
                error = ""
            )
            is Action.StartProgress -> state.copy(
                progressVisible = true
            )
            is Action.StopProgress -> state.copy(
                progressVisible = false
            )
            is Action.ShowAuth -> state.copy(
                showAuth = true
            )
            is Action.AuthShown -> state.copy(
                showAuth = false
            )
            is Action.ShowRefer -> state.copy(
                showRefer = true
            )
            is Action.ReferShown -> state.copy(
                showRefer = false
            )
            else -> state
        }
    }


    private fun start(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.Start::class.java)
            .switchMap {
                var deviceId = sessionProvider.getDeviceId()
                if (deviceId == null) {
                    deviceId = SecurityHelper.generateDeviceId()
                    memberApi.enrollGuest(deviceId)
                        .subscribeOn(schedulerProvider.io())
                        .flatMap {
                            Observable.just(Action.ShowGold("∞"), Action.HideExchange)
                        }
                } else {
                    if (sessionProvider.isMember()) {
                        Observable.just(Action.FetchMemberProfile)
                    } else {
                        Observable.just(Action.ShowGold("∞"), Action.HideExchange)
                    }
                }
            }

    private fun qrPicked(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.QrPicked::class.java)
            .switchMap { action ->
                if (sessionProvider.isMember()) {
                    Observable.just(Action.ShowRefer)
                } else {
                    Observable.just(Action.ShowScanQr)
                }
            }

    private fun kyqpPicked(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.KyqpPicked::class.java)
            .switchMap { action ->
                gameApi.getGameUrl(
                        "ky",
                        "7",
                        "dating",
                        null
                    )
                    .subscribeOn(Schedulers.newThread())
                    .map {
                        Action.ShowKyqp(
                            it.url
                        ) as Action
                    }
                    .concatWith(Observable.just(Action.StopProgress as Action))
                    .onErrorReturn { error ->
                        Action.ShowError(
                            error
                        )
                    }
                    .startWith(Action.StartProgress)
            }

    private fun myGamesPicked(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.MyGamesPicked::class.java)
            .switchMap { action ->
                Observable.just(Action.ShowMyGames)
            }

    private fun update(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(HomeViewModel.Action.Update::class.java)
            .switchMap { action ->
                if (action.data != null) {
    //                    val url = utilProvider.getRedirectUrl(action.data)
                    val url = action.data
                    val parsed = url.split("/")
                    if (parsed.size >= 4) {
                        val command = parsed[3]
                        when (command) {
                            "ub" -> {
                                sessionProvider.setBaseUrl(parsed[4])
                                Observable.just(Action.Start)
                            }
                            "ua" -> {
                                Observable.just(Action.ShowUpgrade(parsed[4]))
                            }
                            "vt" -> {
                                Observable.just(Action.VerifyToken(parsed[4]))
                            }
                            else -> {
                                Observable.just(Action.Start)
                            }
                        }
                    } else{
                        Observable.just(Action.Start)
                    }
                } else {
                    Observable.just(Action.Start)
                }
            }

    private fun verifyToken(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.VerifyToken::class.java)
            .switchMap { action ->
                affiliateApi.verifyToken(action.token)
                    .subscribeOn(Schedulers.newThread())
                    .map {
                        if (it.isSuccess) Action.ShowAuth as Action else throw Exception("无法识别")
                    }
                    .concatWith(Observable.just(Action.StopProgress as Action))
                    .onErrorReturn { error -> Action.ShowError(error) }
                    .startWith(Action.StartProgress)
            }

    private fun fetchMemberProfile(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.FetchMemberProfile::class.java)
            .switchMap { action ->
                memberApi.getMemberProfile()
                    .subscribeOn(Schedulers.newThread())
                    .flatMap {
                        Observable.just(Action.ShowGold(it.user.balance.toString()) as Action, Action.ShowExchange)
                    }
                    .concatWith(Observable.just(Action.StopProgress as Action))
                    .onErrorReturn { error -> Action.ShowError(error) }
                    .startWith(Action.StartProgress)
            }

    private fun submitRequest(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.SubmitRequest::class.java)
            .switchMap {
                var applyCount = state().requestCount
                if (state().withdrawVisible) {
                    applyCount = "-" + applyCount
                }
                memberApi.requestTran(
                        amount = applyCount.toLong()
                    )
                    .subscribeOn(Schedulers.newThread())
                    .flatMap {
                        Observable.just(Action.ShowRequestSuccess as Action,
                            Action.HideExchangeRequest)
                    }
                    .concatWith(Observable.just(Action.StopProgress as Action))
                    .onErrorReturn {
                            error -> Action.ShowError(error)
                    }
                    .startWith(Observable.just(Action.StartProgress))
            }

    private fun toggleExchange(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.ToggleExchange::class.java)
            .switchMap {
                Observable.just(if (state().showExchangeRequest) Action.HideExchangeRequest else Action.ShowExchangeRequest, Action.ShowDeposit)
            }

    class Factory(val gameApi: GameApiProvider, val memberApi: MemberApiProvider, val affiliateApi: AffiliateApiProvider, val sessionProvider: SessionProvider, val stringProvider: StringProvider, val schedulerProvider: SchedulerProvider) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return HomeViewModel(
                gameApi,
                memberApi,
                affiliateApi,
                sessionProvider,
                stringProvider,
                schedulerProvider
            ) as T
        }
    }
}