package com.caipiao.caipiao.ui.history

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.caipiao.caipiao.R
import com.caipiao.caipiao.model.Draw
import com.caipiao.caipiao.model.Sequence
import com.caipiao.caipiao.providers.ApiProvider
import com.caipiao.caipiao.providers.SessionProvider
import com.caipiao.caipiao.providers.StringProvider
import com.caipiao.caipiao.ui.AppViewModel
import com.caipiao.caipiao.ui.xyttc.XyttcViewModel
import com.freeletics.rxredux.StateAccessor
import com.freeletics.rxredux.reduxStore
import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers


class HistoryViewModel(
    val api: ApiProvider,
    val sessionProvider: SessionProvider,
    val stringProvider: StringProvider
) : AppViewModel() {
    private val TAG = HistoryViewModel::class.qualifiedName
    data class State(
        val toggle: SessionProvider.HistoryToggle,
        val items: List<Item>,
        val bookmark: String,

        val progressVisible: Boolean,
        val error: String?
    )

    sealed class Action {
        object Start : Action()
        object StartProgress: Action()
        object StopProgress: Action()
        object RefreshRequired: Action()
        object LoadNextPage: Action()
        data class HistoryToggled(val toggle: SessionProvider.HistoryToggle): Action()

        object ShowHistory: Action()
        data class SetBookmark(val bookmark: String): Action()
        data class ShowError(val error: Throwable) : Action()
        object HideError: Action()
    }

    private val _state = MutableLiveData<State>()
    val state: LiveData<State> = _state

    private val _actions: Relay<Action> = PublishRelay.create()
    val actions: Consumer<Action> = _actions

    private val disposables = CompositeDisposable()

    init {
        disposables.add(
            _actions.reduxStore(
                initialState = State(
                    toggle = SessionProvider.HistoryToggle.NUMBER,
                    items = emptyList(),
                    bookmark = "",

                    progressVisible = false,
                    error = ""
                ),
                sideEffects = listOf(
                    ::start,
                    ::refreshRequired,
                    ::loadNextPage,
                    ::historyToggled
                ),
                reducer = ::reducer
            )
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { newState ->
                    _state.value = newState
                })
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }

    sealed class Item {
        data class SingleDraw(val drawId: String, val number1: String, val number2: String, val number3: String, val number4: String, val number5: String): Item()
        data class SpecialDraw(val drawId: String, val largeSmall: String, val oddEven: String, val lhh: String, val front: String, val mid: String, val end: String): Item()
    }


    private fun reducer(state: State, action: Action): State {
        return when (action) {
            is Action.ShowHistory -> state.copy(
                toggle = sessionProvider.getCurrentHistoryToggle(),
                items = {
                    val history = sessionProvider.getDrawHistory()
                    val toggle  = sessionProvider.getCurrentHistoryToggle()
                    var items = mutableListOf<Item>()
                    for (draw in history) {
                        if (draw.draw == null || draw.draw.isEmpty()) {
                            continue
                        }
                        val n1 = draw.draw.substring(0, 1)
                        val n2 = draw.draw.substring(1, 2)
                        val n3 = draw.draw.substring(2, 3)
                        val n4 = draw.draw.substring(3, 4)
                        val n5 = draw.draw.substring(4, 5)
                        when(toggle) {
                            SessionProvider.HistoryToggle.NUMBER ->
                                items.add(
                                    Item.SingleDraw(
                                        draw.drawId,
                                        n1,
                                        n2,
                                        n3,
                                        n4,
                                        n5
                                    )
                                )
                            SessionProvider.HistoryToggle.ODD_EVEN ->
                                items.add(
                                    Item.SingleDraw(
                                        draw.drawId,
                                        oddEven(n1),
                                        oddEven(n2),
                                        oddEven(n3),
                                        oddEven(n4),
                                        oddEven(n5)
                                    )
                                )
                            SessionProvider.HistoryToggle.SMALL_BIG ->
                                items.add(
                                    Item.SingleDraw(
                                        draw.drawId,
                                        largeSmall(n1),
                                        largeSmall(n2),
                                        largeSmall(n3),
                                        largeSmall(n4),
                                        largeSmall(n5)
                                    )
                                )
                            SessionProvider.HistoryToggle.SPECIAL -> {
                                val sum =
                                    n1.toInt() + n2.toInt() + n3.toInt() + n4.toInt() + n5.toInt()
                                items.add(
                                    Item.SpecialDraw(
                                        draw.drawId,
                                        largeSmallSum(sum),
                                        oddEven(sum.toString()),
                                        lhh(n1, n5),
                                        cal3(n1, n2, n3),
                                        cal3(n2, n3, n4),
                                        cal3(n3, n4, n5)
                                    )
                                )
                            }
                        }
                    }
                    items
                }())
            is Action.ShowError -> state.copy(
                error = stringProvider.get(action.error.message ?: ""),
                progressVisible = false
            )
            is Action.HideError -> state.copy(
                error = ""
            )
            is Action.StartProgress -> state.copy(
                progressVisible = true
            )
            is Action.StopProgress -> state.copy(
                progressVisible = false
            )
            is Action.SetBookmark -> state.copy(
                bookmark = action.bookmark
            )
            else -> state
        }
    }

    private fun historyToggled(
        actions: Observable<HistoryViewModel.Action>,
        state: StateAccessor<HistoryViewModel.State>
    ): Observable<HistoryViewModel.Action> =
        actions.ofType(HistoryViewModel.Action.HistoryToggled::class.java)
            .switchMap { action ->
                sessionProvider.setCurrentHistoryToggle(action.toggle)
                Observable.just(Action.ShowHistory as Action)
            }

    private fun oddEven(n: String): String {
        return when(n.toInt() % 2) {
            1 -> stringProvider.get(R.string.odd)
            else -> stringProvider.get(R.string.even)
        }
    }

    private fun largeSmall(n: String): String {
        return when(n.toInt()) {
            in 0..4 -> stringProvider.get(R.string.small)
            else -> stringProvider.get(R.string.big)
        }
    }

    private fun lhh(n1: String, n5: String): String {
        if (n1.toInt() > n5.toInt()) {
            return stringProvider.get(R.string.leong)
        } else if (n1.toInt() < n5.toInt()) {
            return stringProvider.get(R.string.hu)
        } else {
            return stringProvider.get(R.string.he)
        }
    }

    private fun largeSmallSum(n: Int): String {
        return when(n) {
            in 0..22 -> stringProvider.get(R.string.small)
            else -> stringProvider.get(R.string.big)
        }
    }

    private fun cal3(n1: String, n2: String, n3: String): String {
        return if (n1 == n2 && n2 == n3) {
            stringProvider.get(R.string.baozi)
        } else if (n1 == n2 || n2 == n3 || n3 == n1) {
            return stringProvider.get(R.string.duizi)
        } else {
            val key = setOf(n1.toInt(), n2.toInt(), n3.toInt())
            if (Sequence.contains(key)) {
                stringProvider.get(R.string.shunzi)
            } else {
                "------"
            }
        }
    }

    private fun start(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.Start::class.java)
            .switchMap {
                Observable.just(Action.ShowHistory as Action)
                    .mergeWith(Observable.just(Action.RefreshRequired)
                    )
            }

    private fun refreshRequired(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.RefreshRequired::class.java)
            .switchMap {
                fetchHistory(state(), "")
            }

    private fun loadNextPage(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.LoadNextPage::class.java)
            .switchMap {
                fetchHistory(state(), state().bookmark)
            }

    private fun fetchHistory(state: State, bookmark: String): Observable<Action> {
        val userId = sessionProvider.getUserId()
        return api.getDrawHistory(
            "xyttc",
            bookmark
        )
            .subscribeOn(Schedulers.newThread())
            .toObservable()
            .doOnNext {
                if (bookmark == "") {
                    sessionProvider.setDrawHistory(it.first)
                } else {
                    sessionProvider.addDrawHistory(it.first)
                }
            }
            .flatMap {
                Observable.just(Action.ShowHistory as Action, Action.SetBookmark(it.second))
            }
            .concatWith(Observable.just(Action.StopProgress))
            .onErrorReturn { error -> Action.ShowError(error) }
            .startWith(Action.StartProgress)
    }

    class Factory(val api: ApiProvider, val sessionProvider: SessionProvider, val stringProvider: StringProvider) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return HistoryViewModel(api, sessionProvider, stringProvider) as T
        }
    }
}