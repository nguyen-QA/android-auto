package com.caipiao.caipiao.ui.xyttc

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.constraintlayout.widget.Group
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.caipiao.caipiao.R
import com.caipiao.caipiao.providers.SessionProvider
import com.caipiao.caipiao.ui.AppActivity
import com.caipiao.caipiao.ui.finished.FinishedIntent
import com.caipiao.caipiao.ui.history.HistoryIntent
import com.caipiao.caipiao.ui.me.MeIntent
import com.caipiao.caipiao.ui.unfinished.UnfinishedIntent
import com.shopify.livedataktx.distinct
import com.shopify.livedataktx.map
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.content_xyttc.*
import kotlinx.android.synthetic.main.custom_actionbar.*
import javax.inject.Inject

fun Context.XyttcIntent(): Intent {
    return Intent(this, XyttcActivity::class.java).apply {

    }
}

class XyttcActivity : AppActivity() {
    @Inject
    lateinit var viewModelFactory: XyttcViewModel.Factory

    val viewModel: XyttcViewModel
        get() = _viewModel as XyttcViewModel

    private val TAG = XyttcActivity::class.qualifiedName
    private lateinit var view: View
    private val disposeables = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        _viewModel = ViewModelProviders.of(this, viewModelFactory).get(XyttcViewModel::class.java)

        setContentView(R.layout.activity_xyttc)

        view = findViewById<View>(android.R.id.content)
        val mInflater = LayoutInflater.from(this)

        val customView = mInflater.inflate(R.layout.custom_actionbar, null)

        val actionBar = supportActionBar!!
        actionBar.setDisplayShowTitleEnabled(true)
        actionBar.title = stringProvider.get(R.string.xyttc)
        actionBar.setDisplayShowCustomEnabled(true)
        actionBar.setCustomView(customView)
        titleImageView.setImageResource(sessionProvider.getTitleDrawable())

        ball1Button.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetCategoryPicked("1"))
        }
        ball2Button.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetCategoryPicked("2"))
        }
        ball3Button.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetCategoryPicked("3"))
        }
        ball4Button.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetCategoryPicked("4"))
        }
        ball5Button.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetCategoryPicked("5"))
        }
        ballsButton.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetCategoryPicked("s"))
        }

        number0Button.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetPicked(it.tag as String))
        }
        number1Button.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetPicked(it.tag as String))
        }
        number2Button.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetPicked(it.tag as String))
        }
        number3Button.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetPicked(it.tag as String))
        }
        number4Button.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetPicked(it.tag as String))
        }
        number5Button.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetPicked(it.tag as String))
        }
        number6Button.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetPicked(it.tag as String))
        }
        number7Button.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetPicked(it.tag as String))
        }
        number8Button.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetPicked(it.tag as String))
        }
        number9Button.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetPicked(it.tag as String))
        }
        bigButton.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetPicked(it.tag as String))
        }
        smallButton.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetPicked(it.tag as String))
        }
        oddButton.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetPicked(it.tag as String))
        }
        evenButton.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetPicked(it.tag as String))
        }

        sumBigButton.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetPicked(it.tag as String))
        }
        sumSmallButton.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetPicked(it.tag as String))
        }
        sumOddButton.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetPicked(it.tag as String))
        }
        sumEvenButton.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetPicked(it.tag as String))
        }
        leongButton.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetPicked(it.tag as String))
        }
        huButton.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetPicked(it.tag as String))
        }
        heButton.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetPicked(it.tag as String))
        }

        baoziFrontButton.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetPicked(it.tag as String))
        }
        shunziFrontButton.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetPicked(it.tag as String))
        }
        duiziFrontButton.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetPicked(it.tag as String))
        }

        baoziMidButton.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetPicked(it.tag as String))
        }
        shunziMidButton.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetPicked(it.tag as String))
        }
        duiziMidButton.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetPicked(it.tag as String))
        }

        baoziEndButton.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetPicked(it.tag as String))
        }
        shunziEndButton.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetPicked(it.tag as String))
        }
        duiziEndButton.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.BetPicked(it.tag as String))
        }


        timesEditText.setOnFocusChangeListener { view, hasFocus ->
            (view as EditText).isCursorVisible = hasFocus
        }

        timesEditText.setOnEditorActionListener { _, actionId, _ ->
            if (actionId === EditorInfo.IME_ACTION_DONE) {
                timesEditText.clearFocus()
                val imm =
                    getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(
                    timesEditText.windowToken,
                    InputMethodManager.HIDE_NOT_ALWAYS
                )
                true
            }
            false
        }
        timesEditText.addTextChangedListener { editable ->
            viewModel.actions.accept(XyttcViewModel.Action.TimesChanged(editable?.toString() ?: ""))
        }
        trashButton.setOnClickListener {
            viewModel.actions.accept(XyttcViewModel.Action.TrashBets)
            timesEditText.clearFocus()
            val imm =
                getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(
                timesEditText.windowToken,
                InputMethodManager.HIDE_NOT_ALWAYS
            )
        }

        submitButton.setOnClickListener {
            timesEditText.clearFocus()
            val imm =
                getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(
                timesEditText.windowToken,
                InputMethodManager.HIDE_NOT_ALWAYS
            )
            viewModel.actions.accept(XyttcViewModel.Action.SubmitBets)
        }

        unfinishButton.setOnClickListener {
            startActivity(UnfinishedIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION))
            finish()
            overridePendingTransition(0, 0)
        }
        finishButton.setOnClickListener {
            startActivity(FinishedIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION))
            finish()
            overridePendingTransition(0, 0)
        }
        mineButton.setOnClickListener {
            startActivity(MeIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION))
            finish()
            overridePendingTransition(0, 0)
        }
        historyButton.setOnClickListener {
            startActivity(HistoryIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION))
            finish()
            overridePendingTransition(0, 0)
        }


        viewModel.state.map { state ->
            state!!.lastDraw
        }
            .distinct()
            .observe(this, Observer { lastDraw ->
                drawTextView.text = lastDraw.drawId
                number1TextView.text = lastDraw.number1
                number2TextView.text = lastDraw.number2
                number3TextView.text = lastDraw.number3
                number4TextView.text = lastDraw.number4
                number5TextView.text = lastDraw.number5
            })

        viewModel.state.map { state ->
            state!!.currentDraw
        }
            .distinct()
            .observe(this, Observer { currentDraw ->
                drawNameTextView.text = currentDraw.drawId
                stopTimerTextView.text = currentDraw.formattedStopTime
                endTimerTextView.text = currentDraw.formattedEndTime
            })

        viewModel.state.map { state ->
            state!!.betOptions
        }
            .distinct()
            .observe(this, Observer { betOptions ->
                (betCategoryGroup as Group).referencedIds.forEach { id ->
                    (view.findViewById(id) as Button).apply {
                        if (betOptions.betCategoryTagToggledOn == this.tag) {
                            this.setBackgroundColor(resources.getColor(R.color.palePeach))
                            this.setTextColor(Color.BLACK)
                        } else {
                            this.setBackgroundColor(resources.getColor(R.color.mudBrown))
                            this.setTextColor(Color.WHITE)
                        }
                    }
                }

                if (betOptions.speciallBetVisible) {
                    specialBetScrollView.visibility = View.VISIBLE
                    singleBallBetScrollView.visibility = View.GONE
                } else {
                    specialBetScrollView.visibility = View.GONE
                    singleBallBetScrollView.visibility = View.VISIBLE
                }
            })

        viewModel.state.map { state ->
            state!!.singleBallBets
        }
            .distinct()
            .observe(this, Observer { singleBallBets ->
                (singleBallBetGroup as Group).referencedIds.forEach { id ->
                    (view.findViewById(id) as Button).apply {
                        if (singleBallBets.contains(this.tag)) {
                            this.background = resources.getDrawable(R.drawable.btn_selected_bg)
                            this.setTextColor(Color.BLACK)
                        } else {
                            this.background = resources.getDrawable(R.drawable.btn_unselected_bg)
                            this.setTextColor(Color.WHITE)
                        }
                    }
                }
            })

        viewModel.state.map { state ->
            state!!.specialBets
        }
            .distinct()
            .observe(this, Observer { specialBets ->
                (specialBetGroup as Group).referencedIds.forEach { id ->
                    (view.findViewById(id) as Button).apply {
                        if (specialBets.contains(this.tag)) {
                            this.background = resources.getDrawable(R.drawable.btn_selected_bg)
                            this.setTextColor(Color.BLACK)
                        } else {
                            this.background = resources.getDrawable(R.drawable.btn_unselected_bg)
                            this.setTextColor(Color.WHITE)
                        }
                    }
                }
            })

        viewModel.state.map { state ->
            state!!.submitVisible
        }
            .distinct()
            .observe(this, Observer { submitVisible ->
                if (submitVisible) {
                    submitGroup.visibility = View.VISIBLE
                } else {
                    submitGroup.visibility = View.GONE
                }
            })

        viewModel.state.map { state ->
                state!!.submitEnable
            }
            .distinct()
            .observe(this, Observer { submitEnable ->
                submitButton.isEnabled = submitEnable
                val image = resources.getDrawable(R.drawable.ic_submit).mutate()
                if (!submitEnable) {
                    image.setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN)
                }
                submitButton.setImageDrawable(image)
            })

        viewModel.state.map { state ->
            state!!.betCount
        }
            .distinct()
            .observe(this, Observer { betCount ->
                totalBetsTextView.text =
                    String.format(stringProvider.get(R.string.total_bets), betCount)
            })

        viewModel.state.map { state ->
            state!!.popupSubmitSuccess
        }
            .distinct()
            .observe(this, Observer { popupSubmitSuccess ->
                if (popupSubmitSuccess) {
                    Toast.makeText(
                        applicationContext,
                        stringProvider.get(R.string.bet_success),
                        Toast.LENGTH_LONG
                    ).show()
                    viewModel.actions.accept(XyttcViewModel.Action.HideSubmitSuccess)
                }
            })

        viewModel.state.map { state ->
            state!!.progressVisible
        }
            .distinct()
            .observe(this, Observer { progressVisible ->
                progressBar.visibility = if (progressVisible) View.VISIBLE else View.GONE
            })

        viewModel.state.map { state ->
            state!!.error
        }
            .distinct()
            .observe(this, Observer { error ->
                if (!error.isEmpty()) {
                    val msgs = error.split("|")
                    if (msgs.size == 2) {
                        Toast.makeText(
                            applicationContext,
                            stringProvider.get(msgs[0]) + "(" + msgs[1] + ")",
                            Toast.LENGTH_LONG
                        ).show()
                    } else {
                        Toast.makeText(applicationContext, error, Toast.LENGTH_LONG).show()
                    }
                    viewModel.actions.accept(XyttcViewModel.Action.HideError)
                }
            })

        viewModel.actions.accept(XyttcViewModel.Action.Start)

    }

    override fun onDestroy() {
        disposeables.dispose()
        super.onDestroy()
    }
}
