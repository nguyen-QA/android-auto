package com.caipiao.caipiao.ui.finished

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.caipiao.caipiao.model.BET_DISPLAY
import com.caipiao.caipiao.model.Draw
import com.caipiao.caipiao.model.DrawFactory
import com.caipiao.caipiao.model.Bet
import com.caipiao.caipiao.providers.ApiProvider
import com.caipiao.caipiao.providers.SessionProvider
import com.caipiao.caipiao.providers.StringProvider
import com.caipiao.caipiao.ui.AppViewModel
import com.freeletics.rxredux.StateAccessor
import com.freeletics.rxredux.reduxStore
import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import java.text.DecimalFormat

class FinishedViewModel(
    val api: ApiProvider,
    val sessionProvider: SessionProvider,
    val stringProvider: StringProvider
) : AppViewModel() {
    private val TAG = FinishedViewModel::class.qualifiedName

    data class State(
        val items: List<Item>,
        val bookmark: String,

        val progressVisible: Boolean,
        val error: String?
    )

    sealed class Action {
        object Start : Action()
        object StartProgress: Action()
        object StopProgress: Action()
        object RefreshRequired: Action()
        object LoadNextPage: Action()

        data class ShowFinished(val finished: List<Bet>, val draws: Map<String, Draw>) : Action()
        data class SetBookmark(val bookmark: String): Action()
        data class ShowError(val error: Throwable) : Action()
        object HideError: Action()
    }

    private val _state = MutableLiveData<State>()
    val state: LiveData<State> = _state

    private val _actions: Relay<Action> = PublishRelay.create()
    val actions: Consumer<Action> = _actions

    private val disposables = CompositeDisposable()

    init {
        disposables.add(
            _actions.reduxStore(
                initialState = State(
                    items = emptyList(),
                    bookmark = "",

                    progressVisible = false,
                    error = ""
                ),
                sideEffects = listOf(
                    ::start,
                    ::refreshRequired,
                    ::loadNextPage
                ),
                reducer = ::reducer
            )
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { newState ->
                    _state.value = newState
                })
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }

    sealed class Item {
        data class Draw(val drawId: String, val number1: String, val number2: String, val number3: String, val number4: String, val number5: String): Item()
        data class Bet(val bet: String, val count: String, val reward: String): Item()
    }


    private fun reducer(state: State, action: Action): State {
        return when (action) {
            is Action.ShowFinished -> state.copy(
                items = {
                    var lastDrawId: String? = null
                    var items = mutableListOf<Item>()
                    for (bet in action.finished) {
                        val drawId = bet.drawId
                        val bets = bet.bets
                        val count = bet.count
                        val draw = action.draws[drawId]?.draw ?: ""
                        if (draw.length == 0) {
                            continue
                        }

                        if (lastDrawId == null || lastDrawId != drawId) {
                            items.add(Item.Draw(
                                drawId,
                                draw?.substring(4, 5) ?: "",
                                draw?.substring(3, 4) ?: "",
                                draw?.substring(2, 3) ?: "",
                                draw?.substring(1, 2) ?: "",
                                draw?.substring(0, 1) ?: ""
                            ))
                        }

                        val rewards = DrawFactory.getApplicableRewards(draw)
                        val myBets = bets.split(",")
                        for (myBet in myBets) {
                            val reward = count * (rewards[myBet] ?: 0)
                            items.add(Item.Bet(BET_DISPLAY.get(myBet) ?: "", count.toString(), String.format("%d.%2d", reward / 100, reward % 100)))
                        }
                        lastDrawId = drawId
                    }
                    items
                }())
            is Action.SetBookmark -> state.copy(
                bookmark = action.bookmark
            )
            is Action.ShowError -> state.copy(
                error = stringProvider.get(action.error.message ?: ""),
                progressVisible = false
            )
            is Action.HideError -> state.copy(
                error = ""
            )
            is Action.StartProgress -> state.copy(
                progressVisible = true
            )
            is Action.StopProgress -> state.copy(
                progressVisible = false
            )
            else -> state
        }
    }


    private fun start(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.Start::class.java)
            .switchMap {
                reloadFinshed(state())
                    .mergeWith(Observable.just(Action.RefreshRequired)
                    )
            }

    private fun refreshRequired(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.RefreshRequired::class.java)
            .switchMap {
                fetchFinished(state(), "")
            }

    private fun loadNextPage(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.LoadNextPage::class.java)
            .switchMap {
                fetchFinished(state(), state().bookmark)
            }

    private fun fetchFinished(state: State, bookmark: String): Observable<Action> {
        val userId = sessionProvider.getUserId()
        return api.getFinished(
            userId ?: "",
            bookmark
        )
            .subscribeOn(Schedulers.newThread())
            .toObservable()
            .doOnNext {
                if (bookmark.isEmpty()) {
                    sessionProvider.setFinished(it.first)
                } else {
                    sessionProvider.addFinished(it.first)
                }
            }
            .map {
                val draws = sessionProvider.getFinishedDraws()
                var missing: MutableList<String> = ArrayList<String>()
                for (result in it.first) {
                    if (!draws.containsKey(result.drawId)) {
                        missing.add(result.drawId)
                    }
                }
                Pair(missing, it.second)
            }
            .flatMap {
                api.getDraws(it.first)
                    .toObservable()
                    .doOnNext {
                        sessionProvider.addFinishedDraws(it.filter {it.draw != null  && !it.draw.isEmpty() }.map { it.drawId to it }.toMap())
                    }
                    .map {draws ->
                        it.second
                    }
            }
            .flatMap {
                reloadFinshed(state)
                    .startWith(Action.SetBookmark(it))
            }
            .concatWith(Observable.just(Action.StopProgress))
            .onErrorReturn { error -> Action.ShowError(error) }
            .startWith(Action.StartProgress)
    }

    private fun reloadFinshed(state: State): Observable<Action> {
        val finished = sessionProvider.getFinished()
        val draws = sessionProvider.getFinishedDraws()
        return Observable.just(Action.ShowFinished(finished, draws) as Action)

    }

    class Factory(val api: ApiProvider, val sessionProvider: SessionProvider, val stringProvider: StringProvider) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return FinishedViewModel(api, sessionProvider, stringProvider) as T
        }
    }
}