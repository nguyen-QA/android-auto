package com.caipiao.caipiao.ui.me

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.InputFilter
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.caipiao.caipiao.R
import com.caipiao.caipiao.providers.SessionProvider
import com.caipiao.caipiao.ui.AppActivity
import com.caipiao.caipiao.ui.auth.AuthIntent
import com.caipiao.caipiao.ui.finished.FinishedIntent
import com.caipiao.caipiao.ui.history.HistoryIntent
import com.caipiao.caipiao.ui.refer.ReferIntent
import com.caipiao.caipiao.ui.unfinished.UnfinishedIntent
import com.caipiao.caipiao.ui.xyttc.XyttcIntent
import com.shopify.livedataktx.distinct
import com.shopify.livedataktx.map
import dagger.android.HasAndroidInjector
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.content_me.*
import kotlinx.android.synthetic.main.content_me.buyButton
import kotlinx.android.synthetic.main.content_me.finishButton
import kotlinx.android.synthetic.main.content_me.historyButton
import kotlinx.android.synthetic.main.content_me.progressBar
import kotlinx.android.synthetic.main.content_me.submitButton
import kotlinx.android.synthetic.main.content_me.unfinishButton
import kotlinx.android.synthetic.main.custom_actionbar.*
import javax.inject.Inject

fun Context.MeIntent(): Intent {
    return Intent(this, MeActivity::class.java).apply {

    }
}

class MeActivity : AppActivity(), HasAndroidInjector {
    @Inject
    lateinit var viewModelFactory: MeViewModel.Factory

    val viewModel: MeViewModel
        get() = _viewModel as MeViewModel

    private lateinit var view: View
    private val TAG = MeActivity::class.qualifiedName
    private val disposables = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _viewModel = ViewModelProviders.of(this, viewModelFactory).get(MeViewModel::class.java)

        setContentView(R.layout.activity_me)

        view = findViewById<View>(android.R.id.content)
        val mInflater = LayoutInflater.from(this)

        val customView = mInflater.inflate(R.layout.custom_actionbar, null)

        val actionBar = supportActionBar
        actionBar!!.setDisplayShowHomeEnabled(true)
        actionBar.setDisplayShowTitleEnabled(true)
        actionBar.title = stringProvider.get(R.string.xyttc)
        actionBar.setDisplayShowCustomEnabled(true)
        actionBar.setCustomView(customView)
        titleImageView.setImageResource(sessionProvider.getTitleDrawable())

        promoEditText.filters = arrayOf(InputFilter.AllCaps())

        buyButton.setOnClickListener {
            startActivity(XyttcIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION))
            finish()
            overridePendingTransition(0, 0)
        }
        unfinishButton.setOnClickListener {
            startActivity(UnfinishedIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION))
            finish()
            overridePendingTransition(0, 0)
        }
        finishButton.setOnClickListener {
            startActivity(FinishedIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION))
            finish()
            overridePendingTransition(0, 0)
        }
        historyButton.setOnClickListener {
            startActivity(HistoryIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION))
            finish()
            overridePendingTransition(0, 0)
        }

        depositButton.setOnClickListener {
            viewModel.actions.accept(MeViewModel.Action.ShowDeposit)
        }
        withdrawButton.setOnClickListener {
            viewModel.actions.accept(MeViewModel.Action.ShowWithdraw)
        }

        promoEditText.addTextChangedListener { editable ->
            viewModel.actions.accept(MeViewModel.Action.InvitationTokenChanged(editable?.toString() ?: ""))
        }
        promoEditText.setOnFocusChangeListener { view, hasFocus ->
            (view as EditText).isCursorVisible = hasFocus
        }
        countEditText.addTextChangedListener { editable ->
            viewModel.actions.accept(MeViewModel.Action.RequestCountChanged(editable?.toString() ?: ""))
        }
        countEditText.setOnFocusChangeListener { view, hasFocus ->
            (view as EditText).isCursorVisible = hasFocus
        }
        submitButton.setOnClickListener {
            promoEditText.clearFocus()
            val imm =
                getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(
                promoEditText.windowToken,
                InputMethodManager.HIDE_NOT_ALWAYS
            )
            viewModel.actions.accept(MeViewModel.Action.SubmitToken)
        }
        submitRequestButton.setOnClickListener {
            countEditText.clearFocus()
            val imm =
                getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(
                countEditText.windowToken,
                InputMethodManager.HIDE_NOT_ALWAYS
            )
            viewModel.actions.accept(MeViewModel.Action.SubmitRequest)
        }

        generateTokenButton.setOnClickListener {
            viewModel.actions.accept(MeViewModel.Action.ShowToken)
        }

        viewModel.state.map { state ->
                state!!.balance
            }
            .distinct()
            .observe(this, Observer { balance ->
                balanceTextView.text = balance
            })

        viewModel.state.map { state ->
            state!!.isMember
        }
            .distinct()
            .observe(this, Observer { isMember ->
                if (isMember == null) {
                    depositButton.visibility = View.GONE
                    withdrawButton.visibility = View.GONE
                    requestBorder.visibility = View.GONE
                    countEditText.visibility = View.GONE
                    submitRequestButton.visibility = View.GONE
                    generateTokenButton.visibility = View.GONE
                    promoEditText.visibility = View.GONE
                    submitButton.visibility = View.GONE
                } else if (isMember) {
                    depositButton.visibility = View.VISIBLE
                    withdrawButton.visibility = View.VISIBLE
                    requestBorder.visibility = View.GONE
                    countEditText.visibility = View.GONE
                    submitRequestButton.visibility = View.GONE
                    generateTokenButton.visibility = View.VISIBLE
                    promoEditText.visibility = View.GONE
                    submitButton.visibility = View.GONE
                } else {
                    depositButton.visibility = View.GONE
                    withdrawButton.visibility = View.GONE
                    requestBorder.visibility = View.GONE
                    countEditText.visibility = View.GONE
                    submitRequestButton.visibility = View.GONE
                    generateTokenButton.visibility = View.GONE
                    promoEditText.visibility = View.VISIBLE
                    submitButton.visibility = View.VISIBLE
                }
            })

        viewModel.state.map { state ->
            state!!.depositVisible
        }
            .distinct()
            .observe(this, Observer { depositVisible ->
                if (depositVisible) {
                    depositButton.setBackgroundColor(resources.getColor(R.color.palePeach))
                    withdrawButton.background = resources.getDrawable(R.drawable.btn_unselected_bg)
                    depositButton.setTextColor(Color.BLACK)
                    withdrawButton.setTextColor(Color.WHITE)
                    requestBorder.visibility = View.VISIBLE
                    countEditText.visibility = View.VISIBLE
                    submitRequestButton.visibility = View.VISIBLE
                }
            })

        viewModel.state.map { state ->
            state!!.withdrawVisible
        }
            .distinct()
            .observe(this, Observer { withdrawVisible ->
                if (withdrawVisible) {
                    depositButton.background = resources.getDrawable(R.drawable.btn_unselected_bg)
                    withdrawButton.setBackgroundColor(resources.getColor(R.color.palePeach))
                    depositButton.setTextColor(Color.WHITE)
                    withdrawButton.setTextColor(Color.BLACK)
                    requestBorder.visibility = View.VISIBLE
                    countEditText.visibility = View.VISIBLE
                    submitRequestButton.visibility = View.VISIBLE
                }
            })

        viewModel.state.map { state ->
            state!!.submitRequestText
        }
            .distinct()
            .observe(this, Observer { submitRequestText ->
                submitRequestButton.setText(submitRequestText)
            })

        viewModel.state.map { state ->
                state!!.submitRequestEnabled
            }
            .distinct()
            .observe(this, Observer { submitRequestEnabled ->
                if (submitRequestEnabled) {
                    submitRequestButton.setBackgroundColor(resources.getColor(R.color.palePeach))
                    submitRequestButton.setTextColor(Color.BLACK)
                } else {
                    submitRequestButton.setBackgroundColor(resources.getColor(R.color.mudBrown))
                    submitRequestButton.setTextColor(Color.GRAY)
                }
                submitRequestButton.isEnabled = submitRequestEnabled
            })

        viewModel.state.map { state ->
                state!!.progressVisible
            }
            .distinct()
            .observe(this, Observer { progressVisible ->
                progressBar.visibility = if (progressVisible) View.VISIBLE else View.GONE
            })

        viewModel.state.map { state ->
            state!!.showLogin
        }
            .distinct()
            .observe(this, Observer { showLogin ->
                if (showLogin) {
                    startActivity(AuthIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION))
                    finish()
                    overridePendingTransition(0, 0)
                    viewModel.actions.accept(MeViewModel.Action.HideLogin)
                }
            })

        viewModel.state.map { state ->
                state!!.showToken
            }
            .distinct()
            .observe(this, Observer { showToken ->
                if (showToken) {
                    startActivity(ReferIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION))
                    finish()
                    overridePendingTransition(0, 0)
                    viewModel.actions.accept(MeViewModel.Action.HideToken)
                }
            })

        viewModel.state.map { state ->
            state!!.showInvalidToken
        }
            .distinct()
            .observe(this, Observer { showInvalidToken ->
                if (showInvalidToken) {
                    Toast.makeText(applicationContext, stringProvider.get(R.string.invalid_refer_token), Toast.LENGTH_LONG).show()
                    viewModel.actions.accept(MeViewModel.Action.HideInvalidToken)
                }
            })

        viewModel.state.map { state ->
                state!!.showRequestSuccess
            }
            .distinct()
            .observe(this, Observer { showRequestSuccess ->
                if (showRequestSuccess) {
                    Toast.makeText(applicationContext, stringProvider.get(R.string.request_tran_success), Toast.LENGTH_LONG).show()
                    countEditText.setText("")
                    viewModel.actions.accept(MeViewModel.Action.HideInvalidToken)
                }
            })

        viewModel.state.map { state ->
            state!!.submitEnabled
        }
            .distinct()
            .observe(this, Observer { submitEnabled ->
                if (submitEnabled) {
                    submitButton.setBackgroundColor(resources.getColor(R.color.palePeach))
                    submitButton.setTextColor(Color.BLACK)
                } else {
                    submitButton.setBackgroundColor(resources.getColor(R.color.mudBrown))
                    submitButton.setTextColor(Color.GRAY)
                }
                submitButton.isEnabled = submitEnabled
            })

        viewModel.state.map { state ->
                state!!.error
            }
            .distinct()
            .observe(this, Observer { error ->
                if (!error.isEmpty()) {
                    val msgs = error.split("|")
                    if (msgs.size == 2) {
                        Toast.makeText(applicationContext, stringProvider.get(msgs[0]) + "(" + msgs[1] + ")", Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(applicationContext, error, Toast.LENGTH_LONG).show()
                    }
                    viewModel.actions.accept(MeViewModel.Action.HideError)
                }
            })

        viewModel.actions.accept(MeViewModel.Action.Start)

    }

}
