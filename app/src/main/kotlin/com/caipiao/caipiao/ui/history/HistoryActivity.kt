package com.caipiao.caipiao.ui.history

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.caipiao.caipiao.R
import com.caipiao.caipiao.providers.SessionProvider
import com.caipiao.caipiao.ui.AppActivity
import com.caipiao.caipiao.ui.finished.FinishedIntent
import com.caipiao.caipiao.ui.me.MeIntent
import com.caipiao.caipiao.ui.unfinished.UnfinishedIntent
import com.caipiao.caipiao.ui.xyttc.XyttcIntent
import com.shopify.livedataktx.distinct
import com.shopify.livedataktx.map
import dagger.android.HasAndroidInjector
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.content_history.*
import kotlinx.android.synthetic.main.content_history.buyButton
import kotlinx.android.synthetic.main.content_history.finishButton
import kotlinx.android.synthetic.main.content_history.mineButton
import kotlinx.android.synthetic.main.content_history.progressBar
import kotlinx.android.synthetic.main.content_history.unfinishButton
import kotlinx.android.synthetic.main.custom_actionbar.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

fun Context.HistoryIntent(): Intent {
    return Intent(this, HistoryActivity::class.java).apply {

    }
}

class HistoryActivity : AppActivity(), HasAndroidInjector {
    @Inject
    lateinit var viewModelFactory: HistoryViewModel.Factory

    val viewModel: HistoryViewModel
        get() = _viewModel as HistoryViewModel

    private lateinit var view: View
    private val TAG = HistoryActivity::class.qualifiedName
    private val disposables = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _viewModel = ViewModelProviders.of(this, viewModelFactory).get(HistoryViewModel::class.java)

        setContentView(R.layout.activity_history)

        view = findViewById<View>(android.R.id.content)
        val mInflater = LayoutInflater.from(this)

        val customView = mInflater.inflate(R.layout.custom_actionbar, null)

        val actionBar = supportActionBar
        actionBar!!.setDisplayShowHomeEnabled(true)
        actionBar.setDisplayShowTitleEnabled(true)
        actionBar.title = stringProvider.get(R.string.xyttc)
        actionBar.setDisplayShowCustomEnabled(true)
        actionBar.setCustomView(customView)
        titleImageView.setImageResource(sessionProvider.getTitleDrawable())

        (recyclerView as RecyclerView).apply {
            setHasFixedSize(true)

            layoutManager = LinearLayoutManager(this@HistoryActivity)
            adapter = Adapter(listOf(), SessionProvider.HistoryToggle.NUMBER)
        }

        numberButton.setOnClickListener {
            viewModel.actions.accept(HistoryViewModel.Action.HistoryToggled(SessionProvider.HistoryToggle.NUMBER))
        }
        smallLargeButton.setOnClickListener {
            viewModel.actions.accept(HistoryViewModel.Action.HistoryToggled(SessionProvider.HistoryToggle.SMALL_BIG))
        }
        oddEvenButton.setOnClickListener {
            viewModel.actions.accept(HistoryViewModel.Action.HistoryToggled(SessionProvider.HistoryToggle.ODD_EVEN))
        }
        specialButton.setOnClickListener {
            viewModel.actions.accept(HistoryViewModel.Action.HistoryToggled(SessionProvider.HistoryToggle.SPECIAL))
        }


        buyButton.setOnClickListener {
            startActivity(XyttcIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION))
            finish()
            overridePendingTransition(0, 0)
        }
        unfinishButton.setOnClickListener {
            startActivity(UnfinishedIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION))
            finish()
            overridePendingTransition(0, 0)
        }
        finishButton.setOnClickListener {
            startActivity(FinishedIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION))
            finish()
            overridePendingTransition(0, 0)
        }
        mineButton.setOnClickListener {
            startActivity(MeIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION))
            finish()
            overridePendingTransition(0, 0)
        }

        disposables.add(
            endOfRecyclerViewReached
                .map { HistoryViewModel.Action.LoadNextPage }
                .subscribe(viewModel.actions)
        )


        viewModel.state.map { state ->
                state!!.toggle
            }
            .distinct()
            .observe(this, Observer { toggle ->
                ((recyclerView as RecyclerView).adapter as Adapter).update(toggle)

                numberButton.setTextColor(if (toggle == SessionProvider.HistoryToggle.NUMBER) Color.parseColor("#000000") else Color.parseColor("#FFFFFF"))
                numberButton.background = if (toggle == SessionProvider.HistoryToggle.NUMBER) resources.getDrawable(R.drawable.btn_selected_bg) else resources.getDrawable(R.drawable.btn_unselected_bg)
                smallLargeButton.setTextColor(if (toggle == SessionProvider.HistoryToggle.SMALL_BIG) Color.parseColor("#000000") else Color.parseColor("#FFFFFF"))
                smallLargeButton.background = if (toggle == SessionProvider.HistoryToggle.SMALL_BIG) resources.getDrawable(R.drawable.btn_selected_bg) else resources.getDrawable(R.drawable.btn_unselected_bg)
                oddEvenButton.setTextColor(if (toggle == SessionProvider.HistoryToggle.ODD_EVEN) Color.parseColor("#000000") else Color.parseColor("#FFFFFF"))
                oddEvenButton.background = if (toggle == SessionProvider.HistoryToggle.ODD_EVEN) resources.getDrawable(R.drawable.btn_selected_bg) else resources.getDrawable(R.drawable.btn_unselected_bg)
                specialButton.setTextColor(if (toggle == SessionProvider.HistoryToggle.SPECIAL) Color.parseColor("#000000") else Color.parseColor("#FFFFFF"))
                specialButton.background = if (toggle == SessionProvider.HistoryToggle.SPECIAL) resources.getDrawable(R.drawable.btn_selected_bg) else resources.getDrawable(R.drawable.btn_unselected_bg)
            })

        viewModel.state.map { state ->
                state!!.items
            }
            .distinct()
            .observe(this, Observer { items ->
                ((recyclerView as RecyclerView).adapter as Adapter).update(items)
            })

        viewModel.state.map { state ->
                state!!.progressVisible
            }
            .distinct()
            .observe(this, Observer { progressVisible ->
                progressBar.visibility = if (progressVisible) View.VISIBLE else View.GONE
            })

        viewModel.state.map { state ->
                state!!.error
            }
            .distinct()
            .observe(this, Observer { error ->
                if (!error.isEmpty()) {
                    val msgs = error.split("|")
                    if (msgs.size == 2) {
                        Toast.makeText(applicationContext, stringProvider.get(msgs[0]) + "(" + msgs[1] + ")", Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(applicationContext, error, Toast.LENGTH_LONG).show()
                    }
                    viewModel.actions.accept(HistoryViewModel.Action.HideError)
                }
            })

        viewModel.actions.accept(HistoryViewModel.Action.Start)

    }

    val endOfRecyclerViewReached = Observable.create<Unit> { emitter ->
        val listener = object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                val endReached = !recyclerView!!.canScrollVertically(1)
                if (endReached) {
                    emitter.onNext(Unit)
                }
            }
        }

        emitter.setCancellable { recyclerView.removeOnScrollListener(listener) }

        recyclerView.addOnScrollListener(listener)
    }.debounce(200, TimeUnit.MILLISECONDS)


    inner class Adapter(private var data: List<HistoryViewModel.Item>, private var toggle: SessionProvider.HistoryToggle) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
        private val VIEW_TYPE_SINGLE_DRAW = 1
        private val VIEW_TYPE_SPECIAL_DRAW = 2

        inner class SingleDrawViewHolder(@NonNull val contentView: View) : RecyclerView.ViewHolder(contentView) {
            private val drawTextView: TextView = contentView.findViewById(R.id.drawTextView) as TextView
            private val number5TextView: TextView = contentView.findViewById(R.id.number5TextView) as TextView
            private val number4TextView: TextView = contentView.findViewById(R.id.number4TextView) as TextView
            private val number3TextView: TextView = contentView.findViewById(R.id.number3TextView) as TextView
            private val number2TextView: TextView = contentView.findViewById(R.id.number2TextView) as TextView
            private val number1TextView: TextView = contentView.findViewById(R.id.number1TextView) as TextView

            fun bind(draw: HistoryViewModel.Item.SingleDraw) {
                drawTextView.text = draw.drawId
                number1TextView.text = draw.number1
                number1TextView.background = backgroundFor(draw.number1)
                number2TextView.text = draw.number2
                number2TextView.background = backgroundFor(draw.number2)
                number3TextView.text = draw.number3
                number3TextView.background = backgroundFor(draw.number3)
                number4TextView.text = draw.number4
                number4TextView.background = backgroundFor(draw.number4)
                number5TextView.text = draw.number5
                number5TextView.background = backgroundFor(draw.number5)
            }

        }

        inner class SpecialDrawViewHolder(@NonNull val contentView: View) : RecyclerView.ViewHolder(contentView) {
            private val drawTextView: TextView = contentView.findViewById(R.id.drawTextView) as TextView
            private val largeSmallTextView: TextView = contentView.findViewById(R.id.largeSmallTextView) as TextView
            private val oddEvenTextView: TextView = contentView.findViewById(R.id.oddEvenTextView) as TextView
            private val lhhTextView: TextView = contentView.findViewById(R.id.lhhTextView) as TextView
            private val frontTextView: TextView = contentView.findViewById(R.id.frontTextView) as TextView
            private val midTextView: TextView = contentView.findViewById(R.id.midTextView) as TextView
            private val endTextView: TextView = contentView.findViewById(R.id.endTextView) as TextView

            fun bind(draw: HistoryViewModel.Item.SpecialDraw) {
                drawTextView.text = draw.drawId
                largeSmallTextView.text = draw.largeSmall
                largeSmallTextView.background = backgroundFor(draw.largeSmall)
                oddEvenTextView.text = draw.oddEven
                oddEvenTextView.background = backgroundFor(draw.oddEven)
                lhhTextView.text = draw.lhh
                lhhTextView.background = backgroundFor(draw.lhh)
                frontTextView.text = draw.front
                frontTextView.setTextColor(textColorFor(draw.front))
                midTextView.text = draw.mid
                midTextView.setTextColor(textColorFor(draw.mid))
                endTextView.text = draw.end
                endTextView.setTextColor(textColorFor(draw.end))
            }

        }

        private fun textColorFor(text: String): Int {
            return when(text) {
                stringProvider.get(R.string.baozi) ->
                    resources.getColor(R.color.yellow)
                stringProvider.get(R.string.duizi) ->
                    resources.getColor(R.color.green)
                stringProvider.get(R.string.shunzi) ->
                    resources.getColor(R.color.orange)
                else ->
                    resources.getColor(R.color.black)
            }
        }

        private fun backgroundFor(text: String): Drawable {
            return when(text) {
                stringProvider.get(R.string.small), stringProvider.get(R.string.odd), stringProvider.get(R.string.leong) ->
                    resources.getDrawable(R.drawable.green_circle_background)
                stringProvider.get(R.string.he) ->
                    resources.getDrawable(R.drawable.yellow_circle_background)
                else ->
                    resources.getDrawable(R.drawable.orange_circle_background)
            }
        }


        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            when(viewType) {
                VIEW_TYPE_SINGLE_DRAW -> {
                    val view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.history_single, parent, false)
                    return SingleDrawViewHolder(view)
                }
                else -> {
                    val view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.history_summary, parent, false)
                    return SpecialDrawViewHolder(view)
                }
            }
        }

        override fun getItemCount(): Int {
            return data.size
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            when(holder) {
                is SingleDrawViewHolder -> holder.bind(data[position] as HistoryViewModel.Item.SingleDraw)
                is SpecialDrawViewHolder -> holder.bind(data[position] as HistoryViewModel.Item.SpecialDraw)
                else -> {}
            }

        }

        override fun getItemViewType(position: Int): Int {
            if (data[position] is HistoryViewModel.Item.SingleDraw) {
                return VIEW_TYPE_SINGLE_DRAW
            } else {
                return VIEW_TYPE_SPECIAL_DRAW
            }
        }

        fun update(items: List<HistoryViewModel.Item>) {
            val oldCount = data.size
            data = items
            notifyItemRangeInserted(oldCount, items.size - oldCount)
        }

        fun update(toggle: SessionProvider.HistoryToggle) {
            this.toggle = toggle
            notifyDataSetChanged()
        }
    }

}
