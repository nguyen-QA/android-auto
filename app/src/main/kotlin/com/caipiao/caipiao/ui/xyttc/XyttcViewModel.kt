package com.caipiao.caipiao.ui.xyttc

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.caipiao.caipiao.R
import com.caipiao.caipiao.model.Draw
import com.caipiao.caipiao.providers.ApiProvider
import com.caipiao.caipiao.providers.SessionProvider
import com.caipiao.caipiao.providers.StringProvider
import com.caipiao.caipiao.ui.AppViewModel
import com.freeletics.rxredux.StateAccessor
import com.freeletics.rxredux.reduxStore
import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class XyttcViewModel (
    val api: ApiProvider,
    val sessionProvider: SessionProvider,
    val stringProvider: StringProvider
) : AppViewModel() {
    private val TAG = XyttcViewModel::class.qualifiedName
    private val special = setOf("o", "e", "S", "L", ">", "<", "=",
        "===--", "<<<--", "=+=--",
        "-===-", "-<<<-", "-=+=-",
        "--===", "--<<<", "--=+=")
    private val singleBall = setOf("o", "e", "S", "L", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9")

    private val _state = MutableLiveData<State>()
    val state: LiveData<State> = _state

    private val _actions: Relay<Action> = PublishRelay.create()
    val actions: Consumer<Action> = _actions

    data class State(
        val lastDraw: LastDrawData,
        val currentDraw: CurrentDrawData,
        val betOptions: BetOptionsData,

        val singleBallBets: Set<String>,
        val specialBets: Set<String>,

        val submitVisible: Boolean,
        val submitEnable: Boolean,
        val betCount: Int,

        val times: String,

        val popupSubmitSuccess: Boolean,

        val progressVisible: Boolean,
        val error: String
    )

    sealed class Action {
        object Start: Action()
        object StartProgress: Action()
        object StopProgress: Action()
        object DrawsRefreshRequired: Action()
        data class BetCategoryPicked(val category: String): Action()
        data class BetPicked(val bet: String): Action()
        object TrashBets: Action()
        object SubmitBets: Action()
        data class TimesChanged(val text: String): Action()
        object GetNtp: Action()
        object FetchHistory: Action()

        object ShowHistory: Action()
        data class ShowDraw(val drawId: String, val formattedStopTime: String, val formattedEndTime: String): Action()
        data class ShowSpecialBets(val bets: Set<String>): Action()
        data class ShowSingleBallBets(val bets: Set<String>): Action()
        data class ShowBetCategory(val category: String): Action()
        data class ShowSubmit(val count: Int): Action()
        data class ShowTimes(val times: String): Action()
        object ShowSubmitSuccess: Action()
        object HideSubmitSuccess: Action()
        object HideSubmit: Action()
        data class ShowError(val error: Throwable): Action()
        object HideError: Action()
    }

    data class LastDrawData(val drawId: String, val number1: String, val number2: String, val number3: String, val number4: String, val number5: String)
    data class CurrentDrawData(val drawId: String, val formattedStopTime: String, val formattedEndTime: String)
    data class BetOptionsData(val betCategoryTagToggledOn: String, val speciallBetVisible: Boolean)

    private val disposables = CompositeDisposable()

    init {
        disposables.add(
            _actions.reduxStore(
                initialState = State(
                    lastDraw = LastDrawData("", "", "", "", "", ""),
                    currentDraw = CurrentDrawData("", "", ""),
                    betOptions = BetOptionsData("c1", false),

                    singleBallBets = emptySet(),
                    specialBets = emptySet(),

                    submitVisible = false,
                    submitEnable = true,
                    betCount = 0,

                    times = "10",

                    popupSubmitSuccess = false,

                    progressVisible = false,
                    error = ""
                ),
                sideEffects = listOf(
                    ::start,
                    ::drawsRefreshRequired,
                    ::betCategoryPicked,
                    ::betPicked,
                    ::trashBets,
                    ::submitBets,
                    ::timesChanged,
                    ::getNtp,
                    ::fetchHistory
                ),
                reducer = ::reducer)
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { newState ->
                    _state.value = newState
                })
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }

    private fun reducer(state: State, action: Action): State {
        return when(action) {
            is Action.ShowHistory -> state.copy(
                lastDraw = {
                    val history = sessionProvider.getDrawHistory()
                    val draw = history[0]
                    val n1 = draw.draw.substring(0, 1)
                    val n2 = draw.draw.substring(1, 2)
                    val n3 = draw.draw.substring(2, 3)
                    val n4 = draw.draw.substring(3, 4)
                    val n5 = draw.draw.substring(4, 5)
                    LastDrawData(
                        if (draw.drawId.isEmpty()) "" else draw.drawId + stringProvider.get(R.string.draw),
                        n1,
                        n2,
                        n3,
                        n4,
                        n5
                    )
                }())
            is Action.ShowDraw -> state.copy(
                currentDraw = CurrentDrawData(action.drawId, action.formattedStopTime, action.formattedEndTime)
            )
            is Action.ShowBetCategory -> state.copy(
                betOptions = BetOptionsData("c" + action.category, action.category == "s")
            )
            is Action.ShowSingleBallBets -> state.copy(
                singleBallBets = action.bets
            )
            is Action.ShowSpecialBets -> state.copy(
                specialBets = action.bets
            )
            is Action.ShowSubmit -> state.copy(
                submitVisible = true,
                betCount = action.count
            )
            is Action.HideSubmit -> state.copy(
                submitVisible = false,
                betCount = 0
            )
            is Action.ShowTimes -> state.copy(
                times = action.times,
                submitEnable = action.times.toIntOrNull() ?: 0 > 0
            )
            is Action.ShowSubmitSuccess -> state.copy(
                popupSubmitSuccess = true
            )
            is Action.HideSubmitSuccess -> state.copy(
                popupSubmitSuccess = false
            )
            is Action.ShowError -> state.copy(
                error = stringProvider.get(action.error.message ?: ""),
                progressVisible = false
            )
            is Action.HideError -> state.copy(
                error = ""
            )
            is Action.StartProgress -> state.copy(
                progressVisible = true
            )
            is Action.StopProgress -> state.copy(
                progressVisible = false
            )
            else -> state
        }
    }

    private fun start(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.Start::class.java)
            .switchMap {
                reloadBets(state())
                    .mergeWith(Observable.just(Action.GetNtp, Action.DrawsRefreshRequired)
                        )
                    .concatWith(startCurrentDrawTimer(state()))
            }

    private fun drawsRefreshRequired(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.DrawsRefreshRequired::class.java)
            .switchMap {
                fetchCurrentDraws(state())
            }

    private fun betCategoryPicked(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.BetCategoryPicked::class.java)
            .switchMap { action ->
                sessionProvider.setCurrentBetCategory(action.category)
                reloadBets(state())
            }

    private fun betPicked(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.BetPicked::class.java)
            .switchMap { action ->
                when(val category = sessionProvider.getCurrentBetCategory()) {
                    "s" -> sessionProvider.toggleBet(action.bet)

                    else -> {
                        val i = category.toInt() - 1
                        val bet = "-".repeat(i) + action.bet + "-".repeat(4 - i)
                        sessionProvider.toggleBet(bet)
                    }
                }
                reloadBets(state())
            }

    private fun timesChanged(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.TimesChanged::class.java)
            .switchMap { action ->
                sessionProvider.setCurrentTimes(action.text)
                reloadBets(state())
            }

    private fun trashBets(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.TrashBets::class.java)
            .switchMap {
                sessionProvider.removeAllBets()
                reloadBets(state())
            }

    private fun submitBets(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.SubmitBets::class.java)
            .switchMap {
                val bets = sessionProvider.getCurrentBets().keys.sorted().joinToString(separator = ",")
                api.buyLottery(
                    userId = sessionProvider.getUserId() ?: "",
                    name = "xyttc",
                    bets = bets,
                    count = sessionProvider.getCurrentTimes().toInt(),
                    plan = ""
                )
                    .subscribeOn(Schedulers.newThread())
                    .andThen(Observable.just(Action.ShowSubmitSuccess as Action))
                    .concatWith(Observable.just(Action.StopProgress))
                    .concatWith(Observable.just(Action.TrashBets))
                    .onErrorReturn { error -> Action.ShowError(error) }
                    .startWith(Action.StartProgress)
            }

    private fun getNtp(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.GetNtp::class.java)
            .switchMap {
                api.getNtp()
                .subscribeOn(Schedulers.newThread())
                .toObservable()
                .map { Action.StopProgress as Action }
                .onErrorReturn { error -> Action.ShowError(error) }
                .startWith(Action.StartProgress)
            }

    private fun fetchCurrentDraws(state: State): Observable<Action> {
                return api.getCurrentDraws()
                    .subscribeOn(Schedulers.newThread())
                    .toObservable()
                    .flatMap<Action> {
                        val timestamp = sessionProvider.timestamp()
                        val lastDraw = sessionProvider.getLastDraw()
                        val delay = if (lastDraw.startTime > timestamp) (lastDraw.startTime - timestamp + Math.random() * (lastDraw.endTime - lastDraw.startTime)).toLong() else 0
                        Observable.just(Action.DrawsRefreshRequired)
                            .delay(delay, TimeUnit.SECONDS)
                    }
                    .concatWith(Observable.just(Action.StopProgress))
                    .onErrorReturn { error -> Action.ShowError(error) }
                    .startWith(Action.StartProgress)
            }

    private fun startCurrentDrawTimer(state: State): Observable<Action> {
        return sessionProvider.observeCurrentDraw()
            .switchMap { draw ->
                val timestamp = sessionProvider.timestamp()
                val duration = draw.endTime - timestamp

                Observable.interval(0, 1, TimeUnit.SECONDS)
                    .take(duration)
                    .map { seconds ->
                        val remaingSeconds = duration - seconds - 1
                        Action.ShowDraw(
                            if (draw.drawId.isEmpty()) "" else draw.drawId + stringProvider.get(R.string.draw),
                            formatStopTimer(draw, remaingSeconds),
                            formatEndTimer(draw, remaingSeconds)
                        ) as Action
                    }
                    .onErrorReturn { error ->
                        Log.w(TAG, error)
                        Action.ShowError(error)
                    }
                    .startWith(
                        Observable.just(Action.FetchHistory)
                            .delay(1, TimeUnit.SECONDS)
                    )
            }
    }

    private fun reloadBets(state: State): Observable<Action> {
        val category = sessionProvider.getCurrentBetCategory()
        val bets = sessionProvider.getCurrentBets()
        val times = sessionProvider.getCurrentTimes()
        return when (category) {
            "s" -> Observable.just(Action.ShowSpecialBets(bets.keys intersect special) as Action)

            else -> {
                val visibleBets = singleBall
                    .map {
                        val i = category.toInt() - 1
                        "-".repeat(i) + it + "-".repeat(4 - i)
                    }
                val tags = (bets.keys subtract special intersect visibleBets)
                    .map {
                        it.replace("-", "")
                    }
                    .toSet()
                Observable.just(Action.ShowSingleBallBets(tags) as Action)
            }
        }
            .startWith(Action.ShowBetCategory(category))
            .concatWith(Observable.just(if (bets.isEmpty()) Action.HideSubmit else Action.ShowSubmit(bets.size)))
            .concatWith(Observable.just(Action.ShowTimes(times)))
    }

    private fun fetchHistory(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.FetchHistory::class.java)
            .switchMap {
                val userId = sessionProvider.getUserId()
                api.getDrawHistory(
                    "xyttc",
                    ""
                )
                    .subscribeOn(Schedulers.newThread())
                    .toObservable()
                    .doOnNext {
                        sessionProvider.setDrawHistory(it.first)
                    }
                    .flatMap {
                        Observable.just(Action.ShowHistory as Action)
                    }
                    .concatWith(Observable.just(Action.StopProgress))
                    .onErrorReturn { error -> Action.ShowError(error) }
                    .startWith(Action.StartProgress)
            }

    private fun formatEndTimer(draw: Draw, remaingSeconds: Long): String {
        val endDuration = draw.endTime - draw.stopTime
        if (endDuration > remaingSeconds ) {
            return formatTimer(remaingSeconds)
        } else {
            return formatTimer(endDuration)
        }
    }

    private fun formatStopTimer(draw: Draw, remaingSeconds: Long): String {
        val endDuration = draw.endTime - draw.stopTime
        val stopDuration = draw.stopTime - draw.startTime
        if (endDuration > remaingSeconds ) {
            return formatTimer(0)
        } else if (stopDuration > remaingSeconds - endDuration){
            return formatTimer(remaingSeconds - endDuration)
        } else {
            return formatTimer(stopDuration)
        }
    }

    private fun formatTimer(seconds: Long): String {
        return String.format("%02d:%02d", (seconds % 3600) / 60, (seconds % 60))
    }

    class Factory(val api: ApiProvider, val sessionProvider: SessionProvider, val stringProvider: StringProvider) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return XyttcViewModel(api, sessionProvider, stringProvider) as T
        }
    }
}