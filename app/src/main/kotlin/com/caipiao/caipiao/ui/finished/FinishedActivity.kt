package com.caipiao.caipiao.ui.finished

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.caipiao.caipiao.R
import com.caipiao.caipiao.providers.SessionProvider
import com.caipiao.caipiao.ui.AppActivity
import com.caipiao.caipiao.ui.history.HistoryIntent
import com.caipiao.caipiao.ui.me.MeIntent
import com.caipiao.caipiao.ui.unfinished.UnfinishedIntent
import com.caipiao.caipiao.ui.xyttc.XyttcIntent
import com.shopify.livedataktx.distinct
import com.shopify.livedataktx.map
import dagger.android.HasAndroidInjector
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.content_finished.*
import kotlinx.android.synthetic.main.content_finished.buyButton
import kotlinx.android.synthetic.main.content_finished.historyButton
import kotlinx.android.synthetic.main.content_finished.mineButton
import kotlinx.android.synthetic.main.content_finished.progressBar
import kotlinx.android.synthetic.main.content_finished.unfinishButton
import kotlinx.android.synthetic.main.custom_actionbar.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

fun Context.FinishedIntent(): Intent {
    return Intent(this, FinishedActivity::class.java).apply {

    }
}

class FinishedActivity : AppActivity(), HasAndroidInjector {
    @Inject
    lateinit var viewModelFactory: FinishedViewModel.Factory

    val viewModel: FinishedViewModel
        get() = _viewModel as FinishedViewModel

    lateinit var view: View

    private val TAG = FinishedActivity::class.qualifiedName
    private val disposables = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _viewModel = ViewModelProviders.of(this, viewModelFactory).get(FinishedViewModel::class.java)

        setContentView(R.layout.activity_finished)

        view = findViewById<View>(android.R.id.content)
        val mInflater = LayoutInflater.from(this)

        val customView = mInflater.inflate(R.layout.custom_actionbar, null)

        val actionBar = supportActionBar
        actionBar!!.setDisplayShowHomeEnabled(true)
        actionBar.setDisplayShowTitleEnabled(true)
        actionBar.title = stringProvider.get(R.string.xyttc)
        actionBar.setDisplayShowCustomEnabled(true)
        actionBar.setCustomView(customView)
        titleImageView.setImageResource(sessionProvider.getTitleDrawable())

        (recyclerView as RecyclerView).apply {
            setHasFixedSize(true)

            layoutManager = LinearLayoutManager(this@FinishedActivity)
            adapter = Adapter(listOf())
        }

        buyButton.setOnClickListener {
            startActivity(XyttcIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION))
            finish()
            overridePendingTransition(0, 0)
        }
        unfinishButton.setOnClickListener {
            startActivity(UnfinishedIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION))
            finish()
            overridePendingTransition(0, 0)
        }
        mineButton.setOnClickListener {
            startActivity(MeIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION))
            finish()
            overridePendingTransition(0, 0)
        }
        historyButton.setOnClickListener {
            startActivity(HistoryIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION))
            finish()
            overridePendingTransition(0, 0)
        }

        disposables.add(
            endOfRecyclerViewReached
                .map { FinishedViewModel.Action.LoadNextPage }
                .subscribe(viewModel.actions)
        )


        viewModel.state.map { state ->
                state!!.items
            }
            .distinct()
            .observe(this, Observer { items ->
                ((recyclerView as RecyclerView).adapter as Adapter).update(items)
            })

        viewModel.state.map { state ->
                state!!.progressVisible
            }
            .distinct()
            .observe(this, Observer { progressVisible ->
                progressBar.visibility = if (progressVisible) View.VISIBLE else View.GONE
            })

        viewModel.state.map { state ->
                state!!.error
            }
            .distinct()
            .observe(this, Observer { error ->
                if (!error.isEmpty()) {
                    val msgs = error.split("|")
                    if (msgs.size == 2) {
                        Toast.makeText(applicationContext, stringProvider.get(msgs[0]) + "(" + msgs[1] + ")", Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(applicationContext, error, Toast.LENGTH_LONG).show()
                    }
                    viewModel.actions.accept(FinishedViewModel.Action.HideError)
                }
            })

        viewModel.actions.accept(FinishedViewModel.Action.Start)

    }

    val endOfRecyclerViewReached = Observable.create<Unit> { emitter ->
        val listener = object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                val endReached = !recyclerView!!.canScrollVertically(1)
                if (endReached) {
                    emitter.onNext(Unit)
                }
            }
        }

        emitter.setCancellable { recyclerView.removeOnScrollListener(listener) }

        recyclerView.addOnScrollListener(listener)
    }.debounce(200, TimeUnit.MILLISECONDS)


    class Adapter(private var data: List<FinishedViewModel.Item>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
        private val VIEW_TYPE_DRAW = 1
        private val VIEW_TYPE_BET = 2

        class HeaderViewHolder(@NonNull val contentView: View) : RecyclerView.ViewHolder(contentView) {
            private val drawTextView: TextView = contentView.findViewById(R.id.drawTextView) as TextView
            private val number5TextView: TextView = contentView.findViewById(R.id.number5TextView) as TextView
            private val number4TextView: TextView = contentView.findViewById(R.id.number4TextView) as TextView
            private val number3TextView: TextView = contentView.findViewById(R.id.number3TextView) as TextView
            private val number2TextView: TextView = contentView.findViewById(R.id.number2TextView) as TextView
            private val number1TextView: TextView = contentView.findViewById(R.id.number1TextView) as TextView

            fun bind(draw: FinishedViewModel.Item.Draw) {
                drawTextView.text = draw.drawId
                number1TextView.text = draw.number1
                number2TextView.text = draw.number2
                number3TextView.text = draw.number3
                number4TextView.text = draw.number4
                number5TextView.text = draw.number5
            }

        }

        class RowViewHolder(@NonNull val contentView: View) : RecyclerView.ViewHolder(contentView) {
            private val betTextView: TextView = contentView.findViewById(R.id.betTextView) as TextView
            private val countTextView: TextView = contentView.findViewById(R.id.countTextView) as TextView
            private val resultTextView: TextView = contentView.findViewById(R.id.resultTextView) as TextView

            fun bind(bet: FinishedViewModel.Item.Bet) {
                betTextView.text = bet.bet
                countTextView.text = bet.count
                resultTextView.text = bet.reward
            }

        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            when(viewType) {
                VIEW_TYPE_DRAW -> {
                    val view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.finished_header, parent, false)
                    return HeaderViewHolder(view)
                }
                else -> {
                    val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.finished_cell, parent, false)
                    return RowViewHolder(view)
                }
            }
        }

        override fun getItemCount(): Int {
            return data.size
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            when(holder) {
                is HeaderViewHolder -> holder.bind(data[position] as FinishedViewModel.Item.Draw)
                is RowViewHolder -> holder.bind(data[position] as FinishedViewModel.Item.Bet)
                else -> {}
            }

        }

        override fun getItemViewType(position: Int): Int {
            if (data[position] is FinishedViewModel.Item.Draw) {
                return VIEW_TYPE_DRAW
            } else {
                return VIEW_TYPE_BET
            }
        }

        fun update(items: List<FinishedViewModel.Item>) {
            val oldCount = data.size
            data = items
            notifyItemRangeInserted(oldCount, items.size - oldCount)
        }

    }

}
