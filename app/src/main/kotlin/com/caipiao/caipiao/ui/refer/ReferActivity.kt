package com.caipiao.caipiao.ui.refer

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.caipiao.caipiao.R
import com.caipiao.caipiao.providers.SessionProvider
import com.caipiao.caipiao.ui.AppActivity
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.shopify.livedataktx.distinct
import com.shopify.livedataktx.map
import dagger.android.HasAndroidInjector
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.content_refer.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import javax.inject.Inject


fun Context.ReferIntent(): Intent {
    return Intent(this, ReferActivity::class.java).apply {

    }
}

class ReferActivity : AppActivity(), HasAndroidInjector {
    @Inject
    lateinit var viewModelFactory: ReferViewModel.Factory

    val viewModel: ReferViewModel
        get() = _viewModel as ReferViewModel

    private lateinit var view: View
    private val TAG = ReferActivity::class.qualifiedName
    private val disposables = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _viewModel = ViewModelProviders.of(this, viewModelFactory).get(ReferViewModel::class.java)

        setContentView(R.layout.activity_refer)

        view = findViewById<View>(android.R.id.content)
        val mInflater = LayoutInflater.from(this)

        shareButton.setOnClickListener {
            viewModel.actions.accept(ReferViewModel.Action.ShowShareToken)
        }

        viewModel.state.map { state ->
                state!!.referUrl
            }
            .distinct()
            .observe(this, Observer { referUrl ->
                if (referUrl != "") {
                    val bitmap = encodeAsBitmap(referUrl)
                    tokenImageView.setImageBitmap(bitmap)
                }
            })

        viewModel.state.map { state ->
                state!!.timer
            }
            .distinct()
            .observe(this, Observer { timer ->
                timerTextView.text = timer
            })

        viewModel.state.map { state ->
            state!!.showShareToken
        }
            .distinct()
            .observe(this, Observer { showShareToken ->
                if (showShareToken) {
                    val bitmap = (tokenImageView.getDrawable() as BitmapDrawable).getBitmap()
                    val url = saveImage(bitmap)
                    shareImageUri(url ?: Uri.EMPTY)
                }
            })

        viewModel.state.map { state ->
                state!!.progressVisible
            }
            .distinct()
            .observe(this, Observer { progressVisible ->
                progressBar.visibility = if (progressVisible) View.VISIBLE else View.GONE
            })

        viewModel.state.map { state ->
                state!!.error
            }
            .distinct()
            .observe(this, Observer { error ->
                if (!error.isEmpty()) {
                    val msgs = error.split("|")
                    if (msgs.size == 2) {
                        Toast.makeText(applicationContext, stringProvider.get(msgs[0]) + "(" + msgs[1] + ")", Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(applicationContext, error, Toast.LENGTH_LONG).show()
                    }
                    viewModel.actions.accept(ReferViewModel.Action.HideError)
                }
            })

        viewModel.actions.accept(ReferViewModel.Action.Start)

    }

    fun encodeAsBitmap(str: String?): Bitmap? {
        try {
            val result = MultiFormatWriter().encode(
                str,
                BarcodeFormat.QR_CODE, 400, 400, null
            )
            val w = result.width
            val h = result.height
            val pixels = IntArray(w * h)
            for (y in 0 until h) {
                val offset = y * w
                for (x in 0 until w) {
                    pixels[offset + x] = if (result[x, y]) Color.BLACK else Color.WHITE
                }
            }
            val bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
            bitmap.setPixels(pixels, 0, 400, 0, 0, w, h)
            return bitmap
        } catch (e: Exception) {
            // Unsupported format
            Log.e(TAG, "Excepion while encode qr ccode: ", e)
            return null
        }

    }

    private fun saveImage(image: Bitmap): Uri? {
        // - Should be processed in another thread
        val imagesFolder = File(cacheDir, "images")
        var uri: Uri? = null
        try {
            imagesFolder.mkdirs()
            val file = File(imagesFolder, "shared_image.png")
            val stream = FileOutputStream(file)
            image.compress(Bitmap.CompressFormat.PNG, 90, stream)
            stream.flush()
            stream.close()
            uri = FileProvider.getUriForFile(this, "com.mydomain.fileprovider", file)
        } catch (e: IOException) {
            Log.e(TAG, "IOException while trying to write file for sharing: ", e)
        }
        return uri
    }

    private fun shareImageUri(uri: Uri) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.putExtra(Intent.EXTRA_STREAM, uri)
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        intent.type = "image/png"
        startActivity(intent)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

}
