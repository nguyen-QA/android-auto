package com.caipiao.caipiao.ui.verify

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.caipiao.caipiao.R
import com.caipiao.caipiao.providers.*
import com.caipiao.caipiao.ui.AppViewModel
import com.freeletics.rxredux.StateAccessor
import com.freeletics.rxredux.reduxStore
import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class AuthVerifyViewModel(
    val memberApi: MemberApiProvider,
    val sessionProvider: SessionProvider,
    val stringProvider: StringProvider,
    val schedulerProvider: SchedulerProvider
) : AppViewModel() {
    private val TAG = AuthVerifyViewModel::class.qualifiedName

    data class State(
        val progressVisible: Boolean,
        val token: String,
        val submitEnabled: Boolean,
        val showHome: Boolean,
        val showInvalidToken: Boolean,
        val error: String
    )

    sealed class Action {
        object Start : Action()
        object StartProgress: Action()
        object StopProgress: Action()
        object StartTimer: Action()
        data class ShowTimer(val timer: Long): Action()
        data class TokenChanged(val token: String): Action()
        object Submit: Action()
        object ShowHome: Action()
        object HideHome: Action()
        object ShowInvalidToken: Action()
        object HideInvalidToken: Action()

        data class ShowError(val error: Throwable) : Action()
        object HideError: Action()
    }

    private val _state = MutableLiveData<State>()
    val state: LiveData<State> = _state

    private val _actions: Relay<Action> = PublishRelay.create()
    val actions: Consumer<Action> = _actions

    private val disposables = CompositeDisposable()

    init {
        disposables.add(
            _actions.reduxStore(
                initialState = State(
                    progressVisible = false,
                    token = "",
                    submitEnabled = false,
                    showHome = false,
                    showInvalidToken = false,
                    error = ""
                ),
                sideEffects = listOf(
                    ::start,
                    ::submit,
                    ::startTimer
                ),
                reducer = ::reducer
            )
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { newState ->
                    _state.value = newState
                })
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }

    private fun reducer(state: State, action: Action): State {
        return when (action) {
            is Action.ShowError -> state.copy(
                error = stringProvider.get(action.error.message ?: ""),
                progressVisible = false
            )
            is Action.HideError -> state.copy(
                error = ""
            )
            is Action.StartProgress -> state.copy(
                progressVisible = true
            )
            is Action.StopProgress -> state.copy(
                progressVisible = false
            )
            is Action.TokenChanged -> state.copy(
                token = action.token,
                submitEnabled = action.token.length == 6
            )
            is Action.ShowHome -> state.copy(
                showHome = true
            )
            is Action.HideHome -> state.copy(
                showHome = false
            )
            is Action.ShowInvalidToken -> state.copy(
                showInvalidToken = true
            )
            is Action.HideInvalidToken -> state.copy(
                showInvalidToken = false
            )
            else -> state
        }
    }


    private fun start(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.Start::class.java)
            .switchMap {
                Observable.just(Action.StartTimer)
            }

    private fun submit(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.Submit::class.java)
            .switchMap {
                val token = state().token
                memberApi.enrollWechat(
                    token
                )
                    .subscribeOn(schedulerProvider.io())
                    .map {
                        if (it.isSuccess) Action.ShowHome else Action.ShowInvalidToken}
                    .concatWith(Observable.just(Action.StopProgress as Action))
                    .onErrorReturn { error ->
                        Action.ShowError(
                            error
                        )
                    }
                    .startWith(Observable.just(Action.StartProgress))
            }

    private fun startTimer(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.StartTimer::class.java)
            .switchMap {
                Observable.interval(1, TimeUnit.SECONDS)
                    .take(60)
                    .map { Action.ShowTimer(59 - it) }
            }

    class Factory(val memberApi: MemberApiProvider, val sessionProvider: SessionProvider, val stringProvider: StringProvider, val schedulerProvider: SchedulerProvider) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return AuthVerifyViewModel(
                memberApi,
                sessionProvider,
                stringProvider,
                schedulerProvider
            ) as T
        }
    }
}