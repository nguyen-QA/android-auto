package com.caipiao.caipiao.ui.refer

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.caipiao.caipiao.providers.*
import com.caipiao.caipiao.ui.AppViewModel
import com.freeletics.rxredux.StateAccessor
import com.freeletics.rxredux.reduxStore
import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class ReferViewModel(
    val memberApi: MemberApiProvider,
    val affiliateApi: AffiliateApiProvider,
    val sessionProvider: SessionProvider,
    val stringProvider: StringProvider,
    val utilProvider: UtilProvider
) : AppViewModel() {
    private val TAG = ReferViewModel::class.qualifiedName

    data class State(
        val progressVisible: Boolean,
        val referUrl: String,
        val timer: String,
        val showShareToken: Boolean,
        val error: String
    )

    sealed class Action {
        object Start : Action()
        object StartProgress: Action()
        object StopProgress: Action()
        object FetchToken: Action()
        data class StartTimer(val timer: Int): Action()
        data class ShowReferUrl(val referUrl: String): Action()
        data class ShowTimer(val timer: Int): Action()
        object ShowShareToken: Action()
        object HideShareToken: Action()
        data class ShowError(val error: Throwable) : Action()
        object HideError: Action()
    }

    private val _state = MutableLiveData<State>()
    val state: LiveData<State> = _state

    private val _actions: Relay<Action> = PublishRelay.create()
    val actions: Consumer<Action> = _actions

    private val disposables = CompositeDisposable()

    init {
        disposables.add(
            _actions.reduxStore(
                initialState = State(
                    progressVisible = false,
                    referUrl = "",
                    timer = "00:00",
                    showShareToken = false,
                    error = ""
                ),
                sideEffects = listOf(
                        ::start,
                        ::fetchToken,
                        ::startTimer
                ),
                reducer = ::reducer
            )
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { newState ->
                    _state.value = newState
                })
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }

    private fun reducer(state: State, action: Action): State {
        return when (action) {
            is Action.ShowShareToken -> state.copy(
                showShareToken = true
            )
            is Action.HideShareToken -> state.copy(
                showShareToken = false
            )
            is Action.ShowReferUrl -> state.copy(
                referUrl = action.referUrl
            )
            is Action.ShowError -> state.copy(
                error = stringProvider.get(action.error.message ?: ""),
                progressVisible = false
            )
            is Action.HideError -> state.copy(
                error = ""
            )
            is Action.StartProgress -> state.copy(
                progressVisible = true
            )
            is Action.StopProgress -> state.copy(
                progressVisible = false
            )
            is Action.ShowTimer -> state.copy(
                timer = String.format("%02d:%02d", action.timer / 60, action.timer % 60)
            )
            else -> state
        }
    }


    private fun start(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.Start::class.java)
            .switchMap {
                Observable.just(
                    Action.FetchToken
                )
            }

    private fun fetchToken(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.FetchToken::class.java)
            .switchMap {
                val userId = sessionProvider.getUserId()
                affiliateApi.getReferToken()
                    .subscribeOn(Schedulers.newThread())
                    .flatMap {
                        Observable.just(Action.ShowReferUrl(utilProvider.getReferUrl(it.token ?: "")) as Action,
                            Action.StartTimer(it.expire ?: 0))
                    }
                    .concatWith(Observable.just(Action.StopProgress))
                    .onErrorReturn { error ->
                        Action.ShowError(
                            error
                        )
                    }
                    .startWith(Action.StartProgress)
            }

    private fun startTimer(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.StartTimer::class.java)
            .switchMap { action ->
                Observable.interval(1, TimeUnit.SECONDS)
                    .take(action.timer.toLong())
                    .map { Action.ShowTimer(action.timer - 1 - it.toInt()) }
            }


    class Factory(val memberApi: MemberApiProvider, val affiliateApi: AffiliateApiProvider, val sessionProvider: SessionProvider, val stringProvider: StringProvider, val utilProvider: UtilProvider) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return ReferViewModel(
                memberApi,
                affiliateApi,
                sessionProvider,
                stringProvider,
                utilProvider
            ) as T
        }
    }
}