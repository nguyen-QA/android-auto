package com.caipiao.caipiao.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.caipiao.caipiao.SecurityHelper
import com.caipiao.caipiao.providers.*
import com.caipiao.caipiao.ui.AppViewModel
import com.freeletics.rxredux.StateAccessor
import com.freeletics.rxredux.reduxStore
import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers

class MainViewModel(
    val memberApi: MemberApiProvider,
    val sessionProvider: SessionProvider,
    val stringProvider: StringProvider,
    val utilProvider: UtilProvider,
    val schedulerProvider: SchedulerProvider
) : AppViewModel() {
    private val TAG = MainViewModel::class.qualifiedName

    data class State(
        val progressVisible: Boolean,
        val showHome: Boolean,
        val error: String
    )

    sealed class Action {
        object Start : Action()
        object StartProgress: Action()
        object StopProgress: Action()
        object ShowHome: Action()
        object HideHome: Action()

        data class ShowError(val error: Throwable) : Action()
        object HideError: Action()
    }

    private val _state = MutableLiveData<State>()
    val state: LiveData<State> = _state

    private val _actions: Relay<Action> = PublishRelay.create()
    val actions: Consumer<Action> = _actions

    private val disposables = CompositeDisposable()

    init {
        disposables.add(
            _actions.reduxStore(
                    initialState = State(
                        progressVisible = false,
                        showHome = false,
                        error = ""
                    ),
                    sideEffects = listOf(
                        ::start
                    ),
                    reducer = ::reducer
                )
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { newState ->
                    _state.value = newState
                })
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }

    private fun reducer(state: State, action: Action): State {
        return when (action) {
            is Action.ShowError -> state.copy(
                error = stringProvider.get(action.error.message ?: ""),
                progressVisible = false
            )
            is Action.HideError -> state.copy(
                error = ""
            )
            is Action.StartProgress -> state.copy(
                progressVisible = true
            )
            is Action.StopProgress -> state.copy(
                progressVisible = false
            )
            is Action.ShowHome -> state.copy(
                showHome = true
            )
            is Action.HideHome -> state.copy(
                showHome = false
            )
            else -> state
        }
    }


    private fun start(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.Start::class.java)
            .switchMap {
                Observable.just(Action.ShowHome)
            }

    class Factory(val memberApi: MemberApiProvider, val sessionProvider: SessionProvider, val stringProvider: StringProvider, val utilProvider: UtilProvider,  val schedulerProvider: SchedulerProvider) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return MainViewModel(
                memberApi,
                sessionProvider,
                stringProvider,
                utilProvider,
                schedulerProvider
            ) as T
        }
    }
}