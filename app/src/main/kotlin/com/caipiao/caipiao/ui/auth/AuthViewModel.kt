package com.caipiao.caipiao.ui.auth

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.caipiao.caipiao.providers.MemberApiProvider
import com.caipiao.caipiao.providers.SchedulerProvider
import com.caipiao.caipiao.providers.SessionProvider
import com.caipiao.caipiao.providers.StringProvider
import com.caipiao.caipiao.ui.AppViewModel
import com.freeletics.rxredux.StateAccessor
import com.freeletics.rxredux.reduxStore
import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers

class AuthViewModel(
    val memberApi: MemberApiProvider,
    val sessionProvider: SessionProvider,
    val stringProvider: StringProvider,
    val schedulerProvider: SchedulerProvider
) : AppViewModel() {
    private val TAG = AuthViewModel::class.qualifiedName

    data class State(
        val progressVisible: Boolean,
        val wechat: String,
        val phone: String,
        val submitEnabled: Boolean,
        val showVerify: Boolean,
        val showInvalidPhone: Boolean,
        val error: String
    )

    sealed class Action {
        object Start : Action()
        object StartProgress: Action()
        object StopProgress: Action()
        data class WechatChanged(val wechat: String): Action()
        data class PhoneChanged(val phone: String): Action()
        object Submit: Action()
        object ShowVerify: Action()
        object HideVerify: Action()
        object ShowInvalidPhone: Action()
        object HideInvalidPhone: Action()

        data class ShowError(val error: Throwable) : Action()
        object HideError: Action()
    }

    private val _state = MutableLiveData<State>()
    val state: LiveData<State> = _state

    private val _actions: Relay<Action> = PublishRelay.create()
    val actions: Consumer<Action> = _actions

    private val disposables = CompositeDisposable()

    init {
        disposables.add(
            _actions.reduxStore(
                initialState = State(
                    progressVisible = false,
                    wechat = "",
                    phone = "",
                    submitEnabled = false,
                    showVerify = false,
                    showInvalidPhone = false,
                    error = ""
                ),
                sideEffects = listOf(
                    ::start,
                    ::submit
                ),
                reducer = ::reducer
            )
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { newState ->
                    _state.value = newState
                })
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }

    private fun reducer(state: State, action: Action): State {
        return when (action) {
            is Action.ShowError -> state.copy(
                error = stringProvider.get(action.error.message ?: ""),
                progressVisible = false
            )
            is Action.HideError -> state.copy(
                error = ""
            )
            is Action.StartProgress -> state.copy(
                progressVisible = true
            )
            is Action.StopProgress -> state.copy(
                progressVisible = false
            )
            is Action.WechatChanged -> state.copy(
                wechat = action.wechat,
                submitEnabled = action.wechat.length >0
            )
            is Action.PhoneChanged -> state.copy(
                phone = action.phone,
                submitEnabled = action.phone.length >0 && state.wechat.length > 0
            )
            is Action.ShowVerify -> state.copy(
                showVerify = true
            )
            is Action.HideVerify -> state.copy(
                showVerify = false
            )
            is Action.ShowInvalidPhone -> state.copy(
                showInvalidPhone = true
            )
            is Action.HideInvalidPhone -> state.copy(
                showInvalidPhone = false
            )
            else -> state
        }
    }


    private fun start(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.Start::class.java)
            .switchMap {
                Observable.empty<Action>()
            }

    private fun submit(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.Submit::class.java)
            .switchMap {
                val wechat = state().wechat
                val phone = state().phone
                sessionProvider.setPhone(phone)
                sessionProvider.setWechat(wechat)
                memberApi.registerWechat(
                    wechat
                )
                    .subscribeOn(Schedulers.newThread())
                    .map { if (it.isSuccess) Action.ShowVerify as Action else Action.ShowInvalidPhone as Action }
                    .concatWith(Observable.just(Action.StopProgress as Action))
                    .onErrorReturn { error ->
                        Action.ShowError(
                            error
                        )
                    }
                    .startWith(Observable.just(Action.StartProgress))
            }


    class Factory(val memberApi: MemberApiProvider, val sessionProvider: SessionProvider, val stringProvider: StringProvider, val schedulerProvider: SchedulerProvider) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return AuthViewModel(
                memberApi,
                sessionProvider,
                stringProvider,
                schedulerProvider
            ) as T
        }
    }
}