package com.caipiao.caipiao.ui.verify

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.caipiao.caipiao.R
import com.caipiao.caipiao.providers.SessionProvider
import com.caipiao.caipiao.ui.AppActivity
import com.caipiao.caipiao.ui.finished.FinishedIntent
import com.caipiao.caipiao.ui.xyttc.XyttcIntent
import com.shopify.livedataktx.distinct
import com.shopify.livedataktx.map
import com.shopify.livedataktx.nonNull
import dagger.android.HasAndroidInjector
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.content_auth_verify.*
import kotlinx.android.synthetic.main.custom_actionbar.*
import javax.inject.Inject

fun Context.AuthVerifyIntent(): Intent {
    return Intent(this, AuthVerifyActivity::class.java).apply {

    }
}

class AuthVerifyActivity : AppActivity(), HasAndroidInjector {
    @Inject
    lateinit var viewModelFactory: AuthVerifyViewModel.Factory

    val viewModel: AuthVerifyViewModel
        get() = _viewModel as AuthVerifyViewModel

    private lateinit var view: View
    private val TAG = AuthVerifyActivity::class.qualifiedName
    private val disposables = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _viewModel = ViewModelProviders.of(this, viewModelFactory).get(AuthVerifyViewModel::class.java)

        setContentView(R.layout.activity_auth_verify)

        view = findViewById<View>(android.R.id.content)

        tokenEditText.addTextChangedListener { editable ->
            viewModel.actions.accept(
                AuthVerifyViewModel.Action.TokenChanged(
                    editable?.toString() ?: ""
                )
            )
        }

        submitButton.setOnClickListener {
            tokenEditText.clearFocus()
            val imm =
                getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(
                tokenEditText.windowToken,
                InputMethodManager.HIDE_NOT_ALWAYS
            )
            viewModel.actions.accept(AuthVerifyViewModel.Action.Submit)
        }

        viewModel.state.map { state ->
                state!!.progressVisible
            }
            .distinct()
            .observe(this, Observer { progressVisible ->
                progressBar.visibility = if (progressVisible) View.VISIBLE else View.GONE
            })

        viewModel.state.map { state ->
            state!!.showHome
        }
            .distinct()
            .observe(this, Observer { showVerify ->
                if (showVerify) {
                    finish()
                }
            })

        viewModel.state.map { state ->
            state!!.showInvalidToken
        }
            .distinct()
            .observe(this, Observer { showInvalidPhone ->
                if (showInvalidPhone) {
                    Toast.makeText(applicationContext, stringProvider.get(R.string.invalid_verify_token), Toast.LENGTH_LONG).show()
                    viewModel.actions.accept(AuthVerifyViewModel.Action.HideInvalidToken)
                }
            })

        viewModel.state.map { state ->
            state!!.submitEnabled
        }
            .distinct()
            .observe(this, Observer { submitEnabled ->
                submitButton.setBackgroundColor(if (submitEnabled) resources.getColor(R.color.palePeach) else resources.getColor(R.color.mudBrown))
                submitButton.setTextColor(if (submitEnabled) resources.getColor(R.color.black) else resources.getColor(R.color.white))
                submitButton.isEnabled = submitEnabled
            })

        viewModel.state.map { state ->
                state!!.error
            }
            .distinct()
            .observe(this, Observer { error ->
                if (!error.isEmpty()) {
                    val msgs = error.split("|")
                    if (msgs.size == 2) {
                        Toast.makeText(applicationContext, stringProvider.get(msgs[0]) + "(" + msgs[1] + ")", Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(applicationContext, error, Toast.LENGTH_LONG).show()
                    }
                    viewModel.actions.accept(AuthVerifyViewModel.Action.HideError)
                }
            })

        viewModel.actions.accept(AuthVerifyViewModel.Action.Start)

    }

}
