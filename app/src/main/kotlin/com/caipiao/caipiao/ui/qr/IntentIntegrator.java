package com.caipiao.caipiao.ui.qr;

import android.app.Activity;

public class IntentIntegrator extends com.google.zxing.integration.android.IntentIntegrator {
    public IntentIntegrator(Activity activity) {
        super(activity);
    }

    @Override
    protected Class<?> getDefaultCaptureActivity() {
        return CaptureActivity.class;
    }
}
