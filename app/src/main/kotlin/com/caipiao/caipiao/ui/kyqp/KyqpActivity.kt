package com.caipiao.caipiao.ui.kyqp

import android.content.Context
import android.content.Intent
import android.content.pm.ApplicationInfo
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.View
import android.webkit.WebResourceResponse
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.caipiao.caipiao.R
import com.caipiao.caipiao.ui.AppActivity
import com.shopify.livedataktx.distinct
import com.shopify.livedataktx.map
import dagger.android.HasAndroidInjector
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_kyqp.*
import javax.inject.Inject

const val GAME_ACTIVITY_EXTRA_URL = "GAME_ACTIVITY_EXTRA_URL"

fun Context.KyqpIntent(url: String): Intent {
    return Intent(this, KyqpActivity::class.java).apply {
        putExtra(GAME_ACTIVITY_EXTRA_URL, url)
    }
}

class KyqpActivity : AppActivity(), HasAndroidInjector {
    @Inject
    lateinit var viewModelFactory: KyqpViewModel.Factory

    val viewModel: KyqpViewModel
        get() = _viewModel as KyqpViewModel

    private lateinit var view: View
    private val TAG = KyqpActivity::class.qualifiedName
    private val disposables = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _viewModel = ViewModelProviders.of(this, viewModelFactory).get(KyqpViewModel::class.java)

        setContentView(R.layout.activity_kyqp)

        val url = intent.getStringExtra(GAME_ACTIVITY_EXTRA_URL)

        kyqpWeb.settings.javaScriptEnabled = true
        kyqpWeb.settings.domStorageEnabled = true
        kyqpWeb.settings.databaseEnabled = true
        kyqpWeb.settings.setAppCacheEnabled(true)
        kyqpWeb.webViewClient = object: WebViewClient() {
            override fun shouldInterceptRequest(view: WebView, url: String): WebResourceResponse? {
                if (url == "https://www.ky34.com/" || url == "http://game.api-cdn.com/") {
                    finish()
                }
                return super.shouldInterceptRequest(view, url)
            }
        }
        kyqpWeb.loadUrl(url)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (0 != applicationInfo.flags and ApplicationInfo.FLAG_DEBUGGABLE) {
                WebView.setWebContentsDebuggingEnabled(true)
            }
        }

        viewModel.state.map { state ->
                state!!.error
            }
            .distinct()
            .observe(this, Observer { error ->
                if (!error.isEmpty()) {
                    val msgs = error.split("|")
                    if (msgs.size == 2) {
                        Toast.makeText(applicationContext, stringProvider.get(msgs[0]) + "(" + msgs[1] + ")", Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(applicationContext, error, Toast.LENGTH_LONG).show()
                    }
                    viewModel.actions.accept(KyqpViewModel.Action.HideError)
                }
            })
    }

    override fun onBackPressed() {
        viewModel.actions.accept(KyqpViewModel.Action.Back)
    }
}



