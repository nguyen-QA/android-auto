package com.caipiao.caipiao.ui.main

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.caipiao.caipiao.R
import com.caipiao.caipiao.providers.SessionProvider
import com.caipiao.caipiao.providers.UtilProvider
import com.caipiao.caipiao.ui.AppActivity
import com.caipiao.caipiao.ui.home.HomeIntent
import com.shopify.livedataktx.distinct
import com.shopify.livedataktx.map
import dagger.android.HasAndroidInjector
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class MainActivity : AppActivity(), HasAndroidInjector {
    @Inject
    lateinit var viewModelFactory: MainViewModel.Factory


    val viewModel: MainViewModel
        get() = _viewModel as MainViewModel

    private lateinit var view: View
    private val TAG = MainActivity::class.qualifiedName
    private val disposables = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)

        setContentView(R.layout.activity_main)

        viewModel.state.map { state ->
                state!!.showHome
            }
            .distinct()
            .observe(this, Observer { showHome ->
                if (showHome) {
                    startActivity(HomeIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION))
                    finish()
                    overridePendingTransition(0, 0)
                }
            })

        viewModel.state.map { state ->
                state!!.error
            }
            .distinct()
            .observe(this, Observer { error ->
                if (!error.isEmpty()) {
                    val msgs = error.split("|")
                    if (msgs.size == 2) {
                        Toast.makeText(applicationContext, stringProvider.get(msgs[0]) + "(" + msgs[1] + ")", Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(applicationContext, error, Toast.LENGTH_LONG).show()
                    }
                    viewModel.actions.accept(MainViewModel.Action.HideError)
                }
            })

        viewModel.actions.accept(MainViewModel.Action.Start)
    }
}



