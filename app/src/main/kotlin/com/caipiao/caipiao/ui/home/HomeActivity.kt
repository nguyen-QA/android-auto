package com.caipiao.caipiao.ui.home

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.caipiao.caipiao.R
import com.caipiao.caipiao.ui.AppActivity
import com.caipiao.caipiao.ui.auth.AuthIntent
import com.caipiao.caipiao.ui.kyqp.KyqpIntent
import com.caipiao.caipiao.ui.mygames.MyGamesIntent
import com.caipiao.caipiao.ui.qr.IntentIntegrator
import com.caipiao.caipiao.ui.refer.ReferIntent
import com.shopify.livedataktx.distinct
import com.shopify.livedataktx.map
import dagger.android.HasAndroidInjector
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_home.countEditText
import kotlinx.android.synthetic.main.activity_home.depositButton
import kotlinx.android.synthetic.main.activity_home.requestBorder
import kotlinx.android.synthetic.main.activity_home.submitRequestButton
import kotlinx.android.synthetic.main.activity_home.withdrawButton
import javax.inject.Inject


fun Context.HomeIntent(): Intent {
    return Intent(this, HomeActivity::class.java).apply {

    }
}

class HomeActivity : AppActivity(), HasAndroidInjector {
    @Inject
    lateinit var viewModelFactory: HomeViewModel.Factory

    val viewModel: HomeViewModel
        get() = _viewModel as HomeViewModel

    private lateinit var view: View
    private val TAG = HomeActivity::class.qualifiedName
    private val disposables = CompositeDisposable()
    private var bgPlayer: MediaPlayer? = null
    private var openPlayer: MediaPlayer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _viewModel = ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)

        setContentView(R.layout.activity_home)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        qrButton.setOnClickListener { view ->
            openPlayer?.start()
            viewModel.actions.accept(HomeViewModel.Action.QrPicked)
        }

        kyqpButton.setOnClickListener { view ->
            openPlayer?.start()
            viewModel.actions.accept(HomeViewModel.Action.KyqpPicked)
        }

        myGamesButton.setOnClickListener { view ->
            openPlayer?.start()
            viewModel.actions.accept(HomeViewModel.Action.MyGamesPicked)
        }

        exchangeButton.setOnClickListener { view ->
            openPlayer?.start()
            viewModel.actions.accept(HomeViewModel.Action.ToggleExchange)
        }

        depositButton.setOnClickListener {
            viewModel.actions.accept(HomeViewModel.Action.ShowDeposit)
        }
        withdrawButton.setOnClickListener {
            viewModel.actions.accept(HomeViewModel.Action.ShowWithdraw)
        }

        countEditText.addTextChangedListener { editable ->
            viewModel.actions.accept(HomeViewModel.Action.RequestCountChanged(editable?.toString() ?: ""))
        }
        countEditText.setOnFocusChangeListener { view, hasFocus ->
            (view as EditText).isCursorVisible = hasFocus
        }
        submitRequestButton.setOnClickListener {
            countEditText.clearFocus()
            val imm =
                getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(
                countEditText.windowToken,
                InputMethodManager.HIDE_NOT_ALWAYS
            )
            viewModel.actions.accept(HomeViewModel.Action.SubmitRequest)
        }

        viewModel.state.map { state ->
                state!!.gold
            }
            .distinct()
            .observe(this, Observer { gold ->
                goldTextView.text = gold
            })

        viewModel.state.map { state ->
                state!!.showScanQr
            }
            .distinct()
            .observe(this, Observer { showScanQr ->
                if (showScanQr) {
                    IntentIntegrator(this).initiateScan()
                    viewModel.actions.accept(HomeViewModel.Action.ScanQrShown)
                }
            })

        viewModel.state.map { state ->
                state!!.showAuth
            }
            .distinct()
            .observe(this, Observer { showAuth ->
                if (showAuth) {
                    startActivity(AuthIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION))
                    overridePendingTransition(0, 0)
                    viewModel.actions.accept(HomeViewModel.Action.AuthShown)
                }
            })

        viewModel.state.map { state ->
                state!!.showMyGames
            }
            .distinct()
            .observe(this, Observer { showMyGames ->
                if (showMyGames) {
                    startActivity(MyGamesIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION))
                    overridePendingTransition(0, 0)
                    viewModel.actions.accept(HomeViewModel.Action.MyGamesShown)
                }
            })

        viewModel.state.map { state ->
                state!!.showRefer
            }
            .distinct()
            .observe(this, Observer { showRefer ->
                if (showRefer) {
                    startActivity(ReferIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION))
                    overridePendingTransition(0, 0)
                    viewModel.actions.accept(HomeViewModel.Action.ReferShown)
                }
            })

        viewModel.state.map { state ->
                state!!.showKyqp
            }
            .distinct()
            .observe(this, Observer { showKyqp ->
                if (!showKyqp.isEmpty()) {
                    startActivity(KyqpIntent(showKyqp).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION))
                    overridePendingTransition(0, 0)
                    viewModel.actions.accept(HomeViewModel.Action.KyqpShown)
                }
            })

        viewModel.state.map { state ->
                state!!.showExchange
            }
            .distinct()
            .observe(this, Observer { showExchange ->
                if (showExchange) {
                    exchangeButton.visibility = View.VISIBLE
                } else {
                    exchangeButton.visibility = View.GONE
                }
            })

            viewModel.state.map { state ->
                state!!.showExchangeRequest
            }
            .distinct()
            .observe(this, Observer { showExchangeRequest ->
                if (showExchangeRequest) {
                    requestBackground.visibility = View.VISIBLE
                    depositButton.visibility = View.VISIBLE
                    withdrawButton.visibility = View.VISIBLE
                    requestBorder.visibility = View.VISIBLE
                    countEditText.visibility = View.VISIBLE
                    submitRequestButton.visibility = View.VISIBLE
                } else {
                    requestBackground.visibility = View.GONE
                    depositButton.visibility = View.GONE
                    withdrawButton.visibility = View.GONE
                    requestBorder.visibility = View.GONE
                    countEditText.visibility = View.GONE
                    submitRequestButton.visibility = View.GONE
                }
            })

        viewModel.state.map { state ->
                state!!.depositVisible
            }
            .distinct()
            .observe(this, Observer { depositVisible ->
                if (depositVisible) {
                    depositButton.setBackgroundColor(resources.getColor(R.color.palePeach))
                    withdrawButton.background = resources.getDrawable(R.drawable.btn_unselected_bg)
                    depositButton.setTextColor(Color.BLACK)
                    withdrawButton.setTextColor(Color.WHITE)
                    requestBorder.visibility = View.VISIBLE
                    countEditText.visibility = View.VISIBLE
                    submitRequestButton.visibility = View.VISIBLE
                }
            })

        viewModel.state.map { state ->
                state!!.withdrawVisible
            }
            .distinct()
            .observe(this, Observer { withdrawVisible ->
                if (withdrawVisible) {
                    depositButton.background = resources.getDrawable(R.drawable.btn_unselected_bg)
                    withdrawButton.setBackgroundColor(resources.getColor(R.color.palePeach))
                    depositButton.setTextColor(Color.WHITE)
                    withdrawButton.setTextColor(Color.BLACK)
                    requestBorder.visibility = View.VISIBLE
                    countEditText.visibility = View.VISIBLE
                    submitRequestButton.visibility = View.VISIBLE
                }
            })

        viewModel.state.map { state ->
                state!!.submitRequestText
            }
            .distinct()
            .observe(this, Observer { submitRequestText ->
                submitRequestButton.setText(submitRequestText)
            })

        viewModel.state.map { state ->
                state!!.submitRequestEnabled
            }
            .distinct()
            .observe(this, Observer { submitRequestEnabled ->
                if (submitRequestEnabled) {
                    submitRequestButton.setBackgroundColor(resources.getColor(R.color.palePeach))
                    submitRequestButton.setTextColor(Color.BLACK)
                } else {
                    submitRequestButton.setBackgroundColor(resources.getColor(R.color.mudBrown))
                    submitRequestButton.setTextColor(Color.GRAY)
                }
                submitRequestButton.isEnabled = submitRequestEnabled
            })


        viewModel.state.map { state ->
                state!!.error
            }
            .distinct()
            .observe(this, Observer { error ->
                if (!error.isEmpty()) {
                    val msgs = error.split("|")
                    if (msgs.size == 2) {
                        Toast.makeText(applicationContext, stringProvider.get(msgs[0]) + "(" + msgs[1] + ")", Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(applicationContext, error, Toast.LENGTH_LONG).show()
                    }
                    viewModel.actions.accept(HomeViewModel.Action.HideError)
                }
            })

        val action: String? = intent.action
        when {
            action == Intent.ACTION_SEND -> {
                if ("text/plain" == intent.type) {
                    intent.getStringExtra(Intent.EXTRA_TEXT)?.let {
                        viewModel.actions.accept(HomeViewModel.Action.Update(it))
                    }
                } else if (intent.type?.startsWith("image/") == true) {
                    (intent.getParcelableExtra<Parcelable>(Intent.EXTRA_STREAM) as? Uri)?.let {
                        val qrCodes = utilProvider.readQrCode(it)
                        if (qrCodes.count() > 0) {
                            viewModel.actions.accept(HomeViewModel.Action.Update(qrCodes.get(0)))
                        }
                    }
                }
            }
            action == Intent.ACTION_SEND_MULTIPLE -> {
                intent.getParcelableArrayListExtra<Parcelable>(Intent.EXTRA_STREAM)?.let {
                    if (it.size > 0) {
                        val item = it[0] as? Uri
                        if (item != null) {
                            val qrCodes = utilProvider.readQrCode(item)
                            if (qrCodes.count() > 0) {
                                viewModel.actions.accept(HomeViewModel.Action.Update(qrCodes.get(0)))
                            }
                        }
                    }
                }
            }
            else -> {
                action?.let {
                    val data: Uri? = intent?.data
                    viewModel.actions.accept(HomeViewModel.Action.Update(data?.toString()))
                }
            }
        }

    }

    override fun onResume() {
        super.onResume()
        bgPlayer = MediaPlayer.create(this@HomeActivity, R.raw.hall_bg)
        bgPlayer?.isLooping = true
        bgPlayer?.start()

        openPlayer = MediaPlayer.create(this@HomeActivity, R.raw.window_open)

        viewModel.actions.accept(HomeViewModel.Action.Start)
    }

    override fun onPause() {
        super.onPause()
        bgPlayer?.stop()
        bgPlayer?.release()
        bgPlayer = null

        openPlayer?.release()
        openPlayer = null
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        val result =
            IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            if (result.contents == null) {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(this, "Scanned: " + result.contents, Toast.LENGTH_LONG).show()
                viewModel.actions.accept(HomeViewModel.Action.Update(result.contents))
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

}



