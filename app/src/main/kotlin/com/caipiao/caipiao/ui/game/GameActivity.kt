package com.caipiao.caipiao.ui.game

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.caipiao.caipiao.R
import com.caipiao.caipiao.providers.SessionProvider
import com.caipiao.caipiao.ui.AppActivity
import com.shopify.livedataktx.distinct
import com.shopify.livedataktx.map
import dagger.android.HasAndroidInjector
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_game.*
import javax.inject.Inject

const val GAME_ACTIVITY_EXTRA_URL = "GAME_ACTIVITY_EXTRA_URL"

fun Context.GameIntent(url: String): Intent {
    return Intent(this, GameActivity::class.java).apply {
        putExtra(GAME_ACTIVITY_EXTRA_URL, url)
    }
}

class GameActivity : AppActivity(), HasAndroidInjector {

    @Inject
    lateinit var viewModelFactory: GameViewModel.Factory

    val viewModel: GameViewModel
        get() = _viewModel as GameViewModel

    private lateinit var view: View
    private val TAG = GameActivity::class.qualifiedName
    private val disposables = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _viewModel = ViewModelProviders.of(this, viewModelFactory).get(GameViewModel::class.java)

        setContentView(R.layout.activity_game)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        gameWebView.settings.javaScriptEnabled = true
        gameWebView.settings.domStorageEnabled = true
        gameWebView.settings.databaseEnabled = true
        gameWebView.settings.setAppCacheEnabled(true)
        gameWebView.webViewClient = WebViewClient()
        gameWebView.loadUrl(intent.getStringExtra(GAME_ACTIVITY_EXTRA_URL))

        viewModel.state.map { state ->
                state!!.error
            }
            .distinct()
            .observe(this, Observer { error ->
                if (!error.isEmpty()) {
                    val msgs = error.split("|")
                    if (msgs.size == 2) {
                        Toast.makeText(applicationContext, stringProvider.get(msgs[0]) + "(" + msgs[1] + ")", Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(applicationContext, error, Toast.LENGTH_LONG).show()
                    }
                    viewModel.actions.accept(GameViewModel.Action.HideError)
                }
            })

    }

}



