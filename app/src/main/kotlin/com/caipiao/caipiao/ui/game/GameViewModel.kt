package com.caipiao.caipiao.ui.game

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.caipiao.caipiao.providers.MemberApiProvider
import com.caipiao.caipiao.providers.SchedulerProvider
import com.caipiao.caipiao.providers.SessionProvider
import com.caipiao.caipiao.providers.StringProvider
import com.caipiao.caipiao.ui.AppViewModel
import com.freeletics.rxredux.StateAccessor
import com.freeletics.rxredux.reduxStore
import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer

class GameViewModel(
    val api: MemberApiProvider,
    val sessionProvider: SessionProvider,
    val stringProvider: StringProvider,
    val schedulerProvider: SchedulerProvider
) : AppViewModel() {
    private val TAG = GameViewModel::class.qualifiedName

    data class State(
        val progressVisible: Boolean,
        val showUrl: String,
        val error: String
    )

    sealed class Action {
        object Start : Action()
        object StartProgress : Action()
        object StopProgress : Action()

        data class ShowError(val error: Throwable) : Action()
        object HideError : Action()
    }

    private val _state = MutableLiveData<State>()
    val state: LiveData<State> = _state

    private val _actions: Relay<Action> = PublishRelay.create()
    val actions: Consumer<Action> = _actions

    private val disposables = CompositeDisposable()

    init {
        disposables.add(
            _actions.reduxStore(
                    initialState = State(
                        progressVisible = false,
                        showUrl = "",
                        error = ""
                    ),
                    sideEffects = listOf(
                        ::start
                    ),
                    reducer = ::reducer
                )
                .distinctUntilChanged()
                .observeOn(schedulerProvider.ui())
                .subscribe { newState ->
                    _state.value = newState
                })
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }

    private fun reducer(state: State, action: Action): State {
        return when (action) {
            is Action.ShowError -> state.copy(
                error = stringProvider.get(action.error.message ?: ""),
                progressVisible = false
            )
            is Action.HideError -> state.copy(
                error = ""
            )
            is Action.StartProgress -> state.copy(
                progressVisible = true
            )
            is Action.StopProgress -> state.copy(
                progressVisible = false
            )
            else -> state
        }
    }


    private fun start(
        actions: Observable<Action>,
        state: StateAccessor<State>
    ): Observable<Action> =
        actions.ofType(Action.Start::class.java)
            .switchMap {
                Observable.empty<Action>()
            }

    class Factory(val api: MemberApiProvider, val sessionProvider: SessionProvider, val stringProvider: StringProvider, val schedulerProvider: SchedulerProvider) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return GameViewModel(
                api,
                sessionProvider,
                stringProvider,
                schedulerProvider
            ) as T
        }
    }
}