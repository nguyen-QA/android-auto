package com.caipiao.caipiao.ui.mygames

import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageButton
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.caipiao.caipiao.R
import com.caipiao.caipiao.ui.AppActivity
import com.caipiao.caipiao.ui.auth.AuthIntent
import com.caipiao.caipiao.ui.game.GAME_ACTIVITY_EXTRA_URL
import com.caipiao.caipiao.ui.game.GameIntent
import com.caipiao.caipiao.ui.qr.IntentIntegrator
import com.caipiao.caipiao.ui.refer.ReferIntent
import com.shopify.livedataktx.distinct
import com.shopify.livedataktx.map
import com.squareup.picasso.Picasso
import dagger.android.HasAndroidInjector
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_my_games.*
import javax.inject.Inject


fun Context.MyGamesIntent(): Intent {
    return Intent(this, MyGamesActivity::class.java).apply {

    }
}

class MyGamesActivity : AppActivity(), HasAndroidInjector {
    @Inject
    lateinit var viewModelFactory: MyGamesViewModel.Factory

    val viewModel: MyGamesViewModel
        get() = _viewModel as MyGamesViewModel

    private lateinit var view: View
    private val TAG = MyGamesActivity::class.qualifiedName
    private val disposables = CompositeDisposable()
    private var bgPlayer: MediaPlayer? = null
    private var openPlayer: MediaPlayer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _viewModel = ViewModelProviders.of(this, viewModelFactory).get(MyGamesViewModel::class.java)

        setContentView(R.layout.activity_my_games)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        (recyclerView as RecyclerView).apply {
            setHasFixedSize(true)

            layoutManager = GridLayoutManager(this@MyGamesActivity, 2, RecyclerView.HORIZONTAL, false)
            adapter = Adapter(listOf())
        }

        viewModel.state.map { state ->
                state!!.items
            }
            .distinct()
            .observe(this, Observer { items ->
                ((recyclerView as RecyclerView).adapter as Adapter).update(items)
            })

        viewModel.state.map { state ->
                state!!.gold
            }
            .distinct()
            .observe(this, Observer { gold ->
                goldTextView.text = gold
            })

        viewModel.state.map { state ->
                state!!.showUrl
            }
            .distinct()
            .observe(this, Observer { showUrl ->
                if (!showUrl.isEmpty()) {
                    startActivity(GameIntent(showUrl).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION))
                    overridePendingTransition(0, 0)
                    viewModel.actions.accept(MyGamesViewModel.Action.GameShown)
                }
            })

        viewModel.state.map { state ->
                state!!.error
            }
            .distinct()
            .observe(this, Observer { error ->
                if (!error.isEmpty()) {
                    val msgs = error.split("|")
                    if (msgs.size == 2) {
                        Toast.makeText(applicationContext, stringProvider.get(msgs[0]) + "(" + msgs[1] + ")", Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(applicationContext, error, Toast.LENGTH_LONG).show()
                    }
                    viewModel.actions.accept(MyGamesViewModel.Action.HideError)
                }
            })
    }

    override fun onResume() {
        super.onResume()
        bgPlayer = MediaPlayer.create(this@MyGamesActivity, R.raw.hall_bg)
        bgPlayer?.isLooping = true
        bgPlayer?.start()

        openPlayer = MediaPlayer.create(this@MyGamesActivity, R.raw.window_open)

        viewModel.actions.accept(MyGamesViewModel.Action.Start)
    }

    override fun onPause() {
        super.onPause()
        bgPlayer?.stop()
        bgPlayer?.release()
        bgPlayer = null

        openPlayer?.release()
        openPlayer = null
    }

    inner class Adapter(private var data: List<MyGamesViewModel.Item>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
        private val VIEW_TYPE_GAME = 1

        inner class GameViewHolder(@NonNull val contentView: View) : RecyclerView.ViewHolder(contentView) {
            private val gameButton: ImageButton = contentView.findViewById(R.id.gameButton) as ImageButton

            fun bind(game: MyGamesViewModel.Item.Game) {
                Picasso.get().load(game.thumbnail).into(gameButton)
                gameButton.setOnClickListener { view ->
                    openPlayer?.start()
                    viewModel.actions.accept(MyGamesViewModel.Action.GamePicked(adapterPosition))
                }
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            when(viewType) {
                VIEW_TYPE_GAME -> {
                    val view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_game, parent, false)
                    return GameViewHolder(
                        view
                    )
                }
                else -> {
                    val view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_game, parent, false)
                    return GameViewHolder(
                        view
                    )
                }
            }
        }

        override fun getItemCount(): Int {
            return data.size
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            when(holder) {
                is GameViewHolder -> holder.bind(data[position] as MyGamesViewModel.Item.Game)
                else -> {}
            }

        }

        override fun getItemViewType(position: Int): Int {
            if (data[position] is MyGamesViewModel.Item.Game) {
                return VIEW_TYPE_GAME
            } else {
                return 0
            }
        }

        fun update(items: List<MyGamesViewModel.Item>) {
            val oldCount = data.size
            data = items
            if (items.isEmpty()) {
                notifyDataSetChanged()
            } else {
                notifyItemRangeInserted(oldCount, items.size - oldCount)
            }
        }
    }

}



