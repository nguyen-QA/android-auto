package com.caipiao.caipiao.di.activity

import android.app.Activity
import com.caipiao.caipiao.di.scopes.ActivityScope
import com.caipiao.caipiao.providers.MemberApiProvider
import com.caipiao.caipiao.providers.SchedulerProvider
import com.caipiao.caipiao.providers.SessionProvider
import com.caipiao.caipiao.providers.StringProvider
import com.caipiao.caipiao.ui.auth.AuthActivity
import com.caipiao.caipiao.ui.auth.AuthViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import dagger.android.AndroidInjector

@ActivityScope
@Subcomponent(modules = [
    AuthActivitySubcomponent.AuthActivityModule::class,
    AuthActivitySubcomponent.AuthActivityModule.ProvideViewModel::class
])
interface AuthActivitySubcomponent : AndroidInjector<AuthActivity> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<AuthActivity>()

    @Module
    abstract class AuthActivityModule {
        @Binds
        @ActivityScope
        abstract fun provideActivity(activity: AuthActivity): Activity

        @Module
        class ProvideViewModel {
            @Provides
            @ActivityScope
            fun provideAuthViewModelFactory(memberApi: MemberApiProvider, sessionProvider: SessionProvider, stringProvider: StringProvider, schedulerProvider: SchedulerProvider) =
                AuthViewModel.Factory(memberApi, sessionProvider, stringProvider, schedulerProvider)

        }
    }
}