package com.caipiao.caipiao.di.app

import android.app.Application
import android.content.Context
import com.caipiao.caipiao.di.scopes.AppScope
import com.caipiao.caipiao.providers.*
import com.caipiao.caipiao.providers.impl.*
import dagger.Module
import dagger.Provides

@Module
class AppModule {
    @Provides
    @AppScope
    fun provideContext(application: Application): Context = application

    @Provides
    @AppScope
    fun provideSession(context: Context): SessionProvider = AppSessionProvider(context)

    @Provides
    @AppScope
    fun provideScheduler(): SchedulerProvider = AppSchedulerProvider()

    @Provides
    @AppScope
    fun provideUtil(context: Context): UtilProvider = AppUtilProvider(context)

    @Provides
    @AppScope
    fun provideString(context: Context): StringProvider = AppStringProvider(context)

    @Provides
    fun provideApi(sessionProvider: SessionProvider): ApiProvider = AppApiProvider(sessionProvider)

    @Provides
    fun provideMemberApi(sessionProvider: SessionProvider): MemberApiProvider = AppMemberApiProvider(sessionProvider)

    @Provides
    fun provideGameApi(sessionProvider: SessionProvider): GameApiProvider = AppGameApiProvider(sessionProvider)

    @Provides
    fun provideAffiliateApi(sessionProvider: SessionProvider): AffiliateApiProvider = AppAffiliateApiProvider(sessionProvider)
}