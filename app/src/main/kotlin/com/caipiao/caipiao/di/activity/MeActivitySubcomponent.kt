package com.caipiao.caipiao.di.activity

import android.app.Activity
import com.caipiao.caipiao.di.scopes.ActivityScope
import com.caipiao.caipiao.providers.ApiProvider
import com.caipiao.caipiao.providers.SessionProvider
import com.caipiao.caipiao.providers.StringProvider
import com.caipiao.caipiao.ui.me.MeActivity
import com.caipiao.caipiao.ui.me.MeViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import dagger.android.AndroidInjector

@ActivityScope
@Subcomponent(modules = [
    MeActivitySubcomponent.MeActivityModule::class,
    MeActivitySubcomponent.MeActivityModule.ProvideViewModel::class
])
interface MeActivitySubcomponent : AndroidInjector<MeActivity> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<MeActivity>()

    @Module
    abstract class MeActivityModule {
        @Binds
        @ActivityScope
        abstract fun provideActivity(activity: MeActivity): Activity

        @Module
        class ProvideViewModel {
            @Provides
            @ActivityScope
            fun provideMeViewModelFactory(api: ApiProvider, sessionProvider: SessionProvider, stringProvider: StringProvider) =
                MeViewModel.Factory(api, sessionProvider, stringProvider)

        }
    }
}