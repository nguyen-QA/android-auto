package com.caipiao.caipiao.di.activity

import android.app.Activity
import com.caipiao.caipiao.di.scopes.ActivityScope
import com.caipiao.caipiao.providers.ApiProvider
import com.caipiao.caipiao.providers.SessionProvider
import com.caipiao.caipiao.providers.StringProvider
import com.caipiao.caipiao.ui.history.HistoryActivity
import com.caipiao.caipiao.ui.history.HistoryViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import dagger.android.AndroidInjector

@ActivityScope
@Subcomponent(modules = [
    HistoryActivitySubcomponent.HistoryActivityModule::class,
    HistoryActivitySubcomponent.HistoryActivityModule.ProvideViewModel::class
])
interface HistoryActivitySubcomponent : AndroidInjector<HistoryActivity> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<HistoryActivity>()

    @Module
    abstract class HistoryActivityModule {
        @Binds
        @ActivityScope
        abstract fun provideActivity(activity: HistoryActivity): Activity

        @Module
        class ProvideViewModel {
            @Provides
            @ActivityScope
            fun provideHistoryViewModelFactory(api: ApiProvider, sessionProvider: SessionProvider, stringProvider: StringProvider) =
                HistoryViewModel.Factory(api, sessionProvider, stringProvider)

        }
    }
}