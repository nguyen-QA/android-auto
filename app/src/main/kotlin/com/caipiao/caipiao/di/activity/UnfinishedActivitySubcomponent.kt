package com.caipiao.caipiao.di.activity

import android.app.Activity
import com.caipiao.caipiao.di.scopes.ActivityScope
import com.caipiao.caipiao.providers.ApiProvider
import com.caipiao.caipiao.providers.SessionProvider
import com.caipiao.caipiao.providers.StringProvider
import com.caipiao.caipiao.ui.unfinished.UnfinishedActivity
import com.caipiao.caipiao.ui.unfinished.UnfinishedViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import dagger.android.AndroidInjector

@ActivityScope
@Subcomponent(modules = [
    UnfinishedActivitySubcomponent.FinishedActivityModule::class,
    UnfinishedActivitySubcomponent.FinishedActivityModule.ProvideViewModel::class
])
interface UnfinishedActivitySubcomponent : AndroidInjector<UnfinishedActivity> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<UnfinishedActivity>()

    @Module
    abstract class FinishedActivityModule {
        @Binds
        @ActivityScope
        abstract fun provideActivity(activity: UnfinishedActivity): Activity

        @Module
        class ProvideViewModel {
            @Provides
            @ActivityScope
            fun provideUnfinishedViewModelFactory(api: ApiProvider, sessionProvider: SessionProvider, stringProvider: StringProvider) =
                UnfinishedViewModel.Factory(api, sessionProvider, stringProvider)

        }
    }
}