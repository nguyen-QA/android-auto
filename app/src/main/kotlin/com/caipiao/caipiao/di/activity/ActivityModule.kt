package com.caipiao.caipiao.di.activity

import com.caipiao.caipiao.ui.auth.AuthActivity
import com.caipiao.caipiao.ui.finished.FinishedActivity
import com.caipiao.caipiao.ui.game.GameActivity
import com.caipiao.caipiao.ui.history.HistoryActivity
import com.caipiao.caipiao.ui.home.HomeActivity
import com.caipiao.caipiao.ui.kyqp.KyqpActivity
import com.caipiao.caipiao.ui.main.MainActivity
import com.caipiao.caipiao.ui.me.MeActivity
import com.caipiao.caipiao.ui.mygames.MyGamesActivity
import com.caipiao.caipiao.ui.qr.CaptureActivity
import com.caipiao.caipiao.ui.refer.ReferActivity
import com.caipiao.caipiao.ui.unfinished.UnfinishedActivity
import com.caipiao.caipiao.ui.verify.AuthVerifyActivity
import com.caipiao.caipiao.ui.xyttc.XyttcActivity
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.multibindings.ClassKey
import dagger.multibindings.IntoMap

@Module(subcomponents = [
    MainActivitySubcomponent::class,
    XyttcActivitySubcomponent::class,
    FinishedActivitySubcomponent::class,
    MeActivitySubcomponent::class,
    HistoryActivitySubcomponent::class,
    UnfinishedActivitySubcomponent::class,
    AuthActivitySubcomponent::class,
    AuthVerifyActivitySubcomponent::class,
    ReferActivitySubcomponent::class,
    HomeActivitySubcomponent::class,
    KyqpActivitySubcomponent::class,
    GameActivitySubcomponent::class,
    CaptureActivitySubcomponent::class,
    MyGamesActivitySubcomponent::class
])
abstract class ActivityModule {
    @Binds
    @IntoMap
    @ClassKey(MainActivity::class)
    abstract fun bindMainActivityInjectorFactory(builder: MainActivitySubcomponent.Builder): AndroidInjector.Factory<*>

    @Binds
    @IntoMap
    @ClassKey(XyttcActivity::class)
    abstract fun bindXyttcActivityInjectorFactory(builder: XyttcActivitySubcomponent.Builder): AndroidInjector.Factory<*>

    @Binds
    @IntoMap
    @ClassKey(FinishedActivity::class)
    abstract fun bindFinishedActivityInjectorFactory(builder: FinishedActivitySubcomponent.Builder): AndroidInjector.Factory<*>

    @Binds
    @IntoMap
    @ClassKey(HistoryActivity::class)
    abstract fun bindHistoryActivityInjectorFactory(builder: HistoryActivitySubcomponent.Builder): AndroidInjector.Factory<*>

    @Binds
    @IntoMap
    @ClassKey(MeActivity::class)
    abstract fun bindMeActivityInjectorFactory(builder: MeActivitySubcomponent.Builder): AndroidInjector.Factory<*>

    @Binds
    @IntoMap
    @ClassKey(UnfinishedActivity::class)
    abstract fun bindUnfinishedActivityInjectorFactory(builder: UnfinishedActivitySubcomponent.Builder): AndroidInjector.Factory<*>

    @Binds
    @IntoMap
    @ClassKey(AuthActivity::class)
    abstract fun bindAuthActivityInjectorFactory(builder: AuthActivitySubcomponent.Builder): AndroidInjector.Factory<*>

    @Binds
    @IntoMap
    @ClassKey(AuthVerifyActivity::class)
    abstract fun bindAuthVerifyActivityInjectorFactory(builder: AuthVerifyActivitySubcomponent.Builder): AndroidInjector.Factory<*>

    @Binds
    @IntoMap
    @ClassKey(ReferActivity::class)
    abstract fun bindReferActivityInjectorFactory(builder: ReferActivitySubcomponent.Builder): AndroidInjector.Factory<*>

    @Binds
    @IntoMap
    @ClassKey(HomeActivity::class)
    abstract fun bindHomeActivityInjectorFactory(builder: HomeActivitySubcomponent.Builder): AndroidInjector.Factory<*>

    @Binds
    @IntoMap
    @ClassKey(KyqpActivity::class)
    abstract fun bindKyqpActivityInjectorFactory(builder: KyqpActivitySubcomponent.Builder): AndroidInjector.Factory<*>

    @Binds
    @IntoMap
    @ClassKey(GameActivity::class)
    abstract fun bindGameActivityInjectorFactory(builder: GameActivitySubcomponent.Builder): AndroidInjector.Factory<*>

    @Binds
    @IntoMap
    @ClassKey(CaptureActivity::class)
    abstract fun bindCaptureActivityInjectorFactory(builder: CaptureActivitySubcomponent.Builder): AndroidInjector.Factory<*>

    @Binds
    @IntoMap
    @ClassKey(MyGamesActivity::class)
    abstract fun bindMyGamesActivityInjectorFactory(builder: MyGamesActivitySubcomponent.Builder): AndroidInjector.Factory<*>
}