package com.caipiao.caipiao.di.activity

import android.app.Activity
import com.caipiao.caipiao.di.scopes.ActivityScope
import com.caipiao.caipiao.providers.*
import com.caipiao.caipiao.ui.verify.AuthVerifyActivity
import com.caipiao.caipiao.ui.verify.AuthVerifyViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import dagger.android.AndroidInjector

@ActivityScope
@Subcomponent(modules = [
    AuthVerifyActivitySubcomponent.AuthVerifyActivityModule::class,
    AuthVerifyActivitySubcomponent.AuthVerifyActivityModule.ProvideViewModel::class
])
interface AuthVerifyActivitySubcomponent : AndroidInjector<AuthVerifyActivity> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<AuthVerifyActivity>()

    @Module
    abstract class AuthVerifyActivityModule {
        @Binds
        @ActivityScope
        abstract fun provideActivity(activity: AuthVerifyActivity): Activity

        @Module
        class ProvideViewModel {
            @Provides
            @ActivityScope
            fun provideAuthVerifyViewModelFactory(memberApi: MemberApiProvider, sessionProvider: SessionProvider, stringProvider: StringProvider, schedulerProvider: SchedulerProvider) =
                AuthVerifyViewModel.Factory(memberApi, sessionProvider, stringProvider, schedulerProvider)

        }
    }
}