package com.caipiao.caipiao.di.activity

import android.app.Activity
import com.caipiao.caipiao.di.scopes.ActivityScope
import com.caipiao.caipiao.providers.*
import com.caipiao.caipiao.ui.refer.ReferActivity
import com.caipiao.caipiao.ui.refer.ReferViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import dagger.android.AndroidInjector

@ActivityScope
@Subcomponent(modules = [
    ReferActivitySubcomponent.ReferActivityModule::class,
    ReferActivitySubcomponent.ReferActivityModule.ProvideViewModel::class
])
interface ReferActivitySubcomponent : AndroidInjector<ReferActivity> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<ReferActivity>()

    @Module
    abstract class ReferActivityModule {
        @Binds
        @ActivityScope
        abstract fun provideActivity(activity: ReferActivity): Activity

        @Module
        class ProvideViewModel {
            @Provides
            @ActivityScope
            fun provideReferViewModelFactory(memberApi: MemberApiProvider, affiliateApi: AffiliateApiProvider, sessionProvider: SessionProvider, stringProvider: StringProvider, utilProvider: UtilProvider) =
                ReferViewModel.Factory(memberApi, affiliateApi, sessionProvider, stringProvider, utilProvider)

        }
    }
}