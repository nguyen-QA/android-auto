package com.caipiao.caipiao.di.activity

import android.app.Activity
import com.caipiao.caipiao.di.scopes.ActivityScope
import com.caipiao.caipiao.providers.*
import com.caipiao.caipiao.ui.main.MainActivity
import com.caipiao.caipiao.ui.main.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import dagger.android.AndroidInjector

@ActivityScope
@Subcomponent(modules = [
    MainActivitySubcomponent.MainActivityModule::class,
    MainActivitySubcomponent.MainActivityModule.ProvideViewModel::class
])
interface MainActivitySubcomponent : AndroidInjector<MainActivity> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<MainActivity>()

    @Module
    abstract class MainActivityModule {
        @Binds
        @ActivityScope
        abstract fun provideActivity(activity: MainActivity): Activity

        @Module
        class ProvideViewModel {
            @Provides
            @ActivityScope
            fun provideMainViewModelFactory(memberApi: MemberApiProvider, sessionProvider: SessionProvider, stringProvider: StringProvider, utilProvider: UtilProvider, schedulerProvider: SchedulerProvider) =
                MainViewModel.Factory(memberApi, sessionProvider, stringProvider, utilProvider, schedulerProvider)

        }
    }
}