package com.caipiao.caipiao.di.activity

import android.app.Activity
import com.caipiao.caipiao.di.scopes.ActivityScope
import com.caipiao.caipiao.providers.MemberApiProvider
import com.caipiao.caipiao.providers.SchedulerProvider
import com.caipiao.caipiao.providers.SessionProvider
import com.caipiao.caipiao.providers.StringProvider
import com.caipiao.caipiao.ui.game.GameActivity
import com.caipiao.caipiao.ui.game.GameViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import dagger.android.AndroidInjector

@ActivityScope
@Subcomponent(modules = [
    GameActivitySubcomponent.GameActivityModule::class,
    GameActivitySubcomponent.GameActivityModule.ProvideViewModel::class
])
interface GameActivitySubcomponent : AndroidInjector<GameActivity> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<GameActivity>()

    @Module
    abstract class GameActivityModule {
        @Binds
        @ActivityScope
        abstract fun provideActivity(activity: GameActivity): Activity

        @Module
        class ProvideViewModel {
            @Provides
            @ActivityScope
            fun provideGameViewModelFactory(api: MemberApiProvider, sessionProvider: SessionProvider, stringProvider: StringProvider, schedulerProvider: SchedulerProvider) =
                GameViewModel.Factory(api, sessionProvider, stringProvider, schedulerProvider)

        }
    }
}