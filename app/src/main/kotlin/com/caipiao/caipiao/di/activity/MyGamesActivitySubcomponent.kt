package com.caipiao.caipiao.di.activity

import android.app.Activity
import com.caipiao.caipiao.di.scopes.ActivityScope
import com.caipiao.caipiao.providers.*
import com.caipiao.caipiao.ui.mygames.MyGamesActivity
import com.caipiao.caipiao.ui.mygames.MyGamesViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import dagger.android.AndroidInjector

@ActivityScope
@Subcomponent(modules = [
    MyGamesActivitySubcomponent.MyGamesActivityModule::class,
    MyGamesActivitySubcomponent.MyGamesActivityModule.ProvideViewModel::class
])
interface MyGamesActivitySubcomponent : AndroidInjector<MyGamesActivity> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<MyGamesActivity>()

    @Module
    abstract class MyGamesActivityModule {
        @Binds
        @ActivityScope
        abstract fun provideActivity(activity: MyGamesActivity): Activity

        @Module
        class ProvideViewModel {
            @Provides
            @ActivityScope
            fun provideMyGamesViewModelFactory(gameApi: GameApiProvider, memberApi: MemberApiProvider, sessionProvider: SessionProvider, stringProvider: StringProvider, schedulerProvider: SchedulerProvider) =
                MyGamesViewModel.Factory(gameApi, memberApi, sessionProvider, stringProvider, schedulerProvider)

        }
    }
}