package com.caipiao.caipiao.di.activity

import android.app.Activity
import com.caipiao.caipiao.di.scopes.ActivityScope
import com.caipiao.caipiao.providers.ApiProvider
import com.caipiao.caipiao.providers.SessionProvider
import com.caipiao.caipiao.providers.StringProvider
import com.caipiao.caipiao.ui.kyqp.KyqpActivity
import com.caipiao.caipiao.ui.kyqp.KyqpViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import dagger.android.AndroidInjector

@ActivityScope
@Subcomponent(modules = [
    KyqpActivitySubcomponent.KyqpActivityModule::class,
    KyqpActivitySubcomponent.KyqpActivityModule.ProvideViewModel::class
])
interface KyqpActivitySubcomponent : AndroidInjector<KyqpActivity> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<KyqpActivity>()

    @Module
    abstract class KyqpActivityModule {
        @Binds
        @ActivityScope
        abstract fun provideActivity(activity: KyqpActivity): Activity

        @Module
        class ProvideViewModel {
            @Provides
            @ActivityScope
            fun provideKyqpViewModelFactory(api: ApiProvider, sessionProvider: SessionProvider, stringProvider: StringProvider) =
                KyqpViewModel.Factory(api, sessionProvider, stringProvider)

        }
    }
}