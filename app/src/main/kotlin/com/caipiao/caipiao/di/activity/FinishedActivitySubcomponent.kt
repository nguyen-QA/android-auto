package com.caipiao.caipiao.di.activity

import android.app.Activity
import com.caipiao.caipiao.di.scopes.ActivityScope
import com.caipiao.caipiao.providers.ApiProvider
import com.caipiao.caipiao.providers.SessionProvider
import com.caipiao.caipiao.providers.StringProvider
import com.caipiao.caipiao.ui.finished.FinishedActivity
import com.caipiao.caipiao.ui.finished.FinishedViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import dagger.android.AndroidInjector

@ActivityScope
@Subcomponent(modules = [
    FinishedActivitySubcomponent.FinishedActivityModule::class,
    FinishedActivitySubcomponent.FinishedActivityModule.ProvideViewModel::class
])
interface FinishedActivitySubcomponent : AndroidInjector<FinishedActivity> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<FinishedActivity>()

    @Module
    abstract class FinishedActivityModule {
        @Binds
        @ActivityScope
        abstract fun provideActivity(activity: FinishedActivity): Activity

        @Module
        class ProvideViewModel {
            @Provides
            @ActivityScope
            fun provideFinishedViewModelFactory(api: ApiProvider, sessionProvider: SessionProvider, stringProvider: StringProvider) =
                FinishedViewModel.Factory(api, sessionProvider, stringProvider)

        }
    }
}