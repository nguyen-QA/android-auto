package com.caipiao.caipiao.di.activity

import android.app.Activity
import com.caipiao.caipiao.di.scopes.ActivityScope
import com.caipiao.caipiao.providers.ApiProvider
import com.caipiao.caipiao.providers.SessionProvider
import com.caipiao.caipiao.providers.StringProvider
import com.caipiao.caipiao.ui.xyttc.XyttcActivity
import com.caipiao.caipiao.ui.xyttc.XyttcViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import dagger.android.AndroidInjector

@ActivityScope
@Subcomponent(modules = [
    XyttcActivitySubcomponent.XyttcActivityModule::class,
    XyttcActivitySubcomponent.XyttcActivityModule.ProvideViewModel::class
])
interface XyttcActivitySubcomponent : AndroidInjector<XyttcActivity> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<XyttcActivity>()

    @Module
    abstract class XyttcActivityModule {
        @Binds
        @ActivityScope
        abstract fun provideActivity(activity: XyttcActivity): Activity

        @Module
        class ProvideViewModel {
            @Provides
            @ActivityScope
            fun provideXyttcViewModelFactory(api: ApiProvider, sessionProvider: SessionProvider, stringProvider: StringProvider) =
                XyttcViewModel.Factory(api, sessionProvider, stringProvider)

        }
    }
}