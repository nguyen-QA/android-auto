package com.caipiao.caipiao.di.activity

import android.app.Activity
import com.caipiao.caipiao.di.scopes.ActivityScope
import com.caipiao.caipiao.providers.*
import com.caipiao.caipiao.ui.home.HomeActivity
import com.caipiao.caipiao.ui.home.HomeViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import dagger.android.AndroidInjector

@ActivityScope
@Subcomponent(modules = [
    HomeActivitySubcomponent.HomeActivityModule::class,
    HomeActivitySubcomponent.HomeActivityModule.ProvideViewModel::class
])
interface HomeActivitySubcomponent : AndroidInjector<HomeActivity> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<HomeActivity>()

    @Module
    abstract class HomeActivityModule {
        @Binds
        @ActivityScope
        abstract fun provideActivity(activity: HomeActivity): Activity

        @Module
        class ProvideViewModel {
            @Provides
            @ActivityScope
            fun provideHomeViewModelFactory(gameApi: GameApiProvider, memberApi: MemberApiProvider, affiliateApi: AffiliateApiProvider, sessionProvider: SessionProvider, stringProvider: StringProvider, schedulerProvider: SchedulerProvider) =
                HomeViewModel.Factory(gameApi, memberApi, affiliateApi, sessionProvider, stringProvider, schedulerProvider)

        }
    }
}