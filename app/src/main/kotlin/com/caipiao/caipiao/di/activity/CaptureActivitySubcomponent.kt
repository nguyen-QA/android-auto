package com.caipiao.caipiao.di.activity

import android.app.Activity
import com.caipiao.caipiao.di.scopes.ActivityScope
import com.caipiao.caipiao.ui.qr.CaptureActivity
import dagger.Binds
import dagger.Module
import dagger.Subcomponent
import dagger.android.AndroidInjector

@ActivityScope
@Subcomponent(modules = [
    CaptureActivitySubcomponent.CaptureActivityModule::class
])
interface CaptureActivitySubcomponent : AndroidInjector<CaptureActivity> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<CaptureActivity>()

    @Module
    abstract class CaptureActivityModule {
        @Binds
        @ActivityScope
        abstract fun provideActivity(activity: CaptureActivity): Activity

    }
}