/*
 * Caipiao Server API
 * Caipiao Server APIs provides certificate authority services.
 *
 * OpenAPI spec version: 1.0.0
 *
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.client.model.BetData;
import io.swagger.client.model.ResponseMessageData;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * GetUnfinishedResponseData
 */

public class GetUnfinishedResponseData {
    @SerializedName("success")
    private Boolean success = null;

    @SerializedName("error")
    private ResponseMessageData error = null;

    @SerializedName("message")
    private ResponseMessageData message = null;

    @SerializedName("history")
    private List<BetData> history = null;

    public GetUnfinishedResponseData success(Boolean success) {
        this.success = success;
        return this;
    }

    /**
     * Boolean indicating if the request was successful.
     * @return success
     **/
    @ApiModelProperty(required = true, value = "Boolean indicating if the request was successful.")
    public Boolean isSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public GetUnfinishedResponseData error(ResponseMessageData error) {
        this.error = error;
        return this;
    }

    /**
     * error message (i.e. code and string messages).
     * @return error
     **/
    @ApiModelProperty(value = "error message (i.e. code and string messages).")
    public ResponseMessageData getError() {
        return error;
    }

    public void setError(ResponseMessageData error) {
        this.error = error;
    }

    public GetUnfinishedResponseData message(ResponseMessageData message) {
        this.message = message;
        return this;
    }

    /**
     * informational messages (i.e. code and string messages).
     * @return message
     **/
    @ApiModelProperty(value = "informational messages (i.e. code and string messages).")
    public ResponseMessageData getMessage() {
        return message;
    }

    public void setMessage(ResponseMessageData message) {
        this.message = message;
    }

    public GetUnfinishedResponseData history(List<BetData> history) {
        this.history = history;
        return this;
    }

    public GetUnfinishedResponseData addHistoryItem(BetData historyItem) {
        if (this.history == null) {
            this.history = new ArrayList<BetData>();
        }
        this.history.add(historyItem);
        return this;
    }

    /**
     * list of bets
     * @return history
     **/
    @ApiModelProperty(value = "list of bets")
    public List<BetData> getHistory() {
        return history;
    }

    public void setHistory(List<BetData> history) {
        this.history = history;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GetUnfinishedResponseData getUnfinishedResponse = (GetUnfinishedResponseData) o;
        return Objects.equals(this.success, getUnfinishedResponse.success) &&
                Objects.equals(this.error, getUnfinishedResponse.error) &&
                Objects.equals(this.message, getUnfinishedResponse.message) &&
                Objects.equals(this.history, getUnfinishedResponse.history);
    }

    @Override
    public int hashCode() {
        return Objects.hash(success, error, message, history);
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class GetUnfinishedResponseData {\n");

        sb.append("    success: ").append(toIndentedString(success)).append("\n");
        sb.append("    error: ").append(toIndentedString(error)).append("\n");
        sb.append("    message: ").append(toIndentedString(message)).append("\n");
        sb.append("    history: ").append(toIndentedString(history)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

}

