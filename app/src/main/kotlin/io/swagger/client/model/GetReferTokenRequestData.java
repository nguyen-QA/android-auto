/*
 * Caipiao Server API
 * Caipiao Server APIs provides certificate authority services.
 *
 * OpenAPI spec version: 1.0.0
 *
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * GetReferTokenRequestData
 */

public class GetReferTokenRequestData {
  @SerializedName("userId")
  private String userId = null;

  @SerializedName("forceNew")
  private Boolean forceNew = false;

  public GetReferTokenRequestData userId(String userId) {
    this.userId = userId;
    return this;
  }

  /**
   * the user id requesting the token
   * @return userId
   **/
  @ApiModelProperty(required = true, value = "the user id requesting the token")
  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public GetReferTokenRequestData forceNew(Boolean forceNew) {
    this.forceNew = forceNew;
    return this;
  }

  /**
   * force to generate new token even it&#39;s not expired
   * @return forceNew
   **/
  @ApiModelProperty(value = "force to generate new token even it's not expired")
  public Boolean isForceNew() {
    return forceNew;
  }

  public void setForceNew(Boolean forceNew) {
    this.forceNew = forceNew;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GetReferTokenRequestData getReferTokenRequest = (GetReferTokenRequestData) o;
    return Objects.equals(this.userId, getReferTokenRequest.userId) &&
            Objects.equals(this.forceNew, getReferTokenRequest.forceNew);
  }

  @Override
  public int hashCode() {
    return Objects.hash(userId, forceNew);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetReferTokenRequestData {\n");

    sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
    sb.append("    forceNew: ").append(toIndentedString(forceNew)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

