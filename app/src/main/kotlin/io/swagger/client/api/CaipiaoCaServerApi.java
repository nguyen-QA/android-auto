package io.swagger.client.api;

import io.swagger.client.CollectionFormats.*;

import io.reactivex.Observable;
import io.reactivex.Completable;
import retrofit2.http.*;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.MultipartBody;

import io.swagger.client.model.ActionTranRequestData;
import io.swagger.client.model.ActionTranResponseData;
import io.swagger.client.model.BuyLotteryRequestData;
import io.swagger.client.model.BuyLotteryResponseData;
import io.swagger.client.model.CurrentDrawsResponseData;
import io.swagger.client.model.EnrollGuestRequestData;
import io.swagger.client.model.EnrollGuestResponseData;
import io.swagger.client.model.EnrollPhoneRequestData;
import io.swagger.client.model.EnrollPhoneResponseData;
import io.swagger.client.model.EnrollWechatRequestData;
import io.swagger.client.model.EnrollWechatResponseData;
import io.swagger.client.model.GetDrawHistoryResponseData;
import io.swagger.client.model.GetDrawsResponseData;
import io.swagger.client.model.GetFinishedResponseData;
import io.swagger.client.model.GetReferTokenResponseData;
import io.swagger.client.model.GetUnfinishedResponseData;
import io.swagger.client.model.GetWalletResponseData;
import io.swagger.client.model.ListTransResponseData;
import io.swagger.client.model.NtpResponseData;
import io.swagger.client.model.RegisterPhoneRequestData;
import io.swagger.client.model.RegisterPhoneResponseData;
import io.swagger.client.model.RegisterWechatRequestData;
import io.swagger.client.model.RegisterWechatResponseData;
import io.swagger.client.model.RequestTranRequestData;
import io.swagger.client.model.RequestTranResponseData;
import io.swagger.client.model.VerifyTokenResponseData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface CaipiaoCaServerApi {
  /**
   *
   * action
   * @param body The request body (required)
   * @return Observable&lt;ActionTranResponseData&gt;
   */
  @Headers({
          "Content-Type:application/json"
  })
  @POST("api/v1/actiontran")
  Observable<ActionTranResponseData> actionTran(
          @retrofit2.http.Body ActionTranRequestData body
  );

  /**
   *
   * buy xin yun tian tian cai
   * @param body The request body (required)
   * @return Observable&lt;BuyLotteryResponseData&gt;
   */
  @Headers({
          "Content-Type:application/json"
  })
  @POST("api/v1/buylottery")
  Observable<BuyLotteryResponseData> buyLottery(
          @retrofit2.http.Body BuyLotteryRequestData body
  );

  /**
   *
   * Enroll a guest and return an enrollment certificate.
   * @param body The request body (required)
   * @return Observable&lt;EnrollGuestResponseData&gt;
   */
  @Headers({
          "Content-Type:application/json"
  })
  @POST("api/v1/enrollguest")
  Observable<EnrollGuestResponseData> enrollGuest(
          @retrofit2.http.Body EnrollGuestRequestData body
  );

  /**
   *
   * Enroll a new identity and return an enrollment certificate.
   * @param body The request body (required)
   * @return Observable&lt;EnrollPhoneResponseData&gt;
   */
  @Headers({
          "Content-Type:application/json"
  })
  @POST("api/v1/enrollphone")
  Observable<EnrollPhoneResponseData> enrollPhone(
          @retrofit2.http.Body EnrollPhoneRequestData body
  );

  /**
   *
   * Enroll a new identity and return an enrollment certificate.
   * @param body The request body (required)
   * @return Observable&lt;EnrollWechatResponseData&gt;
   */
  @Headers({
          "Content-Type:application/json"
  })
  @POST("api/v1/enrollwechat")
  Observable<EnrollWechatResponseData> enrollWechat(
          @retrofit2.http.Body EnrollWechatRequestData body
  );

  /**
   *
   * Get current active draw, error if no active draw
   * @return Observable&lt;CurrentDrawsResponseData&gt;
   */
  @Headers({
          "Content-Type:application/json"
  })
  @GET("api/v1/currentdraws")
  Observable<CurrentDrawsResponseData> getCurrentDraws();


  /**
   *
   * get draw history page by page
   * @param privateKey  (required)
   * @param cert  (required)
   * @param name  (required)
   * @param bookmark  (optional)
   * @return Observable&lt;GetDrawHistoryResponseData&gt;
   */
  @Headers({
          "Content-Type:application/json"
  })
  @GET("api/v1/drawHistory")
  Observable<GetDrawHistoryResponseData> getDrawHistory(
          @retrofit2.http.Header("PrivateKey") String privateKey, @retrofit2.http.Header("Cert") String cert, @retrofit2.http.Query("name") String name, @retrofit2.http.Query("bookmark") String bookmark
  );

  /**
   *
   * get specified draws
   * @param drawIds  (required)
   * @return Observable&lt;GetDrawsResponseData&gt;
   */
  @Headers({
          "Content-Type:application/json"
  })
  @GET("api/v1/draws")
  Observable<GetDrawsResponseData> getDraws(
          @retrofit2.http.Query("drawIds") CSVParams drawIds
  );

  /**
   *
   * get my finished bets page by page
   * @param privateKey  (required)
   * @param cert  (required)
   * @param userId  (required)
   * @param bookmark  (optional)
   * @return Observable&lt;GetFinishedResponseData&gt;
   */
  @Headers({
          "Content-Type:application/json"
  })
  @GET("api/v1/finished")
  Observable<GetFinishedResponseData> getFinished(
          @retrofit2.http.Header("PrivateKey") String privateKey, @retrofit2.http.Header("Cert") String cert, @retrofit2.http.Query("userId") String userId, @retrofit2.http.Query("bookmark") String bookmark
  );

  /**
   *
   * Get server timestamp to calculate time discrepency to client
   * @param clientTimezoneId  (required)
   * @return Observable&lt;NtpResponseData&gt;
   */
  @Headers({
          "Content-Type:application/json"
  })
  @GET("api/v1/ntp")
  Observable<NtpResponseData> getNtp(
          @retrofit2.http.Query("clientTimezoneId") String clientTimezoneId
  );

  /**
   *
   * get refer token if not expired. or generate and return new token if expired or force to
   * @param privateKey  (required)
   * @param cert  (required)
   * @param userId  (required)
   * @param forceNew  (optional, default to false)
   * @return Observable&lt;GetReferTokenResponseData&gt;
   */
  @Headers({
          "Content-Type:application/json"
  })
  @GET("api/v1/refertoken")
  Observable<GetReferTokenResponseData> getReferToken(
          @retrofit2.http.Header("PrivateKey") String privateKey, @retrofit2.http.Header("Cert") String cert, @retrofit2.http.Query("userId") String userId, @retrofit2.http.Query("forceNew") Boolean forceNew
  );

  /**
   *
   * get my unfinished bets page by page
   * @param privateKey  (required)
   * @param cert  (required)
   * @param userId  (required)
   * @return Observable&lt;GetUnfinishedResponseData&gt;
   */
  @Headers({
          "Content-Type:application/json"
  })
  @GET("api/v1/unfinished")
  Observable<GetUnfinishedResponseData> getUnfinished(
          @retrofit2.http.Header("PrivateKey") String privateKey, @retrofit2.http.Header("Cert") String cert, @retrofit2.http.Query("userId") String userId
  );

  /**
   *
   * get my wallet balance
   * @param privateKey  (required)
   * @param cert  (required)
   * @param userId  (required)
   * @return Observable&lt;GetWalletResponseData&gt;
   */
  @Headers({
          "Content-Type:application/json"
  })
  @GET("api/v1/wallet")
  Observable<GetWalletResponseData> getWallet(
          @retrofit2.http.Header("PrivateKey") String privateKey, @retrofit2.http.Header("Cert") String cert, @retrofit2.http.Query("userId") String userId
  );

  /**
   *
   * get list of transactions
   * @param operator  (required)
   * @param type  (required)
   * @param privateKey  (required)
   * @param cert  (required)
   * @return Observable&lt;ListTransResponseData&gt;
   */
  @Headers({
          "Content-Type:application/json"
  })
  @GET("api/v1/listtrans")
  Observable<ListTransResponseData> listTrans(
          @retrofit2.http.Query("operator") String operator, @retrofit2.http.Query("type") String type, @retrofit2.http.Header("PrivateKey") String privateKey, @retrofit2.http.Header("Cert") String cert
  );

  /**
   *
   * Give a phone number and get the token in the SMS
   * @param body The request body (required)
   * @return Observable&lt;RegisterPhoneResponseData&gt;
   */
  @Headers({
          "Content-Type:application/json"
  })
  @POST("api/v1/registerphone")
  Observable<RegisterPhoneResponseData> registerPhone(
          @retrofit2.http.Body RegisterPhoneRequestData body
  );

  /**
   *
   * Give a phone number and get the token in the SMS
   * @param body The request body (required)
   * @return Observable&lt;RegisterWechatResponseData&gt;
   */
  @Headers({
          "Content-Type:application/json"
  })
  @POST("api/v1/registerwechat")
  Observable<RegisterWechatResponseData> registerWechat(
          @retrofit2.http.Body RegisterWechatRequestData body
  );

  /**
   *
   * request transaction
   * @param body The request body (required)
   * @return Observable&lt;RequestTranResponseData&gt;
   */
  @Headers({
          "Content-Type:application/json"
  })
  @POST("api/v1/requesttran")
  Observable<RequestTranResponseData> requestTran(
          @retrofit2.http.Body RequestTranRequestData body
  );

  /**
   *
   * verify invitation token
   * @param privateKey  (required)
   * @param cert  (required)
   * @param userId  (required)
   * @param token  (required)
   * @return Observable&lt;VerifyTokenResponseData&gt;
   */
  @Headers({
          "Content-Type:application/json"
  })
  @GET("api/v1/verifytoken")
  Observable<VerifyTokenResponseData> verifyToken(
          @retrofit2.http.Header("PrivateKey") String privateKey, @retrofit2.http.Header("Cert") String cert, @retrofit2.http.Query("userId") String userId, @retrofit2.http.Query("token") String token
  );

}
