
# EnrollPhoneResponseMessages

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **Integer** | Integer code denoting the type of message. | 
**message** | **String** | A more specific message. | 



