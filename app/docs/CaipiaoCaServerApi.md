# CaipiaoCaServerApi

All URIs are relative to *http://18.204.207.30:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**enrollPhone**](CaipiaoCaServerApi.md#enrollPhone) | **POST** api/v1/enrollphone | 


<a name="enrollPhone"></a>
# **enrollPhone**
> EnrollPhoneResponse enrollPhone(body)



Enroll a new identity and return an enrollment certificate.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.CaipiaoCaServerApi;


CaipiaoCaServerApi apiInstance = new CaipiaoCaServerApi();
EnrollPhoneRequest body = new EnrollPhoneRequest(); // EnrollPhoneRequest | The request body
try {
    EnrollPhoneResponse result = apiInstance.enrollPhone(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CaipiaoCaServerApi#enrollPhone");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**EnrollPhoneRequest**](EnrollPhoneRequest.md)| The request body |

### Return type

[**EnrollPhoneResponse**](EnrollPhoneResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

