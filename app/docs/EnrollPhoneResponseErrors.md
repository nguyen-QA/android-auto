
# EnrollPhoneResponseErrors

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **Integer** | Integer code denoting the type of error. | 
**message** | **String** | An error message | 



