
# EnrollPhoneRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**phone** | **String** | the unique phone number of the user | 
**token** | **String** | the token sent to the user through SMS | 
**csr** | **String** | A PEM-encoded string containing the CSR (Certificate Signing Request) based on PKCS #10. | 



