
# EnrollPhoneResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | **Boolean** | Boolean indicating if the request was successful. | 
**result** | [**EnrollPhoneResponseResult**](EnrollPhoneResponseResult.md) |  | 
**errors** | [**List&lt;EnrollPhoneResponseErrors&gt;**](EnrollPhoneResponseErrors.md) | A array of error messages (i.e. code and string messages). | 
**messages** | [**List&lt;EnrollPhoneResponseMessages&gt;**](EnrollPhoneResponseMessages.md) | A array of informational messages (i.e. code and string messages). | 



